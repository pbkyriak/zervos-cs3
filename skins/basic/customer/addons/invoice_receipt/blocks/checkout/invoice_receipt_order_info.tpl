
{if $completed_steps.step_two}
	
	{if $cart.receipt_from_store=='N'}
	<h4>{$lang.shipping_address}:</h4>
	<ul class="shipping-adress clearfix">
		{foreach from=$profile_fields.S item="field"}
			{assign var="value" value=$cart.user_data|fn_get_profile_field_value:$field}
			{if $value}
				<li class="{$field.field_name|replace:"_":"-"}">{$value}</li>
			{/if}
		{/foreach}
	</ul>
	{else}
		<h4>{$lang.invoice_receipt_question}</h4>
		<ul class="shipping-adress clearfix">
			<li class="i_invoice_type_receipt">{$lang.receipt_store_title}</li>
		</ul>
	{/if}
	<hr />
	{if $cart.invoice_or_receipt=='I'}
	<h4>{$lang.billing_address}:</h4>

	{assign var="profile_fields" value="I"|fn_get_profile_fields}
	<ul class="shipping-adress clearfix">
		{foreach from=$profile_fields.B item="field"}
			{assign var="value" value=$cart.user_data|fn_get_profile_field_value:$field}
			{if $value}
				<li class="{$field.field_name|replace:"_":"-"}">{if !$field.field_name}<b>{$field.description}</b>: {/if}{$value}</li>
			{/if}
		{/foreach}
	</ul>
	
	{else}
		<h4>{$lang.invoice_receipt_question}</h4>
		<ul class="shipping-adress clearfix">
			<li class="i_invoice_type_receipt">{$lang.invoice_receipt_receipt}</li>
		</ul>
	{/if}
	{if $cart.shipping}
		<hr /><h4>{$lang.shipping_method}:</h4>
		<ul>
			{foreach from=$cart.shipping item="shipping"}
				<li>{$shipping.shipping}</li>
			{/foreach}
		</ul>
	{/if}
	
	<hr />
	
{/if}

{assign var="block_wrap" value="checkout_order_info_`$block.snapping_id`_wrap"}
