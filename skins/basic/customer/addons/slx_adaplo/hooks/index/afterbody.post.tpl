{literal}
<script type="text/javascript">
  //<![CDATA[
        (function(){function a(a){var b,c,d=window.document.createElement("iframe");d.src="javascript:false",(d.frameElement||d).style.cssText="width: 0; height: 0; border: 0";var e=window.document.createElement("div");e.style.display="none";var f=window.document.createElement("div");e.appendChild(f),window.document.body.insertBefore(e,window.document.body.firstChild),f.appendChild(d);try{c=d.contentWindow.document}catch(g){b=document.domain,d.src="javascript:var d=document.open();d.domain='"+b+"';void(0);",c=d.contentWindow.document}return c.open()._l=function(){b&&(this.domain=b);var c=this.createElement("scr".concat("ipt"));c.src=a,this.body.appendChild(c)},c.write("<bo".concat('dy onload="document._l();">')),c.close(),d}var b="nostojs";window[b]=window[b]||function(a){(window[b].q=window[b].q||[]).push(a)},window[b].l=new Date;var c=function(d,e){if(!document.body)return setTimeout(function(){c(d,e)},30);e=e||{},window[b].o=e;var f=document.location.protocol,g=["https:"===f?f:"http:","//",e.host||"connect.nosto.com",e.path||"/include/",d].join("");a(g)};window[b].init=c})(); 
    nostojs.init('hhfxrd23');

  //]]>
</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','//connect.facebook.net/en_US/fbevents.js');
  // Insert Your Custom Audience Pixel ID below.
  fbq('init', '213598515677505');
  fbq('init', '589865307785550');
  fbq('track', 'PageView');
</script>
<!-- Insert Your Custom Audience Pixel ID below. -->
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=213598515677505&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=589865307785550&ev=PageView&noscript=1"
/></noscript>
<!-- End Custom Audience Pixel Code -->
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37752094-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Google Analytics -->

<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75880229-1', {
    'name': 'adaplotracker',
    'cookieName': '_ga_adaplo',
    'cookieDomain': 'auto'
  });

  ga('adaplotraker.require', 'linkid');
{/literal}

ga(function() {ldelim}
  var tracker = ga.getByName('adaplotracker');
  var visitorId = tracker.get('clientId');
  var sessionId = new Date().getTime() + '.' + Math.random().toString(36).substring(5);
  var hitTimestamp = (+new Date()).toString();
  tracker.set('dimension1', visitorId);
  tracker.set('dimension2', sessionId);
  tracker.set('dimension3', hitTimestamp);
  tracker.set('transport', 'beacon');
  ga('adaplotracker.require', 'ec');

  {if $controller=="checkout" && $mode=="complete"}
	tracker.set('referrer', null);
	{foreach from=$order_info.items item="oi"}
		ga('adaplotracker.ec:addProduct', {ldelim} 
			'id': "{$oi.product_code}", 
			'name': "{$oi.product}",
			'category': "{assign var="categoryname" value=$oi.product_id|fn_slx_adaplo_find_category_name}{$categoryname}",
			'brand': "{assign var="productbrand" value=$oi.product_id|fn_slx_adaplo_find_brand_name}{$productbrand}",
			'price': "{$oi.price|string_format:"%.2f"}",
			'quantity': {$oi.amount}
		{rdelim});
	{/foreach}
	// Execute the following code once
	ga('adaplotracker.ec:setAction', 'purchase', {ldelim}
		'id': "{$order_info.order_id}",
		'revenue': "{$order_info.total|string_format:"%.2f"}", 
	{rdelim});
  {/if}
  ga('adaplotracker.send', 'pageview');
{rdelim});
</script>

