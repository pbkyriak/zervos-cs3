{literal}
<!--adaplo-->
<script type="text/javascript">
fbq('track', 'Purchase', {
    value: "{/literal}{$order_info.total|string_format:"%.2f"}{literal}",
    currency: "EUR",
    content_ids: [{/literal}{foreach from=$order_info.items item="oi" name="prods"}"{$oi.product_code}"{if not $smarty.foreach.prods.last},{/if}{/foreach}{literal}],
    content_type: 'product'
});
 </script>
{/literal}
