{literal}
<!--adaplo-->
<script type="text/javascript" class="cm-ajax-force">
fbq('track', 'ViewContent', {
  value: {/literal}"{$product.price|string_format:"%.2f"}"{literal},
  currency: "EUR",
  content_ids:[{/literal}"{$product.product_code}"{literal}],
  content_type: 'product'
});
</script>
{/literal}


{if $product.main_pair.icon || $product.main_pair.detailed}
	{assign var="image_pair_var" value=$product.main_pair}
{elseif $product.option_image_pairs}
	{assign var="image_pair_var" value=$product.option_image_pairs|reset}
{/if}

{if $image_pair_var.image_id == 0}
	{assign var="image_id" value=$image_pair_var.detailed_id}
{else}
	{assign var="image_id" value=$image_pair_var.image_id}
{/if}
{assign var="image_path" value=$image_pair_var.detailed.image_path}

<div class="nosto_product" style="display:none">
	<span class="url">{"products.view?product_id=`$product.product_id`"|fn_url:'C':'current'}</span>
	<span class="product_id">{$product.product_code}</span>
	<span class="name">{$product.product}</span>
	<span class="image_url">{$config.https_location}{$image_path}</span>
	<span class="price">{$product.price|string_format:"%.2f"}</span>
	<span class="price_currency_code">EUR</span>
	<span class="availability">{if $product.shop_availability <=7}InStock{else}OutOfStock{/if}</span> 	
	<span class="category">{assign var="categoryname" value=$product.product_id|fn_slx_adaplo_find_category_name}{$categoryname}</span>
</div>