    {if $added_products}
		{assign var="totalAmount" value=0}
        {foreach from=$added_products item=product key="key"}
			{assign var="totalAmount" value=$product.display_price+$totalAmount}
		{/foreach}
		<!--adaplo-->
		{literal}
		<script type="text/javascript" class="cm-ajax-force">
		fbq('track', 'AddToCart', {
		  value: "{/literal}{$totalAmount}{literal}",
		  currency: "EUR",
		  content_ids: [{/literal}{foreach from=$added_products item="oi" name="prods"}"{$oi.product_id|fn_slx_adaplo_get_product_code}"{if not $smarty.foreach.prods.last},{/if}{/foreach}{literal}],
		  content_type: 'product'
		});
		</script>
		{/literal}
	{/if}