{if $order_info.interest|floatval}
  <tr>
      <td>{if $order_info.payment_info.period==1 }{__("epivarinsi_pistotikis")}{else}{__("interest")}{/if}:</td>
      <td data-ct-totals="payment_interest">{include file="common/price.tpl" value=$order_info.interest}</td>
  </tr>
  {math equation="x+y" x=$_total|default:$cart.total y=$cart.interest|default:0 assign="_total"}
{/if}
