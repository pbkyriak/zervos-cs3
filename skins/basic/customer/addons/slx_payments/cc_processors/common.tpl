{* [panos] *}
{assign var="ctotal" value=$cart.total-$cart.interest}
{assign var="insts" value=$ctotal|fn_slx_payments_check_installments:$cart.payment_id}
{assign var="period" value=$cart.payment_info.payment_period|default:1}

{if count($insts) > 0}
	{if count($insts)==1 && $insts[0]==1 } 
		{*efaks pliromi*}
		<input type="hidden" name="payment_info[payment_period]" value="1" />
	{else}
		<div class="form-field" style="padding-left: 10px">
			<table border="0" width="50%" class="table" cellspacing="0" cellpadding="0">
			<tr>
				<th>&nbsp;</th>
				<th nowrap>{$lang.installments}</th>
				<th nowrap>{$lang.payment_per_month}</th>
				<th nowrap>{$lang.total}</th>
			</tr>
			{foreach from=$insts item=istallmentCount name="period"}
				<tr {cycle values=",class='table-row'"}>
					<td>
						<input type="radio" 
						  id="payment_method_{$istallmentCount}" name="payment_info[payment_period]" 
						  value="{$istallmentCount}" 
						  class ="cm-select-payment-period"
						  data-payment-id="{$cart.payment_id}"
						  {if $period == $istallmentCount || (!$period && !$selected_value)}checked="checked"{assign var="selected_value" value=true}{/if}  />
					</td>
					{assign var="interest" value=$ctotal|fn_slx_payments_check_installment:$istallmentCount:$cart.payment_id}
					{if $istallmentCount == 1}
						<td colspan="2"><b>{$lang.first_installment}.</b></td>
					{else}
						<td><b>{$istallmentCount}</b></td>
						<td>
							{math equation="(y + z) / x" y=$interest z=$ctotal x=$istallmentCount assign="per_month"}
							{include file="common/price.tpl" value=$per_month}
						</td>
					{/if}
					<td>
						{include file="common/price.tpl" value=$interest+$ctotal}
					</td>
				</tr>
			{/foreach}
			</table>
		</div>
	{/if}
{else}
  <p>This payment method is not setuped properly.</p>
{/if}
{* [/panos] *}
