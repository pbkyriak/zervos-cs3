{* $Id$ *}
{if $addons.janrain.appdomain}
<div class="janrain-wrap">
	{include file="buttons/button.tpl" but_role="janrain" but_meta="janrainEngage nobg" but_text=$lang.social_login}
</div>
{/if}