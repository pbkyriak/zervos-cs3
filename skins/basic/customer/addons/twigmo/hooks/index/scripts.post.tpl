{assign var="state" value=$smarty.session.twg_state}

{if $state.twg_can_be_used and !$state.mobile_link_closed}
    <div class="mobile-avail-notice">
        <div class="buttons-container">
            <a href="{$config.current_url|fn_query_remove:"mobile":"auto":"desktop"|fn_link_attach:"mobile"}">
                {$lang.twg_visit_our_mobile_store}
            </a>

            {if $state.device == "android" and $state.url_on_googleplay}
                <a href="{$state.url_on_googleplay}">{$lang.twg_app_for_android}</a>
            {elseif ($state.device == "iphone" or $state.device == "ipad") and $state.url_on_appstore}
                <a href="{$state.url_on_appstore}">
                    {if $state.device == "iphone"}
                        {$lang.twg_app_for_iphone}
                    {else}
                        {$lang.twg_app_for_ipad}
                    {/if}
                </a>
            {/if}
            <span id="close_notification_mobile_avail_notice" class="cm-notification-close hand close" title="Close" />&times;</span>
        </div>
    </div>

    <script>
    //<![CDATA[
    {literal}
    $(function () {
        $('.mobile-avail-notice').insertBefore('a[name="top"]');
        $('#close_notification_mobile_avail_notice').bind('click', function () {
            $(this).parents('div.mobile-avail-notice').hide();
            $.ajax({
                url: '{/literal}{"twigmo.post&close_notice=1"|fn_url:"C":"rel":"&"}{literal}',
                dataType: 'json'
            });
        });
    });
    {/literal}
    //]]>
    </script>
{/if}
