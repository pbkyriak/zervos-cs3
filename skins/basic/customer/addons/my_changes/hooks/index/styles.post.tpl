<link href="{$config.skin_path}/addons/my_changes/styles.base.css" rel="stylesheet" type="text/css" />
<link href="{$config.skin_path}/addons/my_changes/fonts.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700&subset=latin,greek' rel='stylesheet' type='text/css'>

{literal}
<style type='text/css'>
/* amdreas */
.promo-products-instock {
	margin-top: 20px;
}
.promo-products-instock ul li {
	box-shadow: 0px 8px 8px -8px rgb(0, 0, 0);
	border-radius: 6px 6px 6px 6px;
	background-clip: padding-box;
	border: 1px solid rgb(220, 220, 220);
	padding: 3px;
	margin: 0px 0px 10px;
	background: none repeat scroll 0% 0% rgb(255, 255, 255);
}
.promo-products-instock .product-item-image.compact {
	box-shadow: 0px 8px 8px -8px rgb(0, 0, 0);
	border-radius: 6px 6px 6px 6px;
	background-clip: padding-box;
	border: 1px solid rgb(220, 220, 220);
	padding: 3px;
	margin: 0px 0px 20px 20px;
	background: none repeat scroll 0% 0% rgb(255, 255, 255);
    float: left;
    width: 260px;

}
</style>
{/literal}