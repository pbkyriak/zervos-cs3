{*

	Editor: Ioannis Matziaris [imatz]
	Email: imatzgr@gmail.com
	Date: December 2014
	Details: 

*}

{hook name="products:prices_block"}
	{if $product.price|floatval || $product.zero_price_action == "P" || ($hide_add_to_cart_button == "Y" && $product.zero_price_action == "A")}
		{if $show_phone_price}
            <div class="price-normal-cntr">
                {* <span class="normal_price_label">{$lang.web_price}</span> *}
                <span class="price{if !$product.price|floatval} hidden{/if}" id="line_discounted_price_{$obj_prefix}{$obj_id}">{if $details_page}{/if}{include file="common_templates/price.tpl" value=$product.price span_id="discounted_price_`$obj_prefix``$obj_id`" class="price-num"}</span>
            </div>
           {*  <div class="price-phone-cntr">
                <span class="phone_price_label">{$lang.phone_ordering_price}</span>
                <span class="price{if !$product.price|floatval} hidden{/if}" id="line_discounted_phone_price_{$obj_prefix}{$obj_id}">{if $details_page}{/if}{include file="common_templates/price.tpl" value=$product.phone_price span_id="discounted_price_`$obj_prefix``$obj_id`" class="price-num"}</span>
            </div> *}
            <div class="clr"></div>
        {else}
			<span class="price{if !$product.price|floatval} hidden{/if}" id="line_discounted_price_{$obj_prefix}{$obj_id}">{if $details_page}{/if}{include file="common_templates/price.tpl" value=$product.price span_id="discounted_price_`$obj_prefix``$obj_id`" class="price-num"}</span>
		{/if}
	{elseif $product.zero_price_action == "A"}
		{assign var="base_currency" value=$currencies[$smarty.const.CART_PRIMARY_CURRENCY]}
		<span class="price-curency">{$lang.enter_your_price}: {if $base_currency.after != "Y"}{$base_currency.symbol}{/if}<input class="input-text-short" type="text" size="3" name="product_data[{$obj_id}][price]" value="" />{if $base_currency.after == "Y"}&nbsp;{$base_currency.symbol}{/if}</span>
	{elseif $product.zero_price_action == "R"}
		<span class="price">{$lang.contact_us_for_price}</span>
	{/if}
{/hook}