<!-- Send the Order data -->
{math equation="x - y /z" x=$order_info.total y=$order_info.total z=1.24 assign="skTaxes"}

<script>
  skroutz_analytics('ecommerce', 'addOrder', JSON.stringify({ldelim}
    order_id: '{$order_info.order_id}',  // Order ID. Required.
    revenue:  '{$order_info.total-$order_info.payment_surcharge}', // Grand Total. Includes Tax and Shipping.
    shipping: '{$order_info.shipping_cost}',    // Total Shipping Cost.
    tax:      '{$skTaxes|round:2}'  // Total Tax.
  {rdelim}));

{foreach from=$order_info.items item="product"}
  {math equation="x/y" x=$product.display_subtotal y=$product.amount assign="skPprice"}
  skroutz_analytics('ecommerce', 'addItem', JSON.stringify({ldelim}
    order_id:   '{$order_info.order_id}',
    product_id: '{$product.product_id}',
    name:       '{$product.product}',
    price:      '{$skPprice|round:2}',
    quantity:   '{$product.amount}'
  {rdelim}));
{/foreach}

</script>
