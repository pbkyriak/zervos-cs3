
{* [panos] *}
{assign var="insts" value=$cart.total|fn_check_installmnets:$cart.payment_id}
{assign var="period" value=$cart.payment_info.period|default:1}

{if $insts > 1}
<div class="form-field" style="padding-left: 10px">
	<table border="0" width="50%" class="table" cellspacing="0" cellpadding="0">
	<tr>
		<th>&nbsp;</th>
		<th nowrap>{$lang.installments}</th>
		<th nowrap>{$lang.payment_per_month}</th>
		<th nowrap>{$lang.total}</th>
	</tr>
	<!--
	<tr>
		<td class="table-row">
			<input type="radio" id="payment_method_1" name="payment_info[period]" 
			value="1" 
			{if $period == 1 || (!$period && !$selected_value)}checked="checked"{assign var="selected_value" value=true}{/if} 
			onClick="fn_set_payment_interest('{$cart.total}', this.value)" />
		</td>
		<td colspan="2">{$lang.first_installment}..</td>
		<td>
				<b>{include file="common_templates/price.tpl" value=$cart.total}</b>
		</td>
	</tr>
	-->
	{foreach from=$insts item=istallmentCount name="period"}
		<tr {cycle values=",class='table-row'"}>
			<td>
				<input type="radio" id="payment_method_{$istallmentCount}" name="payment_info[period]" 
				value="{$istallmentCount}" 
				{if $period == $istallmentCount || (!$period && !$selected_value)}checked="checked"{assign var="selected_value" value=true}{/if} 
				onClick="fn_set_payment_interest('{$cart.total}', this.value)" />
			</td>
	
			{assign var="interest" value=$cart.total|fn_check_installment:$istallmentCount:$cart.payment_id}
	
			{if $istallmentCount == 1}
				<td colspan="2"><b>{$lang.first_installment}.</b></td>
			{else}
				<td><b>{$istallmentCount}</b></td>
				<td>
					{math equation="(y + z) / x" y=$interest z=$cart.total x=$istallmentCount assign="per_month"}
					{include file="common_templates/price.tpl" value=$per_month}
				</td>
			{/if}
			<td>
				{include file="common_templates/price.tpl" value=$interest+$cart.total}</td>
		</tr>
		{/foreach}
	</table>
</div>
{else}
hello!
{/if}
{* [/panos] *}
