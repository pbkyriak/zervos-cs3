<!-- exentric -->
<div class="product-social box_round box_shadow" style="display:block; width: 374px; line-height: 15px; margin-bottom: 20px; float: right;">
<strong>Εγγύηση: 2 χρόνια</strong>
</div>
<!-- exentric -->
<div style="clear:both"></div>
<div class="product-social box_round box_shadow" style="display:block;">
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style" >
<a class="addthis_button_facebook_like" fb:like:layout="button_count" fb:like:width="115" ></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
<a class="addthis_counter addthis_pill_style"></a>


</div>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4edf776220b4e8cc"></script>
<!-- AddThis Button END  addthis_toolbox addthis_default_style -->
</div>

{capture name="tabsbox"}
	{foreach from=$tabs item="tab" key="tab_id"}
		{if $tab.show_in_popup != "Y" && $tab.status == "A"}
			{assign var="tab_content_capture" value="tab_content_capture_`$tab_id`"}

			{capture name=$tab_content_capture}
				{if $tab.tab_type == 'B'}
					{block block_id=$tab.block_id dispatch="products.view"}
				{elseif $tab.tab_type == 'T'}
					{include file=$tab.template product_tab_id=$tab.html_id}
				{/if}
			{/capture}

			{if $smarty.capture.$tab_content_capture|trim}
				{if $settings.Appearance.product_details_in_tab == "N"}
					<h1 class="tab-list-title">{$tab.name}</h1>
				{/if}
			{/if}

			<div id="content_{$tab.html_id}" class="wysiwyg-content">
				{$smarty.capture.$tab_content_capture}
			</div>
		{/if}
	{/foreach}
{/capture}

{capture name="tabsbox_content"}
{if $settings.Appearance.product_details_in_tab == "Y"}
	{include file="common_templates/tabsbox.tpl" content=$smarty.capture.tabsbox }
{else}
	{$smarty.capture.tabsbox}
{/if}
{/capture}