
{assign var="url" value="`$config.http_location`"}
<p>{$lang.text_proxypay3i_notice|replace:"[cart_url]":$url}</p>
<hr />

<div class="form-field">
	<label for="merchantid">{$lang.merchant_id}:</label>
	<input type="text" name="payment_data[processor_params][merchantid]" id="merchantid" value="{$processor_params.merchantid}" class="input-text" />
</div>

<div class="form-field">
	<label for="url">{$lang.payment} {$lang.url}:</label>
	<input type="text" name="payment_data[processor_params][url]" id="url" value="{$processor_params.url|default:"eptest.eurocommerce.gr/proxypay/apacs"}" class="input-text" size="60" />
</div>

<div class="form-field">
	<label for="details">{$lang.payment_details}:</label>
	<input type="text" name="payment_data[processor_params][details]" id="details" value="{$processor_params.details}" class="input-text" size="60" />
</div>

<div class="form-field">
	<label for="currency">{$lang.currency}:</label>
	<select name="payment_data[processor_params][currency]" id="currency">
		<option value="0840" {if $processor_params.currency == "0840"}selected="selected"{/if}>{$lang.currency_code_usd}</option>
		<option value="0978" {if $processor_params.currency == "0978"}selected="selected"{/if}>{$lang.currency_code_eur}</option>
		<option value="0300" {if $processor_params.currency == "0300"}selected="selected"{/if}>{$lang.currency_9}</option>
	</select>
</div>

<br />
{include file="common_templates/subheader.tpl" title=$lang.installments}
<table cellpadding="1" cellspacing="2" border="0" width="80%">
<tr id="header">
	<td class="manage-header" width="1%" align="center">
		<input type="checkbox" name="check_all" value="Y" onclick="fn_check_all_checkboxes('gw_form', this.checked, 'del_cbox');" title="{$lang.check_uncheck_all}" /></td>
	<td class="manage-header" align="center">&nbsp;{$lang.installments}&nbsp;</td>
	<td class="manage-header" align="center">&nbsp;{$lang.interest}&nbsp;</td>
	<td class="manage-header" align="center">&nbsp;{$lang.from}&nbsp;</td>
	<td class="manage-header">&nbsp;</td>
</tr>
{if count($installments)>0 }
	{foreach from=$installments item="inst"}
	<tr {cycle values='class="manage-row", '}>
		<td align="center"><input type="checkbox" id="del_cbox" name="delete_insts[{$inst.installment_id}]" value="Y" /></td>
		<td align="center" nowrap="nowrap" class="side-padding">
			<input type="text" name="insts_data[{$inst.installment_id}][installments]" value="{$inst.installments}" size="6" class="input-text" />
		</td>
		<td nowrap>
			<input type="text" name="insts_data[{$inst.installment_id}][p_interest]" value="{$inst.p_interest|escape:html}" class="input-text" size="4" /> % + <input type="text" name="insts_data[{$inst.installment_id}][a_interest]" value="{$inst.a_interest|escape:html}" class="input-text" size="4"/> {$currencies.$primary_currency.symbol}
		</td>
		<td align="center" nowrap="nowrap" class="side-padding">
			<input type="text" name="insts_data[{$inst.installment_id}][amount_from]" value="{$inst.amount_from}" size="6" class="input-text" />
		</td>
		<td>&nbsp;</td>
	</tr>
	{/foreach}
{/if}
<tr id="box_insts">
	<td>&nbsp;</td>
	<td align="center" nowrap="nowrap">
		<input type="text" name="add_insts[0][installments]" value="" class="input-text" size="6" />
	</td>
	<td>
		<input type="text" name="add_insts[0][p_interest]" value="" class="input-text" size="4" /> % + <input type="text" name="add_insts[0][a_interest]" value="" class="input-text" size="4"/> {$currencies.$primary_currency.symbol}
	</td>
	<td align="center" nowrap="nowrap">
		<input type="text" name="add_insts[0][amount_from]" value="" class="input-text" size="6" />
		</td>
	<td>
		{include file="buttons/multiple_buttons.tpl" item_id="insts" tag_level="1"}</td>
</tr>
</table>
