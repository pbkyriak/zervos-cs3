<div id="content_store_bridges">
	<fieldset>
	  	{include file="common_templates/subheader.tpl" title=$lang.auto_updates}
	  	<div class="form-field">
	      <label for="page_title">{$lang.supplier_category_name}:</label>
	      <input type="text" name="category_data[supplier_category_name]" id="supplier_category_name" size="120" value="{$category_data.supplier_category_name}" class="input-text-large" />
	      <p>Μπορεί να είναι λίστα με κόμματα, αλλά χωρίς κενά μετά ή πριν από το κόμμα. Νοκια, Nokia -> Λάθος! Νοκια,Nokia -> σωστό!</p>
	    </div>
	  	<div class="form-field">
	      <label for="page_title">{$lang.supplier_import_id}:</label>
	      <input type="text" name="category_data[supplier_import_id]" id="supplier_import_id" size="120" value="{$category_data.supplier_import_id}" class="input-text-large" />
	    </div>   
	    <div class="form-field">
	      <label for="add_hat">{$lang.add_hat}:</label>
	      <input type="hidden" value="N" name="category_data[add_hat]"/>
	      <input type="checkbox" class="checkbox cm-toggle-checkbox" value="Y" name="category_data[add_hat]" id="add_hat"{if $category_data.add_hat=='Y'} checked="checked"{/if} />
	    </div>
	    <div class="form-field">
			<label for="bridge_comments">{$lang.comments}:</label>
			<textarea name="category_data[bridge_comments]" id="bridge_comments" cols="55" rows="4" class="input-textarea-long">{$category_data.bridge_comments}</textarea>
		</div>
	</fieldset>
<!--content_store_bridges--></div>