{* $Id$ *}
{*

	Author Ioannis Matziaris - imatzgr@gmail.com - Maios 2013
	
	Einai custom Hook sto arxeio views/payments/update.tpl
	Exei sto div pou exei tin lista me ta tabs organwseis ton tropon pliromis
	
	Exei mpei gia na mporoume na prosthesoume epiplewn tropous pliromis
	
*}

<div class="form-field">
	<label for="elm_payment_category_{$id}">{$lang.payment_category}:</label>
	<select id="elm_payment_category_{$id}" name="payment_data[payment_category]">
		<option value="tab1" {if $payment.payment_category == "tab1"}selected="selected"{/if}>{$lang.payments_tab1}</option>
		<option value="tab2" {if $payment.payment_category == "tab2"}selected="selected"{/if}>{$lang.payments_tab2}</option>
		<option value="tab3" {if $payment.payment_category == "tab3"}selected="selected"{/if}>{$lang.payments_tab3}</option>
		<option value="tab4" {if $payment.payment_category == "tab4"}selected="selected"{/if}>{$lang.payments_tab4}</option>
	</select>
	<p class="description">
		{$lang.payment_category_note}
	</p>
</div>