{*

	Author: Ioannis Matziaris [imatz]
	Email: imatzgr@gmail.com
	Date: June 2015
	Details:

*}

<div class="form-field">
	<label for="feature_display_is_brand_{$id}">{$lang.brand_feature}:</label>
	<input type="hidden" name="feature_data[is_brand]" value="0" />
	<input type="checkbox" class="checkbox" name="feature_data[is_brand]" value="1" {if $feature.is_brand}checked="checked"{/if} id="feature_display_on_product_{$id}" {if $feature.parent_id}disabled="disabled"{/if} />
</div>