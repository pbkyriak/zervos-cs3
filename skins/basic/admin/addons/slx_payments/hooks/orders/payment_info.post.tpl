{if $order_info.payment_period>1}
<div class="control-group">
    <div class="control-label">{$lang.installments}</div>
    <div id="tygh_payment_info" class="controls">{$order_info.payment_period}
    </div>
</div>
{/if}