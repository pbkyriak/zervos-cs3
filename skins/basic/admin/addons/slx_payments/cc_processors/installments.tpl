
<br />
<h2>{$lang.installments}</h2>
{if $payment_id!=0}
<table class="table table-middle" width="100%">
<tr id="header">
	<th class="manage-header" width="1%" align="center">
		<input type="checkbox" name="check_all" value="Y" onclick="fn_check_all_checkboxes('gw_form', this.checked, 'del_cbox');" title="{$lang.check_uncheck_all}" /></th>
	<th class="manage-header" align="center">&nbsp;{$lang.installments}&nbsp;</th>
	<th class="manage-header" align="center">&nbsp;{$lang.interest}&nbsp;</th>
	<th class="manage-header" align="center">&nbsp;{$lang.from}&nbsp;</th>
	<th class="manage-header">&nbsp;</th>
</tr>
{if count($installments)>0 }
	{foreach from=$installments item="inst"}
	<tr class="cm-row-item">
		<td align="center"><input type="checkbox" id="del_cbox" name="delete_insts[{$inst.installment_id}]" value="Y" /></td>
		<td align="center" nowrap="nowrap" class="side-padding">
			<input type="text" name="insts_data[{$inst.installment_id}][installments]" value="{$inst.installments}" size="6" class="input-text" />
		</td>
		<td nowrap>
			<input type="text" name="insts_data[{$inst.installment_id}][p_interest]" value="{$inst.p_interest|escape:html}" class="input-mini" size="4" /> % + 
            <input type="text" name="insts_data[{$inst.installment_id}][a_interest]" value="{$inst.a_interest|escape:html}" class="input-mini" size="4"/> {$currencies.$primary_currency.symbol}
		</td>
		<td align="center" nowrap="nowrap" class="side-padding">
			<input type="text" name="insts_data[{$inst.installment_id}][amount_from]" value="{$inst.amount_from}" size="6" class="input-text" />
		</td>
		<td>&nbsp;</td>
	</tr>
	{/foreach}
{/if}
<tr id="box_insts">
	<td>&nbsp;</td>
	<td align="center" nowrap="nowrap">
		<input type="text" name="add_insts[0][installments]" value="" class="input-text" size="6" />
	</td>
	<td>
		<input type="text" name="add_insts[0][p_interest]" value="" class="input-mini" size="4" /> % + 
        <input type="text" name="add_insts[0][a_interest]" value="" class="input-mini" size="4"/> {$currencies.$primary_currency.symbol}
	</td>
	<td align="center" nowrap="nowrap">
		<input type="text" name="add_insts[0][amount_from]" value="" class="input-text" size="6" />
		</td>
	<td>
		{include file="buttons/multiple_buttons.tpl" item_id="insts" tag_level="1"}</td>
</tr>
</table>
{else}
  <p>Save the payment and then set installment settings.</p>
{/if}