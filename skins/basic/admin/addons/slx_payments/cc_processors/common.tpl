{* [panos] *}
{assign var="ctotal" value=$cart.total-$cart.interest}
{assign var="insts" value=$ctotal|fn_check_installmnets:$cart.payment_id}
{assign var="period" value=$cart.payment_period|default:1}

{if $insts > 1}
<div class="form-field" style="padding-left: 10px">
	<input type="hidden" name="payment_period" value="{$period}"  />
	<table border="0" width="50%" class="table" cellspacing="0" cellpadding="0">
	<tr>
		<th>&nbsp;</th>
		<th nowrap>{__("installments")}</th>
		<th nowrap>{__("payment_per_month")}</th>
		<th nowrap>{__("total")}</th>
	</tr>
	
	{foreach from=$insts item=istallmentCount name="period"}
		<tr {cycle values=",class='table-row'"}>
			<td>
                {if $period == $istallmentCount} &#10144;  {/if} 
			</td>
			{assign var="interest" value=$ctotal|fn_check_installment:$istallmentCount:$cart.payment_id}
			{if $istallmentCount == 1}
				<td colspan="2"><b>{__("first_installment")}.</b></td>
			{else}
				<td><b>{$istallmentCount}</b></td>
				<td>
					{math equation="(y + z) / x" y=$interest z=$ctotal x=$istallmentCount assign="per_month"}
					{include file="common/price.tpl" value=$per_month}
				</td>
			{/if}
			<td>
				{include file="common/price.tpl" value=$interest+$ctotal}
            </td>
		</tr>
    {/foreach}
	</table>
</div>
{else}
  <p>This payment method is not setuped properly.</p>
{/if}
{* [/panos] *}
