<div id="content_shopfeed">
  <fieldset>
    {include file="common_templates/subheader.tpl" title=$lang.shop_feed}
    <div class="form-field">
        <label for="category_data_exclude_from_shopfeed">{$lang.exclude_from_shopfeed}:</label>
        <input type="hidden" name="category_data[exclude_from_shopfeed]" value="N" />
        <input type="checkbox" name="category_data[exclude_from_shopfeed]" id="category_data_brands" value="Y" {if $category_data.exclude_from_shopfeed == "Y"}checked="checked"{/if} class="checkbox" />
        <p class="note">Αν εξαιρέσετε αυτή την κατηγορία, στο xml δεν θα εμφανιστούν και οι υποκατηγορίες της</p>
    </div>
  </fieldset>

  <!--content_shopfeed--></div>
