{* $Id$ *}
{*

	Author Ioannis Matziaris - imatzgr@gmail.com - April 2013

	Prosthiki stin selida tropou apostolis tin dinatotia na kathorizoume an isxiei gia paralavi apo to katastima
	
*}
<div class="form-field">
	<label for="receipt-from-store" id="receipt-from-store-label">{$lang.receipt_store_title_admin}{include file="common_templates/tooltip.tpl" tooltip=$lang.receipt_store_shipping_desc_admin}:</label>	
	<input type="hidden" value="N" name="shipping_data[receipt_from_store]">
	<input type="checkbox" class="checkbox" value="Y" name="shipping_data[receipt_from_store]" id="receipt-from-store"{if $shipping.receipt_from_store=='Y'} checked="checked"{/if}>				
</div>