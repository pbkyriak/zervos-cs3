<script type="text/javascript">
//<![CDATA[
	$(document).ready(function() {ldelim}
		{if $smarty.const.PRODUCT_TYPE == 'ULTIMATE' && 'COMPANY_ID'|defined && $s_companies.$s_company.storefront}
			{assign var='se_async_url' value="http://`$s_companies.$s_company.storefront`/index?dispatch=searchanise.async&no_session=Y"}
		{else}
			{assign var='se_async_url' value='searchanise.async?no_session=Y'|fn_url:'C':'current':'&'}
		{/if}

		{if $settings.store_mode == 'closed'}
			{assign var='se_async_url' value="`$se_async_url`&store_access_key=`$settings.General.store_access_key`"}
		{/if}

		{if 'HTTPS'|defined}
			{assign var='se_async_url' value=$se_async_url|replace:'http://':'https://'}
		{else}
			{assign var='se_async_url' value=$se_async_url}
		{/if}

		jQuery.get('{$se_async_url}');
	{rdelim});
//]]>
</script>