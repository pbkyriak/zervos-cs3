﻿{if $prepaid_total}
	<li class="total">
		<em>{$lang.totally_prepaid}:</em>
		<span>{include file="common_templates/price.tpl" value=$prepaid_total}</span>
	</li>
{/if}