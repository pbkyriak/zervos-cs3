{if $addons.twigmo.is_connected}

    <li class="twg-mobile-app-link">
        <a href="{"twigmo_admin_app.view"|fn_url}">{$lang.twgadmin_mobile_app}</a>
    </li>

    {literal}
        <script type="text/javascript">
            $(document).ready(function () {
                $('li.twg-mobile-app-link').detach().insertBefore('li.search').show();
            });
        </script>
    {/literal}

{/if}