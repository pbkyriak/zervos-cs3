{include file="addons/twigmo/settings/components/contact_twigmo_support.tpl"}

{if $admin_access_id}
    {include file="common_templates/subheader.tpl" title=$lang.twgadmin_manage_settings}
{else}
    {include file="common_templates/subheader.tpl" title=$lang.twgadmin_connect_your_store}
{/if}

<fieldset>

<div id="connect_settings">

<input type="hidden" name="result_ids" value="connect_settings,storefront_settings,addon_upgrade,twg_admin_app,twg_push"/>

{assign var="tw_email" value=$tw_settings.email|default:$user_info.email}

<div class="form-field">
    <label {if !$admin_access_id}class="cm-required cm-email"{/if} for="elm_tw_email">{$lang.email}:</label>
    {if $admin_access_id}
        <div class="twg-text-value">{$tw_email}</div>
    {else}
        <input type="text" id="elm_tw_email" name="tw_register[email]" value="{$tw_email}" class="input-text-large" size="60" />
    {/if}
</div>

{if !$admin_access_id}
    <div class="form-field">
        <label for="elm_tw_password" class="cm-required">{$lang.password}:</label>
        <input type="password" id="elm_tw_password" name="tw_register[password]" class="input-text-large" size="32" maxlength="32" value="" autocomplete="off" />
        {include file="buttons/button.tpl" but_text=$lang.forgot_password_question but_href=$reset_pass_link but_id="elm_reset_tw_password" but_role="link" but_target="_blank"}
    </div>
{/if}

<div class="form-field">
    <label {if !$twg_all_stores_connected}class="cm-required cm-multiple-checkboxes"{/if} for="store">{$lang.twgadmin_stores}:</label>
    <table class="table twg-stores" cellpadding="0" cellspacing="0">
        <tr>
            <th>
                {if !$twg_all_stores_connected}
                    <input type="checkbox" class="checkbox cm-check-items" name="check_all" checked="checked" title="{$lang.check_uncheck_all}">
                {/if}
            </th>
            <th>
                {$lang.store}
            </th>
            {if $twg_is_connected}
                <th>
                    {$lang.twgadmin_access_id}
                </th>
                <th>
                    {$lang.plan}
                </th>
            {/if}
            <th>
                {$lang.status}
            </th>
            {if $is_disconnect_mode}
                <th>
                    {$lang.twgadmin_disconnect}
                </th>
            {/if}
        </tr>
        {foreach from=$stores item='store'}
            <tr class="table-row">
                <td>
                    <input type="checkbox" {if $store.is_connected}disabled="disabled"{else}id="store_{$store.company_id}"{/if} checked="checked" class="checkbox cm-item cm-required form-checkbox" name="tw_register[stores][]" value="{$store.company_id}">
                </td>
                <td title="{$store.clear_url}">
                    {$store.company}
                </td>
                {if $twg_is_connected}
                    <td>
                        {$store.access_id}
                    </td>
                    <td>
                        {$store.plan_display_name|escape:false}
                    </td>
                {/if}
                <td>
                    {if $store.is_connected}
                        <span class="twg-connected">{$lang.twgadmin_connected}</span>
                    {else}
                        <span class="twg-disconnected">{$lang.twgadmin_disconnected}</span>
                    {/if}
                </td>
                {if $is_disconnect_mode}
                    <td>
                        {if $store.is_connected}
                            <input type="checkbox" class="checkbox" name="disconnect_stores[]" value="{$store.company_id}">
                        {/if}
                    </td>
                {/if}
            </tr>
        {/foreach}
    </table>
</div>

{if !$admin_access_id}
    {include file="addons/twigmo/settings/components/connect/license.tpl"}
{/if}


{if !$twg_all_stores_connected}
    <div class="form-field">
        {include file="buttons/button.tpl" but_role="button" but_meta="cm-ajax cm-skip-avail-switch" but_name="dispatch[addons.tw_connect]" but_text=$lang.twgadmin_connect}
    </div>
{/if}

{if $is_disconnect_mode}
    <div class="form-field">
        <label for="elm_tw_disconnect_admin">{$lang.twgadmin_disconnect_whole}:</label>
        <input type="hidden" name="disconnect_admin" value="N" />
        <input type="checkbox" class="checkbox" id="elm_tw_disconnect_admin" name="disconnect_admin" value="Y" />
    </div>

    <div class="form-field">
        {include file="buttons/button.tpl" but_role="button" but_meta="cm-ajax cm-confirm cm-skip-avail-switch" but_name="dispatch[addons.tw_disconnect]" but_text=$lang.twgadmin_disconnect}
    </div>
{/if}


{if $admin_access_id}
    <div class="form-field">
        {include file="buttons/button.tpl" but_role="button" but_meta="cm-new-window" but_name="dispatch[addons.tw_svc_auth.cp]" but_text=$lang.twgadmin_open_cp}
    </div>
{/if}

{include file="common_templates/subheader.tpl" title=$lang.twgadmin_about}

<div class="form-field">
    <label for="version">{$lang.twgadmin_addon_version}:</label>
    <div class="twg-text-value" id="version">{$tw_settings.version|default:$smarty.const.TWIGMO_VERSION}</div>
</div>

<div class="form-field">
    <label for="social_links">{$lang.twgadmin_on_social}:</label>
    <div id="social_links">
        <a target="_blank" href="//facebook.com/twigmo">
            <span class="facebook-btn float-left"></span>
        </a>
        <a target="_blank" href="//twitter.com/twigmo">
            <span class="twitter-btn float-left"></span>
        </a>
    </div>
</div>

<script type="text/javascript">
//<![CDATA[
var twg_is_connected = {if $twg_is_connected}true{else}false{/if};
var has_platinum_stores = {if $platinum_stores|count}true{else}false{/if};
{literal}
    $(document).ready(function () {
        $('#twigmo_storefront,#twigmo_admin_app').toggle(twg_is_connected);
        $('#twigmo_twg_push').toggle(twg_is_connected && has_platinum_stores);
    });
{/literal}
//]]>
</script>

<!--connect_settings--></div>

</fieldset>
