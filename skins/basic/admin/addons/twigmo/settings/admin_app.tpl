<div id="twg_admin_app">

{include file="addons/twigmo/settings/components/contact_twigmo_support.tpl"}

{if !$hide_header}
    {include file="common_templates/subheader.tpl" title=$lang.twgadmin_mobile_admin_application}
{/if}

<fieldset>

{assign var="img_lang" value="en"}
{if $smarty.const.CART_LANGUAGE == 'RU'}
    {assign var="img_lang" value="ru"}
{/if}


<div class="form-field">
    <label>{$lang.twgadmin_download_app}:</label>
    <div>
        <a target="_blank" href="//itunes.apple.com/us/app/twigmo-admin-2.0/id895364611">
            <span class="twg-app-store-btn-{$img_lang} float-left"></span>
        </a>
        <a target="_blank" href="//play.google.com/store/apps/details?id=com.simtech.twigmoAdmin">
            <span class="twg-google-play-btn-{$img_lang} float-left"></span>
        </a>
        <div class="twg-app-label">{$lang.twgadmin_download_app_hint}</div>
        {if !$connected_access_id}
            <div class="twg-app-label">{$lang.twgadmin_connect_to_first_ult}</div>
        {/if}
    </div>
</div>

{if $connected_access_id}
    <div class="form-field">
        <label>{$lang.twgadmin_qr_for_admin}:</label>
        <div>
            <img style="width: 200px" src="{'twigmo_admin_app.show_qr'|fn_url}" />
            <div class="twg-app-label">{$lang.twgadmin_qr_for_admin_comment|replace:'[access_id]':$connected_access_id}</div>
        </div>
    </div>
{/if}

</fieldset>

<!--twg_admin_app--></div>
