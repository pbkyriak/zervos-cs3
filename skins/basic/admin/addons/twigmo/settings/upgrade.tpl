<div id="addon_upgrade">

{include file="addons/twigmo/settings/components/contact_twigmo_support.tpl"}

{include file="common_templates/subheader.tpl" title=$lang.upgrade}

{if $next_version_info.next_version and $next_version_info.next_version != $smarty.const.TWIGMO_VERSION}
    <p>{$next_version_info.description|unescape}</p>

    <div class="submit-button">
        <input type="submit" name="dispatch[upgrade_center.upgrade_twigmo]" value="{$lang.upgrade}" class="cm-skip-validation">
    </div>

    <script type="text/javascript">
    //<![CDATA[
    {literal}
    $(document).ready(function () {
        var upgradeIndicator = ' *';
        var $link = $('#twigmo_addon a');
        var oldHtml = $link.html().replace(upgradeIndicator, '');
        $link.html(oldHtml + upgradeIndicator);
    });
    {/literal}
    //]]>
    </script>
{else}
    <p>{$lang.text_no_upgrades_available}</p>
    <div class="buttons-container">
        {include file="buttons/button.tpl" but_name="dispatch[twigmo_updates.check]" but_text=$lang.twgadmin_check_for_updates but_role="submit" but_meta="cm-ajax cm-skip-avail-switch"}
        {if $twg_is_connected}
            <input type="hidden" name="result_ids" value="addon_upgrade" />
        {/if}
    </div>
{/if}
<!--addon_upgrade--></div>
