<div class="form-field">
    <label>{$lang.twgadmin_terms}:</label>
    <input type="checkbox" id="id_accept_terms" name="accept_terms" value="Y" class="checkbox" />
    <label for="id_accept_terms" style="margin-left: 17px; width: auto; margin-top: -24px;" class="cm-custom (tw_check_agreement)">{$lang.twgadmin_accept_terms_n_conditions}</label>
</div>


<script type="text/javascript">
//<![CDATA[
lang.checkout_terms_n_conditions_alert = '{$lang.checkout_terms_n_conditions_alert|escape:javascript}';

{literal}
function fn_tw_check_agreement() {
    if (!$('#id_accept_terms').attr('checked')) {
        return lang.checkout_terms_n_conditions_alert;
    }

    return true;
}

$(document).ready(function () {
    $('.form-field a.text-button-link').css({'margin': '0 0 0 10px'});
});
{/literal}
//]]>
</script>
