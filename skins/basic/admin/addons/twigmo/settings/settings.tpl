<div id="storefront_settings">

{include file="addons/twigmo/settings/components/contact_twigmo_support.tpl"}

{include file="common_templates/subheader.tpl" title=$lang.twgadmin_manage_storefront_settings}

{assign var=store value=$stores|reset}

{if !$twg_is_connected}
    {$lang.twgadmin_connect_to_first_ult}
{elseif $smarty.const.PRODUCT_TYPE == 'ULTIMATE' && !"COMPANY_ID"|defined}
    {$lang.text_select_vendor} - {include file="common_templates/ajax_select_object.tpl" data_url="companies.get_companies_list?show_all=Y&action=href" text=$lang.select id="top_company_id"}

{elseif $smarty.const.PRODUCT_TYPE == 'ULTIMATE' && "COMPANY_ID"|defined && !$store.is_connected}
    {$lang.twgadmin_connect_to_first_ult}
{else}

<fieldset>
    {* Use mobile frontend for ... *}
    <div class="form-field">
        <label>{$lang.twgadmin_use_mobile_frontend_for}:</label>
        <div class="select-field">
            <input type="hidden" name="tw_settings[use_for_phones]" value="N">
            <input type="checkbox" name="tw_settings[use_for_phones]" id="elem_use_for_phones" {if $tw_settings.use_for_phones == 'Y'}checked="checked"{/if} class="checkbox" value="Y">
            <label for="elem_use_for_phones">{$lang.twgadmin_phones}</label>
            <input type="hidden" name="tw_settings[use_for_tablets]" value="N">
            <input type="checkbox" name="tw_settings[use_for_tablets]" id="elem_use_for_tablets" {if $tw_settings.use_for_tablets == 'Y'}checked="checked"{/if} class="checkbox" value="Y">
            <label for="elem_use_for_tablets">{$lang.twgadmin_tablets}</label>
        </div>
    </div>

    {* Home page content *}
    <div class="form-field">
        <label for="elm_tw_home_page_content">{$lang.twgadmin_home_page_content}:</label>
        <select id="elm_tw_home_page_content" name="tw_settings[home_page_content]">
            <option    value="home_page_blocks" {if $tw_settings.home_page_content == "home_page_blocks"}selected="selected"{/if}>- {$lang.twgadmin_home_page_blocks} -</option>
            <option    value="tw_home_page_blocks" {if $tw_settings.home_page_content == "tw_home_page_blocks"}selected="selected"{/if}>- {$lang.twgadmin_tw_home_page_blocks} -</option>
            {foreach from=0|fn_get_plain_categories_tree:false item="cat"}
                {if $cat.status == "A"}
                    <option    value="{$cat.category_id}" {if $tw_settings.home_page_content == $cat.category_id}selected="selected"{/if}>{$cat.category|escape|indent:$cat.level:"&#166;&nbsp;&nbsp;&nbsp;&nbsp;":"&#166;--&nbsp;"}</option>
                {/if}
            {/foreach}
        </select>
        {include file="buttons/button.tpl" but_text=$lang.twgadmin_edit_these_blocks but_href="block_manager.manage&selected_location=`$locations_info.index`" but_id="elm_edit_home_page_blocks" but_role="link" but_meta="hidden"  but_target="_blank"}
        {include file="buttons/button.tpl" but_text=$lang.twgadmin_edit_these_blocks but_href="block_manager.manage&selected_location=`$locations_info.twigmo`" but_id="elm_edit_tw_home_page_blocks" but_role="link" but_meta="hidden"  but_target="_blank"}
    </div>

    {* Go to theme editor *}
    <div class="form-field">
        <label>{$lang.design}:</label>
        {include file="buttons/button.tpl" but_role="button" but_meta="cm-new-window" but_name="dispatch[addons.tw_svc_auth.te]" but_text=$lang.twgadmin_open_te}
    </div>

    {* Logo *}
    <div class="form-field">
        <label>{$lang.twgadmin_mobile_logo}:</label>
        <div class="float-left">
            {include file="common_templates/fileuploader.tpl" var_name="tw_settings[logo]" image=true}
        </div>
        <div class="float-left attach-images-alt logo-image">
            <img class="solid-border" src="{$tw_settings.logo_url|default:$default_logo}" />
        </div>
    </div>

    {* Favicon *}
    <div class="form-field">
        <label>{$lang.twgadmin_mobile_favicon}:</label>
        <div class="float-left">
            {include file="common_templates/fileuploader.tpl" var_name="tw_settings[favicon]" image=true}
        </div>
        <div class="float-left attach-images-alt logo-image">
            <img class="solid-border" src="{$favicon}" />
        </div>
    </div>

    {* Geolocation *}
    <div class="form-field">
        <label for="elm_tw_geolocation">{$lang.twgadmin_enable_geolocation}:</label>
        <input type="hidden" name="tw_settings[geolocation]" value="N" />
        <span class="checkbox-wrapper">
            <input type="checkbox" class="checkbox" id="elm_tw_geolocation" name="tw_settings[geolocation]" value="Y" {if $tw_settings.geolocation != "N"}checked="checked"{/if} />
        </span>
    </div>

    {* Show only required profile fields *}
    <div class="form-field">
        <label for="elm_tw_only_req_profile_fields">{$lang.twgadmin_only_req_profile_fields}:</label>
        <input type="hidden" name="tw_settings[only_req_profile_fields]" value="N" />
        <span class="checkbox-wrapper">
            <input type="checkbox" class="checkbox" id="elm_tw_only_req_profile_fields" name="tw_settings[only_req_profile_fields]" value="Y" {if $tw_settings.only_req_profile_fields == "Y"}checked="checked"{/if} />
        </span>
    </div>

    {* Show product code *}
    <div class="form-field">
        <label for="elm_tw_show_product_code">{$lang.twgadmin_show_product_code}:</label>
        <input type="hidden" name="tw_settings[show_product_code]" value="N" />
        <span class="checkbox-wrapper">
            <input type="checkbox" class="checkbox" id="elm_tw_show_product_code" name="tw_settings[show_product_code]" value="Y" {if $tw_settings.show_product_code == "Y"}checked="checked"{/if} />
        </span>
    </div>

    {* Send order status push *}
    <div class="form-field">
        <label for="elm_tw_send_order_status_push">{$lang.twgadmin_send_push}:</label>
        {if $store.is_connected && $store.is_platinum}
            <input type="hidden" name="tw_settings[send_order_status_push]" value="N" />
        {/if}
        <span class="checkbox-wrapper">
            <input type="checkbox" class="checkbox" id="elm_tw_send_order_status_push" name="tw_settings[send_order_status_push]" value="Y" {if $store.send_order_status_push == "Y"}checked="checked"{/if} {if !$store.is_connected or !$store.is_platinum}disabled="disabled"{/if} />
        </span>
    </div>

    {* Url for facebook *}
    <div class="form-field">
        <label for="elm_tw_url_for_facebook">{$lang.twgadmin_url_for_facebook}:</label>
        <input id="elm_tw_url_for_facebook" type="text" name="tw_settings[url_for_facebook]" size="30" value="{$tw_settings.url_for_facebook}" class="input-text" />
    </div>

    {* Url for twitter *}
    <div class="form-field">
        <label for="elm_tw_url_for_twitter">{$lang.twgadmin_url_for_twitter}:</label>
        <input id="elm_tw_url_for_twitter" type="text" name="tw_settings[url_for_twitter]" size="30" value="{$tw_settings.url_for_twitter}" class="input-text" />
    </div>

    {* Url on appstore *}
    <div class="form-field">
        <label for="elm_tw_url_for_appstore">{$lang.twgadmin_url_on_appstore}:</label>
        <input id="elm_tw_url_on_appstore" type="text" name="tw_settings[url_on_appstore]" size="30" value="{$tw_settings.url_on_appstore}" class="input-text" />
    </div>

    {* Url on googleplay *}
    <div class="form-field">
        <label for="elm_tw_url_on_googleplay">{$lang.twgadmin_url_on_googleplay}:</label>
        <input id="elm_tw_url_on_googleplay" type="text" name="tw_settings[url_on_googleplay]" size="30" value="{$tw_settings.url_on_googleplay}" class="input-text" />
    </div>

    <script type="text/javascript">
    //<![CDATA[
    {literal}
    $(function(){
        {/literal}
        var has_platinum_stores = {if $platinum_stores|count}true{else}false{/if};
        {literal}
        $('#twigmo_twg_push').toggle(has_platinum_stores);
        $('.form-field a.text-button-link').css({'margin': '0 0 0 10px'});
        $("#elm_tw_home_page_content").bind('change', function(){fn_tw_show_block_link();}).change();
        function fn_tw_show_block_link(){
            var value = $('#elm_tw_home_page_content option:selected').val();
            if ((value == 'home_page_blocks') || (value == 'tw_home_page_blocks')) {
                if (value == 'home_page_blocks') {
                    $('#elm_edit_home_page_blocks').show();
                    $('#elm_edit_tw_home_page_blocks').hide();
                } else {
                    $('#elm_edit_tw_home_page_blocks').show();
                    $('#elm_edit_home_page_blocks').hide();
                }
            } else {
                $('#elm_edit_home_page_blocks').hide();
                $('#elm_edit_tw_home_page_blocks').hide();
            }

            return true;
        }
    });
    {/literal}
    //]]>
    </script>

</fieldset>

{/if}

<!--storefront_settings--></div>
