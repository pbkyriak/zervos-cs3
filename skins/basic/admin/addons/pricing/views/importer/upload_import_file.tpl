{capture name="mainbox"}

{* 20-10-2015 *}
{include file="common_templates/subheader.tpl" title=$lang.quickmobile_stocklist}
<form method="POST" enctype="multipart/form-data" action="{"importer.upload_import_file_quickmobile"|fn_url}" >
	<div class="form-field">
		<label for="elm_from_name_{$id}">{$lang.filename}:</label>
		<input type="file" name="catalog" />
	</div>
	<p>{$lang.upload_xls_file}<br />{*$lang.lego_file_details*}</p>
  	1. <input type="submit" value="{$lang.upload}" />
</form>
<br />
<p style="text-align:left; margin-bottom: 40px;">2. <a href="http://www.homelike.gr/store_bridges.php?supplier_id=20" target="_blank">{$lang.quickmobile_manual_update_prompt}</a></p>
{* 02-12-2015 *}
{include file="common_templates/subheader.tpl" title=$lang.nexion_stocklist}
<form method="POST" enctype="multipart/form-data" action="{"importer.upload_import_file_nexion"|fn_url}" >
	<div class="form-field">
		<label for="elm_from_name_{$id}">{$lang.filename}:</label>
		<input type="file" name="catalog" />
	</div>
	<p>{$lang.upload_xls_file}<br />{*$lang.lego_file_details*}</p>
  	1. <input type="submit" value="{$lang.upload}" />
</form>
<br />
<p style="text-align:left; margin-bottom: 40px;">2. <a href="http://www.homelike.gr/store_bridges.php?supplier_id=23" target="_blank">{$lang.nexion_update_prompt}</a></p>

{* [/ihas] *}

{* [imatz] *}
{*
	{include file="common_templates/subheader.tpl" title=$lang.eurostar}
	<form method="POST" enctype="multipart/form-data" action="{"importer.upload_import_file_eurostar"|fn_url}" >
		<div class="form-field">
			<label for="elm_from_name_{$id}">{$lang.filename}:</label>
			<input type="file" name="catalog" />
		</div>
		<p>{$lang.upload_csv_file}</p>
	  1. <input type="submit" value="{$lang.upload}" />
	</form>
	<br />
	<p style="text-align:left; margin-bottom: 40px;">2. <a href="http://www.homelike.gr/price_updaterEurostar.php" target="_blank">{$lang.eurostar_manual_update_prompt}</a></p>
*}

{* 7/6/2-15 *}
{include file="common_templates/subheader.tpl" title=$lang.lego_stocklist}
<form method="POST" enctype="multipart/form-data" action="{"importer.upload_import_file_lego"|fn_url}" >
	<div class="form-field">
		<label for="elm_from_name_{$id}">{$lang.filename}:</label>
		<input type="file" name="catalog" />
	</div>
	<p>{$lang.upload_xls_file}<br />{$lang.lego_file_details}</p>
  	1. <input type="submit" value="{$lang.upload}" />
</form>
<br />
<p style="text-align:left; margin-bottom: 40px;">2. <a href="http://www.homelike.gr/store_bridges.php?supplier_id=19" target="_blank">{$lang.lego_manual_update_prompt}</a></p>
{* [/imatz] *}

{*
{include file="common_templates/subheader.tpl" title="Vektor Tec"}
<form method="POST" enctype="multipart/form-data" action="{"importer.upload_import_file"|fn_url}" >
		<div class="form-field">
			<label for="elm_from_name_{$id}">{$lang.filename}:</label>
			<input type="file" name="catalog" />
		</div>
<p>Ανεβάζουμε το αρχείο του Vector σε μορφή <b>csv</b>. Στήλες Κατασκευαστής,Τύπος,Περιγραφή,On line demo,Τιμή Καταλόγου,Τιμή VnP,image</p>
   <input type="submit" value="Ανέβασε" />
</form>
<br />
<a href="http://www.vnp.gr/price_updater_v.php" target="_blank">Ενημέρωση Vector</a>
<br />
*}

{*
<br />
{include file="common_templates/subheader.tpl" title="NortonLine"}
<form method="POST" enctype="multipart/form-data" action="{"importer.upload_import_file_norton"|fn_url}" >
	<div class="form-field">
		<label for="elm_from_name_{$id}">{$lang.filename}:</label>
		<input type="file" name="catalog" />
	</div>
	<p>Ανεβάζουμε το αρχείο του NortonLine (Nortonline_Catalogue_HH-MM-EEEE.xml).</p>
  <input type="submit" value="Ανέβασε" />
</form>
<br />
<a href="http://www.vnp.gr/price_updater_v.php" target="_blank">Ενημέρωση Vector</a>&nbsp;|&nbsp;<a href="http://www.vnp.gr/price_updater_n.php" target="_blank">Ενημέρωση NortonLine</a>
<br />
<br />
*}

{*
{include file="common_templates/subheader.tpl" title="AMY"}
<form method="POST" enctype="multipart/form-data" action="{"importer.upload_import_file_amy"|fn_url}" >
	<div class="form-field">
		<label for="elm_from_name_{$id}">{$lang.filename}:</label>
		<input type="file" name="catalog" />
	</div>
	<p>Ανεβάζουμε το αρχείο της ΑΜΥ (EEEE-M-H-ΩΩ-ΛΛ-ΔΔ-ΚΚ.csv).</p>
  <input type="submit" value="Ανέβασε" />
</form>
<br />
<a href="http://www.vnp.gr/price_updater_a.php" target="_blank">Ενημέρωση AMY</a>
<br />
<br />
*}

{*
{include file="common_templates/subheader.tpl" title="Yukatel With Prices"}
<form method="POST" enctype="multipart/form-data" action="{"importer.upload_import_file_yukaprices"|fn_url}" >
	<div class="form-field">
		<label for="elm_from_name_{$id}">{$lang.filename}:</label>
		<input type="file" name="catalog" />
	</div>
	<p>Ανεβάζουμε το αρχείο του Yuka με τιμες (stockxxx.xlsx).</p>
  1. <input type="submit" value="Ανέβασε" />
</form>
<p style="text-align:right">2. <a href="http://www.vnp.gr/price_updater_y.php" target="_blank">Ενημέρωση Yukatel με τιμές</a></p>
<br />
<br />
{include file="common_templates/subheader.tpl" title="Yukatel Only stock"}
<form method="POST" enctype="multipart/form-data" action="{"importer.upload_import_file_yukastock"|fn_url}" >
	<div class="form-field">
		<label for="elm_from_name_{$id}">{$lang.filename}:</label>
		<input type="file" name="catalog" />
	</div>
	<p>Ανεβάζουμε το αρχείο του Yuka με μόνο με ποσότητες (stockxxx.xlsx).</p>
  1. <input type="submit" value="Ανέβασε" />
</form>
<br />
<p style="text-align:right">2. <a href="http://www.vnp.gr/yukatel_stock.php" target="_blank">Ενημέρωση Yukatel μόνο ποσότητες</a></p>
*}

{/capture}

{include file="common_templates/mainbox.tpl" title=$lang.upload_import_file content=$smarty.capture.mainbox tools=$smarty.capture.tools select_languages=false}
