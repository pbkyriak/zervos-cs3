
{capture name="mainbox"}


<div id="open_orders">
<p style="width:400px; text-decoration: none;">Εφαρμόζει την προσαύξυση στα μη-αυτόματα είδη. Δεν έχουμε τιμή βάσης για αυτά οπότε η εφαρμογή γίνετε στην τρέχουσα τιμή. Αν κάνουμε 2,3,4 φορές τη διαδικασία τότε θα μπει το καπέλο 2,3,4 φορές αντίστοιχα.</p>
<a href="{"pricing.apply_hat_to_nonauto"|fn_url}" class="cm-confirm lowercase" style="color: #000">{$lang.apply_hat_to_nonauto}</a>
<!--open_orders--></div>


{/capture}

{include file="common_templates/mainbox.tpl" title=$lang.apply_hat_to_nonauto content=$smarty.capture.mainbox tools=$smarty.capture.tools select_languages=true}



