{* panos@salix.gr *}



{script src="js/picker.js"}
{include file="common_templates/popupbox.tpl" text=$lang.editing_block content=$content id="edit_price_list_picker" edit_picker=true}
{include file="common_templates/pagination.tpl" save_current_page=true save_current_url=true}
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table">
<tr>
	<th class="center" width="3%">
		<input type="checkbox" name="check_all" value="Y" title="{$lang.check_uncheck_all}" class="checkbox cm-check-items" />
  </th>
  <th width="10%">{$lang.id}</th>
  <th>{$lang.name}</th>
  <th>{$lang.price}</th>
	<th>{$lang.market_min_price}</th>
	<th>{$lang.market_max_price}</th>
	<th>{$lang.market_our_position}</th>
	<th>{$lang.market_last_run}</th>
</tr>

{foreach from=$products item=p}
<tr>
	<td class="center">
		<input type="checkbox" name="product_ids[]" value="{$p.product_id}" {if $p.market_suspend=='Y'}checked{/if} class="checkbox cm-item" />
	</td>
	<td>
		<a href="{"products.update?product_id=`$p.product_id`"|fn_url}">{$p.product_id}</a>
	</td>
	<td>
		<a href="{"products.update?product_id=`$p.product_id`"|fn_url}">{$p.product}</a>
	</td>
	<td>
		{$p.price}
	</td>
	<td>
		{$p.market_min_price}
	</td>
	<td>
		{$p.market_max_price}
	</td>
	<td>
		{$p.market_our_position}
	</td>
	<td>
		{$p.market_last_run|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
		<a href="{$p.market_link}" target="_blank">SK</a>
	</td>
</tr>
{/foreach}
</table>
{include file="common_templates/pagination.tpl"}
<script type="text/javascript">
//<![CDATA[
{literal}
function fn_show_price_list_picker(data)	{
	jQuery.show_picker('edit_price_list_picker', null, '.object-container');
}
{/literal}
//]]>
</script>