{* panos@salix.gr *}

{script src="js/picker.js"}
{script src="js/tabs.js"}

{capture name="mainbox"}
<form method="POST" action="{"pricing.market_miner"|fn_url}" >
 <fieldset>
   {include file="common_templates/subheader.tpl" title=$lang.search}
		<div class="form-field">
			<label for="">{$lang.name}:</label>
			<input type="text" name="q" />
		</div>
   <input type="submit" value="Αναζήτηση" />
   
</fieldset>
</form>

<form action="{""|fn_url}" method="post" name="grouped_shipments_form">
<div id="grouped_shipments">
	{if $products }
		{assign var="can_delete" value=true}
		{assign var="show_mode" value="show"}
		{include file="addons/pricing/views/pricing/components/product_list.tpl"}
	{else}
		<div class="items-container" ><p class="no-items">{$lang.no_items}</p></div>
	{/if}
<!--grouped_shipments--></div>

<div class="buttons-container buttons-bg">
	{if $products}
	<div class="float-left">
		{include file="buttons/save.tpl" but_name="dispatch[pricing.m_update]" but_role="button_main"}
	</div>
	{/if}	
</div>

</form>



{/capture}

{include file="common_templates/mainbox.tpl" title=$lang.market_miner content=$smarty.capture.mainbox tools=$smarty.capture.tools select_languages=true}
