{capture name="mainbox"}

{capture name="tabsbox"}
{** /Item menu section **}

{assign var="categories_company_id" value=$product_data.company_id}



{if $product_data.product_id}
	{assign var="id" value=$product_data.product_id}
{else}
	{assign var="id" value=0}
{/if}

<form action="{""|fn_url}" method="post" name="product_update_form" class="cm-form-highlight cm-disable-empty-files {if ""|fn_check_form_permissions || ("COMPANY_ID"|defined && $product_data.shared_product == "Y" && $product_data.company_id != $smarty.const.COMPANY_ID)} cm-hide-inputs{/if}" enctype="multipart/form-data"> {* product update form *}
<input type="hidden" name="fake" value="1" />
<input type="hidden" class="{$no_hide_input_if_shared_product}" name="selected_section" id="selected_section" value="{$smarty.request.selected_section}" />
<input type="hidden" class="{$no_hide_input_if_shared_product}" name="product_id" value="{$id}" />

{** Product description section **}

<div class="product-manage" id="content_detailed"> {* content detailed *}

{** General info section **}
<fieldset>

{include file="common_templates/subheader.tpl" title=$lang.information}

<div class="form-field {$no_hide_input_if_shared_product}">
	<label for="product_description_product" class="cm-required">{$lang.name}:</label>
	<span class="input-helper"><input type="text" name="product_data[product]" id="product_description_product" size="55" value="{$product_data.product}" class="input-text-large main-input" /></span>
	{include file="buttons/update_for_all.tpl" display=$show_update_for_all object_id='product' name="update_all_vendors[product]"}
</div>

{hook name="companies:product_details_fields"}


	{include file="views/companies/components/company_field.tpl" title=$lang.vendor name="product_data[company_id]" id="product_data_company_id" selected=$product_data.company_id tooltip=$companies_tooltip reload_form=$reload_form}
	<input type="hidden" value="product_categories" name="result_ids">
{/hook}

<div class="form-field {$no_hide_input_if_shared_product}" id="product_categories">
	{math equation="rand()" assign="rnd"}
	{if $smarty.request.category_id}
		{assign var="request_category_id" value=","|explode:$smarty.request.category_id}
	{else}
		{assign var="request_category_id" value=""}
	{/if}
	<label for="ccategories_{$rnd}_ids" class="cm-required{if $product_data.shared_product == "Y"} cm-no-tooltip{/if}">{$lang.categories}:</label>

	<div class="select-field categories">{include file="pickers/categories_picker.tpl" hide_input=$product_data.shared_product company_ids=$categories_company_id rnd=$rnd data_id="categories" input_name="product_data[categories]" radio_input_name="product_data[main_category]" main_category=$product_data.main_category item_ids=$product_data.category_ids|default:$request_category_id hide_link=true hide_delete_button=true display_input_id="category_ids" disable_no_item_text=true view_mode="list"}</div>
<!--product_categories--></div>

<div class="form-field {$no_hide_input_if_shared_product}">
	<label for="price_price" class="cm-required">{$lang.price} ({$currencies.$primary_currency.symbol}):</label>
	<input type="text" name="product_data[price]" id="price_price" size="10" value="{$product_data.price|default:"0.00"|fn_format_price:$primary_currency:null:false}" class="input-text-medium" />
	{include file="buttons/update_for_all.tpl" display=$show_update_for_all object_id='price' name="update_all_vendors[price]"}
</div>

<div class="form-field cm-no-hide-input">
	<label for="product_full_descr">{$lang.full_description}:</label>
	<textarea id="product_full_descr" name="product_data[full_description]" cols="55" rows="8" class="cm-wysiwyg input-textarea-long">{$product_data.full_description}</textarea>
	{include file="buttons/update_for_all.tpl" display=$show_update_for_all object_id='full_description' name="update_all_vendors[full_description]"}
</div>
{** /General info section **}

{include file="common_templates/select_status.tpl" input_name="product_data[status]" id="product_data" obj=$product_data hidden=true}

<div class="form-field">
	<label>{$lang.images}:</label>
	{include file="common_templates/attach_images.tpl" image_name="product_main" image_object_type="product" image_pair=$product_data.main_pair icon_text=$lang.text_product_thumbnail detailed_text=$lang.text_product_detailed_image no_thumbnail=true}
</div>
</fieldset>

</div> {* /content detailed *}

{** Form submit section **}

<div class="buttons-container cm-toggle-button buttons-bg">
	{include file="buttons/save_cancel.tpl" but_name="dispatch[products.fastadd]"}
</div>
{** /Form submit section **}

</form> {* /product update form *}

{hook name="products:tabs_extra"}{/hook}



{/capture}
{include file="common_templates/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$controller active_tab=$smarty.request.selected_section track=true}

{/capture}
{if !$id}
	{include file="common_templates/mainbox.tpl" title=$lang.new_product content=$smarty.capture.mainbox}
{else}
	{include file="common_templates/view_tools.tpl" url="products.update?product_id="}
	
	{capture name="preview"}
		
		{assign var="view_uri" value="products.view?product_id=`$id`"}
		{assign var="view_uri_escaped" value="`$view_uri`&amp;action=preview"|fn_url:'C':'http':'&':$smarty.const.DESCR_SL|escape:"url"}
		
		

		<a target="_blank" class="tool-link" title="{$view_uri|fn_url:'C':'http':'&':$smarty.const.DESCR_SL}" href="{$view_uri|fn_url:'C':'http':'&':$smarty.const.DESCR_SL}">{$lang.preview}</a>
		<a target="_blank" class="tool-link" title="{$view_uri|fn_url:'C':'http':'&':$smarty.const.DESCR_SL}" href="{"profiles.act_as_user?user_id=`$auth.user_id`&amp;area=C&amp;redirect_url=`$view_uri_escaped`"|fn_url}">{$lang.preview_as_admin}</a>
	{/capture}
	{include file="common_templates/mainbox.tpl" title="`$lang.editing_product`:&nbsp;`$product_data.product`"|unescape|strip_tags content=$smarty.capture.mainbox select_languages=true tools=$smarty.capture.view_tools}
{/if}