<div id="content_pricemarkup">
  {assign var="suppliers" value=1|fn_get_slxSuppliers_for_select}
  <fieldset>
    {include file="common_templates/subheader.tpl" title=$lang.price_markup}

    <table cellpadding="1" cellspacing="2" border="0" width="80%">
      <tr id="header">
        <td class="manage-header" width="1%" align="center">
          <input type="checkbox" class="checkbox cm-check-items" value="Y" name="check_all" title="{$lang.check_uncheck_all}" /></td>
        <td class="manage-header" align="center">&nbsp;{$lang.supplier}&nbsp;</td>
        <td class="manage-header" align="center">&nbsp;{$lang.from}&nbsp;</td>
        <td class="manage-header" align="center">&nbsp;{$lang.markup}&nbsp;</td>
        <td class="manage-header" align="center">&nbsp;{$lang.markup_type}&nbsp;</td>
        <td class="manage-header" align="center">&nbsp;{$lang.round_to}&nbsp;</td>
        <td class="manage-header">&nbsp;</td>
      </tr>
      {if count($pricing_rules)>0 }
        {foreach from=$pricing_rules item="rule"}
          <tr class="{cycle values='manage-row,other'}" >
            <td align="center"><input type="checkbox" class="checkbox cm-item" value="{$rule.id}" name="delete_rules[]" /></td>
            <td align="center" nowrap="nowrap" class="side-padding">
              <select name="rules_data[{$rule.id}][supplier_id]" >
                {foreach from=$suppliers item="supplier"}
                  <option value="{$supplier.supplier_id}" {if $supplier.supplier_id==$rule.supplier_id}selected{/if}>{$supplier.title}</option>
                {/foreach}
              </select>
            </td>
            <td align="center" nowrap="nowrap" class="side-padding">
              <input type="text" name="rules_data[{$rule.id}][from_price]" value="{$rule.from_price}" size="6" class="input-text" />
            </td>
            <td align="center" nowrap="nowrap" class="side-padding">
              <input type="text" name="rules_data[{$rule.id}][markup]" value="{$rule.markup}" size="6" class="input-text" />
            </td>
            <td align="center" nowrap="nowrap" class="side-padding">
              <select name="rules_data[{$rule.id}][markup_type]">
                <option value="A" {if $rule.markup_type=='A'}selected{/if}>{$lang.value}</option>
                <option value="P" {if $rule.markup_type=='P'}selected{/if}>{$lang.percent}</option>
              </select>
            </td>
            <td nowrap>
              <select name="rules_data[{$rule.id}][round_to]">
                <option value="0" {if $rule.round_to=='0'}selected{/if}>0.20</option>
                <option value="20" {if $rule.round_to=='20'}selected{/if}>0.20</option>
                <option value="50" {if $rule.round_to=='50'}selected{/if}>0.50</option>
              </select>
            </td>
            <td>&nbsp;</td>
          </tr>
        {/foreach}
      {/if}
      <tr id="box_rules">
        <td>&nbsp;</td>
        <td align="center" nowrap="nowrap">
          <select name="add_rules[0][supplier_id]" >
            {foreach from=$suppliers item="supplier"}
              <option value="{$supplier.id}">{$supplier.title}</option>
            {/foreach}
          </select>
        </td>
        <td align="center" nowrap="nowrap">
          <input type="text" name="add_rules[0][from_price]" value="" class="input-text" size="6" /></td>
        <td align="center" nowrap="nowrap">
          <input type="text" name="add_rules[0][markup]" value="" class="input-text" size="6" /></td>
        <td align="center" nowrap="nowrap" class="side-padding">
          <select name="add_rules[0][markup_type]">
            <option value="A" {if $rule.markup_type=='A'}selected{/if}>{$lang.value}</option>
            <option value="P" {if $rule.markup_type=='P'}selected{/if}>{$lang.percent}</option>
          </select>
        </td>
        <td nowrap>
          <select name="add_rules[0][round_to]">
            <option value="0">0.0</option>
            <option value="20">0.20</option>
            <option value="50">0.50</option>
          </select>
        </td>
        <td>
          {include file="buttons/multiple_buttons.tpl" item_id="rules" tag_level="1"}
        </td>
      </tr>
    </table>
  </fieldset>

  <!--content_autoupdater--></div>
