{* panos@salix.gr *}

{script src="js/picker.js"}
{script src="js/tabs.js"}

{capture name="mainbox"}

<form action="{""|fn_url}" method="get" name="grouped_shipments_form">
<div class="search-field">
	<label>{$lang.from}:</label>	
	{include file="common_templates/calendar.tpl" 
      date_id="f_date"
      date_name="f_date" 
      date_val=$f_date  
      start_year=$settings.Company.company_start_year}
</div>
<div class="search-field">
	<label>{$lang.to}:</label>
	{include file="common_templates/calendar.tpl" 
      date_id="t_date" 
      date_name="t_date" 
      date_val=$t_date  
      start_year=$settings.Company.company_start_year}
</div>
<div class="search-field">
	<label>{$lang.showDeleted}:</label>
	<select name="showDeleted">
		<option value="0" {if $showDeleted==0}selected{/if}>{$lang.file_normal}</option>
		<option value="1" {if $showDeleted==1}selected{/if}>{$lang.file_deleted}</option>
	</select>
</div>
{include file="buttons/button.tpl" but_text=$lang.search but_name="dispatch[offlineorders.manage]" but_role="submit"}
</form>

<form action="{""|fn_url}" method="post" name="offlineorder_form">

<div id="offlineorder_list">
	{if $orders }
		{assign var="can_delete" value=true}
		{assign var="show_mode" value="show"}
		{include file="addons/offline_orders/views/offlineorders/components/orders_list.tpl"}
	{else}
		<div class="items-container" ><p class="no-items">{$lang.no_items}</p></div>
	{/if}
<!--offlineorder_list--></div>

<div class="buttons-container buttons-bg">
	{if $orders}
	<div class="float-left">
		{capture name="tools_list"}
		<ul>
			<li><a class="cm-confirm cm-process-items" name="dispatch[offlineorders.m_delete]" rev="offlineorder_form">{$lang.delete_selected}</a></li>
		</ul>
		{/capture}

		{include file="common_templates/tools.tpl" prefix="main" hide_actions=true tools_list=$smarty.capture.tools_list display="inline" link_text=$lang.choose_action}
	</div>
	{/if}

</div>

</form>

{/capture}

{capture name="tools"}
	{include file="common_templates/tools.tpl" tool_href="offlineorders.update" prefix="top" link_text=$lang.add_order hide_tools=true}
{/capture}

{include file="common_templates/mainbox.tpl" title=$lang.offline_orders content=$smarty.capture.mainbox tools=$smarty.capture.tools select_languages=true}
