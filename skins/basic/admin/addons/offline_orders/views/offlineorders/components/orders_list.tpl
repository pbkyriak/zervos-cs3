{* panos@salix.gr *}
{literal}
<style>
	tr.offline_row_0 td { background-color: #ffffff; }
	tr.offline_row_1 td { background-color: #FFA200; }
	tr.offline_row_2 td { background-color: #AFFFD8; }
	tr.offline_row_3 td { background-color: #BFE8FF; }
</style>
{/literal}
{script src="js/picker.js"}
{include file="common_templates/popupbox.tpl" text=$lang.editing_block content=$content id="edit_price_list_picker" edit_picker=true}
{include file="common_templates/pagination.tpl" save_current_page=true save_current_url=true}
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table">
<tr>
	<th class="center" width="3%">
		<input type="checkbox" name="check_all" value="Y" title="{$lang.check_uncheck_all}" class="checkbox cm-check-items" />
  </th>
  <th width="10%">{$lang.id}</th>
  <th width="10%">{$lang.date}</th>
	<th>{$lang.product}</th>
	<th>{$lang.salesman}</th>
	<th>{$lang.client}</th>
	<th>{$lang.price}</th>
	<th>{$lang.shipment}</th>
	<th>{$lang.phone}</th>
	<th>{$lang.memo}</th>
	<th>{$lang.status}</th>
	<th width="20%">&nbsp;</th>
</tr>

{foreach from=$orders item=pl}
<tr class="offline_row_{$pl.color}">
	<td class="center">
		<input type="checkbox" name="order_ids[]" value="{$pl.id}" class="checkbox cm-item" /></td>
	<td>
		<a href="{"offlineorders.update?id=`$pl.id`"|fn_url}">{$pl.id}</a>
	</td>
	<td>
		{$pl.order_date|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
	</td>
	<td>
		{$pl.item}
	</td>
	<td>
		{$pl.salesman}
	</td>
	<td>
		{$pl.client}
	</td>
	<td>
		{$pl.price}
	</td>
	<td>
		{$pl.shipment}
	</td>
	<td>
		{$pl.phone}
	</td>
	<td>
		{$pl.memo}
	</td>
	<td>
		{if $pl.closed=='Y'}{$lang.closed}{else}{$lang.opened}{/if}
	</td>
	<td width="10%">
		{if $pl.deleted==0 }
			<a href="{"offlineorders.delete?id=`$pl.id`"|fn_url}" class="cm-confirm lowercase" style="color: #000">{$lang.delete}</a>
		{else}
			<a href="{"offlineorders.undelete?id=`$pl.id`"|fn_url}" class="cm-confirm lowercase" style="color: #000">{$lang.undelete}</a>
		{/if}
	</td>
</tr>
{/foreach}
</table>
{include file="common_templates/pagination.tpl"}
<script type="text/javascript">
//<![CDATA[
{literal}
function fn_show_price_list_picker(data)	{
	jQuery.show_picker('edit_price_list_picker', null, '.object-container');
}
{/literal}
//]]>
</script>