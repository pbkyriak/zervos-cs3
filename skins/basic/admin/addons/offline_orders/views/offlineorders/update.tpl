
{capture name="mainbox"}
  
  
{capture name="tabsbox"}

{** Grouped section **}
<div id="content_grouped">
	<fieldset>
		{include file="common_templates/subheader.tpl" title=$lang.order}
    <form method="post" name="update_amount_form" action="{"offlineorders.create"|fn_url}">
      <input type="hidden" name="order[id]" value="{$order.id}" />
			<div class="form-field">
				<label for="order_id">{$lang.id}:</label>
				<span class="input-helper">
					{$order.id}
				</span>
			</div>
			<div class="form-field">
				<label for="order_date" class="cm-required">{$lang.date}:</label>
				<span class="input-helper">
					{include file="common_templates/calendar.tpl" 
			      date_id="order_date"
			      date_name="order[order_date]" 
			      date_val=$order.order_date
			      start_year=$settings.Company.company_start_year}
				</span>
			</div>
			<div class="form-field">
				<label for="order_item" class="cm-required">{$lang.product}:</label>
				<span class="input-helper"><input type="text" name="order[item]" id="order_item" size="55" value="{$order.item}" class="input-text-large main-input" /></span>
			</div>
			<div class="form-field">
				<label for="order_salesman" class="cm-required">{$lang.salesman}:</label>
				<span class="input-helper"><input type="text" name="order[salesman]" id="order_" size="55" value="{$order.salesman}" class="input-text-large main-input" /></span>
			</div>
			<div class="form-field">
				<label for="order_client" class="cm-required">{$lang.client}:</label>
				<span class="input-helper"><input type="text" name="order[client]" id="order_client" size="55" value="{$order.client}" class="input-text-large main-input" /></span>
			</div>
			<div class="form-field">
				<label for="order_price" class="cm-required">{$lang.price}:</label>
				<span class="input-helper"><input type="text" name="order[price]" id="order_price" size="55" value="{$order.price}" class="input-text-large main-input" /></span>
			</div>
			<div class="form-field">
				<label for="order_shipment" class="cm-required">{$lang.shipment}:</label>
				<span class="input-helper"><input type="text" name="order[shipment]" id="order_shipment" size="55" value="{$order.shipment}" class="input-text-large main-input" /></span>
			</div>
			<div class="form-field">
				<label for="order_phone" class="cm-required">{$lang.phone}:</label>
				<span class="input-helper"><input type="text" name="order[phone]" id="order_" size="55" value="{$order.phone}" class="input-text-large main-input" /></span>
			</div>
			<div class="form-field">
				<label for="order_memo" class="cm-required">{$lang.memo}:</label>
				<span class="input-helper"><input type="text" name="order[memo]" id="order_" size="55" value="{$order.memo}" class="input-text-large main-input" /></span>
			</div>
			<div class="form-field">
				<label for="order_closed">{$lang.color}:</label>
				<select name="order[color]">
					<option value="0" {if $order.color==0}selected{/if}>{$lang.white}</option>
					<option value="1" {if $order.color==1}selected{/if}>{$lang.red}</option>
					<option value="2" {if $order.color==2}selected{/if}>{$lang.green}</option>
					<option value="3" {if $order.color==3}selected{/if}>{$lang.blue}</option>
				</select>
			</div>
			<div class="form-field">
				<label for="order_closed">{$lang.closed}:</label>
				<input type="hidden" name="order[closed]" value="N" />
				<input type="checkbox" name="order[closed]" id="order_closed" value="Y" {if $order.closed == "Y"}checked="checked"{/if} class="checkbox" />
			</div>
	    <div class="buttons-container buttons-bg">
	      <div class="float-left">
	        <input type="submit" value="{$lang.save}" />&nbsp;
	        <a href="{"offlineorders.manage"|fn_url}">{$lang.cancel}</a>
	      </div>
	    </div>
    </form>
	</fieldset>
<!--content_grouped--></div>
{** /Grouped section **}


{/capture}

{include file="common_templates/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$controller active_tab=$smarty.request.selected_section track=true}
{/capture}

{include file="common_templates/mainbox.tpl" title=$lang.create_order content=$smarty.capture.mainbox tools=$smarty.capture.tools select_languages=true}
