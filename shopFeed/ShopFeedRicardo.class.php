<?php

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

/**
 * Description of ShopFeedRicardo
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedRicardo extends ShopFeedBase {
  
  public function export() {
    $mobileCategories = db_get_fields("SELECT category_id FROM cscart_categories WHERE `id_path` LIKE '2/%'");
    $this->openOutput();
    $query = str_replace('?:', TABLE_PREFIX, "SELECT * FROM ?:shopFeedData ORDER BY category_id, name");
    if( ($result = $this->dbConn->query($query)) ) {
      $this->dbRowCount = $this->dbConn->affected_rows;
      while ($row = $result->fetch_assoc()) {
        if(trim($row['description'])=='' || trim($row['description'])==trim($row['name']) )
          continue;
	      if( strpos($row['image'], 'no_image.gif')===FALSE ) {
          fn_my_changes_update_process($this->process_key);
          $description = $this->validateStringData($row['description']);
          if( strlen($description)>150 || $row['supplier_id']==3 )
            $description = $description;
          else {
            $description = $this->getRemoteDescr($row['ean']);
          }
          $cid = $row['category_id'];
          $ctitle = $row['category'];
          if( in_array($cid, $mobileCategories) ) {
          	if( $row['price']<100 )
          		continue;
          	else {
          		$row['price'] = $this->round_to($row['price'] * 0.95,0.10);
          	}
          }
	        $this->writeln("<product>");
	        $this->writeln( sprintf("<id>%s</id>", $row['uniqueId'] ));
	        $this->writeln( sprintf("<name><![CDATA[%s]]></name>", $row['name'] ));
	        $this->writeln( sprintf("<link><![CDATA[%s]]></link>", $row['link'] ));
	        $this->writeln( sprintf("<image><![CDATA[%s]]></image>", $row['image'] ));
	        $this->writeln( sprintf("<sku><![CDATA[%s]]></sku>", $row['product_code'] ));
	        $this->writeln( sprintf("<category id=\"%s\"><![CDATA[%s]]></category>", $cid, $ctitle ));
	        $this->writeln( sprintf("<price>%s</price>", $row['price'] ));
	        $this->writeln( sprintf("<description><![CDATA[%s]]></description>", $description ));
	        $this->writeln( sprintf("<instock><![CDATA[%s]]></instock>", $row['instock'] ));
	        if( $row['instock']=='N' )
	          $this->writeln( sprintf("<availability><![CDATA[%s]]></availability>", $row['availability'] ));
	        else
	          $this->writeln( sprintf("<availability><![CDATA[%s]]></availability>", 'Άμεσα' ));
	        $this->writeln("</product>");
	        $this->feedRowCount++;
	      }
      }
    }
    $this->closeOutout();
  }
  
  private function getRemoteDescr($ean) {
    $cDir = DIR_ROOT.'/var/ag_specs';
    if( !file_exists($cDir) ) {
      mkdir($cDir);
      chmod($cDir, 0777);
    }
    $fn = $cDir.'/'.str_replace('/','-',$ean).'.txt';
    if( file_exists($fn) ) {
      return file_get_contents($fn);
    }
    else {
      $url = sprintf("http://www.agoraseto.gr/index.php?dispatch=widget.specs&ean=%s&id=vnp39384&type=table2&lang=EL&callback=0", $ean);
      $ch = curl_init();
      $timeout = 5;
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
      $data = curl_exec($ch);
      curl_close($ch);
      $data = substr($data, 3, strlen($data)-5);
      file_put_contents($fn, $data);
      return $data;
    }
  }
  private function round_to($number, $increments) {
    $increments = 1 / $increments;
    return (round($number * $increments) / $increments);
  }

}

