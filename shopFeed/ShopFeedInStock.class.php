<?php
//
// Author: Ioannis Matziaris [imatz]
// Email: imatzgr@gmail.com
// Date: 18/1/2015
// Details: Export to Excel Format
//


require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');
//require_once(dirname(__FILE__) . "/../lib/PHPExcel/PHPExcel.php");

class ShopFeedInStock extends ShopFeedBase {

	public function export() {
	//	require_once(Registry::get('config.dir.root') . "/lib/PHPExcel/PHPExcel/Writer/Excel2007.php");
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("Homelike.gr");

		$objPHPExcel->setActiveSheetIndex(0);

		//Define Headers
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Product Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Title');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Amount');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Price');
		
		$result = db_get_array("SELECT p.product_code, pd.product, p.amount , pp.price
									FROM cscart_products AS p
									LEFT JOIN cscart_product_descriptions AS pd ON (p.product_id=pd.product_id AND pd.lang_code='el')
									LEFT JOIN cscart_product_prices AS pp ON (p.product_id=pp.product_id AND pp.lower_limit=1 AND pp.usergroup_id=0)
									WHERE p.status='A' AND p.shop_availability=0;"
								 );
		if( $result ) {
			$this->dbRowCount = count($result);
			$x=2;
			foreach ($result as $row){
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$x, $row['product_code']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$x, $row['product']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$x, $row['amount']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$x, $row['price']);
				$x++;
				fn_my_changes_update_process($this->process_key);
			}
		}
		$objPHPExcel->getActiveSheet()->setTitle('Products');

		// Save Excel 2007 file
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($this->feedFile);

	}
}
