<?php

/**
 * Description of ShopFeedDataCollectorFactory
 * Created on 31-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedDataCollectorFactory {

  public static function getDataCollector($version, $type='PROFESSIONAL') {
    if ($version >= 2.2 && $type=='PROFESSIONAL')
      return new ShopFeedDataCollector21a();
    elseif ($version >= 2.1 && $type=='MULTIVENDOR')
      return new ShopFeedDataCollector21mv();
    elseif ($version >= 2.1 && $type=='PROFESSIONAL')
      return new ShopFeedDataCollector21a();
    elseif ($version >= 2.014) 
      return new ShopFeedDataCollector20b();
    elseif ($version >= 2.008) 
      return new ShopFeedDataCollector20a();
    else
      throw new Exception('Not compatible with versions prior to 2.0.8');
  }

}

