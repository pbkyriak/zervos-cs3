<?php

/**
 * Description of ShopFeedBase
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class ShopFeedBase {

  protected $feedFolder, $feedFile, $dbConn;
  protected $tmpFeedFile;
  protected $outputHandler, $xmlEncoding;
  protected $dbRowCount, $feedRowCount;
  protected $createdTag;
  protected $cleanNonLatin;
  protected $process_key;
  
  public function __construct($feedFolder, $feedFile, $db_conn) {
    $this->feedFolder = DIR_ROOT . '/' . $feedFolder;
    $this->feedFile = $this->feedFolder . '/' . $feedFile;
    $path_parts=pathinfo($feedFile);
    $this->tmpFeedFile=$this->feedFolder.'/'.$path_parts['filename'].'_'.time().'.'.$path_parts['extension'];
    $this->dbConn = $db_conn;
    $this->xmlEncoding = "utf-8";
    $this->dbRowCount = 0;
    $this->feedRowCount = 0;
    $this->createdTag = 'created_at';
    $this->cleanNonLatin = false;
    
    if (!is_dir($this->feedFolder)) {
      clearstatcache();
      mkdir($this->feedFolder, 0777);
    }
  }

  /**
   *
   * @param boolean $value 
   */
  public function setCleanNonLatin($value) {
    $this->cleanNonLatin = $value;
  }
  public function setProcess($value) {
    $this->process_key = $value;
  }
  protected function openOutput() {
    //$this->outputHandler = fopen($this->feedFile, "w");
    $this->outputHandler = fopen($this->tmpFeedFile, "w");
    $this->writeLn('<?xml version="1.0" encoding="' . $this->xmlEncoding . '" ?>');
    $this->writeLn('<mystore>');
    $this->writeLn(sprintf("\t<%s>%s</%s>", $this->createdTag, date("Y-m-d H:i:s"), $this->createdTag));
    $this->writeLn("\t<products>");
  }

  protected function closeOutout() {
    $this->writeLn("\t</products>");
    $this->writeLn('</mystore>');
    fclose($this->outputHandler);
    /*
    libxml_use_internal_errors(true);
    $sxe = simplexml_load_file($this->feedFile);
    if (!$sxe) {
      foreach (libxml_get_errors() as $error) {
        echo 'Σφάλμα:'. $error->message. '<br />';
      }
    }
    unset($sxe);
     * 
     */

    if(file_exists($this->feedFile)){
      unlink($this->feedFile);
    }

    rename($this->tmpFeedFile, $this->feedFile);
    
    if( file_exists($this->feedFile . '.zip') ) {
      unlink($this->feedFile . '.zip');
    }
    if (filesize($this->feedFile) > 10 * 1048576) {
    	/*
      $zip = new ZipArchive();
      $res = $zip->open($this->feedFile . '.zip', ZipArchive::CREATE);
      if ($res === TRUE) {
        $zip->addFile($this->feedFile, str_replace($this->feedFolder . '/', '', $this->feedFile));
        $zip->close();
      }
      */
      $zip = new PclZip($this->feedFile . '.zip');
      $zip->add($this->feedFile,PCLZIP_OPT_REMOVE_ALL_PATH);
    }
    
    //$this->letMeKnow();
  }

  protected function write($text) {
    fputs($this->outputHandler, $text);
  }

  protected function writeLn($text) {
    $this->write($text . "\n");
  }

  protected function letMeKnow() {
    ShopFeedBase::sendMail(
            'panos@salix.gr', 'ShopFeed Export', sprintf('ShopFeed Export runed dbRows=%s feedRows=%s', $this->dbRowCount, $this->feedRowCount)
    );
  }

  public static function sendMail($recipient, $subject, $body) {
    fn_init_mailer();
    $mailer = & Registry::get('mailer');
    $__from['email'] = Registry::get('settings.Company.company_site_administrator');
    $__from['name'] = Registry::get('settings.Company.company_name');
    if (method_exists($mailer, 'SetFrom'))
      $mailer->SetFrom($__from['email'], $__from['name']);
    else {
      $mailer->From = $__from['email'];
      $mailer->FromName = $__from['name'];
    }
    $mailer->ClearAttachments();
    $mailer->IsHTML(false);
    $mailer->CharSet = 'UTF-8';
    $mailer->Subject = $subject;
    $mailer->Body = $body;
    $mailer->ClearAddresses();
    $mailer->AddAddress($recipient, '');
    $result = $mailer->Send();
  }

  abstract public function export();

  protected function validateStringData($text) {
    libxml_use_internal_errors(true);
    if( $this->cleanNonLatin )
      $text = preg_replace('/[^\00-\255]+/u', '', $text);
    $sxe = simplexml_load_string(sprintf('<?xml version="1.0" encoding="utf-8"?><description><![CDATA[%s]]></description>', $text));
    $errCount =0;
    if (!$sxe) {
      $msg = '';
      foreach (libxml_get_errors() as $error) {
        $msg .= "\n\t".$error->message;
        $errCount++;
      }
    }
    if( $errCount )
      return '';
    else
      return $text;
  }

  //[imatz]
  //Ipologismo eksodwn apostolis ana proion.
  //An ena proion exei polous tropous apostolis tote epilegete o pio akrivos
  public function fn_calculate_shipping_cost($row){
    if(empty((float)$row["weight"])){
      $row["weight"]=2.000;
    } 

    $product_details=db_get_row("SELECT free_shipping, shipping_freight, shipping_params FROM ?:products 
      WHERE product_id=?i",$row["uniqueId"]);
    $cart=array();
    $cart["shipping"]=array();
    $cart["products"]=array();
    $cart["products"][0]['product_id']=$row["uniqueId"];
    $cart["products"][0]['company_id']=(defined('COMPANY_ID')?COMPANY_ID:0);
    $cart["products"][0]['free_shipping']="N";
    $cart["products"][0]['is_edp']="N";
    $cart["products"][0]['edp_shipping']="N";
    $cart["products"][0]['price']=$cart["products"][0]['base_price']=$cart["products"][0]['subtotal']=$row["price"];
    $cart["products"][0]['amount']=1;
    $cart["products"][0]['weight']=$row["weight"];
    if(!empty($product_details)){
      $cart["products"][0]['free_shipping']=$product_details["free_shipping"];
      $cart["products"][0]['shipping_freight']=$product_details["shipping_freight"];
      $cart["products"][0]['shipping_params']=unserialize($product_details["shipping_params"]);
    }
    $cart_products=$cart["products"];
    $auth=array();
    $auth["usergroup_ids"]=array();
    $shipping_rates = fn_calculate_shipping_rates($cart, $cart_products, $auth, false);
    $shipping_cost=0;
    $_rates=array();
    if(!empty($shipping_rates)){
      foreach($shipping_rates as $sid=>$s){
        if(isset($s["rates"])){
          foreach($s["rates"] as $rid=>$r){
            $_rates[]=$r;
          }
        }
      }
    }
    if(!empty($_rates)){
      sort($_rates);
      $shipping_cost=$_rates[0];
    }
    return($shipping_cost);
  }
  //[/imatz]

}

