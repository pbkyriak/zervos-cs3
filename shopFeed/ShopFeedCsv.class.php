<?php

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

/**
 * Description of ShopFeedCsv
 * Created on 29-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedCsv extends ShopFeedBase {
  
  protected function openOutput() {
    $this->outputHandler = fopen($this->feedFile, "w");
  }

  protected function closeOutout() {
    fclose($this->outputHandler);
    if (filesize($this->feedFile) > 10 * 1048576) {
      $zip = new ZipArchive();
      $res = $zip->open($this->feedFile . '.zip', ZipArchive::CREATE);
      if ($res === TRUE) {
        $zip->addFile($this->feedFile, str_replace($this->feedFolder . '/', '', $this->feedFile));
        $zip->close();
      }
    }
    $this->letMeKnow();
  }
  
  public function export() {
    
    $this->openOutput();
    $query = str_replace('?:', TABLE_PREFIX, "SELECT product_code, name, price, instock, amount FROM ?:shopFeedData ORDER BY category_id, name");
    if( ($result = $this->dbConn->query($query)) ) {
      $this->dbRowCount = $this->dbConn->affected_rows;
      while ($row = $result->fetch_assoc()) {
        fputcsv($this->outputHandler, $row);
        fn_my_changes_update_process($this->process_key);
        $this->feedRowCount++;
      }
    }
    $this->closeOutout();
  }
}

