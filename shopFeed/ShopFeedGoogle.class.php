<?php

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

/**
 * Description of ShopFeedGoogle
 * Created on 11-6-2012
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedGoogle extends ShopFeedBase {
  
  public function export() {
    
    $this->openOutput();
    $query = str_replace('?:', TABLE_PREFIX, "SELECT * FROM ?:shopFeedData ORDER BY category_id, name");
    if( ($result = $this->dbConn->query($query)) ) {
      $this->dbRowCount = $this->dbConn->affected_rows;
      $category = '';
      while ($row = $result->fetch_assoc()) {
        if(trim($row['description'])=='' || trim($row['description'])==trim($row['name']) )
          continue;
	      if( strpos($row['image'], 'no_image.gif')===FALSE ) {
          fn_my_changes_update_process($this->process_key);
          $cid = $row['category_id'];
          $ctitle = $row['category'];
          if( $ctitle!=$category ) {
          	$out = array();
          	$out[] = $ctitle;
          	$out[] = '';
          	$out[] = '';
          	fputcsv($this->outputHandler,$out);
          	$category = $ctitle;
          }
          $out = array();
          $out[] = '';
          $out[] = $row['product_code'];
          $out[] = $row['name'];
          fputcsv($this->outputHandler,$out);
	        $this->feedRowCount++;
	      }
      }
    }
    $this->closeOutout();
  }
  
  protected function openOutput() {
    $this->outputHandler = fopen($this->feedFile, "w");
  }

  protected function closeOutout() {
    fclose($this->outputHandler);
  }
  
}

