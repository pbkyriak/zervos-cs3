<?php

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

/**
 * Description of ShopFeedSkroutz
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedSkroutz extends ShopFeedBase {
  
  public function export() {
    $excluded_categories=new ExcludedCategories();
    $this->openOutput();
    $query = str_replace('?:', TABLE_PREFIX, "SELECT * FROM ?:shopFeedData ORDER BY category_id, name");
    $excluded_products=0;
	
	$excludeExcludedIdPaths= array(
		'7809406/7809445',
		'7809406/7809446',
		'7809406/7809449',
		'7809406/7809450',
	);
	
    if( ($result = $this->dbConn->query($query)) ) {
      $this->dbRowCount = $this->dbConn->affected_rows;
      while ($row = $result->fetch_assoc()) {
      	if( substr($row['product_code'], 0,2)=='14' )
      		continue;
      	if( trim($row['name'])=='' )
      		continue;
		//trim(strip_tags($row['description']))=='' || trim(strip_tags($row['description']))==trim($row['name'])) && 
		if( !in_array($row['supplier_id'], array(10,15,20,21,12,22,23,24,11,0,27,26)) )  // andreas difox && mobileshop && telepart && quickmobile && nakas && thomann && nexion && rologia && wave && oxi 26 bigbuy && xeirokinhta pane skroutz && api.de && bigb
			continue;       
        fn_my_changes_update_process($this->process_key); 
        if($excluded_categories->isCategoryExcluded($row['category_id_path']) && !in_array($row['category_id_path'], $excludeExcludedIdPaths)){ 
          $excluded_products++;
          echo($excluded_products.") Skroutz Excluded: ".$row['uniqueId']." - ".$row['product_code']. " * ". $row['supplier_id'] . "<br />");
          continue;
        }
		
        $shipping_cost=0; //$this->fn_calculate_shipping_cost($row);
        $this->writeln("<product>");
        $this->writeln( sprintf("<id>%s</id>", $row['uniqueId'] ));
        //[imatz] - 26/11/2014 - Stamatisan na mpainoun oi doseis mesa ston titlo
        /*
	   if ($row['price'] > 150) {
        $this->writeln( sprintf("<name><![CDATA[%s (έως 36 δόσεις)]]></name>", $row['name'] ));
        }
        else {
        $this->writeln( sprintf("<name><![CDATA[%s]]></name>", $row['name'] ));
        }
        //[/imatz]
		*/
		$this->writeln( sprintf("<name><![CDATA[%s]]></name>", $row['name'] ));
        $this->writeln( sprintf("<link><![CDATA[%s]]></link>", $row['link'] ));
        $this->writeln( sprintf("<image><![CDATA[%s]]></image>", $row['image'] ));
        $this->writeln( sprintf("<sku><![CDATA[%s]]></sku>", $row['product_code'] ));
		if(strpos($row['ean'],'@@')!==false) {
			$mpuu = explode('@@', $row['ean']);
			if(isset($mpuu[0])) {
				$this->writeln( sprintf("<mpu><![CDATA[%s]]></mpu>", $mpuu[0] ));
			}
			if(isset($mpuu[1])) {
				$this->writeln( sprintf("<ean><![CDATA[%s]]></ean>", $mpuu[1] ));
			}
		}
		else {
			$this->writeln( sprintf("<mpu><![CDATA[%s]]></mpu>", $row['ean'] ));
		}
        $this->writeln( sprintf("<category id=\"%s\"><![CDATA[%s]]></category>", $row['category_id'], $row['category'] ));
        $this->writeln( sprintf("<price>%s</price>", $row['price'] ));
				$this->writeln( sprintf("<description><![CDATA[%s]]></description>", $row['name'] ));
		//$this->writeln( sprintf("<description><![CDATA[%s]]></description>", $this->validateStringData($row['description']) ));
        $this->writeln( sprintf("<instock>%s</instock>", $row['instock'] ));
        $this->writeln( sprintf("<availability>%s</availability>", $row['availability'] ));
        //$this->writeln( sprintf("<shipping>%s</shipping>", $shipping_cost)); bgazoyme to shipping!!
		$weight = (float)$row['weight'];
		if($weight>5) {
			$weight += 5;
		}
        $this->writeln( sprintf("<weight>%s kg</weight>", $weight));
        $this->writeln("</product>");
        $this->feedRowCount++;
      }
      //echo("Skroutz Total added products: ".$this->feedRowCount."<br /><br />");
    }
    $this->closeOutout();
  }
}