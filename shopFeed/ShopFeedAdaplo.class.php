<?php

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

/**
 * Description of ShopFeedSkroutz
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedAdaplo extends ShopFeedBase {
  
  public function export() {
    $excluded_categories=new ExcludedCategories();
    $this->openOutput();
	if( $result = db_get_array("SELECT * FROM ?:shopFeedData ORDER BY category_id, name") ) {
		$this->dbRowCount = count($result);
		foreach ($result as $row){
			if( empty($row['product_code']) ) {
				continue;
			}
			$name = $row['name'];
            $str = str_replace('&', '', $row['description']);
			$data = array();
			$data['id'] = $row['product_code'];
			$data['title'] = $row['name'];
			$data['description'] =  $this->validateStringData(strip_tags($str));
			$data['product_type'] = strtr($row['category'], array('>' => '/', '&'=>'&amp;'));
			$data['link'] = $row['link'];
			$data['image_link'] = $row['image'];
			$data['condition'] = 'new';
			$data['availability'] = 'In stock'; //$row['availability'];
			$data['price'] = $row['price'];
			$data['sale_price'] = $row['price'];
			$data['brand'] = $row['manufacturer'];
			if( $this->feedRowCount==0 ) {
				fputcsv($this->outputHandler, array_keys($data));
			}
			fputcsv($this->outputHandler, $data);
			$this->feedRowCount++;        
		}
	}
    $this->closeOutout();
  }
  
  protected function openOutput() {
    $this->outputHandler = fopen($this->feedFile, "w");
  }

  protected function closeOutout() {
    fclose($this->outputHandler);
    if (filesize($this->feedFile) > 10 * 1048576) {
      $zip = new ZipArchive();
      $res = $zip->open($this->feedFile . '.zip', ZipArchive::CREATE);
      if ($res === TRUE) {
        $zip->addFile($this->feedFile, str_replace($this->feedFolder . '/', '', $this->feedFile));
        $zip->close();
      }
    }
  }
  
}