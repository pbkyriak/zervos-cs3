<?php

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

/**
 * Description of ShopFeedQuickmobile
 * Created on 24-8-2011
 * Updated on 30-9-2015
 * @author Andreas <an@weblive.gr>
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedQuickmobile extends ShopFeedBase {
  
  public function export() {
    $excluded_categories=new ExcludedCategories();
    $this->openOutput();
	//category_id_path like ("2%") μονο κινητα
    if( $result = db_get_array("SELECT * FROM ?:shopFeedData WHERE supplier_id=20 ORDER BY category_id, name") ) {  //quickmobile
      $this->dbRowCount = count($result);
      foreach ($result as $row){
	
        fn_my_changes_update_process($this->process_key);

        if(    trim($row['name'])==''
			|| trim(strip_tags($row['description']))=='' 
			|| trim(strip_tags($row['description']))==trim($row['name'])
			|| strlen(strip_tags($row['description']))<=150
			) {
			continue;    
        }

        if(($excluded_categories->isCategoryExcluded($row['category_id_path']))!=7809406)  //MONO KATHGORIA KINHTA 
          continue;

        $this->writeln("<product>");
		$this->writeln( sprintf("<sku><![CDATA[%s]]></sku>", $row['product_code'] ));
		$this->writeln( sprintf("<name><![CDATA[%s]]></name>", $row['name'] ));
		$this->writeln( sprintf("<description><![CDATA[%s]]></description>", $row['description'] ));	
        $this->writeln( sprintf("<image><![CDATA[%s]]></image>", $row['image'] ));
        $this->writeln( sprintf("<amount><![CDATA[%s]]></amount>", $row['amount'] ));        
        $this->writeln( sprintf("<price>%s</price>", $row['price'] ));
        $this->writeln("</product>");
		
        $this->feedRowCount++;        
      }
    }
    $this->closeOutout();
  }
}