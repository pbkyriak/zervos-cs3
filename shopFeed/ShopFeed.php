<?php

define('SHOPFEED_VERSION', '1.2.10');
require_once(dirname(__FILE__).'/ShopFeedUpdater.class.php');
require_once(dirname(__FILE__).'/ShopFeedDataCollectorFactory.class.php');
require_once(dirname(__FILE__).'/ShopFeedDataCollector.class.php');
require_once(dirname(__FILE__).'/ShopFeedDataCollector21mv.class.php');
require_once(dirname(__FILE__).'/ShopFeedDataCollector21a.class.php');
require_once(dirname(__FILE__).'/ShopFeedDataCollector20a.class.php');
require_once(dirname(__FILE__).'/ShopFeedDataCollector20b.class.php');
require_once(dirname(__FILE__).'/ShopFeedSkroutz.class.php');
require_once(dirname(__FILE__).'/ShopFeedBestPrice.class.php');
require_once(dirname(__FILE__).'/ShopFeedRicardo.class.php');
require_once(dirname(__FILE__).'/ShopFeedGoogle.class.php');
require_once(dirname(__FILE__).'/ShopFeedCsv.class.php');
require_once(dirname(__FILE__).'/ShopFeedExcel.class.php'); //[imatz] - 18/1/2015 Export se Excel Format
require_once(dirname(__FILE__).'/ShopFeedPlacemall.class.php');	// [weblive] - 2-6-2015
require_once(dirname(__FILE__).'/ShopFeedShopedia.class.php'); // [imatz] - 5/6/2015
require_once(dirname(__FILE__).'/ShopFeedQuickmobile.class.php'); // weblive - 27/11/2015
require_once(dirname(__FILE__).'/ShopFeedAdaplo.class.php'); // weblive - 27/11/2015
require_once(dirname(__FILE__).'/ShopFeedInStock.class.php'); // weblive - 15/7/2016
require_once(dirname(__FILE__).'/ShopFeedErgaleia.class.php'); // weblive - 15/7/2016
require_once(dirname(__FILE__).'/pclzip.lib.php');

error_reporting(E_ALL & ~E_STRICT);

DEFINE ('AREA', 'A');
DEFINE ('AREA_NAME' ,'customer');
require './prepare.php';

if (!empty($html_catalog))  {
	define ('NO_SESSION', true);
}
require './init.php';

error_reporting(E_ALL & ~E_STRICT);

@ini_set('memory_limit', '256M');
$current_location = 'http://' .Registry::get('config.http_host');

$cart_language='EL';

clearstatcache();

ShopFeedUpdater::create()->checkForUpdates();
// cscart version check
if(defined('PRODUCT_TYPE')) 
  $type = PRODUCT_TYPE;
else
  $type = 'PROFESSIONAL';

  list($major, $minor, $build) = explode('.',PRODUCT_VERSION);
$version = $major+$minor/10+$build/1000;
if( $version>=2.2  && $type=='PROFESSIONAL' )
  $db_conn = & Registry::get('runtime.dbs.main');
elseif( $version>=2.1  && ($type=='PROFESSIONAL' || $type=='MULTIVENDOR' || $type=='COMMUNITY') )
  global $db_conn;
elseif( $version>=2.0 && $type=='PROFESSIONAL' ) {
  global $db_conn;
  $cart_language = 'GR';
}
else
  die('Not compatible with versions prior to 2.0');

// db driver check
if( Registry::get('config.db_type')!='mysqli' )
  die('Works Only with mysqli driver!');

// get datacollector
$dataCollector = ShopFeedDataCollectorFactory::getDataCollector($version, $type);