<?php

/**
 * Description of ShopFeedUpdater
 * Created on 1-9-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedUpdater {
  public function create() {
    return new ShopFeedUpdater();
  }
  
  public function checkForUpdates() {
  	/*
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://salix.gr/shopFeed/check.php?version='.SHOPFEED_VERSION);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    if($result=='Y') {
      $this->letMeKnow();
    }
    */
    
  }

  protected function letMeKnow() {
    ShopFeedBase::sendMail(
            Registry::get('settings.Company.company_site_administrator'), 
            'ShopFeed Export', 
            'Υπάρχει διαθέσιμη νεότερη έκδοση για το ShopFeed Export επικοινωνίστε με τον προμηθευτή σας για να μάθετε σχετικά.'
                    );
  }
}
