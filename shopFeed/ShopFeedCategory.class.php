<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopFeedCategory
 * Created on 23-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedCategory {
  protected $id, $title, $id_path, $idPathTitle;
  
  public function __construct($id, $title, $id_path) {
    $this->id = $id;
    $this->title = $title;
    $this->id_path = $id_path;
    $this->idPathTitle = '';
  }
  
  public function getId() {
    return $this->id;
  }
  
  public function setId($value) {
    $this->id = $value;
  }
  public function getTitle() {
    return $this->title;
  }
  
  public function setTitle($value) {
    $this->title = $value;
  }  
  
  public function getIdPath() {
    return $this->id_path;
  }
  
  public function setIdPath($value) {
    $this->id_path = $value;
  }
  
  public function getIdPathTitle() {
    return $this->idPathTitle;
  }
  
  public function setIdPathTitle($value) {
    $this->idPathTitle = $value;
  }  
  
  public function __toString() {
    return sprintf("%s %s %s %s", $this->getId(), $this->getTitle(), $this->getIdPath(), $this->getIdPathTitle());
  }
}

