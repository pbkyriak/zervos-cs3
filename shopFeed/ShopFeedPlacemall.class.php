<?php

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

/**
 * Description of ShopFeedPlacemall
 * Created on 24-8-2011
 * Updated on 2-6-2015
 * @author Andreas <an@weblive.gr>
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedPlacemall extends ShopFeedBase {
  
  public function export() {
    $excluded_categories=new ExcludedCategories();
    $this->openOutput();
    //LOG
    $fp = fopen('./var/placemall.txt', 'a');
    fwrite($fp, date("H:i:s m/d/Y",time())."\n");
    if( $result = db_get_array("SELECT * FROM ?:shopFeedData ORDER BY category_id, name") ) {
      $this->dbRowCount = count($result);
      foreach ($result as $row){
        fn_my_changes_update_process($this->process_key);
      	if( substr($row['product_code'], 0,2)=='14' )
      		continue;
      	if( trim($row['name'])=='' )
      		continue;
        if(((trim(strip_tags($row['description']))=='' 
          || trim(strip_tags($row['description']))==trim($row['name']) 
          || strlen(strip_tags($row['description']))<=150) && $row['supplier_id']!=10) 
          || is_numeric(trim(strip_tags($row['description'])))){
					continue;    
        }
        if($excluded_categories->isCategoryExcluded($row['category_id_path']))          
          continue;
        //[imatz] Ekseresi tablet apo difox
        if($row['supplier_id']==10&&$row['category_id']==172)
          continue;

        $this->writeln("<product>");
        $this->writeln( sprintf("<id>%s</id>", $row['uniqueId'] ));
	    //katopin apaithshs skroutz prostiuetai 'EU' se ola ta kinhta
	    //dld gia suppliers Telepart(11),Difox(10)  kai Mobileshop(14)  HTC(6), Samsung(9), Apple(18)		
	    $name= trim($row['name']);        
	    if ( 11==$row['supplier_id'] || 10==$row['supplier_id'] || ( 14==$row['supplier_id'] && ( 6==$row['category_id'] || 9==$row['category_id'] || 18==$row['category_id'])) ) $name.=' EU';
	    $this->writeln( sprintf("<name><![CDATA[%s]]></name>", $name ));
        $this->writeln( sprintf("<link><![CDATA[%s]]></link>", $row['link'] ));
        $this->writeln( sprintf("<image><![CDATA[%s]]></image>", $row['image'] ));
        $this->writeln( sprintf("<sku><![CDATA[%s]]></sku>", $row['product_code'] ));
        $this->writeln( sprintf("<mpu><![CDATA[%s]]></mpu>", $row['ean'] ));
        $this->writeln( sprintf("<category id=\"%s\"><![CDATA[%s]]></category>", $row['category_id'], $row['category'] ));
        $this->writeln( sprintf("<price>%s</price>", $row['price'] ));
		//$this->writeln( sprintf("<description><![CDATA[%s]]></description>", $row['name'] ));
	    $this->writeln( sprintf("<description><![CDATA[%s]]></description>", $this->validateStringData($row['description']) ));
        $this->writeln( sprintf("<instock>%s</instock>", $row['instock'] ));
        $this->writeln( sprintf("<availability>%s</availability>", $row['availability'] ));
		$this->writeln( sprintf("<amount>%s</amount>", $row['amount'] ));
        $this->writeln("</product>");
        $this->feedRowCount++;        
      }
    }
    fwrite($fp, "Number of added products: ".$this->feedRowCount."\n\n");
    fclose($fp);
    $this->closeOutout();
  }
}