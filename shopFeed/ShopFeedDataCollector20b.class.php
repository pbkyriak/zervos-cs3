<?php

require_once(dirname(__FILE__) . '/ShopFeedCategory.class.php');

class ShopFeedDataCollector20b extends ShopFeedDataCollector {

  protected function getCategoryProductsQuery() {
    return "
      select p.product_id, pc.category_id, pd.product, pd.full_description, p.product_code, p.amount, p.avail_since, pp.price
      from ?:products as p
      left join ?:products_categories as pc 
        on (p.product_id=pc.product_id and pc.link_type='M')
      left join ?:product_descriptions as pd
        on (p.product_id=pd.product_id and pd.lang_code='%s')
      left join ?:product_prices as pp on (pp.product_id=p.product_id AND lower_limit=1 AND usergroup_id=%s)
      where p.status='A' and pc.category_id=%s      
      ";    
  }

}