<?php

class ShopFeedDataCollector21mv extends ShopFeedDataCollector {

  protected $companyId;
  
  //public function setup($langCode, $userGroup, $db_conn, $companyId, $excludeRootCategories = array()) {
  public function setup($langCode, $userGroup, $db_conn){
    parent::setup($langCode, $userGroup, $db_conn, $excludeRootCategories);
    //$this->companyId=$companyId;
    die(1);
  }
  
  protected function getCategoryProductsQuery() {
    $q= "
      select p.product_id, pc.category_id, pd.product, pd.full_description, p.product_code, p.amount, p.avail_since, p.usergroup_ids, pp.price
      from ?:products as p
      left join ?:products_categories as pc 
        on (p.product_id=pc.product_id and pc.link_type='M')
      left join ?:product_descriptions as pd
        on (p.product_id=pd.product_id and pd.lang_code='%s')
      left join ?:product_prices as pp on (pp.product_id=p.product_id AND lower_limit=1 AND usergroup_id=%s)
      where p.status='A' and pc.category_id=%s and p.company_id=".$this->companyId;   
    //echo $q.'<br />';
    return $q;
  }
  
  public function getCompaniesForFeed() {
    return db_get_fields("SELECT company_id FROM ?:companies WHERE export_xml_feed='Y'");
  }
}