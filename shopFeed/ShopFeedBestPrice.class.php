<?php

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

/**
 * Description of ShopFeedBestPrice
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedBestPrice extends ShopFeedBase {
  
  public function export() {
    $this->createdTag = 'date';
    $this->openOutput();
    $query = str_replace('?:', TABLE_PREFIX, "SELECT * FROM ?:shopFeedData ORDER BY category_id, name");
    if( ($result = $this->dbConn->query($query)) ) {
      $this->dbRowCount = $this->dbConn->affected_rows;
      while ($row = $result->fetch_assoc()) {
       /* if(trim(strip_tags($row['description']))=='' || trim(strip_tags($row['description']))==trim($row['name']) ){
          continue;
        }
		
        fn_my_changes_update_process($this->process_key);
        */
		$this->writeln("<product>");
        $this->writeln( sprintf("<productId>%s</productId>", $row['uniqueId'] ));
        $this->writeln( sprintf("<title><![CDATA[%s (έως 36 δόσεις)]]></title>", $row['name'] ));
        $this->writeln( sprintf("<url><![CDATA[%s]]></url>", $row['link'] ));
        $this->writeln( sprintf("<image><![CDATA[%s]]></image>", $row['image'] ));
        $this->writeln( sprintf("<sku><![CDATA[%s]]></sku>", $row['product_code'] ));
		$this->writeln( sprintf("<mpu><![CDATA[%s]]></mpu>", $row['ean'] ));
		$this->writeln( sprintf("<manufacturer><![CDATA[%s]]></manufacturer>", $row['manufacturer'] ));
        $this->writeln( sprintf("<category_id>%s</category_id>", $row['category_id'] ));
        $this->writeln( sprintf("<category_name><![CDATA[%s]]></category_name>", $row['category'] ));
        $this->writeln( sprintf("<price>%s</price>", $row['price'] ));
        $this->writeln( sprintf("<description><![CDATA[%s]]></description>", $this->validateStringData($row['description']) ));
        //$this->writeln( sprintf("<stock><![CDATA[%s]]></stock>", 1 ));
		    $this->writeln( sprintf("<instock>%s</instock>", $row['instock'] ));
		    $this->writeln( sprintf("<availability>%s</availability>", $row['availability'] ));
        $this->writeln("</product>");
        $this->feedRowCount++;
      }
      //echo("BestPrice Total added products: ".$this->feedRowCount."<br /><br />");
    }
    $this->closeOutout();
  }
}

