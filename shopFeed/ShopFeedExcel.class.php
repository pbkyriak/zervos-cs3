<?php
//
// Author: Ioannis Matziaris [imatz]
// Email: imatzgr@gmail.com
// Date: 18/1/2015
// Details: Export to Excel Format
//

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

class ShopFeedExcel extends ShopFeedBase {

	public function export() {
		require_once(DIR_ROOT . "/my_lib/PHPExcel/PHPExcel.php");
		require_once(DIR_ROOT . "/my_lib/PHPExcel/PHPExcel/Writer/Excel2007.php");
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("Vnp.gr");

		$objPHPExcel->setActiveSheetIndex(0);

		//Define Headers
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Product');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Category');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'SKU');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Description');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Price');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Amount');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Weight');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Image');

		$query = str_replace('?:', TABLE_PREFIX, "SELECT * FROM ?:shopFeedData ORDER BY category_id, name");
		if( ($result = $this->dbConn->query($query)) ) {
			$this->dbRowCount = $this->dbConn->affected_rows;
			$x=2;
			while ($row = $result->fetch_assoc()) {
				$name= trim($row['name']); 
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$x, $name);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$x, $row['category']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$x, $row['product_code']);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$x, $row['description']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$x, $row['price']);
				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$x, $row['amount']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$x, $row['weight']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H'.$x, $row['image']);
				$x++;
				fn_my_changes_update_process($this->process_key);
			}
		}
		$objPHPExcel->getActiveSheet()->setTitle('Products');

		// Save Excel 2007 file
		//echo date('H:i:s') . " Write to Excel2007 format\n";
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($this->feedFile);
	}
}