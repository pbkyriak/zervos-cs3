<?php

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

class ShopFeedShopedia extends ShopFeedBase {

	public function export() {
		$allowed_suppliers=array(10);//Difox, Telepart

		$this->openOutput();
    	$query = str_replace('?:', TABLE_PREFIX, "SELECT * FROM ?:shopFeedData ORDER BY category_id, name");
    	if( ($result = $this->dbConn->query($query)) ) {
      		$this->dbRowCount = $this->dbConn->affected_rows;
      		while ($row = $result->fetch_assoc()) {
      			if( trim($row['name'])=='' )
      				continue;
				if((trim(strip_tags($row['description']))=='' || trim(strip_tags($row['description']))==trim($row['name'])) && $row['supplier_id']!=10 )
					continue;   
				if(!in_array($row['supplier_id'],$allowed_suppliers))
      				continue;
				fn_my_changes_update_process($this->process_key); 
				$this->writeln("<product>");
				$this->writeln( sprintf("<id>%s</id>", $row['product_code'] ));
				$this->writeln( sprintf("<name><![CDATA[%s]]></name>", $row['name'] ));
				$this->writeln( sprintf("<link><![CDATA[%s]]></link>", $row['link'] ));
				$this->writeln( sprintf("<image><![CDATA[%s]]></image>", $row['image'] ));
				$this->writeln( sprintf("<category><![CDATA[%s]]></category>", $row['category'] ));
				$this->writeln( sprintf("<price>%s</price>", $row['price'] ));
				$availability="no";
     			if($row['amount']>0)
     				$availability="yes";
     			$this->writeln( sprintf("<availability><![CDATA[%s]]></availability>", $availability ));
     			$query="SELECT pfvd.variant FROM ?:product_feature_variant_descriptions AS pfvd 
	     			LEFT JOIN ?:product_feature_variants AS pfvr ON pfvr.variant_id=pfvd.variant_id 
	     			LEFT JOIN ?:product_features_values AS pfvl ON pfvl.variant_id=pfvr.variant_id 
	     			LEFT JOIN ?:product_features AS pf ON pf.feature_id=pfvl.feature_id WHERE 
	     			pfvd.lang_code='el' AND pf.is_brand=1 AND pfvl.product_id=".$row['uniqueId'];
     			$query = str_replace('?:', TABLE_PREFIX, $query);
     			$brand_result =$this->dbConn->query($query)->fetch_assoc();
     			$brand=$brand_result["variant"];
     			$this->writeln( sprintf("<manufacturer><![CDATA[%s]]></manufacturer>", $brand ));
     			$this->writeln( sprintf("<mpu><![CDATA[%s]]></mpu>", $row['ean'] ));//Eina to product codeB
     			$ean=""; //Den iparxei os pedio sto homelik
     			$this->writeln( sprintf("<ean><![CDATA[%s]]></ean>", $ean ));
     			$this->writeln( sprintf("<weight><![CDATA[%s]]></weight>", $row['weight'] ));
				$this->writeln("</product>");
        		$this->feedRowCount++;
      		}
      	}
      	$this->closeOutout();
	}
}