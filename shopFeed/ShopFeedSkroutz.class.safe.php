<?php

require_once(dirname(__FILE__) . '/ShopFeedBase.class.php');

/**
 * Description of ShopFeedSkroutz
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedSkroutz extends ShopFeedBase {
  
  public function export() {
    
    $this->openOutput();
    $query = str_replace('?:', TABLE_PREFIX, "SELECT * FROM ?:shopFeedData ORDER BY category_id, name");
    if( ($result = $this->dbConn->query($query)) ) {
      $this->dbRowCount = $this->dbConn->affected_rows;
      while ($row = $result->fetch_assoc()) {
        $this->writeln("<product>");
        $this->writeln( sprintf("<id>%s</id>", $row['uniqueId'] ));
        $this->writeln( sprintf("<name><![CDATA[%s]]></name>", $row['name'] ));
        $this->writeln( sprintf("<link><![CDATA[%s]]></link>", $row['link'] ));
        $this->writeln( sprintf("<image><![CDATA[%s]]></image>", $row['image'] ));
        $this->writeln( sprintf("<sku><![CDATA[%s]]></sku>", $row['product_code'] ));
        $this->writeln( sprintf("<category id=\"%s\"><![CDATA[%s]]></category>", $row['category_id'], $row['category'] ));
        $this->writeln( sprintf("<price>%s</price>", $row['price'] ));
        $this->writeln( sprintf("<description><![CDATA[%s]]></description>", $this->validateStringData($row['description']) ));
        $this->writeln( sprintf("<instock>%s</instock>", $row['instock'] ));
        $this->writeln( sprintf("<availability>%s</availability>", $row['availability'] ));
        $this->writeln("</product>");
        $this->feedRowCount++;
      }
    }
    $this->closeOutout();
  }
}

