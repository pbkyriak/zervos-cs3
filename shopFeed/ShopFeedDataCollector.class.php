<?php

require_once(dirname(__FILE__) . '/ShopFeedCategory.class.php');

class ShopFeedDataCollector {

  protected $langCode;
  protected $userGroup;
  protected $categories;
  protected $currentLocation;
  protected $db_conn;
  //2-10-2014 [imatz]: Mpeike se sxolio oste i epilogi me vasi tis excluded cats na ginete tin ora tis dimiourgias tou csv
  //protected $excludeCategories;
  private $nonActiveCategories;
  
  private $productCount=0, $categoriesCount=0;
  private $process_key;

  public function __construct() {
    
  }

  public function setProcessKey($key){
    $this->process_key=$key;
  }

  public function getProcessKey(){
    return($this->process_key);
  }

  private function getNonActiveCategories() {
    $this->nonActiveCategories = db_get_fields("SELECT category_id FROM cscart_categories WHERE STATUS != 'A'");
  }
  
  //2-10-2014 [imatz]: Mpeike se sxolio oste i epilogi me vasi tis excluded cats na ginete tin ora tis dimiourgias tou csv
  public function setup($langCode, $userGroup, $db_conn){ //, $excludeRootCategories = array()) {
    $this->langCode = $langCode;
    $this->userGroup = $userGroup;
    $this->db_conn = $db_conn;
    //2-10-2014 [imatz]: Mpeike se sxolio oste i epilogi me vasi tis excluded cats na ginete tin ora tis dimiourgias tou csv
    //$this->excludeCategories = $excludeRootCategories;
    $categories = array();
    $this->currentLocation = 'http://' . Registry::get('config.http_host');
    $this->getNonActiveCategories();
  }

  public function prepare() {
    $this->createDBTable();
    $this->fetchCategories();
    $this->updateProducts();
  }

  public function cleanUp() {
    db_query("drop table if exists ?:shopFeedData");
  }

  protected function createDBTable() {
    db_query("drop table if exists ?:shopFeedData");
    //2-10-2014 [imatz]: Prosthiki pediou category_id_path gia exclude categories kata tin dimiourgia twn csv
    db_query("
      CREATE TABLE ?:shopFeedData (
      `id` varchar(255) NOT NULL,
      `uniqueId`  varchar(255) NOT NULL ,
      `name`  mediumtext NOT NULL ,
      `description`  mediumtext NOT NULL ,
      `product_code` varchar(255) NOT NULL,
      `link`  mediumtext NOT NULL ,
      `image`  mediumtext NOT NULL ,
      `category_id` int not null default 0,
      `category_id_path` varchar(255) NOT NULL,
      `category`  mediumtext NOT NULL ,
      `price`  decimal(12,2) NOT NULL ,
      `instock`  char(1) NOT NULL ,
      `availability`  varchar(60) NOT NULL ,
      `amount` int not null default 0,
      `ean`  varchar(60) NOT NULL ,
      `supplier_id` int not null default 0,
      `weight` decimal(12,2) NOT NULL DEFAULT '0.00',
	  `manufacturer` varchar(255) NOT NULL,
      PRIMARY KEY (`uniqueId`) 
      ) DEFAULT CHARSET=utf8 ENGINE=InnoDB 
      ");
  }

  protected function fetchCategories() {
    $rows = db_get_array("
        select a.category_id, a.id_path, b.category
        from ?:categories as a
        left join ?:category_descriptions as b on a.category_id=b.category_id
        where b.lang_code=?s AND status='A'
        ORDER BY a.category_id asc", $this->langCode);
    $this->categories = array();
    foreach ($rows as $row) {
      //2-10-2014 [imatz]: Mpeike se sxolio oste i epilogi me vasi tis excluded cats na ginete tin ora tis dimiourgias tou csv
      //if (!$this->isCategoryExcluded($row['id_path']) && !$this->isInNonActivePath($row['id_path'])) {
        $this->categories[$row['category_id']] = new ShopFeedCategory($row['category_id'], $row['category'], $row['id_path']);
      //}
      fn_my_changes_update_process($this->process_key);
    }
    foreach ($this->categories as $cid => $category) {
      $this->categories[$cid]->setIdPathTitle($this->makeCategoryPathTitles($category));
      fn_my_changes_update_process($this->process_key);
    }
  }

  protected function makeCategoryPathTitles(ShopFeedCategory $category) {
    $ids = explode('/', $category->getIdPath());
    $s = array();
    foreach ($ids as $id) {
      if (isset($this->categories[$id]))
        $s[] = $this->categories[$id]->getTitle();
      fn_my_changes_update_process($this->process_key);
    }
    return implode(' > ', $s);
  }
  
  protected function getManufacturerFromFeatures($productId) {
		$manuf = db_get_field("select                   
			if(pf.feature_type='T', 
				pfv.value, 
					if(pf.feature_type in ('S','E'),
					pfvd.variant,
						if( pf.feature_type='M',
						group_concat(pfvd.variant),
						concat(pfd.prefix,' ',pfv.value_int,' ',pfd.suffix)
						)
					)
			) as v
			from cscart_product_features_values as pfv
			left join cscart_product_features as pf on (pf.feature_id=pfv.feature_id)
			left join cscart_product_feature_variant_descriptions as pfvd on (pfvd.variant_id=pfv.variant_id and pfvd.lang_code=pfv.lang_code)
			left join cscart_product_features_descriptions as pfd on (pfd.feature_id=pf.feature_id and pfd.lang_code=pfv.lang_code)
			where  pf.status='A' and pfv.lang_code=?s and pf.feature_id =18 and pfv.product_id=?i", $this->langCode, $productId); //feature_id =18 kataskeyasths
		return $manuf;	 
  }

  //2-10-2014 [imatz]: Mpeike se sxolio oste i epilogi me vasi tis excluded cats na ginete tin ora tis dimiourgias tou csv
  /*
  protected function isCategoryExcluded($id_path) {
    $idPath = explode('/', $id_path);
    foreach ($this->excludeCategories as $exId)
      fn_my_changes_update_process($this->process_key);
      if (in_array($exId, $idPath))
        return true;
    return false;
  }
  */
  private function isInNonActivePath($id_path) {
    $idPath = explode('/', $id_path);
    foreach ($this->nonActiveCategories as $exId)
      fn_my_changes_update_process($this->process_key);
      if (in_array($exId, $idPath))
        return true;
    return false;    
  }
  
  protected function updateProducts() {
    foreach ($this->categories as $cid => $category) {
      $this->getCategoryProducts($category);
      fn_my_changes_update_process($this->process_key);
    }
  }

  protected function getCategoryProductsQuery() {
    return "";
  }

  protected function getCategoryProducts(ShopFeedCategory $category) {
    $query = $this->getCategoryProductsQuery();
    $query = str_replace('?:', TABLE_PREFIX, $query);
    $query = sprintf($query, $this->langCode, $this->userGroup, $category->getId());
    //echo $query.'<br />';
    $result = $this->db_conn->query($query);
    $qryCounter = 0;
    $qryBatch = array();
    $baseLink = Registry::get('config.current_location') . '/' .
            Registry::get('config.customer_index');
    if ($result) {
      while ($row = $result->fetch_assoc()) {
        $query = "INSERT INTO ?:shopFeedData 
        (uniqueId, name, description, link, image, category_id, category, price, instock, availability, 
          product_code, amount, ean, supplier_id, category_id_path, weight, manufacturer) 
        VALUES ( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', %s, '%s', %s, '%s')";
        $query2 = str_replace('?:', TABLE_PREFIX, $query);
        if (isset($this->categories[$row['category_id']])) {
          $catStr = $this->categories[$row['category_id']]->getIdPathTitle();
          $link = $baseLink . "?dispatch=products.view&product_id=" . $row['product_id'];
		  $manufacturer = $this->getManufacturerFromFeatures($row['product_id']);  // andreas 01/12/2015
          $image = $this->getImageLink($row['product_id']);
          fn_my_changes_update_process($this->process_key);
          if( $row['skroutz_availability']!=9 && $image ) {
	          $inStock = ($row['skroutz_availability'] == 0 ? 'Y' : 'N' );
	          $availability = fn_get_lang_var('avail_option'.$row['skroutz_availability']);
            //Proionta pou ginontai update apo icecat mporei na min exoun perigrafei alla tha exoun
            //sigoura perigrafi apo xaraktiristika
            $full_description=$this->strip_javascript($row['full_description']);
            if(empty($full_description)&&!empty($row['full_description2'])){
              $full_description=$row['full_description2'];
            }
	          $query2 = sprintf(
	                  $query2, $row['product_id'], 
	                  $this->db_conn->real_escape_string($row['product']), 
	                  $this->db_conn->real_escape_string($full_description), 
	                  $this->db_conn->real_escape_string(str_replace('&amp;','&',fn_url($link,'C'))),
	                  $this->db_conn->real_escape_string($image), 
	                  $row['category_id'], 
	                  $this->db_conn->real_escape_string($catStr), 
	                  str_replace('.', '.', $row['price']), 
	                  $inStock, $availability, 
	                  $this->db_conn->real_escape_string($row['product_code']), $row['amount'],$row['product_codeB'],$row['my_supplier_id'],
                    $row['id_path'],$row['weight'],
					$this->db_conn->real_escape_string($manufacturer)
	          );
	
	          //echo $query2; //die(1);
	          $qryBatch[] = $query2;
	          $qryCounter++;
        	}
        }
        if ($qryCounter == 50) {
          $this->db_conn->multi_query(implode(';', $qryBatch));
          while ($this->db_conn->next_result());
          if ($this->db_conn->errno) {
            echo "Stopped while retrieving result : " . $this->db_conn->error;
          }
          $qryBatch = array();
          $qryCounter = 0;
        }
      }
      if (count($qryBatch)) {  // δεν έφτασαν τα 50 για να εκτελεστούν στο loop, οπότε τα τρέχουμε τώρα
        $this->db_conn->multi_query(implode(';', $qryBatch));
        while ($this->db_conn->next_result());
      }
      $result->free();
    }

    //db_query($query, $row['product_id'], $row['product'], $row['product'], $link, $image, $row['product_id'], $catStr, $price, $inStock, $availability);
  }
/*
  protected function getImageLink($productId, $imageType) {
    $skin_name = Registry::get('settings.skin_name_' . AREA_NAME);
    $dataType = 'detailed';
    //$imageType = 'detailed';

    $image_pair = fn_get_image_pairs($productId, 'product', 'M');
    $ifn = $this->findValidImagePath($image_pair, $dataType, false, $this->langCode);
    if ($ifn) {
      return 'http://' . Registry::get('config.http_host') . $ifn;
    } else {
      if ($imageType != 'detailed') {
        return $this->getImageLink($productId, 'detailed2');
      }
      else
        return 'http://' . Registry::get('config.http_host') . Registry::get('config.no_image_path');
    }
  }

  protected function findValidImagePath($image_pair, $object_type, $get_flash, $lang_code) {
    print_R($image_pair); die(1);
    if (isset($image_pair[$object_type]['absolute_path']) && is_file($image_pair[$object_type]['absolute_path'])) {
      if (!$get_flash && isset($image_pair[$object_type]['is_flash']) && $image_pair[$object_type]['is_flash']) {
        // We don't need Flash at all -- no need to crawl images any more.
        return false;
      } else {
        return $image_pair[$object_type]['image_path'];
      }
    }

    // Try to get the product's image.
    if (!empty($image_pair['image_id'])) {
      $image = fn_get_image($image_pair['image_id'], $object_type, 0, $lang_code);
      if (isset($image['absolute_path']) && is_file($image['absolute_path'])) {
        if (!$get_flash && isset($image['is_flash']) && $image['is_flash']) {
          return false;
        }

        return $image['image_path'];
      }
    }

    // If everything above failed, try to generate the thumbnail.
    if (!empty($image_pair['detailed_id'])) {
      $image = fn_get_image($image_pair['detailed_id'], 'detailed', 0, $lang_code);

      if (isset($image['absolute_path']) && is_file($image['absolute_path'])) {
        if (isset($image['is_flash']) && $image['is_flash']) {
          if ($get_flash) {
            // No need to call fn_generate_thumbnail()
            return $image['image_path'];
          } else {
            return false;
          }
        }

        $image = fn_generate_thumbnail($image['image_path'], Registry::get('settings.Thumbnails.product_details_thumbnail_width'), Registry::get('settings.Thumbnails.product_details_thumbnail_height'), false);
        if (!empty($image)) {
          return $image;
        }
      }
    }
    return false;
  }
*/
  protected function getImageLink($productId) {
    $skin_name = Registry::get('settings.skin_name_' . AREA_NAME);
    $dataType = 'detailed';

    $image_pair = fn_get_image_pairs($productId, 'product', 'M');
    $ifn = $this->findValidImagePath($image_pair, $dataType, $this->langCode);
    if ($ifn) {
      return 'http://' . Registry::get('config.http_host') . $ifn;
    } else {
    	return false;
      //return 'http://' . Registry::get('config.http_host') .Registry::get('config.no_image_path');
    }
  }

  protected function findValidImagePath($image_pair, $object_type, $lang_code) {
 
    if( isset($image_pair[$object_type]['image_path'])  && !isset($image_pair[$object_type]['absolute_path']) ) {
      $image_pair[$object_type]['absolute_path']= DIR_ROOT.str_replace(Registry::get('config.images_path'),'/images/',$image_pair[$object_type]['image_path']);
    }
    if (isset($image_pair[$object_type]['absolute_path']) && is_file($image_pair[$object_type]['absolute_path'])) {
      if( isset($image_pair[$object_type]['is_flash']) && $image_pair[$object_type]['is_flash']) 
        return false;
      return $image_pair[$object_type]['image_path'];
    }

    // Try to get the product's image.
    if (!empty($image_pair['image_id'])) {
      $image = fn_get_image($image_pair['image_id'], 'product', 0, $lang_code);
      if( !isset($image['absolute_path']))
        $image['absolute_path']= DIR_ROOT.str_replace(Registry::get('config.images_path'),'/images/',$image['image_path']);
      if (isset($image['absolute_path']) && is_file($image['absolute_path'])) {
        if(isset($image['is_flash']) && $image['is_flash']) 
          return false;
        return $image['image_path'];
      }
    }

    // If everything above failed, try to generate the thumbnail.
    if (!empty($image_pair['detailed_id'])) {
      $image = fn_get_image($image_pair['detailed_id'], 'detailed', 0, $lang_code);

      if (isset($image['absolute_path']) && is_file($image['absolute_path'])) {
        if (isset($image['is_flash']) && $image['is_flash']) {
            return false;
        }
        $image = fn_generate_thumbnail($image['image_path'], Registry::get('settings.Thumbnails.product_details_thumbnail_width'), Registry::get('settings.Thumbnails.product_details_thumbnail_height'), false);
        if (!empty($image)) {
          return $image;
        }
      }
    }
    return false;
  }
  
  protected function strip_javascript($filter) {

    // realign javascript href to onclick
    $search = array();
    $search[] = '@<script[^>]*?>.*?</script>@si'; // Strip out javascript
    /*
    $search[] = '@<[\/\!]*?[^<>]*?>@si'; 					// Strip out HTML tags
    $search[] = '@<style[^>]*?>.*?</style>@siU'; 	// Strip style tags properly
    */
    $search[] = '@<![\s\S]*?--[ \t\n\r]*>@';      // Strip multi-line comments including CDATA
    $filter = preg_replace($search, '', $filter);

    return $filter;
  }
  

  public static function getExcludedCategories() {
    $excluded = db_get_fields("SELECT category_id FROM ?:categories WHERE exclude_from_shopfeed='Y'");
    return $excluded;
  }
}

//2-10-2014 [imatz]: Dimiourgia tis class gia xrisi twn excluded katigoriwn se olwn twn feeder
class ExcludedCategories {

  protected $excludeCategories; 

  public function __construct() {
    $this->excludeCategories=$this->getExcludedCategories();
  }

  protected function getExcludedCategories() {
    $excluded = db_get_fields("SELECT category_id FROM ?:categories WHERE exclude_from_shopfeed='Y'");
    return $excluded;
  }

  public function isCategoryExcluded($id_path) {
    $idPath = explode('/', $id_path);
    foreach ($this->excludeCategories as $exId) {
      if (in_array($exId, $idPath)) {
        return true;
      }
    }
    
    return false;
  }
}