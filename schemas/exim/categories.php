<?php

//
// alt-team.com $ hex
//

if ( !defined('AREA') ) { die('Access denied'); }

$schema = array (
	'section' => 'products',
	'name' => fn_get_lang_var('categories'),
	'pattern_id' => 'categories',
	'key' => array('category_id'),
	'table' => 'categories',
	'references' => array (
		'category_descriptions' => array (
			'reference_fields' => array ('category_id' => '#key', 'lang_code' => '@lang_code'),
			'join_type' => 'LEFT'
		),
		'seo_names' => array (
			'reference_fields' => array('object_id' => '#key', 'type' => 'c', 'lang_code' => '@lang_code'),
			'join_type' => 'LEFT'
		),
		'images_links' => array (
			'reference_fields' => array('object_id' => '#key', 'object_type' => 'category', 'type' => 'M'),
			'join_type' => 'LEFT'
		),
	),
	'options' => array (
		'lang_code' => array (
			'title' => 'language',
			'type' => 'languages',
		),
		'images_path' => array (
			'title' => 'images_directory',
			'description' => 'text_images_directory',
			'type' => 'input',
			'default_value' => DIR_IMAGES . 'backup'
		),
	),
	'export_fields' => array (
		'category_id' => array (
			'alt_key' => true,
			'required' => true,
		),
		'Name' => array (
			'required' => true,
			'table' => 'category_descriptions',
			'db_field' => 'category'
		),
		'Description' => array (
			'table' => 'category_descriptions',
			'db_field' => 'description'
		),
		'Parent ID' => array (
			'db_field' => 'parent_id',
		),
		'Location' => array (
			'db_field' => 'id_path',
		),
		'Status' => array (
			'db_field' => 'status',
		),
		'Thumbnail' => array (
			'process_get' => array ('fn_export_image', '#this', 'category', '@images_path'),
			'table' => 'images_links',
			'db_field' => 'image_id',
			'use_put_from' => '%Detailed image%'
		),
		'Detailed image' => array (
			'process_get' => array ('fn_export_image', '#this', 'category', '@images_path'),
			'db_field' => 'detailed_id',
			'table' => 'images_links',
			'process_put' => array('fn_import_images', '@images_path', '%Thumbnail%', '#this', 'M', '#key', 'category')
		),
		'Meta keywords' => array (
			'table' => 'category_descriptions',
			'db_field' => 'meta_keywords'
		),
		'Meta description' => array (
			'table' => 'category_descriptions',
			'db_field' => 'meta_description'
		),
		'Page title' => array (
			'table' => 'category_descriptions',
			'db_field' => 'page_title'
		),
		'SEO name' => array (
			'table' => 'seo_names',
			'db_field' => 'name'
		),
		'Position' => array (
			'db_field' => 'position',
		),
		'Supplier Category Name' => array (
			'db_field' => 'supplier_category_name',
		),		
		'Supplier ID' => array (
			'db_field' => 'supplier_import_id',
		),
	),
);

?>