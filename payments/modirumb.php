<?php

use Tygh\Registry;

if (!defined('AREA')) { die('Access denied'); }

$fp = fopen('./payments/payment_modirumb_log.txt', 'a'); //LOG
fwrite($fp, date("H:i:s m/d/Y",time())."\n"); //LOG
fwrite($fp, "modirumb\n"); //LOG
fwrite($fp, "Mode: $mode\n"); //LOG
$tmp=print_r($_REQUEST,true); //LOG
fwrite($fp, $tmp."\n"); //LOG

if (defined('PAYMENT_NOTIFICATION')) {
  $orderId = 0;
	if(!empty($_REQUEST['orderid'])) {
		$tok = explode('hl',$_REQUEST['orderid']);
		$orderId = (int)$tok[0];
	}

    if ($orderId) {
        $pp_response = array();
        $pp_response['order_status'] = 'F';
        $pp_response['reason_text'] = fn_get_lang_var('text_transaction_declined');
        /* build digest */
        $post_data = array();

        $post_data_values = array(
            'mid',
            'orderid',
            'status',
            'orderAmount',
            'currency',
            'paymentTotal',
            'riskScore',
            'payMethod',
            'txId',
            'paymentRef'
        );

        foreach ($post_data_values as $post_data_value) {
            if (isset($_REQUEST[$post_data_value])) {
                $post_data[] = $_REQUEST[$post_data_value];
            }
        }

		$digest = base64_encode(sha1(implode('', $post_data) . $processor_data['params']['shared_secret'], true));
        $postedDigest = $_REQUEST['digest'];
        $digestMatch = ( $digest==$postedDigest);
        $digestMatch = true; // !!!
        /* end build digest */

        
        if ($mode == 'success' && $digestMatch ) {
            //fn_set_notification('N', fn_get_lang_var('order'), 'Success');
            $pp_response['order_status'] = 'O';
            $pp_response['reason_text'] = fn_get_lang_var('order_id') . '-' . $_REQUEST['orderid'];

        } 
        elseif ($mode == 'failed') {
            //fn_set_notification('W', fn_get_lang_var('error'), 'Failed');
            $pp_response['order_status'] = 'D';
            $pp_response['reason_text'] = fn_get_lang_var('payment_failed');

        }
		fwrite($fp, "PPresponse Status: %s\n", print_r($pp_response,true)); //LOG
        if (fn_check_payment_script('modirumb.php', $orderId)) {
            //fn_set_notification('N', fn_get_lang_var('order'), 'Finish payment');
            fn_finish_payment($orderId, $pp_response, false);
			//fn_set_notification('N', fn_get_lang_var('order'), 'Placement routines');
            fn_order_placement_routines($orderId);
        }
    }

    else {
        fn_set_notification('E', fn_get_lang_var('error'), fn_get_lang_var('connection_error'));
        fn_redirect('checkout.checkout');
    }

} else {
    $postData = array();
	$postData['mid']=$processor_data['params']['merchantid'];
    $languages = array( 'el', 'EL' );
		if (in_array( DESCR_SL, $languages )) {
			$postData['lang'] = 'el';
		}
		else {
			$postData['lang'] = 'en';
		}
    $postData['deviceCategory']='0';
    $postData['orderid'] = sprintf("%shl%s",$order_id, rand());
    $postData['orderDesc'] = $processor_data['params']['details'] .' '. sprintf("%s - %s", 'Order', $order_id);
    $postData['orderAmount'] = str_replace('.', ',', $order_info["total"]);
    $postData['currency'] = $processor_data['params']['currency'];
    $postData['payerEmail']=$order_info['email'];
	$period = $order_info['payment_info']['payment_period'];
	$postData['trType'] = 2;
	//$period = $order_info['payment_period'];
	//var_dump($order_info);die(1);
	if (!empty($period) && $period > 1) {
		$postData['extInstallmentoffset'] = '0';
		$postData['extInstallmentperiod'] = $period;
		//
	}
    if(!empty($processor_data['params']['css_gateway']) ) {
        $postData['cssUrl'] = $processor_data['params']['css_gateway'];
    }
    $sid = session_id();
	//$postData['confirmUrl'] = fn_url("payment_notification.success?payment=modirumb&orderid=".$postData['orderid'], AREA, 'current');
	$postData['confirmUrl'] = "https://www.homelike.gr/index.php?dispatch=payment_notification.success&payment=modirumb&orderid=".$postData['orderid'];
	//$postData['cancelUrl'] = fn_url("payment_notification.failed?payment=modirumb&orderid=".$postData['orderid'], AREA, 'current');
	$postData['cancelUrl'] = "https://www.homelike.gr/index.php?dispatch=payment_notification.failed&payment=modirumb&orderid=".$postData['orderid'];
	fwrite($fp, "confirm url: ".  $postData['confirmUrl']."\n");
    $password = $processor_data['params']['sharedSecretKey'];
    $cargo = implode("", $postData);
    $postData['digest'] = base64_encode( sha1( $cargo.$password, true ) );

	fwrite($fp, "post data: %s\n", print_r($postData,true)); //LOG
	
echo <<<EOT
<body onLoad="document.process.submit();">
<form method="POST" action="{$processor_data['params']['url']}" id="process" name="process">
EOT;

foreach($postData as $hname => $hval) {
	printf('<input type="hidden" name="%s" value="%s" />', $hname, $hval);printf("\n");
}

$msg = fn_get_lang_var('text_cc_processor_connection');
$msg = str_replace('[processor]', 'Eurobank', $msg);
echo <<<EOT
	</form>
	<br>
	<div align="center">{$msg}</div>
 </body>
</html>
EOT;

    exit();
}

