<?php

if (!defined('AREA')) { die('Access denied'); }

if (defined('PAYMENT_NOTIFICATION')) {

    $orderId = 0;
	if(!empty($_REQUEST['orderid'])) {
		$tok = explode('HL',$_REQUEST['orderid']);
		$orderId = (int)$tok[0];
	}
    if ($orderId) {
        $pp_response = array();
        $pp_response['order_status'] = 'F';
        $pp_response['reason_text'] = fn_get_lang_var('text_transaction_declined');
        $digestMatch = true; // !!!
        /* end build digest */
        
        if ($mode == 'success' && $digestMatch ) {
            $pp_response['order_status'] = 'P';
            $pp_response['reason_text'] = fn_get_lang_var('order_id') . '-' . $_REQUEST['orderid'];
        } 
        elseif ($mode == 'failed') {
            $pp_response['order_status'] = 'D';
            $pp_response['reason_text'] = fn_get_lang_var('payment_failed');
        }
        if (fn_check_payment_script('modirumb.php', $orderId)) {
            fn_finish_payment($orderId, $pp_response, false);
            fn_order_placement_routines($orderId);
        }
    }
    else {
        fn_set_notification('E', fn_get_lang_var('error'), fn_get_lang_var('connection_error'));
        fn_redirect('checkout.checkout');
    }
} else {
    
    $postData = array();
    //var_dump($processor_data);die(123);
    $postData['mid']=$processor_data['params']['merchantid'];
	
    $languages = array( 'el', 'EL' );
	if (in_array( DESCR_SL, $languages )) {
		$postData['lang'] = 'el';
	}
	else {
		$postData['lang'] = 'en';
	}
    
    $postData['deviceCategory']='0';
        
    $postData['orderid'] = sprintf("%sHL%s",$order_id, rand());
    $postData['orderDesc'] = $processor_data['params']['payment_details'] .' '. sprintf("%s - %s", 'Order', $order_id);

    $postData['orderAmount'] = str_replace('.', ',', $order_info["total"]);
    $postData['currency'] = $processor_data['params']['currency'];
    
    $postData['payerEmail']=$order_info['email'];
        
	$period = $order_info['payment_info']['payment_period'];
	//$period = $order_info['payment_period'];
	//var_dump($order_info['payment_method']['installments']);
	if (!empty($period) && $period > 1) {
        $postData['extInstallmentoffset'] = '0';
		$postData['extInstallmentperiod'] = $period;
	}

    if(!empty($processor_data['params']['css']) ) {
        $postData['cssUrl'] = $processor_data['params']['css'];
    }
    $sid = session_id();
	$postData['confirmUrl'] = Registry::get('config.current_location') . "/payments/modirumb_ok.php?ref=".$postData['orderid'];
	$postData['cancelUrl'] = Registry::get('config.current_location') . "/payments/modirumb_nok.php?ref=".$postData['orderid'];
	
    $password = trim($processor_data['params']['sharedSecretKey']);
	$postData['password'] = $password;
    $cargo = implode("", $postData);
    $digest = base64_encode(sha1($cargo,true));	
	unset($postData['password']);
	$postData['digest'] = $digest;
//<body onLoad="document.process.submit();">
echo <<<EOT
<body onLoad="document.process.submit();">
<form method="POST" action="{$processor_data['params']['url']}" id="process" name="process">
EOT;
//<form method="POST" action="{$processor_data['params']['url']}" id="process" name="process">
//<form method="POST" action="http://homelike.gr/modtest.php" id="process" name="process">
foreach($postData as $hname => $hval) {
	printf('<input type="hidden" name="%s" value="%s" />', $hname, htmlspecialchars($hval));printf("\n");
}

$msg = fn_get_lang_var('text_cc_processor_connection');
$msg = str_replace('[processor]', 'Eurobank', $msg);
echo <<<EOT
	</form>
	<br>
	<div align="center">{$msg}</div>
 </body>
</html>
EOT;
exit();
}
exit;

