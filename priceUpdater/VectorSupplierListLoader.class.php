<?php

require_once dirname(__FILE__).'/BaseSupplierListLoader.class.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VectorSupplierListLoader
 * Created on 13-1-2012
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class VectorSupplierListLoader extends BaseSupplierListLoader {
  
  public function loadFile() {
    
    echo 'reading file: ' . $this->listFile;
    $products = array();
    if (($handle = fopen($this->listFile, "r")) !== FALSE) {
    	$row = 0;
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
      	if( $row>0 && count($data)>3 )
      		$products[] = $data;
      	$row++;
    	}
      if( !$products ) {
      	throw new Exception("no products");
      }
      echo 'products node found<br />';
      $row = 0;
      foreach ($products as $product) {
	        echo 'importing product '.$row++.'  sku='.$product[1];
          $category= trim($product[0]);
          $categoryCode= trim($product[0]);
          $title = trim($product[0]).' '.$product[1];
          $stock = 1;
          $price = (float)str_replace('.','',$product[5]);	// agoras
          $price2 = (float)str_replace('.','',$product[4]);	// katalogou
          $productCode = trim($product[1]);
          $productCodeB = '';
          $image = $product[6];
          printf("%s <br />",dirname(__FILE__).'/updaterFiles/vector/images/'.$image);
          if( !file_exists(dirname(__FILE__).'/updaterFiles/vector/images/'.$image) )
          	$image = IMAGES_STORAGE_WEB_DIR.'/no_image.gif';
          else
          	$image = 'http://www.vnp.gr/priceUpdater/updaterFiles/vector/images/'.$image;
          printf("%s <br />",$image);
          $description = $product[2];
          //$description = mb_convert_encoding($description, 'ISO-8859-7', 'UTF-8');
	        if ($categoryCode && $stock) {
	        	$skip = false;
	        	if( !$skip ) {
		        	$pr = new ProductRow(
	                    $this->supplierId,
	                    $categoryCode, 
	                    $productCode, 
	                    $productCodeB, 
	                    $stock, 
	                    $price, 
	                    $title, 
	                    $title, 
	                    $description, 
	                    $description, 
	                    $image,
	                    $category);
	            $pr->setCatalogPrice($price2);
	            if( $pr->getCategoryId()!=-1 ) {
	              $this->addProductRow($pr);
	              echo 'loaded in category '.$pr->getCategoryId().' <br />';
	            }
	            else
	              echo 'No match '.$categoryCode.'<br />';
	          }
	        }
	        else
	        	echo 'No categoryCode or no stock <br />';
	    }
    } else {
      throw new Exception('Bad xml file<br />');
    }
  }
}

