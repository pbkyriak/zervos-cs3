<?php 

class MobileShopSupplierListLoader extends BaseSupplierListLoader {
  private $stockLimit=3;
  public $idPrefix;

  /*
  Pedia csv (Den iparxei grami titlwn)
  0: Title (Arxizei me brand kai telionei me xroma)
  1: Stock (Morfi p.x. <5)
  2: UPC
  3: Price (diaxoristiko dekadiwk .)
  4: EAN (den exoun ola ta proionta)
  */

  /*
    30/3/2015: Aferesi twn kinitwn apo katigories
    14-mobilephones, 14-oneplus, 14-Blackberry, 14-Alcatel, 14-lenovo

  */
  public function loadFile() {

	
    $handle = fopen($this->listFile, "r");

    if ($handle) {
    	$rCnt=0;
      while( $data = fgetcsv($handle, 10000, ",") ) {
      	
        //DEBUG
        //fn_print_r($data);
        // /DEBUG

        if(empty($data[0])){
          echo("Kenos titlos<br />\n");
        }

        $titleEl = $data[0];
        $titleEl = iconv("ISO-8859-7","UTF-8",$titleEl);
        $titleEn = $titleEl;

        //Eksagogei brand kai xromatos apo titlo
        $title_parts=explode(" ", $titleEl);
        $brand=$title_parts[0];
        $color=$title_parts[count($title_parts)-1];

        $not_allowed_brands=array("LENOVO");
        if(in_array(strtoupper($brand),$not_allowed_brands)){
          echo("Mi epitrepto brand LENOVO<br />\n");
          continue;
        }

        $eanOverride=false;
        $eans=array();
        $extra=array();
        $options=array();

        /* 
          1.ta macbook sta laptop-apple
          2.ta imac kai mac mini stoys etoimoys y/h apple
          3.ta ipad se tablet
          4.ta ypoloipa kinhta
        */
		$categoryCode = '';
        if (stripos($titleEl,'MacBook')!==false){
          $categoryCode = $this->supplierId.'-MacBook';
        }
		elseif(preg_match('/(imac|mac\smini)/i',$titleEl)){
          $categoryCode = $this->supplierId.'-mac';
        }
		else{
		   continue;
		}
		if (empty($data[4])){
			$data[4]=fn_crc32($titleEl);
			$eanOverride=true;
		}          

        $stock = (int) str_replace('<','',$data[1]);            
        
        $price = (float)$data[3];

        $imageLink="";

        $productCode = $data[2];
        $productCodeB = $data[2];
	
        $descriptionEl = iconv("ISO-8859-7","UTF-8",$data[0]);
        $descriptionEn = $descriptionEl;
        if( $descriptionEl===false ) 
		$stock=0;
		$stockLimitPassed = ($stock>=$this->stockLimit);        

        if(!empty($data[4])){      
          $ean=explode('/',$data[4]);
          $eans=array_map('trim',$ean);
        }
		
		if( !empty($categoryCode) ) {
		 printf("categoryCode=%s price=%s stock=%s\n", $categoryCode, $price, $stock);
		} 
        if(!empty($categoryCode) && !empty($price) && $stockLimitPassed) {
          fn_my_changes_update_process($this->process_key);
          $pr = new ProductRow(
            $this->supplierId,
            $categoryCode,
            $productCode,
            '',
            $stock,
            $price,
            $titleEl,
            $titleEn,
            $descriptionEl,
            $descriptionEn,
            $imageLink,
            $categoryCode,
            0,
            0,
            $brand,
            $this->process_key
          );

          if($stock<=2){
            echo "Stock: $stock<br />";
            $pr->setStatus("D");

          }
          //[/imatz]
          if ($pr->getCategoryId()!=-1) {          	
          	if( $stock && $price && $stockLimitPassed) {
              $icecat_params=array(
                "use_upc_from_site"=>false,
                "ex_weight"=>false,
                "supplier_id"=>$this->supplierId,
				//"ex_title"=>false,
              );
              $product_data=$this->getDataFromIceCat($pr->getProductId(),$eans,$productCodeB,$pr,$icecat_params);
              $this->addProductRow($pr);
              echo 'loaded in category ' . $pr->getCategoryId() . (!empty($product_data)?', ICECAT':'') . "<br />\n";
          	}
		        else {
		        	echo "No stock or no price <br />\n";
		        }	            
          }
          else{
            echo 'No match ' . $categoryCode . ">\n";
          }
        }
      }
    } else {
      fn_my_changes_update_process($this->process_key,0);
      throw new Exception("Bad file<br />\n");
    }
    //DEBUG  
    //fn_my_changes_update_process($this->process_key,0);
    //exit;
    // /DEBUG
  }

}