<?php 

class SunSupplierListLoader extends BaseSupplierListLoader {

  public $idPrefix;

  public function loadFile() {
    $handle = fopen($this->listFile, "r");
    if ($handle) {
      while( $data = fgetcsv($handle, 10000, ";") ) {
        $categoryCode = $this->supplierId.'-'.$data[11];
        
        $brand = $data[8];
        $titleEl = $data[2];
        //if( is_numeric($titleEl) || in_array( $categoryCode, array('8-289','8-177','8-206','8-12','8-13906') )  )
        	$titleEl = sprintf("%s | %s", $titleEl, $brand);
        $titleEl = iconv("ISO-8859-7","UTF-8",$titleEl);
        $titleEn = iconv("ISO-8859-7","UTF-8",$titleEl);
        
        if( iconv("ISO-8859-7","UTF-8",$data[17])=='Διαθέσιμο' )
          $stock = 10;
        else
          $stock = 0;

        $stockLimitPassed = ($stock>1);
        $price = (float)$data[32];	// prev version =28
        $productCode = $data[1];
        $productCodeB = $data[6];
        //$imageLink = sprintf("http://www.oktabit.gr/images/photos/%s.jpg", $productCode);
        $imageLink = $data[27];
        printf("%s<br />", $imageLink);
        printf("category =%s stock=%s price=%s <br />", $categoryCode, $stock, $price);
      	if( !$this->checkRemoteFile($imageLink) )
      		$stock = 0;
        $descriptionEl = iconv("ISO-8859-7","UTF-8",$data[4]);
        if( $descriptionEl===false )
        	$stock=0;
        $descriptionEn = $descriptionEl;
        if ($categoryCode && $stock && $price && $stockLimitPassed ) {
          $pr = new ProductRow(
                          $this->supplierId,
                          $categoryCode,
                          $productCode,
                          $productCodeB,
                          $stock,
                          $price,
                          $titleEl,
                          $titleEn,
                          $descriptionEl,
                          $descriptionEn,
                          $imageLink,
                          $categoryCode);
          if ($pr->getCategoryId()!=-1) {
            $this->addProductRow($pr);
            echo 'loaded in category ' . $pr->getCategoryId() . ' <br />';
          }
          else
            echo 'No match ' . $categoryCode . '<br />';
        }
        else
          echo 'No categoryCode or no stock <br />';
      }
    } else {
      throw new Exception("Bad file<br />\n");
    }  
  }

}