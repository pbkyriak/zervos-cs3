<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SlxSupplier
 * Created on 28-9-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SlxSupplier {
  
  public static function create() {
    return new SlxSupplier();
  }
  
  public function findBySupplierId($id) {
    return db_get_row("SELECT * FROM slx_supplier WHERE supplier_id=". $id);
  }
  
  public function getSuppliersContactArray($type=null) {
    if( $type )
      $rows = db_get_array("SELECT * FROM slx_supplier WHERE type=$type ORDER BY supplier_id");
    else
      $rows = db_get_array("SELECT * FROM slx_supplier ORDER BY supplier_id");
    $out = array();
    foreach($rows as $row) {
      $out[$row['supplier_id']] =  array($row['person_for_contact'], $row['email_for_contact']);
    }
    return $out;
  }
  
  public function getSuppliersForStock($type=1) {
    $rows = db_get_array("SELECT * FROM slx_supplier WHERE type=$type ORDER BY supplier_id");
    $out = array();
    foreach($rows as $row) {
      $out[$row['supplier_id']] = $row;
    }
    return $out;
  }
  
  public function filterBy($field, $value) {
    $rows = db_get_array(sprintf("SELECT * FROM slx_supplier WHERE $field='%s' ORDER BY supplier_id", $value));
    $out = array();
    foreach($rows as $row) {
      $out[$row['supplier_id']] = $row;
    }
    return $out;    
  }
}

function fn_get_slxSupplier($id) {
  return SlxSupplier::create()->findBySupplierId($id);
}


function fn_get_slxSuppliers_for_select() {
  $rows = SlxSupplier::create()->getSuppliersForStock(1);
  return $rows;
}