<?php

class SupplierPricingRules_Sup extends SupplierPricingRules {
		
	public function __construct() {
		$this->rules = array();
		$this->rules['7809440'] = array();
		$this->rules['7809440'][] = array('from_price'=>100.00, 'markup'=>15.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809440'][] = array('from_price'=>0.00, 'markup'=>15.00, 'markup_type'=>'A', 'round_to'=>0);

		printf("rules constructor: %s pricing rules

", count($this->rules));
	}
	
}