<?php

/**
 * Description of Play247Updater
 * Created on 15-11-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class Play247Updater {

  public function __construct() {
    
  }

  /**
   *
   * @return Play247Updater 
   */
  public static function create() {
    return new Play247Updater();
  }

  public function run($forceAll = false) {
    $q = "SELECT product_id, play247_url FROM cscart_products WHERE play247_url!=''";
    if (!$forceAll)
      $q .= " AND updated_play247='N'";
    $products = db_get_array($q);
    $this->theJob($products);
  }

  public function updateSingle($productId, $seq = false) {
    $q = "SELECT product_id, play247_url FROM cscart_products WHERE product_id=".$productId;
    $products = db_get_array($q);
		$this->theJob($products);
    if( $seq ) {
      $q = "SELECT product_id FROM cscart_products WHERE  play247_url!='' and product_id>".$productId;
      $next = db_get_field($q);
      printf('<a href="play247_updater.php?id=%s&seq=1">Epomeno</a>', $next);
    }
  }
  
  private function theJob($products) {
  	$db = (!empty($db) && Registry::get('runtime.dbs.' . $db) ? $db : 'main');
		$db_conn = & Registry::get('runtime.dbs.' . $db);
    $oldSetting = libxml_use_internal_errors(true);
    libxml_clear_errors();
    $cnt = 1;
    foreach ($products as $product) {
      if ($product['play247_url']) {
        $descr = mysqli_real_escape_string($db_conn, $this->getDescription($product['play247_url']));
        if ($descr) {
          $q = sprintf("UPDATE cscart_product_descriptions SET full_description='%s' WHERE product_id=%s AND lang_code='EL' ", $descr, $product['product_id']);
          printf("%s. q = %s <br />",$c++, $q);
          db_query($q);
          $q2 = sprintf("UPDATE cscart_products SET updated_play247='Y', update_only_price='Y' WHERE product_id=%s ", $product['product_id']);
          printf("%s. q2 = %s <br />",$c++, $q2);
          db_query($q2);
          
        }
      }
    }
    libxml_clear_errors();
    libxml_use_internal_errors($oldSetting);
  }
  
  private function getDescription($url) {
    $out = '';
    $html = new DOMDocument();
    $html->loadHtmlFile($url);
    $xpath = new DOMXPath($html);
    $descrCell = null;
    $container = $xpath->query("//div[@class='product-info-segment']/div");
    if( $container->length ) {
    	$descrCell = $container->item(1);
    }
    if ($descrCell) {
    	$out = $this->dump_child_nodes($descrCell);
    	$out = str_replace('<font face="Arial, Helvetica, sans-serif" size="2">', '<font>', $out);
    }
      //$out = mb_convert_encoding($this->dump_child_nodes($descrCell->item(0)), 'ISO-8859-7', 'UTF-8');
    return $out;
  }

  private function dump_child_nodes($node) {
    $output = '';
    $owner_document = $node->ownerDocument;

    foreach ($node->childNodes as $el) {
    	printf("%s<br />", $el->nodeName);
    	if( $el->nodeName=='font' ) {
    		foreach ($el->childNodes as $el2) 
    			$output .= $owner_document->saveXML($el2);
    	}
    	else
      	$output .= $owner_document->saveXML($el);
    }
    return $output;
  }

  public function retreat() {
    $q = "SELECT product_id, play247_url FROM cscart_products WHERE play247_url!=''";
    $products = db_get_array($q);
    foreach ($products as $product) {
      if ($product['play247_url']) {
        $q = sprintf("UPDATE cscart_product_descriptions SET full_description=product WHERE product_id=%s AND lang_code='EL' ", $product['product_id']);
        db_query($q);
        $q2 = sprintf("UPDATE cscart_products SET updated_play247='N' WHERE product_id=%s ", $product['product_id']);
        db_query($q2);
      }
    }
  }
}

