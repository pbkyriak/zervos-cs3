<?php
require_once dirname(__FILE__).'/BasePriceMarkup.class.php';

/**
 * Description of NortonMarkup
 * Created on 7-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class NortonMarkup extends BasePriceMarkup {
  
  public function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode) {
  	return round( ($basePrice + 10) * 1.23, 2);
  	
  /*	if( substr($categoryCode , 0, 2)=='01' )	// accessories
    	return round( ($basePrice + 8) * 1.23, 2);
  	elseif( $categoryCode=='070201' )	// apple mobiles
    	return round( ($basePrice + 8) * 1.23, 2); */
    	
    if( $categoryCode=='070201' )	// apple mobiles
    return round( ($basePrice + 8) * 1.23, 2);
    	    	
    elseif( $basePrice < 20 )
    	return round( ($basePrice * 2 ) * 1.23, 2);

    else
    	return round( ($basePrice + 15 ) * 1.23, 2);

  }
  
}

