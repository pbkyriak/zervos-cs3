<?php 

class AmySupplierListLoader extends BaseSupplierListLoader {

  public $idPrefix;
    
  public function loadFile() {
  	$codes_acc = array("0AH.CAB.0001", "0AH.CAB.0003", "0AH.CAR.0004", "0AH.CDR.0025", "0AH.SPA.0004", "0AH.SPA.0006", "0AH.SPA.0011", "0AH.SPA.0024", "0AH.SPA.0027", "0AH.SPA.0029", "0AH.SPA.0032", "0AH.SPA.0038", "0AH.SPA.0040", "0AH.SPA.0041", "0AH.SPA.0050", "0AH.SPA.0053", "0AH.SPA.0059", "0AH.SPA.0063", "0AH.SPA.0064", "0AH.SPA.0065", "0AH.SPA.0078", "0AH.VAR.0007", "0AH.VAR.0008");
  	$codes_vis = array("0AH.CAR.0010", "0AH.CAR.0027", "0AH.CDR.0030", "0AH.CDR.0058");
  	$codes_tab = array("0AH.CDR.0063", "0AH.CDR.0081", "0AH.CDR.0082", "0AH.CDR.0106", "0AH.SPA.0066", "0AH.SPA.0072", "0AH.SPA.0074", "0AH.SPA.0082");
  	$codes_all = array_merge($codes_acc, $codes_vis, $codes_tab);
  	
    echo 'reading file: ' . $this->listFile;
    $products = array();
    if (($handle = fopen($this->listFile, "r")) !== FALSE) {
    	$row = 0;
      while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
      	if( $row>0 && count($data)>3 )
      		if( in_array($data[0], $codes_all) )
      			$products[] = $data;
      	$row++;
    	}
      if( !$products ) {
      	throw new Exception("no products");
      }
      echo 'products node found<br />';
      $row = 0;
      foreach ($products as $product) {
	        echo 'importing product '.$row++.'  sku='.$product[1];
          $category= in_array($product[0], $codes_acc) ? 'archos-acc' : in_array($product[0], $codes_vis) ? 'archos-vis' : in_array($product[0], $codes_tab) ? 'archos-tab' : 'archos-un';
          $categoryCode= $category;
          $title = $product[1];
          $stock = (int)$product[2];
          $stockLimitPassed = ($stock>1);
          $price2 = (float)str_replace('.','',$product[4]);	// anakyklosi
          $price = (float)str_replace('.','',$product[3]);	// xondriki
          $productCode = trim($product[0]);
          $productCodeB = '';
          $image = IMAGES_STORAGE_WEB_DIR.'/no_image.gif';
          printf("image = %s <br />",$image);
          $description = $product[2];
	        if ($categoryCode && $stock && $price && $stockLimitPassed) {
	        	$pr = new ProductRow(
                    $this->supplierId,
                    $categoryCode, 
                    $productCode, 
                    $productCodeB, 
                    $stock, 
                    $price+$price2, 
                    $title, 
                    $title, 
                    $description, 
                    $description, 
                    $image,
                    $category);
            if( $pr->getCategoryId()!=-1 ) {
              $this->addProductRow($pr);
              echo 'loaded in category '.$pr->getCategoryId().' <br />';
            }
            else
              echo 'No match '.$categoryCode.'<br />';
	        }
	        else
	        	echo 'No categoryCode or no stock or price=0 <br />';
	    }
    } else {
      throw new Exception('Bad file<br />');
    }
  }

}