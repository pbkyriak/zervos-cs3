<?php
require_once dirname(__FILE__).'/BasePriceMarkup.class.php';

class B2BWatchesMarkup extends BasePriceMarkup {
	public function getHat() {
		$hat = (float) Registry::get('settings.AutoPricing.christmass_hat');
		return $hat;
	}

	public function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode) {
  		$christmasHat = $this->getHat($categoryCode);
   		return  ($basePrice+60+$christmasHat)*1.23;
  	}

  	public function statusModifier(ProductRow $product) {
    	$newStatus = $product->getStatus();
    	return $newStatus;
  	}
}