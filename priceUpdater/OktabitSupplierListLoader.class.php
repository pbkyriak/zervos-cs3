<?php 

class OktabitSupplierListLoader extends BaseSupplierListLoader {

  public $idPrefix;
/*      
Στη διαθεσιμοτητα τα νουμερα σημαινουν
1.Διαθεσιμο
2.Οριακο (λιγα κομματια ακομη στις αποθηκες Oktabit)
3 και 4   Αναμενεται    (μηδεν διαθεσιμο αλλα το εχουμε παραγγειλει στον προμηθευτη)
5. Κατοπιν παραγγελιας (μηδεν διαθεσιμο και το παραγγελνουμε στον προμηθευτη εφοσον το παραγγειλετε σε εμας)
6. NOT AVAILABLE
*/
  public function loadFile() {
    $xml = simplexml_load_file($this->listFile);
    if ($xml) {
      echo "finding products node<br />\n";
      foreach ($xml->children() as $product) {
        $categoryCode = $this->supplierId.'-'.$this->idPrefix . (string)$product->subcategory;
        $titleEl = (string)$product->titlos;
        $titleEn = (string)$product->titlos;
        $brand = (string)$product->brand;
        if( (int)$product->availability<2 )
          $stock = 10;
        else
          $stock = 0;

        $stockLimitPassed = ($stock>1);
        $price = (float)$product->timi;
        $productCode = $product->code;
        $productCodeB = $product->part_no;
        //$imageLink = sprintf("http://www.oktabit.gr/images/photos/%s.jpg", $productCode);
        $imageLink = sprintf("%s/priceUpdater/updaterFiles/okt_images/%s.jpg",Registry::get('config.http_location'),$productCode);
        printf("%s<br />", $imageLink);
      	if( !$this->checkRemoteFile($imageLink) )
      		$stock = 0;

        if( in_array($categoryCode, array('5-oktNotebooks up to 14 in','5-oktNotebooks up to 16 in','5-oktNotebooks up to 20 in')) ){
          $categoryCode = trim(sprintf("5-oktNotebooks-%s", strtolower($brand)));
          printf("??>>>>>>>>>>>>>>>>>> category code modified = %s <br />",$categoryCode);
        }
        if( in_array($categoryCode, array('5-oktProjectors LCD')) ){
          $categoryCode = trim(sprintf("5-oktProjectors LCD-%s", strtolower($brand)));
          printf("??>>>>>>>>>>>>>>>>>> category code modified = %s <br />",$categoryCode);
        }
        if( in_array($categoryCode, array('5-oktAdvanced PC')) && strpos($titleEl, 'PACKARD')===0 ) {
        	$stock = 0;
        }
        $descriptionEl = $this->getRemoteDescr($productCode);
        if( $descriptionEl===false )
        	$stock=0;
        $descriptionEn = $descriptionEl;
        if ($categoryCode && $stock && $price && $stockLimitPassed) {
          $pr = new ProductRow(
                          $this->supplierId,
                          $categoryCode,
                          $productCode,
                          $productCodeB,
                          $stock,
                          $price,
                          $titleEl,
                          $titleEn,
                          $descriptionEl,
                          $descriptionEn,
                          $imageLink,
                          $categoryCode);
          if ($pr->getCategoryId()!=-1) {
            $this->addProductRow($pr);
            echo 'loaded in category ' . $pr->getCategoryId() . ' <br />';
          }
          else
            echo 'No match ' . $categoryCode . '<br />';
        }
        else
          echo 'No categoryCode or no stock <br />';
      }
    } else {
      throw new Exception("Bad xml file<br />\n");
    }  
  }

  private function getRemoteDescr($code) {
    $cDir = DIR_ROOT.'/priceUpdater/updaterFiles/okt_specs';
    if( !file_exists($cDir) ) {
      mkdir($cDir);
      chmod($cDir, 0777);
    }
    $fn = $cDir.'/'.$code.'.txt';
    if( file_exists($fn) ) {
      return file_get_contents($fn);
    }
    else {
      $url = sprintf("http://www.oktabit.gr/times_pelatwn/chars_xml_per_prod.asp?prod_code=%s&customercode=012279&logi=vnp", $code);
      $ch = curl_init();
      $timeout = 5;
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
      $data = curl_exec($ch);
      if( $data===false)
      	return false;
      curl_close($ch);
      $xml = simplexml_load_string($data);
      $descr = '';
      if( $xml ) {
      	$descr = '<table class="specs">';
        foreach ($xml->children() as $product) {
          $t = (string)$product->title;
          $v = (string)$product->value;
          if( trim($v) ) {
            $descr .= sprintf("<tr><th>%s</th><td>%s</td></tr>", $t,$v);
          }
        }
        $descr  .= '</table>';
        file_put_contents($fn, $descr);
      }
      
      return $descr;
    }
  }
  
  public function cacheImages() {
    $xml = simplexml_load_file($this->listFile);
    if ($xml) {
      echo "finding products node<br />\n";
      foreach ($xml->children() as $product) {
      	printf("code =%s <br />", (string)$product->code);
        $this->cacheImage((string)$product->code);
      }
    } else {
      throw new Exception("Bad xml file<br />\n");
    }  
  }
  
  public function cacheImage($code) {
    $url = sprintf("http://www.oktabit.gr/images/photos/%s.jpg", $code);
    $cDir = DIR_ROOT.'/priceUpdater/updaterFiles/okt_images';
    if( !file_exists($cDir) ) {
      mkdir($cDir);
      chmod($cDir, 0777);
    }
    $fn = sprintf("%s/%s.jpg",$cDir,$code);
    printf("local=%s url=%s <br />", $fn, $url);
    if( !file_exists($fn) ) {
      $ch = curl_init();
      $timeout = 5;
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
      $data = curl_exec($ch);
      curl_close($ch);
      file_put_contents($fn, $data);
    }
  }
}