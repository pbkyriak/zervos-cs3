<?php

require_once dirname(__FILE__) . '/BaseDataRow.class.php';
require_once dirname(__FILE__) . '/ProductRow.class.php';
require_once dirname(__FILE__) . '/CategoryRow.class.php';
require_once dirname(__FILE__) . '/SkroutzSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/PriceListFetcherHttpFile.class.php';
//require_once dirname(__FILE__) . '/AgorasetoMarkup.class.php';
require_once dirname(__FILE__) . '/SameProductArranger.class.php';
require_once dirname(__FILE__) . '/SlxSupplier.class.php';
require_once dirname(__FILE__) . '/PricingRules.class.php';
require_once dirname(__FILE__) . '/FastProductRow.class.php';
//require_once dirname(__FILE__) . '/AgorasetoFastSupplierListLoader.class.php';
//require_once dirname(__FILE__) . '/IndexCategories.class.php';
require_once dirname(__FILE__) . '/SunSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/SunMarkup.class.php';
require_once dirname(__FILE__) . '/SunFastSupplierListLoader.class.php';
//require_once dirname(__FILE__) . '/EnoSupplierListLoader.class.php'; anenenrgos 06042016
//require_once dirname(__FILE__) . '/EnoMarkup.class.php';
//require_once dirname(__FILE__) . '/EnoFastSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/DifoxSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/DifoxMarkup.class.php';
require_once dirname(__FILE__) . '/DifoxFastSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/WaveSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/WaveMarkup.class.php';
require_once dirname(__FILE__) . '/WaveFastSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/SupplierPricingRules.class.php';
require_once dirname(__FILE__) . '/CachePricingRules.class.php';
require_once dirname(__FILE__) . '/NakasSupplierListLoader.class.php';   //  andreas anenergos 17/06/2015 energos xana 29/10/2015  anenenrgos 06042016
require_once dirname(__FILE__) . '/NakasMarkup.class.php';    // andreas anenergos 17/06/2015  energos xana 29/10/2015
//[imatz]
require_once dirname(__FILE__) . '/EurostarSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/MakantEuropeSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/MakantEuropeMarkup.class.php';
require_once dirname(__FILE__) . '/Weight.class.php';
require_once dirname(__FILE__) . '/B2BWatchesSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/B2BWatchesMarkup.class.php';
require_once dirname(__FILE__) . '/PriceListFetcherB2BWatches.class.php';
require_once dirname(__FILE__) . '/LEGOSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/LEGOMarkup.class.php';
require_once dirname(__FILE__) . '/QuickMobileSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/QuickMobileMakrup.class.php';
require_once dirname(__FILE__) . '/TelePartSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/TelePartMakrup.class.php';
require_once dirname(__FILE__) . '/ThomannSupplierListLoader.class.php'; // 04/10/2015
require_once dirname(__FILE__) . '/ThomannMarkup.class.php'; // 04/10/2015
require_once dirname(__FILE__) . '/MobileShopSupplierListLoader.class.php'; // 27/11/2015
require_once dirname(__FILE__) . '/MobileShopMarkup.class.php'; // 04/10/2015
require_once dirname(__FILE__) . '/NexionSupplierListLoader.class.php'; // 01/12/2015
require_once dirname(__FILE__) . '/NexionMarkup.class.php'; // 01/12/2015
//BigBuy - 10/3/2017
require_once dirname(__FILE__) . '/BigBuyEuSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/BigBuyEuMakrup.class.php';
//Api.de 30/10/2017
require_once dirname(__FILE__) . '/ApiDeSupplierListLoader.class.php';
require_once dirname(__FILE__) . '/ApiDeMakrup.class.php';

//Apo vnp
require_once dirname(__FILE__) . '/classes/icecat.class.php';
//[/imatz]

/**
 * Description of PriceListUpdater
 * Created on 6-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PriceListUpdater {

  private $loader, $supplierId, $supplier;
  private $priceMarkuper;
  private $updatedProducts, $createdProducts;
  private $categoriesInfo;
  private $logContent;
  private $process_key;

  public function __construct(BaseSupplierListLoader $loader, BasePriceMarkup $priceMarkuper, $supplierId, $process_key="") {
    $this->loader = $loader;
    $this->priceMarkuper = $priceMarkuper;
    $this->supplierId = $supplierId;
    $this->process_key=$process_key;
    $this->supplier = SlxSupplier::create()->findBySupplierId($supplierId);
    $this->updatedProducts = array();
    $this->createdProducts = array();
    $this->categoriesInfo = array();
 //   IndexCategories::getInstance()->run();
		$ca = new CachePricingRules();
		$ca->run();
    printf("<br />\nSupplierPricingRules_%s.class.php<br /><br />\n\n", $supplierId);
    require_once dirname(__FILE__). sprintf('/SupplierPricingRules_%s.class.php', $supplierId);
  }

  /**
   * 
   */
  public function update($isStep=false) {
    printf("Starting update...<br />\n");

    if (!$this->supplier)
      die('Supplier not defined!');

    $this->loader->loadFile();
    if( !$isStep )
    	$this->prepare();
    $products = $this->loader->getProductRows();

    //echo("<pre>");
    //print_r($products);
    //echo("</pre>");
    //exit;

    if ($products)
      $this->updateProducts($products);
    if( !$isStep )
    	$this->finish();
    $this->put_in_log($this->logContent);
    echo "send report <br />\n";
    $this->mailReport();
    echo "end of job <br />\n";

    //Log updater executions
    echo "Updater execution has been logged<br />";
    $fp = fopen('/home/homelike/www/var/updater_logs/updaters_executions.log', 'a');
    fwrite($fp, date("H:i:s m/d/Y",time())."\n");
    fwrite($fp, "Updater: ".$this->supplier["title"]."\n");
    fwrite($fp, "IP: ".$_SERVER['REMOTE_ADDR']."\n");
    fwrite($fp, "Products: ".count($products)."\n");
    fclose($fp);
  }
  
  /**
   * 
   */
  public function fastUpdate() {
    printf("Starting update...<br />\n");
    if (!$this->supplier)
      die('Supplier not defined!');
    $this->loader->loadFile();
   	$this->prepare();
    $products = $this->loader->getProductRows();
    if ($products)
      $this->updateProducts($products, true);
   	$this->finish(true);
    echo "send report <br />\n";
    $this->mailReport();
    echo "end of job <br />\n";
  }
  
  public function prepare() {
    //if ($this->supplier['deactivate_products_before_update'] == 'Y')
    $this->disableSuppliersProducts();
    $this->collectSupplierProductIds();
  }
  
  public function finish($isFast=false) {
	$this->hideProductsWithoutImage();
    echo "clean up expired products <br />\n";
    $this->disableNotUpdatedProducts();
    if(!$isFast)
      $this->deleteExpiredProducts();
  }

  /**
   * 
   */
  private function updateCategories($categories) {
    $ug = $this->getUserGroups();
    printf("Categories count = %s \n", count($categories));
    foreach ($categories as $category) {
      $category->setUserGroups($ug);
      $category->updateParent();
      printf("updating cat %s with parent %s \n", $category->getTitleEl(), $category->getParentId());
      if ($category->getCategoryId() == -1)
        $category->createCategory();
      else
        $category->updateCategory();
    }
  }
   
    // orio ta 60eyro timh polhshs gia na mpei sto magazi
  	private function isRetailPriceOverLimit($product) {
		$out = false;
		$price = $product->getProductPriceArray($this->priceMarkuper, 0);
		if( $price['price']>15 ) {
			$out = true;
		}
		printf("Retail price over limit = %s\n", $out ? 'Y' : 'N');
		return $out;
	}
  /**
   * 
   */
	private function updateProducts($products, $isFast=false) {
		$ug = $this->getUserGroups();
		printf("importing %s products<br />\n", count($products));
		$this->getCategoriesInfo($products);
		foreach ($products as $product) {
			$status = $this->supplier['default_product_status'] ?
            $this->supplier['default_product_status'] :
            $this->categoriesInfo[$product->getCategoryId()]['status'];
			$product->setUserGroups($ug);
			$product->setStatus($status);
			$product->setStatus($this->priceMarkuper->statusModifier($product));
			printf("product code=%s<br />\n", $product->getProductCode()); 
			if( !$this->isRetailPriceOverLimit($product) ) {
				continue;
			}			
			if ($product->getProductId() == -1) {
				if( !$isFast ) {
					printf("creating <br />\n");
					$product->createProduct($this->priceMarkuper);
					$this->createdProducts[$product->getProductId()] = $product->toString();
				}
			} else {
				printf("updating <br />\n");
				$this->remoreFromToDisableList($product->getProductId());
				$product->updateProduct($this->priceMarkuper, $isFast);
				$this->updatedProducts[$product->getProductId()] = $product->toString();
			}
			//[imatz]
			//Prosthiki tou kataskeyasti sta fetures kai san filtro sta proionta
			$product->updateFeaturesAndFilters($product);
      
      //Allagi tis diathesimotitas stis tileoraseis otan to stock kai 5 kai perissotero
      if($product->getCategoryId()==380 && $product->getStock()>=5){
        //Allagi diathesimotitas sto katastima apo diathesima (0) se diathesima se 2-3 meres (1)
        db_query("UPDATE ?:products SET shop_availability=?i WHERE product_id=?i;",5,$product->getProductId());
        //Allagi diathesimotitas sto skroutz apo diathesima (0) se diathesima se 2-3 meres (1)
        db_query("UPDATE ?:products SET skroutz_availability=?i WHERE product_id=?i;",5,$product->getProductId());
      }
      if(!empty($this->process_key)){
        fn_my_changes_update_process($this->process_key);
      }
			//[/imatz]
		}	
    //[imatz]
    //An ginete sync me proionta tou naka
    if($this->supplierId==12){
      //Sta proionta pou einai diathesima (0) allazei tin diathesimotita tou katastimatos se diathesima se 2-3 meres (1)
      db_query("UPDATE ?:products SET shop_availability=?i WHERE my_supplier_id=?i AND shop_availability=?i;",1,12,0);
      //Sta proionta pou einai diathesima (0) allazei tin diathesimotita tou skroutz se diathesima se 2-3 meres (1)
      db_query("UPDATE ?:products SET skroutz_availability=?i WHERE my_supplier_id=?i AND skroutz_availability=?i;",1,12,0);
    }
    //[/imatz]	
		printf("END OF updateProducts <br />\n");
	}

  /**
   * 
   */
  private function getCategoriesInfo($products) {
    $categoryIds = array();
    foreach ($products as $row)
      $categoryIds[] = $row->getCategoryId();
    $categoryIds = array_unique($categoryIds);
    $rows = db_get_array("SELECT category_id, status FROM cscart_categories WHERE category_id IN (" . implode(",", $categoryIds) . ")");
    $this->categoriesInfo = array();
    foreach ($rows as $row)
      $this->categoriesInfo[$row['category_id']] = $row;
  }

  /**
   * 
   */
  private function disableSuppliersProducts() {
    /*
  	printf("disabling supplier products A<br />");
  	//define("DEBUG_QUERIES",1);
    db_query("UPDATE cscart_products SET last_disable_timestamp=?i WHERE my_supplier_id=?i and shop_availability!=8 and pricelist_updater_skip='N'", 
    				time(), $this->supplierId
    				);
    printf("disabling supplier products B<br />");
    //db_query("UPDATE cscart_products SET status='D' WHERE my_supplier_id=?s and pricelist_updater_skip='N'", $this->supplierId);
    db_query(sprintf("UPDATE cscart_products SET status='A', shop_availability=8, skroutz_availability=8	WHERE my_supplier_id=%s and pricelist_updater_skip='N'", $this->supplierId));
    printf("disabling supplier products DONE<br />");
    */
  }

	private function deleteExpiredProducts() {
		// 15days * 24hours * 60minutes * 60seconds
    // 21-2-2014 [imatz]: Egine allagi apo katopin paragkelia (4) se Mi diathesimo (8)
		$pIds = db_get_fields("SELECT product_id FROM ?:products 
			WHERE last_disable_timestamp>0 
			and last_disable_timestamp<?i 
			and shop_availability=8 and my_supplier_id!=0", 
			time()-5*24*60*60, $this->supplierId);
    $msg="Proionta mi energa toulaxiston 5 meres (".count($pIds)."): ".implode(", ", $pIds)." <br />\n";
		echo($msg);
    $this->logContent.=$msg;
    $i=0;
		if( $pIds ) {
			foreach($pIds as $pid ) {
				printf("counting with id=%s<br />",$pid);
		    $cnt = db_get_field("
		          select count(*)
		          from ?:order_details 
		          where 
		          product_id=?i", $pid);
		    $cnt = 0;
				if( $cnt==0 ) {
          $msg="De-activating product with id=".$pid."<br />\n";
		      echo($msg);
          $this->logContent.=$msg;
		      //fn_delete_product($pid);
		      db_query("UPDATE cscart_products SET status='D', pricelist_updater_skip='N' WHERE product_id=?i", $pid);
          if(!empty($this->process_key)){
            if($i==10){
              fn_my_changes_update_process($this->process_key);
              $i=0;
            }else{
              $i++;
            }
          }
				}
				else {
					//printf("Skipping De-activation product with id=%s\n",$pid);
					//db_query("UPDATE cscart_products SET status='D', pricelist_updater_skip='Y' WHERE product_id=?i", $pid);
				}
			}
		}
		$this->deleteOldExpiredProducts();
	}
	
  // deeletes products not updated last 30 days (even if in orders)
	private function deleteOldExpiredProducts() {
		// 45days * 24hours * 60minutes * 60seconds
    // 21-2-2014 [imatz]: Egine allagi apo katopin paragkelias (4) se Mi diathesimo (8)
		$pIds = db_get_fields("SELECT product_id FROM ?:products 
			WHERE last_disable_timestamp>0 
			and last_disable_timestamp<?i 
			and status!=?s and my_supplier_id!=0 AND amount<=1
			", time()-45*24*60*60,"A");
			// and CONVERT(product_code, SIGNED INTEGER)!=0 
    $msg="Proionta mi energa toulaxiston 45 meres (".count($pIds)."): ".implode(", ", $pIds)." <br />\n";
    echo($msg);
		$this->logContent.=$msg;
		if( $pIds ) {
      $i=0;
			foreach($pIds as $pid ) {
        $msg="Deleting with id=".$pid."<br />\n";
        echo($msg);
        $this->logContent.=$msg;
				fn_delete_product($pid);
				//db_query("UPDATE cscart_products SET status='D', pricelist_updater_skip='N' WHERE product_id=?i", $pid);
        if(!empty($this->process_key)){
          if($i==10){
            fn_my_changes_update_process($this->process_key);
            $i=0;
          }else{
            $i++;
          }
        }
			}
		}
	}
  private function collectSupplierProductIds() {  	
    db_query("DELETE FROM ?:sup_ids WHERE supplier_id=?i",$this->supplierId);
    db_query("INSERT INTO ?:sup_ids SELECT product_id, my_supplier_id FROM ?:products WHERE my_supplier_id=?i",  $this->supplierId);
    printf("Done. Collecting Supplier ProductIds <br />\n");
  }
  
  private function remoreFromToDisableList($productId) {
    db_query("DELETE FROM ?:sup_ids WHERE product_Id=?i",$productId);
  }
  
  private function disableNotUpdatedProducts() {
  	//Osa proionta tou supplier den exoun ginei update kai den exoun apenergopoiithei ksana
    //tote orizoume xrono apenergopoiiseis se tora
    $pids=db_get_fields("SELECT product_id FROM ?:products 
      WHERE last_disable_timestamp=0 AND product_id in (SELECT product_Id FROM ?:sup_ids WHERE supplier_id=?i)",$this->supplierId);
    $msg="Proionta pou oristike xronos apenergopoiiseis (".count($pids)."): ".implode(", ", $pids)."<br />\n";
    echo($msg);
    $this->logContent.=$msg;
    db_query("UPDATE ?:products SET last_disable_timestamp=?i 
      WHERE last_disable_timestamp=0 AND product_id in (SELECT product_Id FROM ?:sup_ids WHERE supplier_id=?i)",time(),$this->supplierId);

    // Osa proionta tou supplier den exoun ginei update tote ginontai krifa me stock 1 
    $pids=db_get_fields("SELECT product_id FROM ?:products 
      WHERE my_supplier_id=?i AND pricelist_updater_skip='N' 
      AND (status!='H' OR shop_availability!=8 OR skroutz_availability!=9 OR amount>1) 
      AND product_id IN (SELECT product_Id FROM ?:sup_ids WHERE supplier_id=?i)",$this->supplierId,$this->supplierId);
    $msg="Apokripsei proiotwn pou den eginan update (".count($pids)."): ".implode(", ", $pids)."<br />\n";
    
    $this->logContent.=$msg;
    db_query("UPDATE ?:products SET status='H', shop_availability=8, skroutz_availability=9, amount=1 
      WHERE my_supplier_id=?i AND pricelist_updater_skip='N' 
      AND status!='H' AND shop_availability!=8 AND skroutz_availability!=9 AND amount>1 
      AND product_id IN (SELECT product_Id FROM ?:sup_ids WHERE supplier_id=?i)",$this->supplierId,$this->supplierId);
	echo($msg);
    // 26-5-2014 [imatz]: Osa proionta exoun stock 1 na ginoun krifa. Afora ola ta proionta
    //if($this->supplierId!=10){
	
	// andreas οι parakato grammes se sxolio stis 18072016 to zhthse o Giannis na mhn ginontai kryfa osa exoun apothema 1
	/*
      $pids=db_get_fields("SELECT product_id FROM ?:products WHERE amount=1 AND status!='H' AND my_supplier_id=?i",$this->supplierId);
      $msg="Apokripsei proiontwn pou exoun stock 1 (".count($pids)."): ".implode(", ", $pids)."<br />\n";
      echo($msg);
      $this->logContent.=$msg;
	  db_query("UPDATE ?:products SET status='H' WHERE amount=1 AND status!='H' AND my_supplier_id=?i",$this->supplierId);
	*/ 
    // andreas end	
    //}
  }
  /**
   * 
   */
  protected function getUserGroups() {
    return array_merge(array(0), db_get_fields("SELECT usergroup_id FROM ?:usergroups WHERE type='C'"));
  }

  /**
   * 
   */
  protected function mailReport() {
    global $settings, $smarty_mail, $mailer, $languages, $cart_language;
    echo 'mail report ';
    $total = count($this->createdProducts) + count($this->updatedProducts);
    if ($total == 0)
      return;
    $msg = <<<EOT
Ενημέρωση τιμοκαταλόγου
Ενημερώθηκαν %s είδη

Δημιουργήθηκαν τα παρακάτω:

%s

τη λίστα αυτή αν την κάνεις αντιγραφή και επικόληση στο excel θα είναι ωραία.
EOT;
    $msg = sprintf($msg, 
            $total, 
            implode("\n", $this->createdProducts));
echo $msg;
    fn_init_mailer();
    $mailer = & Registry::get('mailer');
    $__from['email'] = Registry::get('settings.Company.company_site_administrator');
    $__from['name'] = Registry::get('settings.Company.company_name');
    $mailer->SetFrom($__from['email'], $__from['name']);
    $mailer->ClearAttachments();
    $mailer->IsHTML(false);
    $mailer->CharSet = 'UTF-8';
    $mailer->Subject = 'Pricelist update';
    $mailer->Body = $msg;
    $mailer->ClearAddresses();
    $mailer->AddAddress('ioanniszervos@gmail.com','');
    $result = $mailer->Send();
    if (!$result) {
      echo $mailer->ErrorInfo;
    } else {
      echo 'mail send';
    }
    if($this->supplierId==10 && !empty($this->createdProducts)){//Difox
      $fp = fopen('./var/updater_difox_new_products.log', 'a');
      fwrite($fp, date("H:i:s m/d/Y",time())."\n");
      fwrite($fp, "Δημιουργήθηκαν τα παρακάτω ".count($this->createdProducts)."\n");
      $tmp=print_r(implode("\n", $this->createdProducts),true);
      fwrite($fp, $tmp);
      fclose($fp);
    }
  }

  protected function put_in_log($msg){
    $fp = fopen('./var/updater_'.$this->supplierId.'.txt', 'a');
    fwrite($fp, date("H:i:s m/d/Y",time())."\n");
    if(is_array($msg)){
      $tmp=print_r($msg);
      fwrite($fp, $tmp);
    }else{
      fwrite($fp, $msg);
    }
    fclose($fp);
  }
  
	private function hideProductsWithoutImage() {
		if( $this->supplierId!=20) {
			return;
		}
		
		$products = db_get_array("
								select p.product_id, product_code
								from cscart_products as p 
								left join cscart_images_links as iml on (iml.object_id=p.product_id and iml.object_type='product' and iml.type='M')
								where p.my_supplier_id=?i and isnull(iml.detailed_id) and p.status='A'
								", $this->supplierId);
		if( $products ) {
			$cnt = 0;
			foreach($products as $product) {
				$cnt++;
				db_query("UPDATE cscart_products SET status='D' WHERE product_id=?i", $product['product_id']);
				echo '. ';
			}
			echo '<br />';
			printf("Hided %s products without image<br />\n", $cnt);
		}

	}
}

