<?php

/**
 * Description of PriceListRow
 * Created on 28-7-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ProductRow extends BaseDataRow {

  protected
  $categoryCode,
  $categoryId,
  $productCode,
  $productCodeB,
  $productId,
  $stock,
  $price,
  $titleEl,
  $descriptionEl,
  $titleEn,
  $descriptionEn,
  $imageLink,
  $supplierId,
  $userGroups,
  $status,
	$manufacturerId,
	$catalogPrice,
	$attachment,
	$suggestedRetail,
	$weight,
	$brand;//[imatz] Prosthiki gia apothikeysi brand os features kai xrisi se filter [/imatz]
	
  public function __construct(
          $supplierId,
          $categoryCode, $productCode, 
          $productCodeB, 
          $stock, $price, 
          $titleEl, $titleEn, 
          $descriptionEl, $descriptionEn, 
          $imageLink, $category,
          $suggestedRetail=0,
		  $weight=0,
		  $brand=''//[imatz] Prosthiki gia apothikeysi brand os features kai xrisi se filter [/imatz]
  ) {
    $this->setSupplierId($supplierId);
    $this->setCategoryCode($categoryCode);
    $this->setProductCode($productCode);
    $this->setProductCodeB($productCodeB);
    $this->setStock($stock);
    $this->setPrice($price);
    $this->setTitleEl($titleEl);
    $this->setDescriptionEl($descriptionEl);
    $this->setTitleEn($titleEl);
    $this->setDescriptionEn($descriptionEl);
    $this->setImageLink($imageLink);
    $this->setCategoryId($this->matchCategoryCode($categoryCode, $supplierId));
    $this->setProductId($this->matchProductCode($productCode, $supplierId));
    $this->status = 'A';
    $this->suggestedRetail = $suggestedRetail;
    $this->weight = $weight;
	$this->setBrand($brand);//[imatz] Prosthiki gia apothikeysi brand os features kai xrisi se filter [/imatz]
    if( $categoryCode=='411' ) {
    	$words = explode(' ',$titleEl);
    	$this->setManufacturer(ucfirst($words[0]));
    }
    else
    	$this->setManufacturer($category);
    $this->catalogPrice = null;
    $this->attachment = null;
/*
    if( in_array(
    $this->getCategoryId(), 
    array(220,221,225,229,222,226,230,223,227,231,224,228,232,236,240,249,259,241,250,260,243,251,261,245,256,262,247,252,263,237) ) )
    	$this->status = 'H';
    	*/
  }
	
	//[imatz] 
	//Prosthiki gia apothikeysi brand os features kai xrisi se filter 
	public function getBrand() {
		return $this->brand;
	}

	public function setBrand($v) {
		$this->brand = $v;
	}
	//[/imatz]

  public function getCategoryCode() {
    return $this->categoryCode;
  }

  public function setCategoryCode($v) {
    $this->categoryCode = $v;
  }

  public function getCategoryId() {
    return $this->categoryId;
  }

  public function setCategoryId($v) {
    $this->categoryId = $v;
  }

  public function getProductCode() {
    return $this->productCode;
  }

  public function setProductCode($v) {
    $this->productCode = $v;
  }

  public function getProductCodeB() {
    return $this->productCodeB;
  }

  public function setProductCodeB($v) {
    $this->productCodeB = $v;
  }

  public function getProductId() {
    return $this->productId;
  }

  public function setProductId($v) {
    $this->productId = $v;
  }

  public function getStock() {
    return $this->stock;
  }

  public function setStock($v) {
    $this->stock = $v;
  }

  public function getPrice() {
    return $this->price;
  }

  public function setPrice($v) {
    $this->price = $v;
  }

  public function getTitleEl() {
    return $this->titleEl;
  }

  public function setTitleEl($v) {
    $this->titleEl = str_replace('(T-Mobile)', '',$v);
  }

  public function getDescriptionEl() {
    return $this->descriptionEl;
  }

  public function setDescriptionEl($v) {
    $this->descriptionEl = $v;
  }

  public function getTitleEn() {
    return $this->titleEn;
  }

  public function setTitleEn($v) {
    $this->titleEn = str_replace('(T-Mobile)', '',$v);
  }

  public function getDescriptionEn() {
    return $this->descriptionEn;
  }

  public function setDescriptionEn($v) {
    $this->descriptionEn = $v;
  }

  public function getImageLink() {
    return $this->imageLink;
  }

  public function setImageLink($v) {
    $this->imageLink = $v;
  }

  public function getSupplierId() {
    return $this->supplierId;
  }

  public function setSupplierId($v) {
    $this->supplierId = $v;
  }

  public function setStatus($v) {
    $this->status = $v;
  }
  public function getStatus() {
    return $this->status;
  }
  
  public function setManufacturer($v) {
  	$p = explode('>',$v);
  	$v= trim( array_pop($p));
  	$q = "SELECT variant_id FROM cscart_product_feature_variant_descriptions WHERE `variant` like '%$v%' and lang_code='EL'";
  	$q = "
			SELECT fvd.variant_id
			FROM `cscart_product_feature_variant_descriptions` as fvd
			left join cscart_product_feature_variants as fv on (fvd.variant_id=fv.variant_id)
			where fvd.lang_code='EL' and feature_id=16 and fvd.variant like '%$v%'
  	";
  	$manId = db_get_field($q);
  	if( $manId )
  		$this->setManufacturerId($manId);
  	else
  		$this->setManufacturerId(0);
  }
  
  public function setManufacturerId($v) {
  	$this->manufacturerId = $v;
  }
  
  public function getManufacturerId() {
  	return $this->manufacturerId;
  }

  public function setCatalogPrice($v) {
  	$this->catalogPrice = $v;
  }
  
  public function getCatalogPrice() {
  	return $this->catalogPrice;
  }

  public function setAttachment($v) {
  	$this->attachment = $v;
  }
  
  public function getAttachment() {
  	return $this->attachment;
  }
  
  public function matchProductCode($productCode) {
    $id= -1;
    if ($productCode)
      $id= db_get_field(sprintf("SELECT product_id FROM cscart_products WHERE product_code='%s'", $productCode));
    if( $id )
      return $id;
    else
      return -1;
  }

  public function toString() {
    return sprintf("%s\t %s\t %s\t %s\t %s", $this->getCategoryCode(), $this->getTitleEl(), $this->getProductCode(), $this->getPrice(), $this->getStock()
    );
  }

  public function getProductPriceArray(BasePriceMarkup $markuper, $userGroupId) {
  	$basePrice = $this->getPrice();
  	if( $this->getCatalogPrice() )
     	$basePrice = $this->getCatalogPrice();
    if( $this->weight>5 )
    	$basePrice+=10;
    if( $this->weight>=3 && $this->weight<=4.9)
    	$basePrice+=5;		
    if( $this->weight>=10.01 )
    	$basePrice+=15;
		
    $price = PricingRules::create($this->categoryId, $this->supplierId)->applyToPrice($basePrice);
    if( $price == 0 )
      $price = $this->round_to($markuper->calcPriceForUserGroup($basePrice, $userGroupId, $this->productCode, $this->categoryCode),0.5);
    if( $this->supplierId==3 ) {
    	$price+=2;
    }

    return array(
        'product_id' => $this->getProductId(),
        'price' => $price,
        'lower_limit' => 1,
        'usergroup_id' => $userGroupId,
    );
  }

  protected function round_to($number, $increments) {
    $increments = 1 / $increments;
    return (round($number * $increments) / $increments);
  }

  private function updateDescription() {
//  	if( $this->supplierId==10 )
//  		return;
    $query = "
      REPLACE INTO cscart_product_descriptions 
        SET
        `product`=?s, `shortname`=?s, `short_description`=?s, `full_description`=?s,
        `meta_keywords`=?s, `meta_description`=?s, `search_words`=?s, `page_title`=?s,
         product_id=?i, lang_code=?s
      ";
    db_query($query, 
            $this->getTitleEl(), $this->getTitleEl(), $this->getTitleEl(), $this->getDescriptionEl(), 
            $this->getTitleEl(), $this->getTitleEl(), $this->getTitleEl(), $this->getTitleEl(), 
            $this->getProductId(), 'EL'
    );
    db_query($query, 
            $this->getTitleEn(), $this->getTitleEn(), $this->getTitleEn(), $this->getDescriptionEn(), 
            $this->getTitleEn(), $this->getTitleEn(), $this->getTitleEn(), $this->getTitleEn(), 
            $this->getProductId(), 'EN'
    );
    
    //$q = "REPLACE INTO cscart_product_features_values SET product_id=%s, feature_id=16, variant_id=%s, lang_code='%s'";
    //printf("%s <br />", $q);
   // echo(sprintf($q, $this->getProductId(), $this->getManufacturerId(), 'EL'));
    //db_query(sprintf($q, $this->getProductId(), $this->getManufacturerId(), 'EL'));
    //db_query(sprintf($q, $this->getProductId(), $this->getManufacturerId(), 'EN'));
    return;
  }

  private function updatePrices(BasePriceMarkup $markuper) {
    foreach ($this->getUserGroups() as $userGroupId) {
      $price = $this->getProductPriceArray($markuper, $userGroupId);
      db_query(sprintf("REPLACE INTO cscart_product_prices SET product_id=%s, price=%s, lower_limit=%s, usergroup_id=%s",
              $price['product_id'], $price['price'], $price['lower_limit'], $price['usergroup_id']));
      $this->suggestedRetail = round(
				$price['price']* (1+round(rand(5,15)/100,2))
				,0);
      printf("our=%s suggested=%s weight=%s", $price['price'], $this->suggestedRetail,$this->weight );
      if( $price['price'] < $this->suggestedRetail && $price['price']>200 ) {
      	db_query(sprintf("UPDATE cscart_products SET list_price=%s WHERE product_id=%s",$this->suggestedRetail, $this->getProductId()));
      	echo 'list price updated<br />';
      }
      else
      	db_query(sprintf("UPDATE cscart_products SET list_price=0 WHERE product_id=%s", $this->getProductId()));
    }
  }

  private function updateImagePair() {
    $detailed = array();
    $im = fn_get_url_data($this->getImageLink());
    printf("<br />link: %s result=%s<br />",$this->getImageLink(), print_R($im, true));
    echo 'image11';
    $detailed[] = $im;
    $a = fn_get_image_pairs(array($this->getProductId()), 'product', 'M', true, true);
    $pair = array_shift($a);
    $pair_data[] = array(
        "pair_id" => $pair['pair_id'],
        "type" => "M",
        "object_id" => $this->getProductId(),
        "image_alt" => $this->getTitleEn(),
        "detailed_alt" => $this->getTitleEn(),
    );
    
    fn_update_image_pairs(
            array(), $detailed, $pair_data, $this->getProductId(), 'product'
    );
    
    echo 'image22';
  }

  public function createProduct(BasePriceMarkup $markuper) {
    if (!$this->getCategoryId())
      return false;
    $query = "
      INSERT INTO ?:products (
        `product_code`, `product_type`, `status`, 
        `company_id`, `list_price`, `amount`, `weight`, `length`, 
        `width`, `height`, `shipping_freight`, `low_avail_limit`, `timestamp`, 
        `usergroup_ids`, `is_edp`, `edp_shipping`, `unlimited_download`, `tracking`, 
        `free_shipping`, `feature_comparison`, `zero_price_action`, `is_pbp`, `is_op`, 
        `is_oper`, `is_returnable`, `return_period`, `avail_since`,
        `localization`, `min_qty`, `max_qty`, `qty_step`, `list_qty_count`, 
        `tax_ids`, `age_verification`, `age_limit`, `options_type`, `exceptions_type`, 
        `details_layout`, 
        `shipping_params`, out_of_stock_actions,
        supplierA_price, product_codeB, my_supplier_id) VALUES 
        (?s, 'P', ?s, 
        0, 0.00, ?i, 0.00, 0, 
        0, 0, 0.00, 0, ?i, 
        ?s, 'N', 'N', 'N', 'B', 
        'N', 'N', 'R', 'N', 'N', 
        'N', 'Y', 10, 0, 
        '', 0, 0, 0, 0, 
        '6', 'N', 0, 'P', 'F', 
        'default', 
        'a:5:{s:16:\"min_items_in_box\";i:0;s:16:\"max_items_in_box\";i:0;s:10:\"box_length\";i:0;s:9:\"box_width\";i:0;s:10:\"box_height\";i:0;}',
        'S',
        ?s, ?s, ?s)
          ";
    $this->setProductId(
            db_query(
                    $query, $this->getProductCode(), $this->status, $this->getStock(), strtotime(date("Y-m-d")), implode(',', $this->getUserGroups()), 
                    $this->getPrice(), $this->getProductCodeB(), $this->getSupplierId()
            )
    );

    $this->updateDescription();
    $this->updatePrices($markuper);

    $query = "
      INSERT INTO ?:products_categories 
      (product_id, category_id, link_type, position )
      VALUES (
      ?i, ?i, 'M', 0
      )
      ";
    db_query($query, $this->getProductId(), $this->getCategoryId());
    fn_update_product_count(array($this->getCategoryId()));

    if ($this->getImageLink()) {
      $this->updateImagePair();
    }
    return true;
  }

  public function updateProduct(BasePriceMarkup $markuper, $isFast = false) {
    printf("updateProduct top <br />\n");
    $r= db_get_row(sprintf('SELECT pricelist_updater_skip, update_only_price, update_only_stock, pricelist_updater_skip_skroutz FROM cscart_products WHERE product_id=%s', $this->productId));
    $skip = $r['pricelist_updater_skip'];
    $onlyPrices = $r['update_only_price'];
    $onlyStock = $r['update_only_stock'];
    $skipSkroutz = $r['pricelist_updater_skip_skroutz'];
    
    if( $skip=='Y' )
      return;
		$weightClause = ", weight=".$this->weight;
		if( $this->categoryId==380 ) {
			$weightClause = ", weight=51";
		}
//    $q = "UPDATE cscart_products SET amount=%s, supplierA_price='%s', status='%s' WHERE product_id=%s";
//    db_query(sprintf($q, $this->stock, $this->price, $this->status, $this->productId));
    $q = "UPDATE cscart_products SET out_of_stock_actions='S', supplierA_price='%s', status='%s', amount=%s, my_supplier_id=%s %s WHERE product_id=%s";
    $q1 = sprintf($q, $this->price, $this->status, $this->stock, $this->getSupplierId(), $weightClause, $this->productId);
    printf("%s <br />\n", $q1);
    db_query($q1);
    if( in_array($this->getCategoryCode(), array('340','364')) ) {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=1,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=1 WHERE product_id=%s",$skroutz, $this->productId));
    }
    elseif( substr($this->getProductCode(),0,1)=='0' ) {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=1,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=1 WHERE product_id=%s",$skroutz, $this->productId));
    }
    elseif( in_array($this->getCategoryCode(), array('1088')) ) {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=8,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=2 WHERE product_id=%s",$skroutz, $this->productId));
    }
    elseif( in_array($this->getCategoryId(), array('380')) ) {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=4,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=4 WHERE product_id=%s",$skroutz, $this->productId));
    }	
    elseif( in_array($this->getCategoryCode(), array('325')) && strpos($this->titleEl,'Alcatel OT-995')===false ) {	// alcatel
    	if( $skipSkroutz!='Y' )
	    	db_query(sprintf("UPDATE cscart_products SET status='D' WHERE product_id=%s", $this->productId));
	    return;
    }
    //elseif( in_array($this->getCategoryCode(), array('846','847')) ) {
    elseif( substr($this->getCategoryCode(), 0,4)=='846-' || substr($this->getCategoryCode(), 0,4)=='847-') {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=5,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=5 WHERE product_id=%s",$skroutz, $this->productId));
    	db_query(sprintf("UPDATE cscart_products SET weight=51 WHERE product_id=%s", $this->productId));
    }
    else {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=1,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=1 WHERE product_id=%s",$skroutz, $this->productId));
    	//db_query(sprintf("UPDATE cscart_products SET weight=0 WHERE product_id=%s", $this->productId));
    }
    if( substr($this->getCategoryCode(), 0,4)=='14' && $this->getSupplierId()==1 ) {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=9,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=8 WHERE product_id=%s",$skroutz, $this->productId));
    }
		// block difox
    //if( in_array($this->getSupplierId(), array(10)) )
    //	db_query(sprintf("UPDATE cscart_products SET skroutz_availability=9 WHERE product_id=%s", $this->productId));	
    
    // summer holiday setting
    /*
    if( in_array($this->getSupplierId(), array(1,4)) )
    	db_query(sprintf("UPDATE cscart_products SET skroutz_availability=5,shop_availability=5 WHERE product_id=%s", $this->productId));
		if( in_array($this->getCategoryCode(), array('325','1088')) ) {	// alcatel
	    	db_query(sprintf("UPDATE cscart_products SET skroutz_availability=9 WHERE product_id=%s", $this->productId));
    }
    */
	
//exentric
	
	//$query = "UPDATE cscart_products SET STATUS = 'D' WHERE NOT product_code LIKE '11%' AND my_supplier_id =10";
	
	
    if( $onlyStock=='Y' ) {
      printf("update ONLY stock \n");
    }
    else {
      printf("updating prices....");
      $this->updatePrices($markuper);
      printf("updated prices <br />\n");
    }
    if( $onlyPrices=='Y' || $onlyStock=='Y' ) {
    	/*
      if ($this->getImageLink()) {
  	    $a = fn_get_image_pairs(array($this->getProductId()), 'product', 'M', true, true);
		    $pair = array_shift($a);
		    if(!$pair['pair_id']) {
        	$this->updateImagePair();
      	}
      }
      */
    }
    else {
      if( !$isFast ) {
        $q = "UPDATE cscart_products SET product_codeB='%s' WHERE product_id=%s";
        printf("%s <br />\n", $q);
        db_query(sprintf($q, $this->productCodeB, $this->productId));
        printf("updating descriptions....");
        if( $this->supplierId!=10 )
	        $this->updateDescription();
        // update product category
        /*
        db_query("UPDATE cscart_products_categories SET category_id=?i WHERE product_id=?i and link_type='M'", $this->getCategoryId(), $this->getProductId());
    		fn_update_product_count(array($this->getCategoryId()));     
    		*/
        printf("updated descriptions <br />\n");
      }
    }
    printf("updateProduct end <br />\n");
  }
  
	//[imatz]  
	public function updateFeaturesAndFilters($product) {
		$brand_feature_id=18;//erexetai apo tin db tou shope
		$variant_id=db_get_field("SELECT variant_id FROM ?:product_feature_variant_descriptions WHERE variant=?s",$this->brand);
		//echo 'Searched variant: Name: '.$this->brand.', Id: '.$variant_id.', ProductId: '.$this->productId;
		//An to fetaure tou kataskeyasti den exei to variant tote prosthiki
		if(empty($variant_id)){
			$insert_data=array(
				'feature_id'=> $brand_feature_id,
				'variant_name'=> $this->brand
			);
			$variant_id=db_query("INSERT INTO ?:product_feature_variants ?e",$insert_data);
			
			$insert_data=array(
				'variant_id'=> $variant_id,
				'variant'=> $this->brand,
				'lang_code'=> 'EN'
			);
			db_query("INSERT INTO ?:product_feature_variant_descriptions ?e",$insert_data);
			
			$insert_data=array(
				'variant_id'=> $variant_id,
				'variant'=> $this->brand,
				'lang_code'=> 'EL'
			);
			db_query("INSERT INTO ?:product_feature_variant_descriptions ?e",$insert_data);
		}
		
		//elegxos an to proion exei to feature
		$variant_exits_in_product=db_get_field("SELECT COUNT(*) FROM ?:product_features_values WHERE feature_id=?i AND product_id=?i AND variant_id=?i",$brand_feature_id,$this->productId,$variant_id);
		
		//To proion den exei to variant tou fetaure sindedemeno
		if(empty($variant_exits_in_product)){
			//prosthiki gia tin ellinik glossa			
			$insert_data=array(
				'feature_id'=> $brand_feature_id,
				'product_id'=> $this->productId,
				'variant_id'=> $variant_id,
				'lang_code'=> 'EL'
			);
			db_query("INSERT INTO ?:product_features_values ?e",$insert_data);
			
			//prosthiki gia tin aggliki glwssa
			$insert_data=array(
				'feature_id'=> $brand_feature_id,
				'product_id'=> $this->productId,
				'variant_id'=> $variant_id,
				'lang_code'=> 'EN'
			);
			db_query("INSERT INTO ?:product_features_values ?e",$insert_data);
			
			//echo 'Added feature value: Productid: '.$this->productId.' - Variant: '.$this->brand;
		}
	}
	//[/imatz]
  
}

