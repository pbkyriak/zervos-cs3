<?php

class SupplierPricingRules {
	protected $rules;
	private static $m_pInstance;
	
	public function __construct() {
		$this->rules = array();
	}
	
	public static function getInstance() {
		//return new SupplierPricingRules_Sup();
	  if (!self::$m_pInstance) {
	      self::$m_pInstance = new SupplierPricingRules_Sup();
	  }
	  return self::$m_pInstance;
	}  
	
	public function getCategoryRules($categoryId) {
//		printf("%s pricing rules", count($this->rules));
		if( isset($this->rules[$categoryId]) )
			return $this->rules[$categoryId];
		return null;
	}
	
}