<?php
require_once dirname(__FILE__).'/BasePriceMarkup.class.php';

/**
 * Description of BatariaMarkup
 * Created on 13-2-2012
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class BatariaMarkup extends BasePriceMarkup {

	public function getHat() {
		$hat = (float) Registry::get('settings.AutoPricing.christmass_hat');
		return $hat;
	}
  public function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode) {
  	$christmasHat = $this->getHat();
		return  ($basePrice/1.23+$christmasHat+5)*1.23;
  }
  
  public function statusModifier(ProductRow $product) {
    $newStatus = $product->getStatus();
    return $newStatus;
  }
}

