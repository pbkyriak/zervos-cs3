<?php 
include_once(__DIR__."/PHPExcel/PHPExcel.php");

class YukaProduct {
	public $productId, $title, $categoryId, $stock, $price;
}

class YukatelCleaner {
	private $supplierId = 7;
	
	public function makePriceList() {
		$fn = 'priceUpdater/updaterFiles/yukatel-pricelist.xlsx';
		$products = $this->loadPriceList($fn);
		$handle = fopen("priceUpdater/updaterFiles/yukaPriceList.csv","w");
		if( $handle ) {
			foreach($products as $product) {
				fputcsv($handle, array($product->categoryId, $product->productId, $product->title, $product->stock, $product->price), ";");
			}
			fclose($handle);
		}
	}
	
	public function updateQuantites() {
		$fn = 'priceUpdater/updaterFiles/yukatel-stocklist.xlsx';
		$products = $this->loadPriceList($fn, false);
		if( $products ) {
			$this->collectSupplierProductIds();
			foreach( $products as $product ) {
				$cProduct = db_get_row("SELECT * FROM cscart_products WHERE product_id=?i", $product->productId);
				if( $cProduct ) {
					if( $cProduct['pricelist_updater_skip']=='N' ) {
						$q = sprintf("update cscart_products set amount=%s,skroutz_availability=1, shop_availability=1, status='A' where product_id=%s", $product->stock, $product->productId);
						printf("%s <br />", $q);
						db_query("update cscart_products set amount=?i,skroutz_availability=1, shop_availability=1, status='A' where product_id=?i", $product->stock, $product->productId);
						$this->remoreFromToDisableList($product->productId);
					}
				}
			}
			$this->disableNotUpdatedProducts();
		}
	}
	
  private function collectSupplierProductIds() {
    db_query("CREATE TEMPORARY TABLE sup_ids (product_Id int)");
    db_query("insert into sup_ids SELECT product_id FROM cscart_products where my_supplier_id=?i",  $this->supplierId);
  }
  
  private function remoreFromToDisableList($productId) {
    db_query("DELETE FROM sup_ids WHERE product_Id=?i",$productId);
  }
  
  private function disableNotUpdatedProducts() {
    db_query("UPDATE cscart_products SET last_disable_timestamp=?i WHERE shop_availability!=8 and pricelist_updater_skip='N' AND product_id in (SELECT product_Id FROM sup_ids)", 
    				time()
    				);
    printf("disabling supplier products B<br />");
    
    db_query(sprintf("UPDATE cscart_products SET status='A', shop_availability=8, skroutz_availability=8	WHERE my_supplier_id=%s and pricelist_updater_skip='N' AND product_id in (SELECT product_Id FROM sup_ids)", $this->supplierId));
    db_query("DROP TABLE sup_ids");
    printf("disabling supplier products DONE<br />");
  }

	private function loadPriceList($inputFileName, $readPrice=true) {
		$inputFileType = 'Excel2007';
		
		$out = array();
		/**  Create a new Reader of the type defined in $inputFileType  **/
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		/**  Load $inputFileName to a PHPExcel Object  **/
		$objPHPExcel = $objReader->load($inputFileName);
		
		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		$cleanedData = array();
		foreach( $sheetData as $dataRow ) {
			if( $dataRow['A'] && is_numeric($dataRow['B']) ) {
				$product = new YukaProduct();
				$product->productId = $this->matchProductCode(md5($dataRow['A']));
				$product->title = $dataRow['A'];
				$product->categoryId = $this->toVNPCategoryId($dataRow['A']);
				$product->stock = (int)$dataRow['B'];
				if( $readPrice ) {
					$product->price = (float)$dataRow['C'];
				}
				else
					$product->price = 0;
				if( $product->productId && $product->categoryId ) {
					$out[] = $product;
				}
			}
		}
		printf("loaded %s products<br />",count($out));
		return $out;
	}

	private function matchProductCode($productCodeC) {
		return db_get_field("SELECT product_id FROM cscart_products WHERE product_codeC=?s", $productCodeC);
	}
	
	private function toVNPCategoryId($title) {
		if( strpos($title, 'Nokia')===0 )
			return 7;
		if( strpos($title, 'Samsung')===0 )
			return 9;
		if( strpos($title, 'Sony')===0 )
			return 8;
		if( strpos($title, 'LG')===0 )
			return 170;
		if( strpos($title, 'Motorola')===0 )
			return 171;
		if( strpos($title, 'Apple Ipad')===0 )
			return 172;
		if( strpos($title, 'Apple NEW Ipad')===0 )
			return 172;
		if( strpos($title, 'Apple Iphone')===0 )
			return 18;
		if( strpos($title, 'HTC')===0 )
			return 6;
		if( strpos($title, 'Apple MacBook')===0 )
			return 117;
			
		return -1;
	}

}