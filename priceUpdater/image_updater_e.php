<?php

error_reporting(E_ALL);

DEFINE ('AREA', 'A');
DEFINE ('AREA_NAME' ,'admin');
require './prepare.php';

if (!empty($html_catalog))  {
	define ('NO_SESSION', true);
}
require './init.php';

@ini_set('memory_limit', '256M');
$current_location = 'http://' .Registry::get('config.http_host');

$cart_language='EL';

clearstatcache();
printf("%s<br />\n", 'Init done.');
$images = loadProductImages(dirname(__FILE__).'/priceUpdater/updaterFiles/eno.csv');
printf("image urls %s<br />", count($images));
$products = db_get_array("
						select p.product_id, product_code
						from cscart_products as p 
						left join cscart_images_links as iml on (iml.object_id=p.product_id and iml.object_type='product' and iml.type='M')
						where p.my_supplier_id=9 and isnull(iml.detailed_id) and p.status='A'
						");
if( $products ) {
	printf("products without images %s <br />", count($products));
	foreach($products as $product) {
		if( isset($images[$product['product_code']]) ) {
			$url = $images[$product['product_code']];
  		printf("pid=%s pc=%s im=%s<br />", $product['product_id'],$product['product_code'], $url);
  		updateImagePair($product['product_id'], $url);
	  }
	}
}


  function updateImagePair($productId, $url) {
    $detailed = array();
    $im = fn_get_url_data($url);
    printf("<br />link: %s result=%s<br />",$url, print_R($im, true));
    $description = db_get_field("SELECT product FROM cscart_product_descriptions WHERE product_id=?i AND lang_code='EL'", $productId);
    $detailed[] = $im;
    $a = fn_get_image_pairs(array($productId), 'product', 'M', true, true);
    $pair = array_shift($a);
    if( !isset($pair['pair_id']) )
    	$pair['pair_id']=0;
    $pair_data[] = array(
        "pair_id" => $pair['pair_id'],
        "type" => "M",
        "object_id" => $productId,
        "image_alt" => $description,
        "detailed_alt" => $description,
    );
    
    fn_update_image_pairs(
            array(), $detailed, $pair_data, $productId, 'product'
    );
    
    echo 'image22';
  }
  
  function loadProductImages($fn) {
  	$images = array();
  	$handle = fopen($fn, "r");
  	if( $handle ) {
  		while( $data=fgetcsv($handle,10000,';') ) {
  			$images[$data[1]] = $data[17];
  		}
  	}
  	return $images;
  }
