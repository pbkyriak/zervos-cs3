<?php
//
// Author: Ioannis Matziaris [imatz]
// Email: imatzgr@gmail.com
// Date: February 2015
// Details:

class IceCat {
	private $api_username;
	private $api_password;
	private $api_lang;

	public function __construct(){
		$this->api_username="evercommobiles";
		$this->api_password="6392884xX";
		$this->api_lang="el";
	}

	private function convertAttributesToArray($xmlObject){
		$attrs=$xmlObject->attributes();
		$array=array();
		foreach($attrs as $attr=>$val){
			$array[$attr]=(string)$val;
		}
		return($array);
	}

	private function getCategoryData($xmlCategory){
		$catId = intval($xmlCategory->attributes());
		$titleXML = new SimpleXMLElement($xmlCategory->asXML());
		$title = $titleXML->xpath("//Name");
		$catName = (string)$title[0]->attributes()['Value'];
		return(array("id"=>$catId,"name"=>$catName));
	}

	private function xml2array ( $xmlObject,$out=array()){
    	foreach ( (array) $xmlObject as $index => $node )
        	$out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

    	return $out;
	}

	public function getICEcatProductSpecs($ean,$params){	
		// Return 0 and exit function if no EAN available
		if($ean == null){
			return array();
		}
	 
		// Get the product specifications in XML format
		$context = stream_context_create(array(
			'http' => array(
				'header'  => "Authorization: Basic " . base64_encode($this->api_username.":".$this->api_password)
			)
		));
		$iceurl = 'https://data.icecat.biz/xml_s3/xml_server3.cgi?ean_upc='.urlencode($ean).';lang='.$this->api_lang.';output=productxml';
		$data = @file_get_contents($iceurl, false, $context);		

		if( $data===false ) {
			return array();
		}
		if(empty($data))
			return array();
		if(strpos($data, 'ErrorMessage="The requested XML data-sheet is not present in the Icecat database."')!==false)
			return array();
		
		if(isset($params["supplier_id"])&&$params["supplier_id"]==19){
			$fp = fopen('/home/vnpgr/www/var/icecat.txt', 'w');
			$tmp=print_r($data,true);
			fwrite($fp, $tmp);
			fclose($fp);
		}

		try {
			$xml = new SimpleXMLElement($data);	  	
		}
		catch(Exception $ex) {
			printf("ICE url = %s\n",$iceurl);
			printf("error: %s\n", $ex->getMessage());
			return array();
		}
		// Create arrays of item elements from the XML feed
		$product_api = $xml->xpath("//Product");
		$productAttrs=$product_api[0]->attributes();

		//Elegxos an iparxei sfalma
		if(isset($productAttrs["ErrorMessage"])&&!empty($productAttrs["ErrorMessage"])){
			return false;
		}

		//Product Attributes
		$product_data=$this->convertAttributesToArray($product_api[0]);

		//Perigrafei proiontos
		$productDescription = $xml->xpath("//ProductDescription");
		//$product_data["url"]="";
		$product_data["description"]="";
		//$product_data["short_description"]="";
		//$product_data["manual_pdf"]="";
		//$product_data["product_pdf"]="";
		foreach($productDescription as $item) {
			$productValues = $item->attributes();
			if($productValues['URL'] != null){
				$product_data["url"]=(string)$productValues['URL'];			
			}
			if(!$params["ex_description"]&&$productValues['LongDesc'] != null){
				$description = str_replace('\n', '', $productValues['LongDesc']);
				$description = str_replace('<b>', '<strong>', $description);
				$description = str_replace('<B>', '<strong>', $description);
				$description = str_replace('</b>', '</strong>', $description);
				$product_data["description"]=(string)$description;
			}
			if($productValues['ShortDesc'] != null){
				$product_data["short_description"]=(string)$productValues['ShortDesc'];			
			}
			if($productValues['ManualPDFURL'] != null){
				$product_data["manual_pdf"]=(string)$productValues['ManualPDFURL'];			
			}
			if($productValues['PDFURL'] != null){
				$product_data["product_pdf"]=(string)$productValues['PDFURL'];			
			}
		}

		if(!$params["ex_ean"]){
			//Ean
			$productEAN = $xml->xpath("//EANCode");
			//$product_data["ean"]="";
		
			foreach($productEAN as $item) {
				$productValues = $item->attributes();
				if($productValues['EAN'] != null){
					if(count($productEAN)==1){
						$product_data["ean"]=(string)$productValues['EAN'];			
					}else{
						$product_data["ean"][]=(string)$productValues['EAN'];			
					}
				}
			}
		}

		if(!$params["ex_brand"]){
			//Brand
			$productBrand = $xml->xpath("//Supplier");
			//product_data["brand"]="";
			foreach($productBrand as $item) {
				$productValues = $item->attributes();
				if($productValues['Name'] != null){
					$product_data["brand"]=(string)$productValues['Name'];			
				}
			}
		}

		if(!$params["ex_summary_description"]){
			//Short description
			$productSummaryDescription= $xml->xpath("//SummaryDescription");
			//$product_data["short_description"]="";
			if($productSummaryDescription!= null){
				$data=$this->xml2array($productSummaryDescription[0]);
				if(isset($data["LongSummaryDescription"])){
					$product_data["summary_short_description"]=$data["LongSummaryDescription"];
				}elseif(isset($data["ShortSummaryDescription"])){
					$product_data["summary_short_description"]=$data["ShortSummaryDescription"];
				}
			}		
		}


		if(!$params["ex_add_images"]){
			//More images
			$productGallery= $xml->xpath("//ProductGallery");
			//$product_data["add_images"]=array();
			if($productGallery!= null){
				$pictures=$productGallery[0]->children();
				foreach($pictures as $item){
					$pic = $item->attributes()["Pic"];
					$product_data["add_images"][]=(string)$pic;
				}
			}
		}

		if(!$params["ex_category"]){
			//Product category
			$category = $xml->xpath("//Category");
			$product_data["category"]=$this->getCategoryData($category[0]);
		}

		if(!$params["ex_features"]){
			//Product categories
			$featureGroups = $xml->xpath("//CategoryFeatureGroup");
			if($featureGroups != null){
				foreach($featureGroups as $group) {
					$_group = new SimpleXMLElement($group->asXML());
					$group_data = $_group->xpath("//FeatureGroup");
					$name = $group_data[0]->xpath("//Name");
					//Ekseresi omadwn features
					if(trim((string)$name[0]->attributes()["Value"])=="Επεξεργαστής ειδικά χαρακτηριστικά")
						continue;

					$product_data["features"][(string)$group->attributes()["ID"]]=array(
						"title"=>(string)$name[0]->attributes()["Value"],
						"featrues"=>array(),
						"id"=>(string)$name[0]->attributes()["ID"]
					);
				}
			}
			//Features
			$this->extractProductFeatures($xml, $product_data);

			//Aferesi feature groups pou den exoun items
			if(isset($product_data["features"])){
				foreach($product_data["features"] as $group_id=>$group_data){
					if(empty($group_data["featrues"]))
						unset($product_data["features"][$group_id]);
				}
			}	
			//Features images
			$productFeatureLogo = $xml->xpath("//FeatureLogo");
			if($productFeatureLogo != null){
				foreach($productFeatureLogo as $item) {
					$attrs = $item->attributes();
					$feature_id=intval($attrs["Feature_ID"]);
					$feature_value=(string)$attrs["Value"];
					$feature_img=(string)$attrs["LogoPic"];;
					foreach($product_data["features"] as $group_id=>$group_data){
						if(isset($group_data["featrues"][$feature_id])&&
							$group_data["featrues"][$feature_id]["value"]==$feature_value){

							$product_data["features"][$group_id]["featrues"][$feature_id]["image"]=$feature_img;
						}
					}
					//Aferesi apo to add images twn eikonwn twn features
					if(isset($product_data["add_images"])&&!empty($product_data["add_images"])){
						if(in_array($feature_img, $product_data["add_images"])){
							$i = array_search($feature_img,  $product_data["add_images"]); 
							unset($product_data["add_images"][$i]);
						}
					}
				}
			}		
		}

		if(!$params["ex_files"]){
			//Videos and pdfs
			$productFiles= $xml->xpath("//ProductMultimediaObject");
			//$product_data["videos"]=array();
			//$product_data["pdfs"]=array();
			//$product_data["files"]=array();
			if($productFiles!= null){
				$files=$productFiles[0]->children();
				foreach($files as $item){
					$contnet_type=(string)$item->attributes()["ContentType"];
					$id=(string)$item->attributes()["MultimediaObject_ID"];
					$data=array(
						"id"=>$id,
						"contnet_type"=>$contnet_type,
						"type"=>(string)$item->attributes()["Type"],
						"title"=>(string)$item->attributes()["Description"],
						"url"=>(string)$item->attributes()["URL"],
						"size"=>(string)$item->attributes()["Size"]
					);
					if($contnet_type=="application/pdf"){
						$product_data["pdfs"][$id]=$data;
					}elseif($contnet_type=="video/x-flv" || $contnet_type=="application/octet-stream"){
						$product_data["videos"][$id]=$data;
					}else{
						$product_data["files"][$id]=$data;
					}
				}
			}
		}

		if(!$params["ex_related_products"]){
			//Sxetika proionta
			$productRelated= $xml->xpath("//ProductRelated");
			//$product_data["related_products"]=array();
			if($productRelated!= null){
				foreach($productRelated as $item){
					$attr=$item->attributes();
					$inner = new SimpleXMLElement($item->asXML());
					$_product = $inner->xpath("//Product");
					if($_product!= null){
						$p_attr=$_product[0]->attributes();
						$product_data["related_products"][(string)$p_attr["ID"]]=array(
							"product_code"=>(string)$p_attr["Prod_id"],
							"thumb"=>(string)$p_attr["ThumbPic"],
							"title"=>(string)$p_attr["Name"]
						);
						$_inner= new SimpleXMLElement($_product[0]->asXML());
						$_supplier=$_inner->xpath("//Supplier");
						if($_supplier!= null){
							$s_attr=$_supplier[0]->attributes();
							$product_data["related_products"][(string)$p_attr["ID"]]["brand"]=(string)$s_attr["Name"];
						}
					}				
				}
			}
		}
		if(!$params["ex_desc_from_features"]){
			$product_data["description2"]=$this->getDescriptionFromFeatures($product_data);
		}

		return $product_data;
	}

	private function getDescriptionFromFeatures($product_data){
		$str='';

		if(isset($product_data["features"])&&!empty($product_data["features"])){
			$str='<table cellpadding="0" cellspacing="0" width="100%">';
				foreach($product_data["features"] as $group_id=>$group){
					$str.='<tr><td class="i-ic-features-header" colspan="2">'.$group["title"].'</td></tr>';
					foreach($group["featrues"] as $feature_id=>$data){
						$str.='<tr>';
						$str.='<td width="40%" class="i-ic-features-label">'.$data["name"].'</td>';
						if($data["value"]=="Y"){
							$str.='<td class="ds_data">Ναι</td>';
						}elseif($data["value"]=="N"){
							$str.='<td class="ds_data">Όχι</td>';
						}else{
							$str.='<td class="ds_data">'.$data["value"].'</td>';
						}
						$str.='</tr>';
					}
				}
			$str.='</table>';
		}
		return($str);
	}

	private function txtCorrection($text){

	}
	
	private function extractProductFeatures($xml, &$product_data) {
		$spec_items = $xml->xpath("//ProductFeature");
		foreach($spec_items as $item) {
			$featureValue='';
			$featureId = '';
			$featureName = '';
			$specCategoryId = 0;
			
			if( $item->attributes() && isset($item->attributes()['Value']) ) {
				$prv = (string)$item->attributes()['Presentation_Value'];
				if( $prv!=='Y' && $prv!=='N' ) {
					$featureValue = (string)$item->attributes()['Presentation_Value'];
				}
				else {
					$featureValue = $prv;
				}
				if( isset($item->attributes()['CategoryFeatureGroup_ID']) ) {
					$specCategoryId = intval($item->attributes()['CategoryFeatureGroup_ID']);
				}
			}
			if( isset($item->Feature) ) {
				$featureId = (string)$item->Feature->attributes()['ID'];
				if( isset($item->Feature->Name) ) {
					$featureName = (string)$item->Feature->Name->attributes()['Value'];
				}
			}
			
			if( $featureId && $featureValue && $featureName && $specCategoryId ) {
				$product_data["features"][$specCategoryId]["featrues"][$featureId]=array(
					"name"=>$featureName,
					"value"=>$featureValue
				);
			}
		}
		
	}
}



/*
grammi 199 eixe ayto to block
			$spec_items = $xml->xpath("//ProductFeature");
			if($featureGroups != null && $spec_items != null){
				foreach($spec_items as $item) {
					$featureValue = $item->attributes()['Value'];
					$featureId = (string)$item->Feature->ID;
					$featureName = (string)$item->Feature->Name->value;
					
					$specValue = $item->attributes();
					
					$titleXML = new SimpleXMLElement($item->asXML());
					$title = $titleXML->xpath("//Name");
					$xmlFeatureID=$titleXML->xpath("//Feature");
					$FeatureID=(string)$xmlFeatureID[0]->attributes()["ID"];
					$specName = $title[0]->attributes();
					$specCategoryId = intval($specValue['CategoryFeatureGroup_ID']);
					if(isset($product_data["features"][$specCategoryId])){
						$value="";
						if($specValue['Presentation_Value'] == "Y"){
							$value="Y";
						}elseif($specValue['Presentation_Value'] == "N"){
							$value="N";
						}else{
							$value=(string)$specValue['Presentation_Value'];
						}
						$product_data["features"][$specCategoryId]["featrues"][$FeatureID]=array(
							"name"=>(string)$specName['Value'],
							"value"=>$value
						);
					}
				}
			}
*/