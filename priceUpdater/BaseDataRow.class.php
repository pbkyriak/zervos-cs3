<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseDataRow
 * Created on 6-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class BaseDataRow {
  
  protected function matchCategoryCode($categoryCode, $supplierId) {
    if( $categoryCode ) {
      /*
      $id = db_get_field(sprintf(
                      "SELECT category_id FROM cscart_categories WHERE find_in_set('%s',supplier_category_name) and find_in_set('%s',supplier_import_id)", 
                      $categoryCode, $supplierId
                      ));
                      */
      $id = db_get_field(sprintf(
                      "SELECT category_id FROM cscart_categories WHERE find_in_set('%s',supplier_category_name)", 
                      $categoryCode
                      ));
      if( $id )
        return $id;
      else
        return -1;
    }
    else
      return -1;
  }
  
  public function setUserGroups($value) {
    if( is_array($value) )
      $this->userGroups = $value;
    else {
      $this->userGroups = explode(',', $value);
    }
  }
  
  public function getUserGroups() {
    return $this->userGroups;
  }
}

