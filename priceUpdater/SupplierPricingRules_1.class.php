<?php

class SupplierPricingRules_Sup extends SupplierPricingRules {
		
	public function __construct() {
		$this->rules = array();
		$this->rules['314'] = array();
		$this->rules['314'][] = array('from_price'=>80.00, 'markup'=>5.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['314'][] = array('from_price'=>80.00, 'markup'=>5.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['314'][] = array('from_price'=>0.00, 'markup'=>10.00, 'markup_type'=>'A', 'round_to'=>0);
		$this->rules['314'][] = array('from_price'=>0.00, 'markup'=>10.00, 'markup_type'=>'A', 'round_to'=>0);
		$this->rules['315'] = array();
		$this->rules['315'][] = array('from_price'=>80.00, 'markup'=>5.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['315'][] = array('from_price'=>80.00, 'markup'=>5.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['315'][] = array('from_price'=>0.00, 'markup'=>10.00, 'markup_type'=>'A', 'round_to'=>0);
		$this->rules['315'][] = array('from_price'=>0.00, 'markup'=>10.00, 'markup_type'=>'A', 'round_to'=>0);
		$this->rules['7809283'] = array();
		$this->rules['7809283'][] = array('from_price'=>0.00, 'markup'=>5.00, 'markup_type'=>'A', 'round_to'=>0);

		printf("rules constructor: %s pricing rules

", count($this->rules));
	}
	
}