<?php
require_once dirname(__FILE__).'/BasePriceMarkup.class.php';

/**
 * Description of VectorMarkup
 * Created on 13-1-2012
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class VectorMarkup extends BasePriceMarkup {

	public function getHat() {
		$hat = (float) Registry::get('settings.AutoPricing.christmass_hat');
		return $hat;
	}
  public function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode) {
  	$christmasHat = $this->getHat();
   	return  ($basePrice+$christmasHat)*1.23;
  }
  
  public function statusModifier(ProductRow $product) {
    $newStatus = $product->getStatus();
    return $newStatus;
  }
}

