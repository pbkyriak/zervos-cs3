<?php

/**
 * Description of BasePriceMarkup
 * Created on 7-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class BasePriceMarkup {
  abstract function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode);
  
  public function statusModifier(ProductRow $product) {
    $newStatus = $product->getStatus();
    return $newStatus;
  }
}

