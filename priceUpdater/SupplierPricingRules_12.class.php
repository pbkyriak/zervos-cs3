<?php

class SupplierPricingRules_Sup extends SupplierPricingRules {
		
	public function __construct() {
		$this->rules = array();
		$this->rules['261'] = array();
		$this->rules['261'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['261'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['271'] = array();
		$this->rules['271'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['271'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['277'] = array();
		$this->rules['277'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['277'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['278'] = array();
		$this->rules['278'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['278'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['279'] = array();
		$this->rules['279'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['279'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['280'] = array();
		$this->rules['280'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['280'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['281'] = array();
		$this->rules['281'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['281'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809271'] = array();
		$this->rules['7809271'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809271'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809493'] = array();
		$this->rules['7809493'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809493'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809500'] = array();
		$this->rules['7809500'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809500'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809501'] = array();
		$this->rules['7809501'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809501'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809503'] = array();
		$this->rules['7809503'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809503'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809553'] = array();
		$this->rules['7809553'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809553'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809554'] = array();
		$this->rules['7809554'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809554'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809268'] = array();
		$this->rules['7809268'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809268'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['268'] = array();
		$this->rules['268'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['268'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['272'] = array();
		$this->rules['272'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['272'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['273'] = array();
		$this->rules['273'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['273'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['274'] = array();
		$this->rules['274'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['274'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['275'] = array();
		$this->rules['275'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['275'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['276'] = array();
		$this->rules['276'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['276'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809332'] = array();
		$this->rules['7809332'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809332'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['269'] = array();
		$this->rules['269'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['269'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['270'] = array();
		$this->rules['270'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['270'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809492'] = array();
		$this->rules['7809492'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809492'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809539'] = array();
		$this->rules['7809539'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809539'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809540'] = array();
		$this->rules['7809540'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809540'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809541'] = array();
		$this->rules['7809541'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809541'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809542'] = array();
		$this->rules['7809542'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809542'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809544'] = array();
		$this->rules['7809544'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809544'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809547'] = array();
		$this->rules['7809547'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809547'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809549'] = array();
		$this->rules['7809549'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809549'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809561'] = array();
		$this->rules['7809561'][] = array('from_price'=>50.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);
		$this->rules['7809561'][] = array('from_price'=>0.00, 'markup'=>-24.00, 'markup_type'=>'P', 'round_to'=>0);

		printf("rules constructor: %s pricing rules

", count($this->rules));
	}
	
}