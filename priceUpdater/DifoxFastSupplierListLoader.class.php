<?php

require_once dirname(__FILE__).'/BaseSupplierListLoader.class.php';

/**
 * Description of SunFastSupplierListLoader
 * Created on 6-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DifoxFastSupplierListLoader extends BaseSupplierListLoader {
  public $fileParts=1;
  
  public function loadFile() {
    $wantedProperties = array('category', 'name', 'sku', 'price', 'image', 'description');
    $db_conn = & Registry::get('runtime.dbs.main');
    printf("reading file: %s.\n", $this->listFile);
    if( $handle = fopen($this->listFile, "r") ) {
      $cnt =1;
      while( $data = fgetcsv($handle, 10000, ";") ) {
        $productCode = '11'.$data[2];
        $price = (float)str_replace(',','.',$data[12]);
				$stock = (int) str_replace('>','',$data[13]);
        $retailPrice = 0;
        //[imatz]
        //30-9-2013: Allagei tou stock apo to opoio tha emfanizontai ta proionta apo 1 se 3
        $stockLimitPassed = ($stock>3);
        //[/imatz]
        if( $stock && $stockLimitPassed ) {
          $pr = new FastProductRow(
                  $this->supplierId,
                  $productCode, 
                  $stock, 
                  $price, 
                  $retailPrice
                  );
          if( $pr->getCategoryId() ) {
            $this->addProductRow($pr);
            echo $cnt.' loaded in category '.$pr->getCategoryId()." <br />\n";
          }
        }
        $cnt++;
      }
    }
  }  
}

