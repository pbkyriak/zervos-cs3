<?php

/**
 * Description of SameProductArranger
 * Created on 9-11-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SameProductArranger {
  
  public function __construct() {
    
  }
  
  public static function create() {
    return new SameProductArranger();
  }
  
  public function arrange() {
    $tCodes = db_get_fields("SELECT distinct product_codeB FROM cscart_products WHERE status!='D'");
    $matchCodes = array();
    foreach($tCodes as $code) {
      $tt = explode(',', $code);
      foreach($tt as $t)
        $matchCodes[] = $t;
    }
    foreach($matchCodes as $code) {
      if( $code )
        $this->arrangeCode ($code);
    }
  }
  
  private function arrangeCode($code) {
    $products = db_get_array(
            sprintf(
                    "SELECT product_id, supplierA_price, shop_availability FROM cscart_products WHERE FIND_IN_SET('%s',product_codeB) AND status!='D' order by supplierA_price ASC",
                    $code));
		
    if( $products ) {
      $c=0;
      printf("arranging code=%s item count=%s <br />", $code, count($products));
      foreach($products as $product) {
        if( $c==0 && $product['shop_availability']<8 ) {
          db_query("UPDATE cscart_products SET status='A' WHERE product_id=".$product['product_id']);
          $c++;
        }
        else {
        	db_query("UPDATE cscart_products SET status='H' WHERE product_id=".$product['product_id']);
        }
      }
    }
  }
}

