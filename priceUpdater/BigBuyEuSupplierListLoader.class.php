﻿<?php 

class BigBuyEuSupplierListLoader extends BaseSupplierListLoader {

  public $idPrefix;
  public $download_url;
  private $brands;
  /* Domi csv 
0	ID
1	CATEGORY
2	NAME
3	ATTRIBUTE1
4	ATTRIBUTE2
5	VALUE1
6	VALUE2
7	DESCRIPTION
8	BRAND
9	FEATURE
10	PRICE
11	PVP_BIGBUY
12	PVD
13	IVA
14	VIDEO
15	EAN13
16	WIDTH
17	HEIGHT
18	DEPTH
19	WEIGHT
20	STOCK
21	DATE_ADD
22	DATE_UPD
23	IMAGE1
IMAGE2
IMAGE3
IMAGE4
IMAGE5
IMAGE6
IMAGE7
IMAGE8

  */
  
  public function loadFile() {

    printf("reading file: %s.\n", $this->listFile);
	if( !$this->bigBuyUnzipAndMergeFiles($this->listFile) ) {
		throw new Exception("Bad zip file<br />\n");
	}
	
	$this->bigBuyLoadBrands();
    if( $handle = fopen($this->listFile, "r") ) {
      $cnt =1;
      while( $data = fgetcsv($handle, 10000, ";") ) {
		if($cnt>1) {
	  //fn_print_R($data);
			$productCode = 'B'.$data[0];
			
			$ean = $data[15];
			$productCodeB = $data[0].'@@'.$ean;
			$price = (float)str_replace(',', '.',$data[12]);  // timh giannh
			$stock = ((int)$data[20]!==0 ? (int)$data[20] : 0);
			$retailPrice = 0;
			$stockLimitPassed = ($stock>=2);
			$brand =  $this->bigBuyGetBrandName($data[8]);
			$titleEl = $data[2];
			$find = array("/Ανδρικά Ρολόγια /", "/Γυναικεία Ρολόγια /", "/Unisex Ρολόγια /", "/Men's Watch /", "/Παιδικά Ρολόγια /");
            $titleEl = preg_replace($find, "", $titleEl);			
			$descr = $data[7];
			$descr = preg_replace("/Δωρεάν.*\./", '', $descr);
			$descriptionEl = $descr;
			$descriptionEn = $descr;
			$titleEl = $this->makeTitleCosmetics($titleEl, $descr);
			$titleEn = $titleEl;
			$weight = $data[19];
			$imageLink = $data[23];
			
			$extras = array();
			$extras["add_images"] = array();
			for($i=24; $i<=30; $i++) {
				if( !empty($data[$i]) ) {
					$extras["add_images"][] = trim($data[$i]);
				}
			}
			
			$categoryCode = $this->makeCategoryName( $data[1] );
			
			printf("code=%s stock=%s cate=%s brand=%s \n", $productCode, $stock, $categoryCode, $brand);
			
			fn_my_changes_update_process($this->process_key);
			if( $stock && $stockLimitPassed && $categoryCode && !empty($brand) ) {
				  $pr = new ProductRow(
					$this->supplierId,
					$categoryCode,
					$productCode,
					$productCodeB,
					$stock,
					$price,
					$titleEl,
					$titleEn,
					$descriptionEl,
					$descriptionEn,
					$imageLink,
					$categoryCode,
					0,
					$weight,
					$brand
					/*,
					array(), array(),
					$ean,
					$this->eanCategories,
					true,
					$this->process_key
					*/);
				$pr->setExtras($extras);
				if ($pr->getCategoryId()!=-1) {
					//$pr->setUpdateByEan(false);
					
						$this->addProductRow($pr);
						echo $pr->getCategoryId().' loaded in category ' . $pr->getCategoryId() . (!empty($product_data)?', ICECAT':'') . ' <br />';
				}
				else {
					echo 'No match ' . $categoryCode . '<br />';
				}

			}
			else {
				printf("No stock or no categorycode\n");
			}
		}
        $cnt++;
      }
    }
	else {
		throw new Exception("Bad file<br />\n");
	}
  }  

  private function makeCategoryName( $catids ) {
	$out = $this->supplierId.'-'. str_replace(',','-',$catids);
	return $out;
  }
  
  private function makeTitleCosmetics($title, $descr) {
	$m = [];
	$capacity = '';
	$color = '';
    if(preg_match("/<li>Χωρητικότητα:([0-9a-zA-Z ]*)<\/li>/", $descr, $m)) {
		if(isset($m[1])) {
			$capacity = trim($m[1]);
		}
	}
	$m = [];
    if(preg_match("/<li>Χρώμα:([0-9a-zA-Z ]*)<\/li>/", $descr, $m)) {
		if(isset($m[1])) {
			$color = trim($m[1]);
		}
	}
	if(!empty($color) && !empty($capacity)) {
		$title = $title .' '. $color .' '. $capacity;
	}
	return $title;
  }
  
  private function bigBuyLoadBrands() {
	$this->brands = array();
	$handle = fopen(dirname(__FILE__).'/updaterFiles/bigbuy/manufacturer-csv-el.csv', "r");
	if( $handle ) {
		while($data=fgetcsv($handle, 1000, ';') ) {
			if( (int)$data[0]!=0 && !empty(trim($data[2])) ) {
				$this->brands[$data[0]] = trim($data[2]);
			}
		}
		fclose($handle);
	}

  }
  
  private function bigBuyGetBrandName($brandId) {
	$out = '';
	if( isset($this->brands[$brandId])) {
		$out = $this->brands[$brandId];
	}
	else {
		$out = 'OEM';
	}
	return $out;
  }
 
  private function bigBuyUnzipAndMergeFiles($outfile) {
	
	
	$out = false;
	$unziped = false;
	// get the absolute path to $file
	$path = 'priceUpdater/updaterFiles/bigbuy/tmp/';
	$unziped = $this->downloadFiles();
	if( $unziped ) {
		$handle = fopen($outfile, 'w+');
		if( $handle) { 
			$fis = glob($path.'/*.csv');
			print_R($fis);
			$cnt=0;
			foreach($fis as $fi) {
			
				if($cnt==0) {
					//$content = file_get_contents($fi);
					//fputs($handle,$content);
					$handle2 = fopen($fi, "r");
					if ($handle2) {
						while (($line = fgets($handle2)) !== false) {
							fputs($handle, $line);
						}
						fclose($handle2);
					}
				}
				else {
					/*
					$lines = file($fi);
					printf("count=%s\n", count($lines));
					$t = array_shift($lines);
					printf("count=%s\n", count($lines));
					foreach($lines as $line) {
						fputs($handle, $line);
					}
					*/
					$handle2 = fopen($fi, "r");
					if ($handle2) {
						$isFirstLine = true;
						while (($line = fgets($handle2)) !== false) {
							if(!$isFirstLine) {
								fputs($handle, $line);
							}
							$isFirstLine = false;
						}
						fclose($handle2);
					}
				}
				$cnt++;
			}
			fclose($handle);
			$out = true;
		}
		else {
			printf("could NOT write to file: %s\n", $outfile);
		}
	}

	return $out;
  }
    
  private function downloadFiles() {
	$out = true;
	// path to remote file
	$remote_dir = '/files/products/csv/standard';
	$local_dir = 'priceUpdater/updaterFiles/bigbuy/tmp/';
	$ftp_server= 'www.dropshippers.com.es';// paizei ayto to ftp arages?
	$ftp_user_name= 'bbNRlBUZklBW';
	$ftp_user_pass= 'BcVwCsbfCK';

	$cnt = 0;
	array_map('unlink', glob($local_dir."*.csv"));
	// set up basic connection
	$conn_id = ftp_connect($ftp_server);
	// login with username and password
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	if($login_result) {
		$rfiles = ftp_nlist($conn_id, $remote_dir);
		foreach($rfiles as $rfile) {
			if( strpos('.',$rfile)===false ) {
				$ext = pathinfo($rfile, PATHINFO_EXTENSION);
				if($ext =='csv') {
					$lfile = $local_dir . basename($rfile);
					ftp_get($conn_id, $lfile, $rfile, FTP_BINARY, 0);
					$cnt++;
				}
			}
		}
		ftp_close($conn_id);
	}
	else {
		$out = false;
	}
	if($cnt==0) {
		$out = false;
	}
	return $out;
  }
}
