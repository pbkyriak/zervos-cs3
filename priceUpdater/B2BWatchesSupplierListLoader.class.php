<?php

class B2BWatchesSupplierListLoader extends BaseSupplierListLoader {

	public $idPrefix;

	public function filesList(){
		$categories=array(489,650,913,741,639,506,916,369,474,527,598,240,928,631,346,153,855,211,45,101,
			699,233,760,487,863,890,759,320,732,757,328,469,751,297,338,697,196,321,931,517,471,497,715,774,
			889,331,246,669,340,288,301,809,646,387,582,943,329,305,192,925,934,278,869,317,822,658,475,281,
			224,412,324,299,413,306,945,65,932,914,531,228,355,453,775,519,277,735,768,643,908,689);

		$files=array();
		foreach($categories as $cid){
			$files[$cid]="http://www.b2bwatches.co.uk/makecsv.php?category=".$cid."&productSearch=";
		}

		return($files);
	}

	// Domi csv
	/*
	//Xondrikis
	[0] => brand
    [1] => Quantity available
    [2] => Reference
    [3] => Product
    [4] => Retail price (€)
    [5] => Discount (%)
    [6] => Price ($) 
    Lianikis - Katebenei an den exei ginei login
    [0] => brand
    [1] => Quantity available
    [2] => Reference
    [3] => Product
    [4] => Retail price (€)
	*/
	public function loadFile() {
		$urls=$this->filesList();
		//Debug
		//$brands=array();
		// /Debug
		foreach($urls as $cid=>$url){
      		$fn=$this->listFile.$cid.'.csv';
      		echo("Reading file: $fn<br />\n");
      		$handle = fopen($fn, "r");
      		if ($handle) {
      			while( $data = fgetcsv($handle, 10000, ";") ) {
      				if($data[0]=="brand")
	     				continue;

	     			if($data[1]<5){
	     				echo("Limited Stock<br />\n");
	     				continue;
	     			}
	     			
	     			if($data[6]<50){
	     				echo("Low price<br />\n");
	     				continue;
	     			}

	     			if(empty(trim($data[3]))){
	     				echo("Title is empty<br />\n");
	     				continue;
	     			}

	     			$data[0]=str_replace("'", "", $data[0]);
	     			$data[3]=str_replace("'", "", $data[3]);

	     			//Debug
	     			//echo("<pre>");
	     			//print_r($data);
	     			//echo("</pre>");
	     			//$brands[$data[0]][]=$data[2].') '.$data[3];
	     			// /Debug

	     			$categoryCode = $this->supplierId; //.'-'.$data[0];	     			

	     			$titleEl = $data[3]. ' '.$data[2];
		     		$titleEn = $titleEl;
		     		$descriptionEl = $data[3];
		     		$descriptionEn = $descriptionEl;
		     		$brand = $data[0];
		     		$stock = $data[1];
		     		$price = (float)str_replace(",",".",$data[6]);
		     		$productCode = $data[2];
		     		if(!empty($this->idPrefix))
	     				$productCode=$this->idPrefix.$productCode;

	     			$productCodeB = "";
	        		$imageLink = "";
	        		$weight = 0;
        			$pr = new ProductRow(
			            $this->supplierId,
			            $categoryCode,
			            $productCode,
			            $productCodeB,
			            $stock,
			            $price,
			            $titleEl,
			            $titleEn,
			            $descriptionEl,
			            $descriptionEn,
			            $imageLink,
			            0,0,
			            $weight,
						$brand);
	        		if ($pr->getCategoryId()!=-1) {
	            		$this->addProductRow($pr);
	            		echo "Loaded in category " . $pr->getCategoryId() . "<br />\n";
	          		}else{
	            		echo "No match " . $categoryCode . "<br />\n";
	          		}
      			}
      		}else{
	    		throw new Exception("Bad file<br />\n\n");
      		}
      	}
      	//Debug
      	/*$counter=1;
      	foreach($brands as $brand=>$products){
      		echo($counter.') '.$brand.'-'.count($products).'<br />');
      		$counter++;
      	}
      	exit;*/
      	// /Debug
	}
}