<?php

/**
 * Description of FastProductRow
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FastProductRow extends ProductRow {
  public $noCategoryFound=false;
  
  public function __construct($supplierId, $productCode, $stock, $price, $suggestedRetail) {
    $categoryCode = '';
    $productCodeB='';
    $titleEl=''; $titleEn=''; $descriptionEl=''; $descriptionEn=''; $imageLink=''; $category='';
    
    $this->setSupplierId($supplierId);
    $this->setCategoryCode($categoryCode);
    $this->setProductCode($productCode);
    $this->setProductCodeB($productCodeB);
    $this->setStock($stock);
    $this->setPrice($price);
    $this->setTitleEl($titleEl);
    $this->setDescriptionEl($descriptionEl);
    $this->setTitleEn($titleEl);
    $this->setDescriptionEn($descriptionEl);
    $this->setImageLink($imageLink);
    $this->setProductId($this->matchProductCode($productCode, $supplierId));
    $this->setCategoryId($this->getProductCategoryId($this->getProductId()));
    $this->status = 'A';
    $this->suggestedRetail = $suggestedRetail;
  }
  
  private function getProductCategoryId($productId) {
    $categoryId = db_get_field("SELECT category_id FROM cscart_products_categories WHERE product_id=?i and link_type='M'", $productId);
    if( $categoryId ) {
      return $categoryId;
    }
    else {
      $this->noCategoryFound = true;
      return null;
    }
  }
  
}

?>
