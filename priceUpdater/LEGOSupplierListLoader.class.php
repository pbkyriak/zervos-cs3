<?php 

include_once(__DIR__."/PHPExcel/PHPExcel.php");

class LEGOSupplierListLoader extends BaseSupplierListLoader {

	public $idPrefix;

	public function loadFile() {
		// Domi xls
		/*
			[A] => Κωδικός
			[B] => Περιγραφή
			[C] => Χον.Τιμή
			[D] => EAN

			[A] => Κωδικός
			[B] => Περιγραφή
			[C] => Υπόλοιπο
			[D] => Χον.Τιμή
			[E] => Barcode
 		*/
		$inputFileType = 'Excel5';

		/**  Create a new Reader of the type defined in $inputFileType  **/
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		/**  Load $inputFileName to a PHPExcel Object  **/
		$objPHPExcel = $objReader->load($this->listFile);

		$sheetData = $objPHPExcel->getSheet(0)->toArray(null,true,true,true);
		foreach( $sheetData as $dataRow ) {
			if($dataRow["A"]=="Κωδικός")
				continue;

			$categoryCode = $this->supplierId.'-Lego';
			$brand="LEGO";
			$titleEl=trim($dataRow["B"]);
			$titleEn=$titleEl;
			$price = (float)$dataRow["D"];
			$productCode = $dataRow["A"];
        	$productCodeB = $dataRow["E"];
        	$imageLink = "";
        	if(is_numeric(trim($dataRow["C"]))){
        		$stock=(int)trim($dataRow["C"]);
        	}elseif(strtolower(trim($dataRow["C"]))=="in stock"){
        		$stock=5;
        	}else{
        		$stock=0;
        	}
        	$stockLimitPassed = ($stock>0);
        	printf("category =%s stock=%s price=%s <br />\n", $categoryCode, $stock, $price);
        	$descriptionEl = trim($dataRow["B"]);
        	$descriptionEn = $descriptionEl;
        	$eans=array(trim($dataRow["B"]));
        	if ($categoryCode && $price && $stockLimitPassed) {
        		$pr = new ProductRow(
		            $this->supplierId,
		            $categoryCode,
		            $productCode,
		            $productCodeB,
		            $stock,
		            $price,
		            $titleEl,
		            $titleEn,
		            $descriptionEl,
		            $descriptionEn,
		            $imageLink,
		            $categoryCode,
					0,0,
					$brand);
        		if ($pr->getCategoryId()!=-1) {
        			if(!empty($eans)){
	        			$icecat_params=array(
			                "use_upc_from_site"=>false,
			                "ex_weight"=>false,
			                "supplier_id"=>$this->supplierId
			            );
	              		if($pr->getCategoryId()==UPDATER_TABLETS_CATEGORY_ID){
	                		$icecat_params["ex_screen_size"]=false;
	                		$icecat_params["ex_external_memory"]=false;
	                		$icecat_params["ex_memory_ram"]=false;
	              		}
	              		$product_data=$this->getDataFromIceCat($pr->getProductId(),$eans,$productCodeB,$pr,$icecat_params);
	              	}
            		$this->addProductRow($pr);
            		echo "loaded in category " . $pr->getCategoryId() . (!empty($product_data)?', ICECAT':'') . " <br />\n";
          		}else{
            		echo "No match " . $categoryCode . "<br />\n";
          		}
        	}else{
          		echo "No categoryCode or no stock or no price<br />\n";
        	}
		}
	}
}