<?php
require_once dirname(__FILE__).'/BasePriceMarkup.class.php';
//use Tygh\Registry;

class NexionMarkup extends BasePriceMarkup {
	public function getHat() {
		$hat = (float) Registry::get('settings.AutoPricing.christmass_hat');
		return $hat;
	}
  //Ginete override an iparxoun pracing rules se katigories
  //To override ginete apo PricingRules->applyToPrice
  //Mesa sto productrow->getProductPriceArray
	public function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode) {
  		$christmasHat = $this->getHat($categoryCode);

   		return  ($basePrice*1.50)*1.23;
  	}

  	public function statusModifier(ProductRow $product) {
    	$newStatus = $product->getStatus();
    	return $newStatus;
  	}
}