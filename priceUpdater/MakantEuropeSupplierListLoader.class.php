<?php 

class MakantEuropeSupplierListLoader extends BaseSupplierListLoader {

	public $idPrefix;
	// Domi csv
	/*
	// Se kapoia proionta to products_description_specifications exei poli logitero keimeno apo to product_description
	Array(
	    [0] => products_id
	    [1] => categorie_id
	    [2] => categorie_path
	    [3] => product_name
	    [4] => product_description
	    [5] => products_description_specifications
	    [6] => products_description_shipment
	    [7] => product_price
	    [8] => imageSoucre_1
	    [9] => image800_1
	    [10] => image320_1
	    [11] => image100_1
	    [12] => imageSoucre_2
	    [13] => image800_2
	    [14] => image320_2
	    [15] => image100_2
	    [16] => imageSoucre_3
	    [17] => image800_3
	    [18] => image320_3
	    [19] => image100_3
	    [20] => products_ean
	    [21] => available
	    [22] => products_commodity_code
	    [23] => scaled_price_1_quantity
	    [24] => scaled_price_1_unitprice
	    [25] => scaled_price_2_quantity
	    [26] => scaled_price_2_unitprice
	    [27] => scaled_price_3_quantity
	    [28] => scaled_price_3_unitprice
	    [29] => products_quantity
	    [30] => products_weight
	    [31] => products_mpn
	    [32] => min_order_quantity
	    [33] => manufacturer
	    [34] => package_tpye
	)
	*/

	public function loadFile() {
	    $handle = fopen($this->listFile, "r");
	    //$categories=array();//TMP
	    if ($handle) {
	     	while( $data = fgetcsv($handle, 10000, ";") ) {
	     		if($data[0]=="products_id")
	     			continue;

	     		if($data[21]==0 || $data[29]==0 || $data[32]>1 || empty(trim($data[5])) || $data[29]<=1){
	     			echo "No available or min_order_qty>1 or No Description or stock<1<br />\n";
	     			continue;
	     		}
	     		
	     		//$categories[$data[1]]=$data[2];//TEMP

	     		$categoryCode = $this->supplierId.'-'.$data[1];

	     		$titleEl = $data[3];
	     		$titleEn = $titleEl;
	     		$descriptionEl = $data[5];
	     		$descriptionEn = $descriptionEl;
	     		$brand = $data[33];
	     		$stock = $data[29];
	     		$price = (float)$data[7];
	     		$productCode = $data[0];
	     		if(!empty($this->idPrefix))
	     			$productCode=$this->idPrefix.$productCode;

        		$productCodeB = $data[31];
        		$imageLink = $data[8];
        		$weight = $data[30];
        		$pr = new ProductRow(
		            $this->supplierId,
		            $categoryCode,
		            $productCode,
		            $productCodeB,
		            $stock,
		            $price,
		            $titleEl,
		            $titleEn,
		            $descriptionEl,
		            $descriptionEn,
		            $imageLink,
		            0,0,
		            $weight,
					$brand);
        		if ($pr->getCategoryId()!=-1) {
            		$this->addProductRow($pr);
            		echo "Loaded in category " . $pr->getCategoryId() . "<br />\n";
          		}else{
            		echo "No match " . $categoryCode . "<br />\n";
          		}
	     	}

	     	//TEMP
	     	/*ksort($categories);
	     	fn_print_r($categories);
	     	$csvhandler=fopen(DIR_ROOT.'/xml_feed/makant_categories.csv', "w");
	     	fn_echo("csvhandler: ".$csvhandler."<br />");
	     	fn_echo(DIR_ROOT."/xml_feed/makant_categories.csv<br />");
	     	fputcsv($csvhandler, array("makant_category_id","makant_category_path","store_category_id"),";");
	     	foreach($categories as $category_id=>$category){
	     		fputcsv($csvhandler, array($category_id,$category,""),";");
	     	}
	     	fclose($csvhandler);
	     	exit;*/
	    }else{
	    	throw new Exception("Bad file<br />\n\n");
	    }
	}
}