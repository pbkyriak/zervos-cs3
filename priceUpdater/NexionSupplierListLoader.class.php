<?php 
//
// Ioannis Matziaris [imatz]
// imatzgr@gmail.com
// January 2015
//

use Tygh\Registry;

include_once(__DIR__."/PHPExcel/PHPExcel.php");

class NexionSupplierListLoader extends BaseSupplierListLoader {
	public $idPrefix;
	private $stockLimit=3;
	
	// Domi xls
	/*
	PROSOXH!!! to filo exei stin arxh 1 grammh prin arxisoun ta proionta
		[A] => Brand
	    [B] => Product name	
	    [C] => Product code
	    [D] => Description
	    [E] => Category
	    [F] => Retail price
	    [G] => Retail price w/o VAT
	    [H] => Price buy
	    [I] => Image
	    [J] => Image link
	    [K] => Stock
     */   
   
	public function loadFile() {

		$out = array();

		$objPHPExcel = PHPExcel_IOFactory::load($this->listFile);
		for($i=0;$i<=0;$i++){
			$sheetData = $objPHPExcel->getSheet($i)->toArray(null,false,true,true);
			foreach( $sheetData as $dataRow ) {

				$product=trim($dataRow["B"]);

				if(!empty($product) && $product!="ΟΝΟΜΑ") {

					$stock=(int)trim($dataRow["K"]);

					if(!is_numeric($stock)) {
						printf("Non numeric stock %s\n",$stock);
						$stock=0;
					}

					if($stock<$this->stockLimit){
	     				echo "Stock " . $stock . " less the min ". $this->stockLimit . "<br />\n";
	     				continue;
	     			}
					
					//$categoryToUtf=iconv("Windows-1253", "UTF-8", trim($dataRow["E"])); 
					$categoryCode = trim($dataRow["E"]);
					if( $categoryCode =='Καλώδια' ) {
						$categoryCode='Cables';
					}
   					$categoryCode = $this->supplierId.'-'.$categoryCode;
					echo $categoryCode.'<br />';
					$brand=trim($dataRow["A"]);
					
					if( strpos($product, $brand)===false ) {
						$product = $brand.' '.$product;
					}
     				$titleEl = $product;
	     			$titleEn = $titleEl;	

	     			$descriptionEl = trim($dataRow["D"]);
	     			$descriptionEn = $descriptionEl;	

	     			$productCode = $dataRow["C"];
	     			if(!empty($this->idPrefix))
	     				$productCode=$this->idPrefix.$productCode;	

	     			$productCodeB=$dataRow['C'];
                    $pricea = str_replace('€','',$dataRow["H"]); 
    				$price=(float)$pricea;
					if(empty($price)){
	     				echo $dataRow["C"].'  '. $price . '  ' ."Zero price<br />\n";
	     				continue;
	     			}

					$imageLink = $dataRow["J"];
					
	     			$weight = 0;
	     			$eans=array();
	     			if($i<=1){
						//Gia na mpoun ta proionta se katigories pou exoun ean prepei na ginei pseftiko ean
	     				$ean=fn_crc32($titleEl);
	     				$eans[]=$ean;
						$eanOverride=true;
						$eancategories=array();
					}else{
						$ean="";
						$eanOverride=false;
						$eancategories=array();
					}					
        			
					fn_my_changes_update_process($this->process_key);
					$pr = new ProductRow(
			            $this->supplierId,
			            $categoryCode,
			            $productCode,
			            $productCodeB,
			            $stock,
			            $price,
			            $titleEl,
			            $titleEn,
			            $descriptionEl,
			            $descriptionEn,
			            $imageLink,
			            0,0,
			            $weight,
						$brand,
						$this->process_key
					);
					
					if ($pr->getCategoryId()!=-1) {          	
						if( $stock && $price ) {
							/*
							$icecat_params=array(
								"use_upc_from_site"=>false,
								"ex_weight"=>false,
								"supplier_id"=>$this->supplierId,
								// "ex_title"=>false,
							);
							$icecat_params["ex_screen_size"]=false;
							$icecat_params["ex_external_memory"]=false;
							$icecat_params["ex_memory_ram"]=false;
							$icecat_params["ex_os"]=false;
							$icecat_params["ex_num_cpu_cores"]=false;
							$icecat_params["ex_camera_resolution"]=false;             
							$product_data=$this->getDataFromIceCat($pr->getProductId(),$eans,$productCodeB,$pr,$icecat_params);
							*/
							$this->addProductRow($pr);
							//echo 'loaded in category ' . $pr->getCategoryId() . (!empty($product_data)?', ICECAT':'') . "<br />\n";
							echo 'loaded in category ' . $pr->getCategoryId() . "<br />\n";
						}
						else {
							echo "No stock or no price <br />\n";
						}	            
					}
					else {
						echo 'No match ' . $categoryCode . "<br />\n";
					}
					
				}
			}
		}
	}
}