<?php 

class YukatelSupplierListLoader extends BaseSupplierListLoader {

  public $idPrefix;
    
  public function loadFile() {
  	
    echo 'reading file: ' . $this->listFile;
    $products = array();
    if (($handle = fopen($this->listFile, "r")) !== FALSE) {
    	$row = 0;
      while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
      	if( count($data)>3 )
      		$products[] = $data;
      	$row++;
    	}
      if( !$products ) {
      	throw new Exception("no products");
      }
      echo 'products node found<br />';
      $row = 0;
      foreach ($products as $product) {
	        echo 'importing product '.$row++.'  sku='.$product[1];
          $categoryId = $product[0];
          $productId = $product[1];
          $title = $product[2];
          $stock = (int)$product[3];
          $stockLimitPassed = ($stock>1);
          $price = (float)$product[4];	
	        if($categoryId && $stock && $price && $stockLimitPassed) {
	        	$pr = new ProductRow(
                    $this->supplierId,
                    '', 
                    '', 
                    md5($title), 
                    $stock, 
                    $price, 
                    $title, 
                    $title, 
                    '', 
                    '', 
                    '',
                    '');
            $pr->setProductId($productId);
            $pr->setCategoryId($categoryId);
            if( $pr->getCategoryId()!=-1 ) {
              $this->addProductRow($pr);
              echo 'loaded in category '.$pr->getCategoryId().' <br />';
            }
            else
              echo 'No match '.$categoryCode.'<br />';
	        }
	        else
	        	echo 'No categoryCode or no stock or price=0 <br />';
	    }
    } else {
      throw new Exception('Bad file<br />');
    }
  }

}