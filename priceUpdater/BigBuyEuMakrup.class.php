﻿<?php
require_once dirname(__FILE__).'/BasePriceMarkup.class.php';
//use Tygh\Registry;

class BigBuyEuMakrup extends BasePriceMarkup {
	public function getHat() {
		$hat = (float) Registry::get('settings.AutoPricing.christmass_hat');
		return $hat;
	}
  //Ginete override an iparxoun pracing rules se katigories
  //To override ginete apo PricingRules->applyToPrice
  //Mesa sto productrow->getProductPriceArray
	public function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode) {
  		$christmasHat = $this->getHat($categoryCode);
        $profit=0.11;
   		return  ($basePrice*(1+$profit)+$christmasHat)*1.24;
  	}

  	public function statusModifier(ProductRow $product) {
    	$newStatus = $product->getStatus();
    	return $newStatus;
  	}
}