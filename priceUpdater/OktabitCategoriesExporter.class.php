<?php 

class OktabitCategoriesExporter {

  private $categories = array();
  private $handle, $idPrefix;
  
  public function __construct($idPrefix) {
    $this->idPrefix = $idPrefix;
  }
  
  public function export($fn, $outFn) {
    $xml = simplexml_load_file($fn);
    if ($xml) {
      echo "finding products node<br />\n";
      foreach ($xml->children() as $product) {
        $cat = (string)$product->category;
        $scat = (string)$product->subcategory;
        if( !isset($this->categories[$cat]) ) {
          $this->categories[$cat] = array();
        }
        if( !in_array($scat, $this->categories[$cat]))
          $this->categories[$cat][] = $scat;
      }
      $this->writeToFile($outFn);
    } else {
      throw new Exception("Bad xml file<br />\n");
    }  
  }

  private function writeToFile($fn) {
    $ident = 1;
    $this->handle = fopen($fn, "w+");
    fwrite($this->handle, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
    fwrite($this->handle, "<structure>\n");
    fwrite($this->handle, sprintf("%s<categories>\n", str_repeat("\t", $ident)));
    
    if (count($this->categories)) {
      $ident++;
      foreach ($this->categories as $category => $subCategories) {
        $this->openCategoryNode($category, $category, $ident);
        if( count($subCategories) ) {
          $ident++;
          fwrite($this->handle, sprintf("%s<subcategories>\n", str_repeat("\t", $ident)));
          foreach($subCategories as $subCategory) {
            $ident++;
            $this->openCategoryNode($subCategory, $subCategory,$ident);
            $this->closeCategoryNode($ident);
            $ident--;
          }
          fwrite($this->handle, sprintf("%s</subcategories>\n", str_repeat("\t", $ident)));
        }
        $ident--;
        $this->closeCategoryNode($ident);
        
      }
    }
    fwrite($this->handle, sprintf("%s</categories>\n", str_repeat("\t", $this->ident)));
    fwrite($this->handle, "</structure>\n");
    fclose($this->handle);

  }
  
  private function openCategoryNode($id, $title,$ident) {
    fwrite($this->handle, sprintf("%s<category id=\"%s%s\">\n", str_repeat("\t", $ident), $this->idPrefix, $id));
    fwrite($this->handle, sprintf("%s\t<title><![CDATA[%s]]></title>\n", str_repeat("\t", $ident), $title));
  }

  private function closeCategoryNode($ident) {
    fwrite($this->handle, sprintf("%s</category>\n", str_repeat("\t", $ident)));
  }

}