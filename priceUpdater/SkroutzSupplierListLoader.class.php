<?php

require_once dirname(__FILE__).'/BaseSupplierListLoader.class.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SkroutzSupplierListLoader
 * Created on 6-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SkroutzSupplierListLoader extends BaseSupplierListLoader {
  public $fileParts=1;
  
  public function loadFile() {
    $wantedProperties = array('category', 'name', 'sku', 'price', 'image', 'description');
    $db_conn = & Registry::get('runtime.dbs.main');
    printf("reading file: %s.", $this->listFile);
    //for($fid=1; $fid<=$this->fileParts; $fid++) {
      printf("reading file: %s.", $this->listFile);
      $use_errors = libxml_use_internal_errors(true);
      $xml = simplexml_load_file($this->listFile);
      printf("after read");
      if ($xml) {
        echo 'finding products node<br />';
        foreach($xml->children() as $node ) {
          echo $node->getName().'<br />';
          if( $node->getName()=='products' ) {
            $products = $node;
            break;
          }
        }
        if( !$products ) {
          throw new Exception("no products");
        }
        echo 'products node found<br />';
        $row = 0;
        $pingLoop = 0;
        foreach ($products as $product) {
          $pingLoop++;
          if( $product->getName()=='product' ) {
            echo 'importing product '.$row++.'  sku='.(string)$product->sku;

            $category= (string)$product->category;
            $categoryCode= (string)$product->category['id'];
            $title = (string)$product->name;
            $title = str_replace('Sony-Ericsson', 'Sony Ericsson', $title);
            $title = str_replace('(Vodafone)', '', $title);
            $title = str_replace(
              array('schwarz', 'Schwarz', 'silber', 'anthrazit', 'arktikgrau', 'graphit',  'dunkelgrau', 'hellgrau', 'perlweia', 'Alu-Silber','weia'), 
              array('black', 'black', 'silver', 'anthracite', 'white', 'graphite', 'dark gray', 'light gray', 'white', 'Aluminium', 'white'), 
              $title);
            $title = preg_replace('/\sQ$/','', $title);
            $title = preg_replace('/\sO2$/','', $title);
						$title = preg_replace('/\s{1}EU$/','', $title);
						$title = preg_replace('/\s{1}\*EU\*$/','', $title);
						$title = preg_replace('/DE$/','EU', $title);
            $title = trim($title);

            if( in_array($categoryCode, array(364,411,412,413)) ){
              $categoryCode = trim(sprintf("409-%s", strtolower(substr($title, 0, strpos($title,' ')))));
              printf("??>>>>>>>>>>>>>>>>>> category code modified = %s <br />",$categoryCode);
            }

            if( $categoryCode=='357' ){
              $categoryCode = trim(sprintf("357-%s", strtolower(substr($title, 0, strpos($title,' ')))));
              printf("??>>>>>>>>>>>>>>>>>> category code modified = %s <br />",$categoryCode);
            }

            $stock=0;
            if( $product->instock=='Y' )
              $stock = (int)$product->amount;
            $price= (float)$product->price;
            $suggestedRetail = (float)$product->retail_price;
            $productCode = (string)$product->sku;
            $productCodeB = (string)$product->ean;
            $image = (string)$product->image;
            
            if(strpos($image, 'no_image.gif')!==false ) // && !$this->matchProductCode($productCode,$this->supplierId ) ) 
              $stock = 0;
            if(strpos($image, 'no_image.gif') )
              $image = '/home/vnp/public_html/images/no_image.gif';
            else {
            	if( !$this->checkRemoteFile($image) )
            		$stock = 0;
            }
            //$image = str_replace("agoraseto.gr/images/detailed","agoraseto.gr/images/detailed_clean", $image);
            //$description = (string)$product->description;
            //$description = mb_convert_encoding($description, 'ISO-8859-7', 'UTF-8');
            $description = $title;
            //if( $categoryCode =='357' && strpos( $title, 'Apple')===false ) {
            //  $stock = 0;
            //}
            if( in_array($categoryCode, array('340')) )
              $description =(string)$product->description. 
                '<br /><p>Το προϊόν αυτό είναι refurbished, δηλαδή μεταχειρισμένο που έχει ανακατασκευαστεί ώστε να είναι καθαρό και σε άψογη λειτουργία. Συνοδεύεται από '.($categoryCode=='341' ? '1 χρόνο' : '2 χρόνια' ).' γραπτή εγγύηση.</p>';
            printf("categoryCode = %s stockk=%s <br />", $categoryCode, $stock);
            // check stock limit
            $stockLimitPassed = ($stock>5);
            if( in_array($categoryCode, array('215', '847', '846', '1363', '1059', '1058', '1057', '1056', '1054', '1055')) ) {	// security
            	$stockLimitPassed = true;
            }
/*
                && ( in_array($categoryCode, array('184','186','185','216','206','207','214','205','284','286','287','288','325','404', '1291', '1292'))
                    || in_array($categoryCode, array('340', '357', '356', '215', '207', '364', '350', '351', '404', '411', '412', '413', '325', '1359', '1376', '1377'))
                    || in_array($categoryCode, array('555', '879', '880', '881', '882', '883', '884', '874', '875', '876', '877', '878', '885', '886', '1349','998','993','994','997','990','989','1207','995','996','1208','991','992','987','988', '415', '416'))
                    || in_array($categoryCode, array('215', '847', '846', '1363', '1059', '1058', '1057', '1056', '1054', '1055'))	// tablet, tv, tv, ssd drives, security, security, security, security, security, security
                    || in_array($categoryCode, array('1177','1178','1179','1236','1358'))	// networking
                    || substr($categoryCode, 0,4)=='409-'
                    || substr($categoryCode, 0,4)=='357-'
                   )
*/            
            if( strpos($productCode, '14')===0 )	// block wave
            	$stockLimitPassed=false;
            	
            if ($categoryCode 
                && $stockLimitPassed
                && $price 
                
              ) {
              	// && substr($productCode, 0,2)!='14'
              	// '378','957', '958','373', '377',	// accessories
              	// '1088', // bateries for cameras and photo
              $skip = false;
              /*
              if( $categoryCode == '215' && (strpos($title,'Apple')===false) )  {
              	printf("Not apple %s %s<br />",$title,strpos($title,'Apple'));
                $skip = true;
              }
              */
              if( !$skip ) {
                $pr = new ProductRow(
                        $this->supplierId,
                        $categoryCode, 
                        $productCode, 
                        $productCodeB, 
                        $stock, 
                        $price, 
                        $title, 
                        $title, 
                        $description, 
                        $description, 
                        $image,
                        $category,
                        $suggestedRetail
                        );
                if( $pr->getCategoryId()!=-1 ) {
                  $this->addProductRow($pr);
                  echo 'loaded in category '.$pr->getCategoryId().' <br />';
                }
                else
                  echo 'No match '.$categoryCode.'<br />';
              }
            }
            else
              echo 'No categoryCode or no stock <br />';
              
						/* check if server is alive */
						if( $pingLoop>500 ) {
							$pingLoop=0;
							if (!mysqli_ping($db_conn)) {
						    printf ("Error: %s\n", mysqli_error($db_conn));
						    die(1);
							}
						}
          }
        }
      } else {
        printf("xml loader error <br />");
        foreach(libxml_get_errors() as $error) {
          printf("error: %s <br />", $this->display_xml_error($error, $xml));
        }

        libxml_clear_errors();
        libxml_use_internal_errors($use_errors);
        throw new Exception('Bad xml file<br />');
      }
    //}
  }
  
  public function matchProductCode($productCode) {
    $id= -1;
    if ($productCode)
      $id= db_get_field(sprintf("SELECT product_id FROM cscart_products WHERE product_code='%s'", $productCode));
    if( $id )
      return $id;
    else
      return -1;
  }
  
  function display_xml_error($error, $xml)
  {
      $return  = $xml[$error->line - 1] . "<br />";
      $return .= str_repeat('-', $error->column) . "^<br />";

      switch ($error->level) {
          case LIBXML_ERR_WARNING:
              $return .= "Warning $error->code: ";
              break;
           case LIBXML_ERR_ERROR:
              $return .= "Error $error->code: ";
              break;
          case LIBXML_ERR_FATAL:
              $return .= "Fatal Error $error->code: ";
              break;
      }

      $return .= trim($error->message) .
                 "<br />  Line: $error->line" .
                 "<br />  Column: $error->column";

      if ($error->file) {
          $return .= "<br />  File: $error->file";
      }

      return "$return\n\n--------------------------------------------\n\n";
  }
  
}

