<?php

/**
 * Description of PriceListFetcherUniqueShop
 * Created on 22/8/2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PriceListFetcherHttpFile {

  public static function create() {
    return new PriceListFetcherHttpFile();
  }
  
  public function fetch($url, $fn, $username="", $password="") {
    // INIT CURL
    $ch = curl_init();

    # Setting CURLOPT_RETURNTRANSFER variable to 1 will force cURL
    # not to print out the results of its query.
    # Instead, it will return the results as a string return value
    # from curl_exec() instead of the usual true/false.
    $userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
    //'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
    $cookieJar = 'cookies.txt';         
    curl_setopt($ch,CURLOPT_COOKIEJAR, $cookieJar); 
    curl_setopt($ch,CURLOPT_COOKIEFILE, $cookieJar);
    curl_setopt($ch,CURLOPT_AUTOREFERER,true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    if(!empty($username) && !empty($password)){
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    }
    $content = curl_exec($ch);
    //Elegxos an to arxeio iparxei
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
printf("htpCde=%s\n", $httpCode);
printf("file=%s\n", $fn);	
printf("len=%s\n", strlen($content));
    if($httpCode==200){
        file_put_contents($fn,$content);
		chmod($fn, 0666);
    }
    // CLOSE CURL
    curl_close($ch);
  }

}

