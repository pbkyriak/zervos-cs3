<?php

require_once dirname(__FILE__).'/BaseSupplierListLoader.class.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SkroutzSupplierListLoader
 * Created on 6-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class BatariaSupplierListLoader extends BaseSupplierListLoader {
  
  public function loadFile() {
    $wantedProperties = array('category', 'name', 'sku', 'price', 'image', 'description');
    
    echo 'reading file: ' . $this->priceListFilename;
    $xml = simplexml_load_file($this->listFile);
    if ($xml) {
      echo 'finding products node<br />';
      foreach($xml->children() as $node ) {
      	echo $node->getName().'<br />';
      	if( $node->getName()=='products' ) {
      		$products = $node;
      		break;
      	}
      }
      if( !$products ) {
      	throw new Exception("no products");
      }
      echo 'products node found<br />';
      $row = 0;
      foreach ($products as $product) {
      	
      	if( $product->getName()=='product' ) {
	        echo 'importing product '.$row++.'  sku='.(string)$product->sku;

          $category= (string)$product->category;
          $categoryCode= $this->supplierId.'-'.(string)$product->category[id];
          $title = (string)$product->name;
          $stock=0;
          if( $product->instock=='Y' )
            $stock = rand(2,5);
          $stockLimitPassed = ($stock>1);
          $price= (float)$product->price;
          $productCode = (string)$product->sku;
          $productCodeB = (string)$product->ean;
          $image = (string)$product->image;
          
          if(strpos($image, 'no_image.gif')!==false ) // && !$this->matchProductCode($productCode,$this->supplierId ) ) 
          	$stock = 0;
          if(strpos($image, 'no_image.gif') )
          	$image = IMAGES_STORAGE_WEB_DIR.'/no_image.gif';
          $description = (string)$product->description;
          if( empty($description) )
            $description = $title;
          printf("categoryCode = %s stockk=%s <br />", $categoryCode, $stock);
	        if ($categoryCode && $productCode && $stockLimitPassed 
	        		&& $stock 
	        	) {
	        	$skip = false;
	        	if( !$skip ) {
		        	$pr = new ProductRow(
	                    $this->supplierId,
	                    $categoryCode, 
	                    $productCode, 
	                    $productCodeB, 
	                    $stock, 
	                    $price, 
	                    $title, 
	                    $title, 
	                    $description, 
	                    $description, 
	                    $image,
	                    $category);
	            if( $pr->getCategoryId()!=-1 ) {
	              $this->addProductRow($pr);
	              echo 'loaded in category '.$pr->getCategoryId().' <br />';
	            }
	            else
	              echo 'No match '.$categoryCode.'<br />';
	          }
	        }
	        else
	        	echo 'No categoryCode or no stock <br />';
	      }
      }
    } else {
      throw new Exception('Bad xml file<br />');
    }
  }
  
  public function matchProductCode($productCode) {
    $id= -1;
    if ($productCode)
      $id= db_get_field(sprintf("SELECT product_id FROM cscart_products WHERE product_code='%s'", $productCode));
    if( $id )
      return $id;
    else
      return -1;
  }
}

