<?php

/**
 * Description of SkroutzSupplierListLoader
 * Created on 16-11-2012
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class NakasSupplierListLoader extends BaseSupplierListLoader {
  
  public $idPrefix;
  /*
    Pedia xml:
    AtlantisCode:
    Title:
    Description:
    OnlinePrice:
    RetailPrice:
    ImageLarge:
    ImageSmall:
    DiscountCategory:
    BarCode:
    Recycle:
    Availability: Y i N
    Availability: N i O  04/07/2017

  */
  public function loadFile() {
    $use_errors = libxml_use_internal_errors(true);
    $xml = simplexml_load_file($this->listFile);
    if ($xml) {
      foreach ($xml->children() as $product) {
		//continue;   // apenergopoihsh proionton 11/11/2015
        if(empty($product)){
          continue;
        }
		$checkminprice = $product->OnlinePrice;
		if($checkminprice < 30){ // andreas 29/10/2015 proionta kato apo 30 na mhn mpainoun sto katasthma
          continue;
        }
        $category= (string)$product->AtlantisCode;
        $categoryCode= $this->supplierId.'-'.$category[0];
        $title = (string)$product->Title;
        $titleEl = trim($title);
        $titleEn = $titleEl;
        $availability = (string)$product->Availability;
        if( $availability=='Ν' ){
          $stock=5;            
		}
        if( $availability=='Ο' ){
          $stock=0;
		}
        $price = (float)str_replace(',','.',$product->OnlinePrice);
        $productCode = (string)$product->AtlantisCode;
        $productCodeB = (string)$product->BarCode;
        $image = (string)$product->ImageLarge;
        //if( !$this->checkRemoteFile($image) )
          //$stock = 0;
        $descriptionEl = (string)$product->Description;
        $descriptionEn = $descriptionEl; 
        $stockLimitPassed = ($stock>1); 
        $brand="";
        $weight =0;
        printf("pcode=%s category =%s stock=%s aval=%s price=%s weight=%s <br />\n", $productCode, $categoryCode, $stock, $availability, $price, $weight);
        if ($categoryCode && $stockLimitPassed && $price) {
          fn_my_changes_update_process($this->process_key);    
          $pr = new ProductRow(
            $this->supplierId,
            $categoryCode, 
            $productCode, 
            $productCodeB, 
            $stock, 
            $price, 
            $titleEl, 
            $titleEn, 
            $descriptionEl, 
            $descriptionEn, 
            $image,
            $categoryCode,
            0,
            $weight,
            $brand,//[imatz]
            $this->process_key
          );
          if( $pr->getCategoryId()!=-1 ) {
            $this->addProductRow($pr);
            echo 'loaded in category '.$pr->getCategoryId().' <br />';
          }else{
            echo 'No match '.$categoryCode.'<br />';
          }
        }else{
          echo 'No categoryCode or no stock or no price<br />';
        }
      }     
    }else{
      throw new Exception("Bad xml file<br />\n");
    }
  }  
}