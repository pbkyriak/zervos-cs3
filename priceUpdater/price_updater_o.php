<?php
require_once(dirname(__FILE__).'/priceUpdater/PriceListUpdater.class.php');
require_once(dirname(__FILE__).'/priceUpdater/OktabitCategoriesExporter.class.php');
require_once(dirname(__FILE__).'/priceUpdater/OktabitSupplierListLoader.class.php');
require_once(dirname(__FILE__).'/priceUpdater/OktabitMarkup.class.php');

error_reporting(E_ALL);

DEFINE ('AREA', 'A');
DEFINE ('AREA_NAME' ,'admin');
require './prepare.php';

if (!empty($html_catalog))  {
	define ('NO_SESSION', true);
}
require './init.php';

@ini_set('memory_limit', '256M');
$current_location = 'http://' .Registry::get('config.http_host');

$cart_language='EL';

clearstatcache();
printf("%s<br />\n", 'Init done.');

$fn = dirname(__FILE__).'/priceUpdater/updaterFiles/oktabit_products.xml';
$url = 'http://www.oktabit.gr/times_pelatwn/prices_xml.asp?customercode=012279&logi=vnp';
try {
  //$pf = PriceListFetcherHttpFile::create()->fetch($url, $fn);
  printf("file downloaded <br />\n");
  //$c = new OktabitCategoriesExporter('okt');
  //$c->export($fn,dirname(__FILE__).'/priceUpdater/updaterFiles/oktabit_categories.xml');
  //$loader = new OktabitSupplierListLoader($fn, 5);
  //$loader->cacheImages();
  printf("image cache finished <br />\n");
  if( filesize($fn)>2000 ) {
    printf("creating loader <br />\n");
    $loader = new OktabitSupplierListLoader($fn, 5);
    $loader->idPrefix = 'okt';
    printf("done. creating markuper <br />\n");
    $markup = new OktabitMarkup();
    printf("done. creating updater <br />\n");
    $p = new PriceListUpdater($loader, $markup, 5);
    printf("done. calling update<br />\n");
    $p->update();
  }
  else
    printf("file size too small <br />\n");
  
}
catch(Exception $ex) {
  echo 'Exception error :'.$ex->getMessage();
}


// http://www.oktabit.gr/images/photos/97-50-DLR2640B.jpg
// http://www.oktabit.gr/times_pelatwn/chars_xml_per_prod.asp?prod_code=97-50-DLR2640B&customercode=012279&logi=vnp