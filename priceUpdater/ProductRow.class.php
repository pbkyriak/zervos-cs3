<?php

/**
 * Description of PriceListRow
 * Created on 28-7-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ProductRow extends BaseDataRow {

  protected
  $categoryCode,
  $categoryId,
  $productCode,
  $productCodeB,
  $productId,
  $stock,
  $price,
  $titleEl,
  $descriptionEl,
  $titleEn,
  $descriptionEn,
  $imageLink,
  $supplierId,
  $userGroups,
  $status,
	$manufacturerId,
	$catalogPrice,
	$attachment,
	$suggestedRetail,
	$weight,
  $extras,
	$brand;//[imatz] Prosthiki gia apothikeysi brand os features kai xrisi se filter [/imatz]
	protected $process_key;

  public function __construct(
          $supplierId,
          $categoryCode, $productCode, 
          $productCodeB, 
          $stock, $price, 
          $titleEl, $titleEn, 
          $descriptionEl, $descriptionEn, 
          $imageLink, $category,
          $suggestedRetail=0,
		  $weight=0,
		  $brand='',
      $extras=array(),
      $process_key=""
  ) {//[imatz] brand: Prosthiki gia apothikeysi brand os features kai xrisi se filter [/imatz]
    $this->setSupplierId($supplierId);
    $this->setCategoryCode($categoryCode);
    $this->setProductCode($productCode);
    $this->setProductCodeB($productCodeB);
    $this->setStock($stock);
    $this->setPrice($price);
    $this->setTitleEl($titleEl);
    $this->setDescriptionEl($descriptionEl);
    $this->setTitleEn($titleEl);
    $this->setDescriptionEn($descriptionEl);
    $this->setImageLink($imageLink);
    $this->setCategoryId($this->matchCategoryCode($categoryCode, $supplierId));
    $this->setProductId($this->matchProductCode($productCode, $supplierId));
    $this->status = 'A';
    $this->suggestedRetail = $suggestedRetail;
    $this->extras=$extras;
    $this->weight = $weight;
	$this->setBrand($brand);//[imatz] Prosthiki gia apothikeysi brand os features kai xrisi se filter [/imatz]
    if( $categoryCode=='411' ) {
    	$words = explode(' ',$titleEl);
    	$this->setManufacturer(ucfirst($words[0]));
    }
    else
    	$this->setManufacturer($category);
    $this->catalogPrice = null;
    $this->attachment = null;
    $this->process_key=$process_key;
/*
    if( in_array(
    $this->getCategoryId(), 
    array(220,221,225,229,222,226,230,223,227,231,224,228,232,236,240,249,259,241,250,260,243,251,261,245,256,262,247,252,263,237) ) )
    	$this->status = 'H';
    	*/
  }
	
	//[imatz] 
	//Prosthiki gia apothikeysi brand os features kai xrisi se filter 
	public function getBrand() {
		return $this->brand;
	}

	public function setBrand($v) {
		$this->brand = $v;
	}
	//[/imatz]

  public function getCategoryCode() {
    return $this->categoryCode;
  }

  public function setCategoryCode($v) {
    $this->categoryCode = $v;
  }

  public function getCategoryId() {
    return $this->categoryId;
  }

  public function setCategoryId($v) {
    $this->categoryId = $v;
  }

  public function getProductCode() {
    return $this->productCode;
  }

  public function setProductCode($v) {
    $this->productCode = $v;
  }

  public function getProductCodeB() {
    return $this->productCodeB;
  }

  public function setProductCodeB($v) {
    $this->productCodeB = $v;
  }

  public function getProductId() {
    return $this->productId;
  }

  public function setProductId($v) {
    $this->productId = $v;
  }

  public function getStock() {
    return $this->stock;
  }

  public function setStock($v) {
    $this->stock = $v;
  }

  public function getPrice() {
    return $this->price;
  }

  public function setPrice($v) {
    $this->price = $v;
  }

  public function getTitleEl() {
    return $this->titleEl;
  }

  public function setTitleEl($v) {
    $this->titleEl = str_replace('(T-Mobile)', '',$v);
  }

  public function getDescriptionEl() {
    return $this->descriptionEl;
  }

  public function setDescriptionEl($v) {
    $this->descriptionEl = $v;
  }

  public function getTitleEn() {
    return $this->titleEn;
  }

  public function setTitleEn($v) {
    $this->titleEn = str_replace('(T-Mobile)', '',$v);
  }

  public function getDescriptionEn() {
    return $this->descriptionEn;
  }

  public function setDescriptionEn($v) {
    $this->descriptionEn = $v;
  }

  public function getImageLink() {
    return $this->imageLink;
  }

  public function setImageLink($v) {
    $this->imageLink = $v;
  }

  public function getSupplierId() {
    return $this->supplierId;
  }

  public function setSupplierId($v) {
    $this->supplierId = $v;
  }

  public function setStatus($v) {
    $this->status = $v;
  }
  public function getStatus() {
    return $this->status;
  }
  
  public function setManufacturer($v) {
  	$p = explode('>',$v);
  	$v= trim( array_pop($p));
  	$q = "SELECT variant_id FROM cscart_product_feature_variant_descriptions WHERE `variant` like '%$v%' and lang_code='EL'";
  	$q = "
			SELECT fvd.variant_id
			FROM `cscart_product_feature_variant_descriptions` as fvd
			left join cscart_product_feature_variants as fv on (fvd.variant_id=fv.variant_id)
			where fvd.lang_code='EL' and feature_id=16 and fvd.variant like '%$v%'
  	";
  	$manId = db_get_field($q);
  	if( $manId )
  		$this->setManufacturerId($manId);
  	else
  		$this->setManufacturerId(0);
  }
  
  public function setManufacturerId($v) {
  	$this->manufacturerId = $v;
  }
  
  public function getManufacturerId() {
  	return $this->manufacturerId;
  }

  public function setCatalogPrice($v) {
  	$this->catalogPrice = $v;
  }
  
  public function getCatalogPrice() {
  	return $this->catalogPrice;
  }

  public function setAttachment($v) {
  	$this->attachment = $v;
  }
  
  public function getAttachment() {
  	return $this->attachment;
  }
  
  public function getExtras() {
    return $this->extras;
  }

  public function setExtras($v) {
    $this->extras = $v;
  }

  public function getWeight() {
    return $this->weight;
  }
  
  public function setWeight($v) {
    $this->weight = $v;
  }

  public function matchProductCode($productCode) {
    $id= -1;
    if ($productCode)
      $id= db_get_field(sprintf("SELECT product_id FROM cscart_products WHERE product_code='%s'", $productCode));
    if( $id )
      return $id;
    else
      return -1;
  }

  public function toString() {
    return sprintf("%s\t %s\t %s\t %s\t %s", $this->getCategoryCode(), $this->getTitleEl(), $this->getProductCode(), $this->getPrice(), $this->getStock()
    );
  }

  public function getProductPriceArray(BasePriceMarkup $markuper, $userGroupId) {
  	$basePrice = $this->getPrice();
  	if( $this->getCatalogPrice() )
     	$basePrice = $this->getCatalogPrice();
     
    //[imatz]
    //Prosayksisi timis me vasi to baros tou proiontos
	printf('update price weight =%s ',$this->weight);
    $basePrice=Weight::updatePrice($basePrice,$this->weight,$this->supplierId);
    //[/imatz]    
				
    $price = PricingRules::create($this->categoryId, $this->supplierId)->applyToPrice($basePrice);
    if( $price == 0 )
      $price = $this->round_to($markuper->calcPriceForUserGroup($basePrice, $userGroupId, $this->productCode, $this->categoryCode),0.5);
    if( $this->supplierId==3 ) {
    	$price+=2;
    }

    return array(
        'product_id' => $this->getProductId(),
        'price' => $price,
        'lower_limit' => 1,
        'usergroup_id' => $userGroupId,
    );
  }

  protected function round_to($number, $increments) {
    $increments = 1 / $increments;
    return (round($number * $increments) / $increments);
  }

  private function updateDescription() {
    //[imatz] 23/11/2014 Antikatastasei ston titlo tou schwarz me black, 
    //                   os ksexoristi leksi kai oxi tmima allis leksis
    $this->titleEl = str_ireplace(" schwarz", " Black", $this->titleEl);
    $this->titleEn = str_ireplace(" schwarz", " Black", $this->titleEn);
    $this->titleEl = str_ireplace("schwarz ", "Black ", $this->titleEl);
    $this->titleEn = str_ireplace("schwarz ", "Black ", $this->titleEn);
    $this->titleEl = str_ireplace("/schwarz", "/Black", $this->titleEl);
    $this->titleEn = str_ireplace("/schwarz", "/Black", $this->titleEn);
    $this->titleEl = str_ireplace("schwarz/", "Black/", $this->titleEl);
    $this->titleEn = str_ireplace("schwarz/", "Black/", $this->titleEn);

    $query = "
      REPLACE INTO cscart_product_descriptions 
        SET
        `product`=?s, `shortname`=?s, `short_description`=?s, `full_description`=?s,
        `meta_keywords`=?s, `meta_description`=?s, `search_words`=?s, `page_title`=?s,
         product_id=?i, lang_code=?s
      ";    
    db_query($query.', full_description2=?s', 
            $this->getTitleEl(), $this->getTitleEl(), $this->getTitleEl(), $this->getDescriptionEl(), 
            $this->getTitleEl(), $this->getTitleEl(), $this->getTitleEl(), $this->getTitleEl(), 
            $this->getProductId(), 'EL', (isset($this->extras["description2"])?$this->extras["description2"]:"")
    );
    db_query($query, 
            $this->getTitleEn(), $this->getTitleEn(), $this->getTitleEn(), $this->getDescriptionEn(), 
            $this->getTitleEn(), $this->getTitleEn(), $this->getTitleEn(), $this->getTitleEn(), 
            $this->getProductId(), 'EN'
    );
    return;
  }

  private function updatePrices(BasePriceMarkup $markuper) {
    foreach ($this->getUserGroups() as $userGroupId) {
      $price = $this->getProductPriceArray($markuper, $userGroupId);
      $q = sprintf("REPLACE INTO cscart_product_prices SET product_id=%s, price=%s, lower_limit=%s, usergroup_id=%s",
              $price['product_id'], $price['price'], $price['lower_limit'], $price['usergroup_id']);
      printf("PRICEUPD: %s \n",$q);
	  db_query($q);
      $this->suggestedRetail = round(
				$price['price']* (1+round(rand(5,15)/100,2))
				,0);
      //printf("our=%s suggested=%s weight=%s <br />", $price['price'], $this->suggestedRetail,$this->weight );
      if( $price['price'] < $this->suggestedRetail && $price['price']>200 ) {
      	db_query(sprintf("UPDATE cscart_products SET list_price=%s WHERE product_id=%s",$this->suggestedRetail, $this->getProductId()));
      	echo 'list price updated<br />';
      }
      else
      	db_query(sprintf("UPDATE cscart_products SET list_price=0 WHERE product_id=%s", $this->getProductId()));
    }
  }

  private function updateImagePair($imgsrc,$object_type="M",$object="product") {
    $detailed = array();
    $im = fn_get_url_data($imgsrc);
    printf("<br />link: %s result=%s<br />",$imgsrc, print_r($im, true));

    $detailed[] = $im;
    //$a = fn_get_image_pairs(array($this->productId), $object, $object_type, true, true);
    //$pair = array_shift($a);
    $pair_data[] = array(
        "pair_id" =>  null, //$pair ? $pair['pair_id'] : null, //FIXME: An ginei klisei tis func kata to update proiontos
        "type" => $object_type,
        "object_id" => $this->productId,
        "image_alt" => $this->titleEl,
        "detailed_alt" => $this->titleEn,
    );
    
    fn_update_image_pairs(array(), $detailed, $pair_data, $this->productId, $object);

    if(!empty($this->process_key))
      fn_my_changes_update_process($this->process_key);      
  }

  public function createProduct(BasePriceMarkup $markuper) {
    if (!$this->getCategoryId())
      return false;

    //files from icecat (pdfs and videos)
    $extras=$this->extras;
    $files=array();
    if(isset($extras["files"])&&!empty($extras["files"])){
      if(isset($extras["files"]["videos"])&&!empty($extras["files"]["videos"])){
        $files["videos"]=$extras["files"]["videos"];
      }
      if(isset($extras["files"]["pdfs"])&&!empty($extras["files"]["pdfs"])){
        $files["pdfs"]=$extras["files"]["pdfs"];
      }      
    }
    $files=serialize($files);

    $query = "INSERT INTO ?:products (
      `product_code`, `product_type`, `status`, 
      `company_id`, `list_price`, `amount`, `weight`, `length`, 
      `width`, `height`, `shipping_freight`, `low_avail_limit`, `timestamp`, 
      `usergroup_ids`, `is_edp`, `edp_shipping`, `unlimited_download`, `tracking`, 
      `free_shipping`, `feature_comparison`, `zero_price_action`, `is_pbp`, `is_op`, 
      `is_oper`, `is_returnable`, `return_period`, `avail_since`,
      `localization`, `min_qty`, `max_qty`, `qty_step`, `list_qty_count`, 
      `tax_ids`, `age_verification`, `age_limit`, `options_type`, `exceptions_type`, 
      `details_layout`, 
      `shipping_params`, out_of_stock_actions,
      supplierA_price, product_codeB, my_supplier_id, my_files) VALUES 
      (?s, 'P', ?s, 
      0, 0.00, ?i, 0.00, 0, 
      0, 0, 0.00, 0, ?i, 
      ?s, 'N', 'N', 'N', 'B', 
      'N', 'N', 'R', 'N', 'N', 
      'N', 'Y', 10, 0, 
      '', 0, 0, 0, 0, 
      '6', 'N', 0, 'P', 'F', 
      'default', 
      'a:5:{s:16:\"min_items_in_box\";i:0;s:16:\"max_items_in_box\";i:0;s:10:\"box_length\";i:0;s:9:\"box_width\";i:0;s:10:\"box_height\";i:0;}',
      'S',
      ?s, ?s, ?s, ?s)
    ";
    $this->setProductId(
      db_query(
        $query, $this->getProductCode(), $this->status, $this->getStock(), strtotime(date("Y-m-d")), implode(',', $this->getUserGroups()), 
        $this->getPrice(), $this->getProductCodeB(), $this->getSupplierId(), $files
      )
    );

    //Weight
	$shipping_freight = 0;
    //19/9/2014: [imatz] Me to create den apothikeyontan to varos
    $weight=$this->weight;
    if( $this->categoryId==380 ) {
      $weight=10;
    }
	// andreas 23/01/2017 karekles 25 kila
    if( $this->categoryId==7809492 ) {
      $weight=25;
    }
	
	/* 
	  andreas 16/10/2017. Σε καποιες κατηγοριες που είναι κάτω από 5kg βάρος, καρφώνουμε 2ευρω μεταφορικά στο προιον
	  VALTES 
	  KAI
	  STHN 
	  UPDATE 
	  PARAKATO 
	  NOOBA
	*/
	
    if( in_array($this->getCategoryId(), array(368,7809404,299,7809451,7809452,7809453,7809454,7809455,7809456,7809457,7809458,7809459,7809460,
	   7809461,7809462,7809463,7809464,7809465,7809466,7809444,7809418,648,7809412,7809444,276,269,277,494,7809533,7809495,648,650,7809490,7809491,7809348,
	   7809442,7809285,7809405,7809396,341,259,301,310,311,312,313,7809546)) ) {
      if($weight<5) {
		  $shipping_freight=2;
	  }
    }
	if( in_array($this->getCategoryId(), array(7809415))){   // monitors
		if($weight<5) {
		  $shipping_freight=4;
	  }
	}
	if( in_array($this->getCategoryId(), array(7809540))){   // skhnes
		if($weight<5) {
		  $shipping_freight=10;
	  }
	}
/*
	printf("pid=%s cid=%s isin=%s w=%s sf=%s \n",
		$this->getProductId(),
		$this->getCategoryId(),
		in_array($this->getCategoryId(), array(7809415,368,7809404,299,7809451,7809452,7809453,7809454,7809455,7809456,7809457,7809458,7809459,7809460,7809461,7809462,7809463,7809464,7809465,7809466)) ? 'Y' : 'N',
		$weight,
		$shipping_freight);
*/
    db_query("UPDATE ?:products SET weight=?d, shipping_freight=?d WHERE product_id=?i",$weight, $shipping_freight,$this->productId);

    $this->updateDescription();
    $this->updatePrices($markuper);

    $query = "INSERT INTO ?:products_categories 
      (product_id, category_id, link_type, position )
      VALUES (
      ?i, ?i, 'M', 0
      )
      ";
    db_query($query, $this->getProductId(), $this->getCategoryId());
    fn_update_product_count(array($this->getCategoryId()));

    if ($this->getImageLink()) {
      $this->updateImagePair($this->getImageLink());
    }

    if(isset($extras["add_images"])&&!empty($extras["add_images"])){
      foreach($extras["add_images"] as $imgsrc){
        $this->updateImagePair($imgsrc,"A");
      }
    }

    //[imatz]
    //19-9-2014: Orismos diathesimotitas se proionta apo kentriko meros gia update kai create product (edw arxika eleipe)
    $this->fn_update_custom_availability();
    //[/imatz]
    return true;
  }

  public function updateProduct(BasePriceMarkup $markuper, $isFast = false) {
    printf("updateProduct top <br />\n");
    $r= db_get_row(sprintf('SELECT pricelist_updater_skip, update_only_price, update_only_stock, pricelist_updater_skip_skroutz, icecat_force_desc_update FROM cscart_products WHERE product_id=%s', $this->productId));
    $skip = $r['pricelist_updater_skip'];
    $onlyPrices = $r['update_only_price'];
    $onlyStock = $r['update_only_stock'];
    $skipSkroutz = $r['pricelist_updater_skip_skroutz'];
    $icecat_force_desc_update = $r['icecat_force_desc_update'];
    $skroutz="";

    $extras=$this->extras;

    //Prosthiki sto proion main image kai additional mono an den exei idi
    //Eite iparxoun periorismi sto update eite oxi
    if ($this->getImageLink()) {
      $product_images = fn_get_image_pairs(array($this->getProductId()), 'product', 'M', true, true); 
      $images=$product_images[$this->getProductId()];
      reset($images);
      $pair_id = key($images);
      if(empty($images[$pair_id])){
        $this->updateImagePair($this->getImageLink(),"M");
        echo "Updated main image pair<br />\n";
      }      
    }
    if(isset($extras["add_images"])&&!empty($extras["add_images"])){
      $product_images = fn_get_image_pairs(array($this->getProductId()), 'product', 'A', true, true); 
      $images=$product_images[$this->getProductId()];
      reset($images);
      $pair_id = key($images);
      if(empty($images[$pair_id])){
        foreach($extras["add_images"] as $imgsrc){
          $this->updateImagePair($imgsrc,"A");
        }
        echo "updated add images pairs<br />\n";
      }
    }
    
    if( $skip=='Y' )
      return;

    //Exei epilegei eksenegkasmeno update perigrafis kai perigrafis apo xaraktiristika gia to proion
    //Efarmozete mono na to proion exei ta stoixeia apo icecat
    if($skip=='Y' || $onlyPrices=="Y" || $onlyStock=="Y"){
      if($icecat_force_desc_update=="Y"&&isset($extras["source"]["desc"])&&$extras["source"]["desc"]=="icecat"){
          db_query("UPDATE ?:product_descriptions SET full_description=?s WHERE product_id=?i",$this->descriptionEl,$this->productId);
      }
      if($icecat_force_desc_update=="Y"&&isset($extras["source"]["desc2"])&&$extras["source"]["desc2"]=="icecat"){
        db_query("UPDATE ?:product_descriptions SET full_description2=?s WHERE product_id=?i",$extras["description2"],$this->productId);
      }
    }

    $weightClause="";
    if(!empty($this->weight)&&$this->weight!="0.00"){
		  $weightClause = sprintf(", weight=%f", (float)$this->weight);
		  
    }
	if( $this->categoryId==380 ) {
			$weightClause = ", weight=10";
	}

    //files from icecat (pdfs and videos)
    
    $files=array();
    if(isset($extras["files"])&&!empty($extras["files"])){
      if(isset($extras["files"]["videos"])&&!empty($extras["files"]["videos"])){
        $files["videos"]=$extras["files"]["videos"];
      }
      if(isset($extras["files"]["pdfs"])&&!empty($extras["files"]["pdfs"])){
        $files["pdfs"]=$extras["files"]["pdfs"];
      }      
    }
    $files=serialize($files);
	
// prosthiki sthn update	
// andreas 16/10/2017. Σε καποιες κατηγοριες που είναι κάτω από 5kg βάρος, καρφώνουμε 2ευρω μεταφορικά στο προιον
    $weight=$this->weight;
    if( in_array($this->getCategoryId(), array(368,7809404,299,7809451,7809452,7809453,7809454,7809455,7809456,7809457,7809458,7809459,7809460,
	   7809461,7809462,7809463,7809464,7809465,7809466,7809444,7809418,648,7809412,7809444,276,269,277,494,7809533,7809495,648,650,7809490,7809491,7809348,
	   7809442,7809285,7809405,7809396,341,259,301,310,311,312,313,7809546)) ) {
      if($weight<5) {
		  $shipping_freight=2;
	  }
    }
	if( in_array($this->getCategoryId(), array(7809415))){   // monitors 
		if($weight<5) {
		  $shipping_freight=4;
	  }
	}
	if( in_array($this->getCategoryId(), array(7809540))){   // skhbes
		if($weight<5) {
		  $shipping_freight=10;
	  }
	}
	db_query("UPDATE ?:products SET shipping_freight=?d WHERE product_id=?i",$shipping_freight,$this->productId);
// mexri edo

    $q = "UPDATE cscart_products SET out_of_stock_actions='S', supplierA_price=?i, status=?s, amount=?s, my_supplier_id=?i, last_disable_timestamp=0, my_files=?s $weightClause WHERE product_id=?i";
    $q1 = db_quote($q, $this->price, $this->status, $this->stock, $this->getSupplierId(), $files, $this->productId);
    printf("%s <br />\n", $q1);
    db_query($q1);
    if( in_array($this->getCategoryCode(), array('340','364')) ) {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=1,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=1 WHERE product_id=%s",$skroutz, $this->productId));
    }
    elseif( substr($this->getProductCode(),0,1)=='0' ) {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=1,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=1 WHERE product_id=%s",$skroutz, $this->productId));
    }
    elseif( in_array($this->getCategoryCode(), array('1088')) ) {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=8,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=2 WHERE product_id=%s",$skroutz, $this->productId));
    }
    elseif( in_array($this->getCategoryId(), array(380)) ) {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=4,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=4 WHERE product_id=%s",$skroutz, $this->productId));
    }	
    elseif( in_array($this->getCategoryCode(), array('325')) && strpos($this->titleEl,'Alcatel OT-995')===false ) {	// alcatel
    	if( $skipSkroutz!='Y' )
	    	db_query(sprintf("UPDATE cscart_products SET status='D' WHERE product_id=%s", $this->productId));
	    return;
    }
    //elseif( in_array($this->getCategoryCode(), array('846','847')) ) {
    //[imatz]
    //19/9/2014: Den iparxoun supplier_category_name pou na arxizoun apo 864- kai 847-, iparxoun omos apo 846, 847
    elseif( substr($this->getCategoryCode(), 0,4)=='846-' || substr($this->getCategoryCode(), 0,4)=='847-') {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=5,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=5 WHERE product_id=%s",$skroutz, $this->productId));
    	db_query(sprintf("UPDATE cscart_products SET weight=51 WHERE product_id=%s", $this->productId));
    }
    elseif( $this->stock==1 ) {
      // 21-2-2014 [imatz]: Egine allagi apo katopin paragkelias (4) se Mi diathesimo (8, 9 - skroutz)
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=4,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=8 WHERE product_id=%s",$skroutz, $this->productId));
    }    
    else {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=1,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=1 WHERE product_id=%s",$skroutz, $this->productId));
    	//db_query(sprintf("UPDATE cscart_products SET weight=0 WHERE product_id=%s", $this->productId));
    }
    if( substr($this->getCategoryCode(), 0,4)=='14' && $this->getSupplierId()==1 ) {
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=9,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=8 WHERE product_id=%s",$skroutz, $this->productId));
    }
    if( $this->getSupplierId()==11) { //wave
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=5,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=7 WHERE product_id=%s",$skroutz, $this->productId));		
    }
    if( $this->getSupplierId()==12 ) { 
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=1,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=1 WHERE product_id=%s",$skroutz, $this->productId));		
    }
	  //[imatz]
    //19-9-2014: Orismos diathesimotitas se proionta apo kentriko meros gia update kai create product
    $this->fn_update_custom_availability($skipSkroutz);
    //[/imatz]
		// block difox
    //if( in_array($this->getSupplierId(), array(10)) )
    //	db_query(sprintf("UPDATE cscart_products SET skroutz_availability=9 WHERE product_id=%s", $this->productId));	
    
    // summer holiday setting
    /*
    if( in_array($this->getSupplierId(), array(1,4)) )
    	db_query(sprintf("UPDATE cscart_products SET skroutz_availability=5,shop_availability=5 WHERE product_id=%s", $this->productId));
		if( in_array($this->getCategoryCode(), array('325','1088')) ) {	// alcatel
	    	db_query(sprintf("UPDATE cscart_products SET skroutz_availability=9 WHERE product_id=%s", $this->productId));
    }
    */
	
    //exentric
	
	  //$query = "UPDATE cscart_products SET STATUS = 'D' WHERE NOT product_code LIKE '11%' AND my_supplier_id =10";
	
	
    if( $onlyStock=='Y' ) {
      //printf("update ONLY stock \n");
    }
    else {
     // printf("updating prices....");
      echo "Update Prices....<br />";
      $this->updatePrices($markuper);
      //printf("updated prices <br />\n");
    }
    if( $onlyPrices=='Y' || $onlyStock=='Y' ) {
    }
    else {
      if( !$isFast ) {
        $q = "UPDATE cscart_products SET product_codeB='%s' WHERE product_id=%s";
        //printf("%s <br />\n", $q);
        db_query(sprintf($q, $this->productCodeB, $this->productId));
        //printf("updating descriptions....");
	      $this->updateDescription();
        // update product category
        /*
        db_query("UPDATE cscart_products_categories SET category_id=?i WHERE product_id=?i and link_type='M'", $this->getCategoryId(), $this->getProductId());
    		fn_update_product_count(array($this->getCategoryId()));     
    		*/
        //printf("updated descriptions <br />\n");
      }
    }
    //printf("updateProduct end <br />\n");
  }
  
	//[imatz]  
	//Prosthiki tou brand os supplier. San xaraktiristiko
	public function updateFeaturesAndFilters($product) {
    if(!empty($this->brand)&&!empty($this->productId)){
      echo 'Features. Brand: '.$this->brand.'. Product Id: '.$this->productId.'<br />';

      $this->update_feature_variant_and_link_with_product($this->productId,UPDATER_BRAND_FEATURE_ID,$this->brand);
      
    }else{
      //erxomaste edw an to brand name einai keno i to product id einai keno
      echo '[Features Error. Brand name empty] Features. Brand: -'.$this->brand.'-. Product Id: -'.$this->productId.'-<br />';
    }
		
    //An einai tablet
    if($this->categoryId==UPDATER_TABLETS_CATEGORY_ID){
      $extras=$this->extras;
      if(isset($extras["screen_size"])&&!empty($extras["screen_size"])){
        echo 'Features. Screen size: '.$extras["screen_size"].'. Product Id: '.$this->productId.'<br />';
        $this->update_feature_variant_and_link_with_product($this->productId,UPDATER_TABLETS_SCREEN_SIZE_FEATURE_ID,$extras["screen_size"]);
      }
      if(isset($extras["memory_ram"])&&!empty($extras["memory_ram"])){
        echo 'Features. Memory Ram: '.$extras["memory_ram"].'. Product Id: '.$this->productId.'<br />';
        $this->update_feature_variant_and_link_with_product($this->productId,UPDATER_TABLETS_MEMORY_RAM_FEATURE_ID,$extras["memory_ram"]);
      }
      if(isset($extras["external_memory"])&&!empty($extras["external_memory"])){
        echo 'Features. External memory: '.$extras["external_memory"].'. Product Id: '.$this->productId.'<br />';
        $this->update_feature_variant_and_link_with_product($this->productId,UPDATER_TABLETS_EXTERNAL_MEMORY_FEATURE_ID,$extras["external_memory"]);
      }
    }
	}

  public function update_feature_variant_and_link_with_product($product_id,$feature_id,$variant){
    $cart_lang_primary="EL";
    $cart_lang_secondary="EN";

    $variant_id=db_get_field("SELECT variant_id FROM ?:product_feature_variant_descriptions 
      WHERE variant=?s",$variant);
    $variant_id=db_get_field("SELECT pfvd.variant_id FROM ?:product_feature_variants AS pfv 
      LEFT JOIN ?:product_feature_variant_descriptions AS pfvd ON pfvd.variant_id=pfv.variant_id
      WHERE pfvd.variant=?s AND pfv.feature_id=?i AND pfvd.lang_code=?s
      ",$variant,$feature_id,$cart_lang_primary);

    if(empty($variant_id)){
      //Mono gia brand
      if($feature_id==UPDATER_BRAND_FEATURE_ID){
        $insert_data=array(
          'feature_id'=> $feature_id,
          'variant_name'=> $variant
        );
      }else{
        $insert_data=array(
          'feature_id'=> $feature_id
        );
      }
      $variant_id=db_query("INSERT INTO ?:product_feature_variants ?e",$insert_data);
      
      $insert_data=array(
        'variant_id'=> $variant_id,
        'variant'=> $variant,
        'lang_code'=> $cart_lang_secondary
      );
      db_query("REPLACE INTO ?:product_feature_variant_descriptions ?e",$insert_data);
      
      $insert_data=array(
        'variant_id'=> $variant_id,
        'variant'=> $variant,
        'lang_code'=> $cart_lang_primary
      );
      db_query("REPLACE INTO ?:product_feature_variant_descriptions ?e",$insert_data);
      
      echo 'Feature Variant Added: '.$variant.' ('.$variant_id.')';
    }

    //elegxos an to proion exei to feature me auto to variant
    $variant_exits_in_product=db_get_field("SELECT COUNT(*) FROM ?:product_features_values 
      WHERE feature_id=?i AND product_id=?i AND variant_id=?i",$feature_id,$product_id,$variant_id);
    
    //An gia kapoio logo to proion exei sto feature 2 variants
    if($variant_exits_in_product>1){
      $feature_type=db_get_field("SELECT feature_type FROM ?:product_features 
        WHERE feature_id=?i",$feature_id);
      if(!empty($feature_type)&&$feature_type!="M"){//Den einai feature me dinatotita pollaplwn vriants se ena proion
        //Diagrafei twn dipltipwn eggrafw tou feature sto proion
        db_query("DELETE FROM ?:product_features_values WHERE feature_id=?i AND product_id=?i",$feature_id,$product_id);
        $variant_exits_in_product=0;
      }
    }
    
    //To proion den exei to variant tou fetaure sindedemeno
    if(empty($variant_exits_in_product)){
      //prosthiki gia tin ellinik glossa      
      $insert_data=array(
        'feature_id'=> $feature_id,
        'product_id'=> $product_id,
        'variant_id'=> $variant_id,
        'lang_code'=> $cart_lang_primary
      );
      db_query("REPLACE INTO ?:product_features_values ?e",$insert_data);
      
      //prosthiki gia tin aggliki glwssa
      $insert_data=array(
        'feature_id'=> $feature_id,
        'product_id'=> $product_id,
        'variant_id'=> $variant_id,
        'lang_code'=> $cart_lang_secondary
      );
      db_query("REPLACE INTO ?:product_features_values ?e",$insert_data);
      
      echo 'Feature Variant add to Product. Productid: '.$product_id.' - Variant: '.$variant;
    }else{
      //To proion exei variant gia to feature ayto
      $product_feature_variant_id=db_get_field("SELECT variant_id FROM ?:product_features_values 
        WHERE feature_id=?i AND product_id=?i",$feature_id,$product_id);
      if($product_feature_variant_id!=$variant_id){//An to variant gia to feature exei allaksei
        //update tou variant id gia oles tis glwsses tou product
        db_query("UPDATE ?:product_features_values SET variant_id=?i 
          WHERE feature_id=?i AND product_id=?i",$variant_id,$feature_id,$product_id);
      }
    }
  }
  //Orizei kai enimeroni tin diathesimotita sto katastima kai sto skroutz
  //Exei ginei me skopo na mpoun edw oloi oi kanones apo to updateProduct.
  //Sto createProduct den ipirxane tetoio kanenons ma apotelezma na min orizete i custom diathesimotita kata tin dimiourgia product
  // Shop Availability:
  // 0 - Διαθέσιμο 
  // 1 - Διαθέσιμο (διαθεσιμότητα: 2-3 μέρες)
  // 2 - Διαθέσιμο (διαθεσιμότητα: 3-5 μέρες)
  // 4 - Κατόπιν παραγγελίας
  // 5 - Διαθέσιμο (διαθεσιμότητα: 4-7 μέρες)
  // 6 - Διαθέσιμο (Κατόπιν παραγγελίας: 7-15 μέρες)
  // 7 - Διαθέσιμο (διαθεσιμότητα: 4-10 μέρες)
  // 8 - Μη διαθέσιμο
  // Skroutz Availability:
  // 0 - Σε απόθεμα
  // 1 - 2-3 μέρες
  // 4 - Κατόπιν παραγγελίας
  // 5 - 4-10 μέρες
  // 8 - Άγνωστη διαθεσιμότητα
  // 9 - Όχι
  //
  public function fn_update_custom_availability($skipSkroutz="N"){
    //TODO: Na mpoun edw oloi oi kanones orismou custom avail apo to updateProduct
    if($this->getSupplierId()==16){
      if( $skipSkroutz!='Y' )
        $skroutz = 'skroutz_availability=4,';
      db_query(sprintf("UPDATE cscart_products SET %s shop_availability=4 WHERE product_id=%s",$skroutz, $this->productId));    
    }elseif($this->getSupplierId()==11){//wave
      if( $skipSkroutz!='Y' )
        $skroutz = 'skroutz_availability=5,';
      db_query(sprintf("UPDATE cscart_products SET %s shop_availability=7 WHERE product_id=%s",$skroutz, $this->productId)); 
    }elseif($this->getSupplierId()==18){
      if( $skipSkroutz!='Y' )
        $skroutz = 'skroutz_availability=1,';
      db_query(sprintf("UPDATE cscart_products SET %s shop_availability=1 WHERE product_id=%s",$skroutz, $this->productId)); 
    }elseif($this->getSupplierId()==10){//Difox
      if( $skipSkroutz!='Y' )
        $skroutz = 'skroutz_availability=5,';
      db_query(sprintf("UPDATE cscart_products SET %s shop_availability=7 WHERE product_id=%s",$skroutz, $this->productId)); 
    }elseif($this->getSupplierId()==9){//ENO
      if( $skipSkroutz!='Y' )
        $skroutz = 'skroutz_availability=1,';
      db_query(sprintf("UPDATE cscart_products SET %s shop_availability=1 WHERE product_id=%s",$skroutz, $this->productId)); 
    }elseif($this->getSupplierId()==19){//LEGO
      if( $skipSkroutz!='Y' )
        $skroutz = 'skroutz_availability=4,';
      db_query(sprintf("UPDATE cscart_products SET %s shop_availability=4 WHERE product_id=%s",$skroutz, $this->productId)); 
    }elseif( $this->getSupplierId()==20 ) { // quickmobile
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=5,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=7 WHERE product_id=%s",$skroutz, $this->productId));		
    }elseif( $this->getSupplierId()==26 ) { // bigbuy
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=4,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=6 WHERE product_id=%s",$skroutz, $this->productId));		
    }elseif( $this->getSupplierId()==27 ) { // apide
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=5,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=7 WHERE product_id=%s",$skroutz, $this->productId));		
    }elseif( $this->getSupplierId()==21 ) { // telepart
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=5,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=5 WHERE product_id=%s",$skroutz, $this->productId));		
    }
	elseif( $this->getSupplierId()==22 ) { // thomann
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=5,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=5 WHERE product_id=%s",$skroutz, $this->productId));		
    }
    elseif( $this->getSupplierId()==15 ) { // mobileshop 27/11/2015
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=5,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=2 WHERE product_id=%s",$skroutz, $this->productId));		
    }
    elseif( $this->getSupplierId()==23 ) { // Nexion 01/12/2015
    	if( $skipSkroutz!='Y' )
    		$skroutz = 'skroutz_availability=1,';
    	db_query(sprintf("UPDATE cscart_products SET %s shop_availability=1 WHERE product_id=%s",$skroutz, $this->productId));		
    }	
	// 29/11/2014: Ta rologia na exoun diathesimotita 4-7 meres
    if(in_array($this->getCategoryId(), array(7809302))){
      if( $skipSkroutz!='Y' )
        $skroutz = 'skroutz_availability=5,';
      db_query(sprintf("UPDATE cscart_products SET %s shop_availability=5 WHERE product_id=%s",$skroutz, $this->productId)); 
    }
  }
	//[/imatz]
  
}

