<?php
require_once dirname(__FILE__).'/BasePriceMarkup.class.php';

/**
 * Description of AgorasetoMarkup
 * Created on 7-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DifoxMarkup extends BasePriceMarkup {

	public function getHat() {
		$hat = (float) Registry::get('settings.AutoPricing.christmass_hat');
		return $hat;
	}


  public function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode) {
    //echo "Prosayksisi timis:<br />";
    //echo "Arxiki timi: $basePrice<br />";
  	$christmasHat = $this->getHat($categoryCode);
    //echo "Prosayksisi: $christmasHat<br />";
    //echo "Teliki timi: ".(($basePrice*1.20+$christmasHat)*1.23)."<br />";
	printf($basePrice);
   	return  ($basePrice*1.20+$christmasHat)*1.24;
  }
  
  public function statusModifier(ProductRow $product) {
    $newStatus = $product->getStatus();
    return $newStatus;
  }
}

