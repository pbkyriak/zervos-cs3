<?php 

class ThomannSupplierListLoader extends BaseSupplierListLoader {

  public $idPrefix;
  /*
   productCode  0
   title        1
   category     2
   price        3
   descr        4
   images       5
  */
  public function loadFile() {
    $handle = fopen($this->listFile, "r");

    if ($handle) {
      while( $data = fgetcsv($handle, 10000, ",") ) {
        if($data[0]=="productCode")
          continue;
      
        $categoryCode = $this->supplierId.'-thomann';//.$data[2];
        $brand = $data[2];
        $titleEl = $data[1];
        $titleEn = $titleEl;
        $stock=5;
    	$stockLimitPassed = ($stock>1);
		$tmp = str_replace(".","",$data[3]); // remove . from price format
		$tmp1 = round(($tmp / 1.23) , 2); // apoforologish 
        $price = (float)$tmp1;
        $productCode = 'TH'.$data[0];
        $productCodeB = '';
		$weight =0; 
        printf("pcode=%s category =%s stock=%s price=%s weight=%s ", $productCode, $categoryCode, $stock, $price, $weight);
        $descriptionEl = $data[4];
        if( trim($titleEl)=='' ) {
        	printf("No title\n");
        	continue;
        }
        $descriptionEn = $descriptionEl;

        $eans=array();

		$image = explode('|', $data[5]);
		$image = reset($image);
        if ($categoryCode ) {   
          if(!empty($this->process_key))
            fn_my_changes_update_process($this->process_key);           	
          
          $pr = new ProductRow(
            $this->supplierId,
            $categoryCode,
            $productCode,
            $productCodeB,
            $stock,
            $price,
            $titleEl,
            $titleEn,
            $descriptionEl,
            $descriptionEn,
			$image,
            $categoryCode,
			0,
			$weight,
			$brand,
            $this->process_key
			);
          if ($pr->getCategoryId()!=-1) {          	
          	if( $stock && $price && $stockLimitPassed) {
              $this->addProductRow($pr);
              echo 'loaded in category ' . $pr->getCategoryId() . "<br />\n";
          	}
		    else {
		     	echo "No stock or no price <br />\n";
		    }	            
          }
          else {
            echo 'No match ' . $categoryCode . "<br />\n";
          }
        }
        else
          echo "No categoryCode \n";
      }
    } else {
      throw new Exception("Bad file<br />\n");
    }
	/*if( count($this->getProducts())<200 ) {
		die("Less than 200 products in the data file<br />\n");
	}*/
  }
}