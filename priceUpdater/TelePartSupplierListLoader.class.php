<?php 
// updated by
// Andreas Morfopoulos
// an@weblive.gr
// Oktober 2015
//

class TelePartSupplierListLoader extends BaseSupplierListLoader {

		  public $idPrefix;
          private $stockLimit=10;
		  
		  public function loadFile() {
			$handle = fopen($this->listFile, "r");
			  echo 'opening '.$this->listFile;
			$row=0;
			if ($handle) {
			  while( $data = fgetcsv($handle, 10000, ";") ) {
				$row++;
				if($row==1)
				  continue;
				//Domi Data
				//0 - Articlenumber
				//1 - Type
				//2 - Manufacturer
				//3 - Model
				//4 - Modeldescription
				//5 - Languages
				//6 - UnitPrice
				//7 - IsDiscontinued : FALSE, TRUE
				//8 - IsNew : FALSE, TRUE
				//9 - IsSpecialPrice : FALSE, TRUE
				//10 - Stock
				//11 - DeliveryQuantity
				//12 - DeliveryDay
				//13 - EAN
				//14 - TimeStamp
				$categoryCode = $this->supplierId.'-'.trim($data[2]);
				if( $data[0]=='Samsung' && strpos($data[3], 'Note')>1 )
					$categoryCode = $this->supplierId.'-'.'Tablet';
				$brand = $data[2];
				$titleEl = $data[3];
				$titleEl=str_replace(array('EU','DE'), array('',''), $titleEl);
				$titleEn = $titleEl;
				$stock = (int) str_replace('>','',$data[10]);
				if($stock<$this->stockLimit){
	     				echo "Stock " . $stock . " less the min ". $this->stockLimit . "<br />\n";
	     				continue;
	     		}
				$price = (float)str_replace(',','.',$data[6]);
				$productCode = $data[0];
				$productCodeB = $data[13];
				//Eikones
				//Morfi url: https://www.telepart.com/shared/getimage/{size}/{Articlenumber}.jpg
				//Opou Size: 3 (250x250), 4(800x800), 5(1200x1200)
				$imageLink = 'https://www.telepart.com/shared/getimage/4/'.$data[0].'.jpg';

				printf("category =%s stock=%s price=%s \n", $categoryCode, $stock, $price);
				
				if(!empty($data[4])){
				  $descriptionEl = $data[4];          
				}else{
				  $descriptionEl = $data[3];
				}
				$_description=$descriptionEl;
				$descriptionEl = nl2br($descriptionEl);
				$descriptionEn = $descriptionEl;
				//prosoxi to pedio 13 mporei na exei polla eans delimited me (,)
				$ean=(!empty($data[13]))?explode(',',$data[13]):array();
				//fn_print_r($ean);
				$weight = 0;
				fn_my_changes_update_process($this->process_key);
				if ($categoryCode && $stock && $price) {

					$pr = new ProductRow(
			            $this->supplierId,
			            $categoryCode,
			            $productCode,
			            $productCodeB,
			            $stock,
			            $price,
			            $titleEl,
			            $titleEn,
			            $descriptionEl,
			            $descriptionEn,
			            $imageLink,
			            0,0,
			            $weight,
						$brand,
						$this->process_key
					);
					
					if ($pr->getCategoryId()!=-1) {          	
						if( $stock && $price ) {
							$icecat_params=array(
								"use_upc_from_site"=>false,
								"ex_weight"=>false,
								"supplier_id"=>$this->supplierId,
								// "ex_title"=>false,
							);
							$icecat_params["ex_screen_size"]=false;
							$icecat_params["ex_external_memory"]=false;
							$icecat_params["ex_memory_ram"]=false;
							$icecat_params["ex_os"]=false;
							$icecat_params["ex_num_cpu_cores"]=false;
							$icecat_params["ex_camera_resolution"]=false;             
							$product_data=$this->getDataFromIceCat($pr->getProductId(),$eans,$productCodeB,$pr,$icecat_params);
							$this->addProductRow($pr);
							echo 'loaded in category ' . $pr->getCategoryId() . (!empty($product_data)?', ICECAT':'') . "<br />\n";
						}
						else {
							echo "No stock or no price <br />\n";
						}	            
					}
					else {
						echo 'No match ' . $categoryCode . "<br />\n";
					}
					
				}
			}
		}
	}
}