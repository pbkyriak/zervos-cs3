<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PricingRules
 *
 * @author panos kyriakakis
 */
class PricingRules {

  private $categoryId,
          $supplierId,
          $rules;

  public function __construct($cId, $supplierId) {
    $this->categoryId = $cId;
    $this->supplierId = $supplierId;
   	$sr = SupplierPricingRules_Sup::getInstance();
    $this->rules = $sr->getCategoryRules($cId);
		printf("category %s has %s rules<br />", $cId, count($this->rules));
  }

  public static function create($cId, $supplierId) {
    return new PricingRules($cId, $supplierId);
  }

  public function applyToPrice($basePrice) {
    $out = 0;
    if (count($this->rules)) {
      foreach ($this->rules as $rule) {
        //printf("rule=%s %s\n", print_R($rule, true),$basePrice);
        echo "\n\n-----------------------------------------\n";
        //echo "Base Price: $basePrice<br />";
        if( (float)$rule['from_price']<$basePrice ) {
          if( $rule['markup_type']=='A' )
            $out = $basePrice + $rule['markup'];
          else 
            $out = $basePrice * (1 + $rule['markup']/100);
          //echo "Price after makrup: $out<br />";
          $out = $out * 1.24;
          //echo "Price after taxes: $out<br />";
          //[imatz]
          //Meta tin metafora ston neo server 79.143.186.167
          //Stamatise na diabazei tin timi apo ta settings
          //$christmass_hat=Registry::get('settings.AutoPricing.christmass_hat');
          //if(empty($christmass_hat)){
            $christmass_hat=db_get_field("SELECT value FROM ?:settings_objects WHERE name=?s AND section_id=?i","christmass_hat","67");
          //}
          echo "Kapelo: $christmass_hat<br />";
          $out += (float) $christmass_hat;
          echo "Price after hat: $out<br />";
          //[/imatz]
          if( $rule['round_to']==0)
            $out = round($out,0);
          else
            $out = $this->round_to($out, $rule['round_to']==0 ? l : $rule['round_to']/100);
          //echo "Price modification. Base Price: $basePrice, Price: $out<br />";
          printf("matched rule=%s \n baseprice=%s price=%s\n", print_R($rule, true), $basePrice, $out);
          //echo "Price after rounding: $out<br /><br />";
          break;
        }
      }
    }
    else
    	printf("no rules for category=%s\n", $this->categoryId);
    //printf("baseprice=%s out=%s <br />", $basePrice, $out);
    return $out;
  }

  private function loadRules() {
    $sId = $this->supplierId;
    $parents = $this->getCategoryParents($this->categoryId);
    $rules = array();
    foreach ($parents as $cId) {
      $rules = $this->loadCategoryForSupplierRules($cId, $sId);
      if (count($rules))
        break;
    }
    $this->rules = $rules;
  }

  private function loadCategoryForSupplierRules($cId, $supplier) {
    $rules = db_get_array("SELECT * FROM cscart_category_pricing_rules WHERE category_id=?i AND supplier_id=?i ORDER BY from_price DESC", $cId, $supplier);
    return $rules;
  }

  private function getCategoryParents($cId) {
    $path_ids = db_get_field("SELECT id_path FROM cscart_categories WHERE category_id=?i", $cId);
    return array_reverse(explode('/', $path_ids));
  }

  protected function round_to($number, $increments) {
    $increments = 1 / $increments;
    return (round($number * $increments) / $increments);
  }
}

