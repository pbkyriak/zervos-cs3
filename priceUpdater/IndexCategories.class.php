<?php 
class IndexCategories {
	
	public static function getInstance() {
		return new IndexCategories();
	}
	
	public function run() {
		echo "Indexing categories <br />";
		$this->reset();
		$categories = db_get_array("SELECT category_id, supplier_category_name FROM cscart_categories");
		foreach( $categories as $category ) {
			$this->index($category);
			echo '. ';
		}
	}
	
	private function index($category) {
		if(empty($category['supplier_category_name']))
			return;
		$names = explode(',', $category['supplier_category_name']);
		foreach($names as $name) {
			db_query("INSERT INTO slx_categories_supplier_matching (category_id, supplier_category_name) VALUES (?i, ?s)", $category['category_id'], $name);
		}
	}
	
	private function reset() {
		db_query("TRUNCATE TABLE slx_categories_supplier_matching");
	}
}
