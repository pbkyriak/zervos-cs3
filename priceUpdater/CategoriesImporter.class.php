<?php 

class CategoriesImporter {
	
	private $categoriesCSV;	// csv file with categories to be added in a category of the target shop
	private $trgRootId;				// the Id of the category in the target shop that the categories will be added
	private $srcRootId;
	private $categoryRows;
	private $supplierId;
	
	public function __construct($supplierId, $catCSV, $srcRootId, $trgRootId) {
		$this->supplierId = $supplierId;
		$this->categoriesCSV = $catCSV;
		$this->srcRootId = $srcRootId;
		$this->trgRootId = $trgRootId;
		$this->categoriesRows = array();
	}
	
	public function run() {
		$this->loadCategories();
		$ug = $this->getUserGroups();
		foreach( $this->categoriesRows as $cat ) {
      $cat->setUserGroups($ug);
      $cat->updateParent();
			$cat->createCategory();
		}
	}
	
	protected function getUserGroups() {
		return array_merge(array(0), db_get_fields("SELECT usergroup_id FROM ?:usergroups WHERE type='C'"));
	}

	private function loadCategories() {
    if (($handle = fopen($this->categoriesCSV, "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $categoryCode = $data[0];
        $parentCode = $data[1];
        $path = $data[2];
        $title = str_replace("σ ","ς ",mb_convert_case(mb_strtolower($data[3], 'UTF-8'), MB_CASE_TITLE, "UTF-8"));
        printf("%s %s %s <br />\n", $categoryCode, $parentCode, $title);
        $c = new CategoryRow($this->supplierId, 
                $title, $title, 
                -1, $categoryCode,
                -1, $parentCode);
        $this->categoriesRows[] = $c;
			}
		}
		fclose($handle);
	}
}