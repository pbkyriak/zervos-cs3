<?php

/**
 * Description of BaseSupplierListLoader
 * Created on 6-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class BaseSupplierListLoader {
  protected
          $listFile,
          $supplierId,
          $productRows,
          $categoryRows;
  
  protected $process_key;
  public function __construct($listFile, $supplierId, $process_key="") {
    $this->listFile = $listFile;
    $this->supplierId = $supplierId;
    $this->process_key=$process_key;
  }
  
  public abstract function loadFile();
  
  public function addProductRow(ProductRow $row) {
    $this->productRows[] = $row;
  }

  public function getProductRows() {
    return $this->productRows;
  }
  
  public function addCategoryRow(CategoryRow $row) {
    $this->categoryRows[] = $row;
  }
  
  public function getCategoryRows() {
    return $this->categoryRows;
  }
  
  //Xrisi icecat gia dedomena
  //An to proion exei antistixithi se katigoria kai eite einai neo eite iparxei kai den einai kleidomeno tote update
  // Tote me ta eans pou dinei o supplier, me ta eans apo site, to upc pou dinei kai to upc apo to site
  // ginete prospathia gia lipsi data apo icecat.
  // An iparxoun tote update full description, full description 2 (custom pedio pou exei ginei apo xaraktiristika), 
  // eikones kai files (pdfs kai videos)
  // An den iparxoun kai einai neo proion tote krifo an einai palio kai den exei eikona tote krifo allios menei active
  public function getDataFromIceCat($product_id,$eans,$productCodeB,ProductRow &$pr,$params=array()){
    $icecat = new IceCat();
    $product_data=array();
    $use_icecat=false;

    //To pedia na min diabastoun apo icecat
    $default_params=array(      
      "ex_title"=>false, //Sto homelike mono
      "ex_description"=>false,
      "ex_desc_from_features"=>false,
      "ex_main_image"=>false,
      "ex_add_images"=>false,
      "ex_features"=>false,
      "ex_files"=>false,
      "ex_related_products"=>true,
      "ex_ean"=>true,
      "ex_brand"=>false,
      "ex_summary_description"=>true,
      "ex_category"=>true,
      "use_upc_from_site"=>true,
      "ex_weight"=>true,
      "ex_screen_size"=>true,
      "ex_external_memory"=>true,
      "ex_memory_ram"=>true
    );
    $params = array_merge($default_params, $params);

    if($product_id>0){
      $use_icecat=fn_my_updaters_allow_product_update($product_id);
    }else{
      $use_icecat=true;
    }

    if(!$use_icecat){
      $icecat_force_desc_update=db_get_field('SELECT icecat_force_desc_update 
        FROM ?:products WHERE product_id=?i', $product_id);
      $use_icecat=($icecat_force_desc_update=="Y");
    }
    
    if($use_icecat){
      //Einai apo vnp. Sto home den iparxei tetoio pedio
      /*if($product_id>0){  
        //Xrisis eans pou mporei na exei orisei o admin 
        $_eans = db_get_fields("SELECT ean FROM ?:eans 
          WHERE object_id=?i AND object_type='P'",$product_id);
        if(!empty($_eans)){
          $eans=array_merge($eans,$_eans);
          $eans = array_unique($eans);
        }
      }*/

      //1) Evresi stoixeiwn apo icecat meso eans      
      if(!empty($eans)){
        foreach($eans as $ean){
          if(!empty($this->process_key))
            fn_my_changes_update_process($this->process_key);
          $product_data=$icecat->getICEcatProductSpecs($ean,$params); 
          if(!empty($product_data))
            break;          
        }         
      }
      //2) Evresi stoixeiwn apo icecat meso upc
      if(empty($product_data)&&!empty($productCodeB)){
        if(!empty($this->process_key))
          fn_my_changes_update_process($this->process_key);
        $product_data=$icecat->getICEcatProductSpecs($productCodeB,$params);  
      }
      //3) Evresi tou upc apo to site
      if(empty($product_data)&&$product_id>0&&$params["use_upc_from_site"]){
        $_productCodeB=db_get_field("SELECT product_codeB FROM ?:products WHERE product_id=?i",$product_id);
        if(!empty($_productCodeB)){
          if(!empty($this->process_key))
            fn_my_changes_update_process($this->process_key);
          $product_data=$icecat->getICEcatProductSpecs($_productCodeB,$params); 
        }
      }

      if(!empty($this->process_key))
        fn_my_changes_update_process($this->process_key);             

      $extras=array();
      if(!$params["ex_files"]){
        if(!empty($product_data)){
          if(isset($product_data["videos"])){
            $extras["source"]["videos"]="icecat";
            $extras["files"]["videos"]=$product_data["videos"];
          }
          if(isset($product_data["pdfs"])){
            $extras["source"]["pdfs"]="icecat";
            $extras["files"]["pdfs"]=$product_data["pdfs"];
          }
        }
      } 
      if(!$params["ex_add_images"]){
        $extras["source"]["add_images"]="icecat"; 
        $extras["add_images"]=isset($product_data["add_images"])?$product_data["add_images"]:array();
      } 
      if(!$params["ex_related_products"]){
        if(!empty($product_data)){
          if(isset($product_data["related_products"])){
            $extras["source"]["related_products"]="icecat";
            $extras["related_products"]=$product_data["related_products"];
          }
        }
      }  
    }           
 if($product_id==71980) {
	var_dump($params);
	var_dump($product_data);
 }
    if(!empty($product_data)){    
      if(!$params["ex_desc_from_features"]&&isset($product_data["description2"])&&!empty($product_data["description2"])){
        $extras["source"]["desc2"]="icecat";
        $extras["description2"]=$product_data["description2"];            
      }
      if(!$params["ex_title"]&&isset($product_data["Title"])&&!empty($product_data["Title"])){
        $extras["source"]["title"]="icecat";             
        $pr->setTitleEl($product_data["Title"]);
        $pr->setTitleEn($product_data["Title"]);
      }
      if(!$params["ex_description"]){ 
        $extras["source"]["desc"]="icecat";
        $pr->setDescriptionEl($product_data["description"]);
        $pr->setDescriptionEn($product_data["description"]);
      }
      if(!$params["ex_brand"]&&isset($product_data["brand"])&&!empty($product_data["brand"])){ 
        $extras["source"]["brand"]="icecat";    
        $pr->setBrand($product_data["brand"]);
      }
      if(!$params["ex_main_image"]&&isset($product_data["HighPic"])&&!empty($product_data["HighPic"])){ 
        $extras["source"]["main_img"]="icecat";   
        $pr->setImageLink($product_data["HighPic"]);              
      }
      if(!$params["ex_weight"]&&isset($product_data["features"])&&!empty($product_data["features"])){
        $_weight=(float)$pr->getWeight(); 
        if(empty($_weight)){          
          $weight=0;
          foreach($product_data["features"] as $gf_id=>$group_feature){  
            // Iparxei omada apo features pou me paraktw onoma pou fenete na exei to baros mesa tis
            if($group_feature["title"]=="Βάρος και διαστάσεις"){   
              if(isset($group_feature["featrues"])){           
                foreach($group_feature["featrues"] as $f_id=>$feature){
                  //Iparxoun pedia barwn me pola onomata: Βάρος, Βάρος πακέτου, Weight (including battery)
                  //iparxoun kai alla omos
                  if((strpos($feature["name"],"Βάρος")!==false || strpos($feature["name"],"Weight")!==false)
                    &&strpos($feature["name"],"Βάρος παλέτας")===false){ 
					printf('feature value %s <br />', $feature["value"]);
                    $tmp_weight=$this->getWeightFromFeatureValue($feature["value"]);
					printf('tmp_weight %s <br />', $tmp_weight);
                    if($tmp_weight>$weight)
                      $weight=$tmp_weight;
					if( $weight>60 ) {
						$weight = round($weight/1000,2);
					}
					printf("final weight %s \n", $weight);
                  }
                }
              }
            }
          }    
          if(!empty($weight)){     
            //Prosthiki 1 epeidi ta pio pola vari den perilamvanoun siskeyasia
            $weight+=1;
            $extras["source"]["weight"]="icecat";
            $pr->setWeight($weight);
          }
        }
      }
      if(!$params["ex_screen_size"]&&isset($product_data["features"])&&!empty($product_data["features"])){
        foreach($product_data["features"] as $gf_id=>$group_feature){  
          if($group_feature["title"]=="Οθόνη"){   
            if(isset($group_feature["featrues"])){           
              foreach($group_feature["featrues"] as $f_id=>$feature){
                if(strpos($feature["name"],"Μέγεθος οθόνης")!==false ){ 
                  $extras["screen_size"]=$feature["value"];
                  $extras["source"]["screen_size"]="icecat";
                }
              }
            }
          }
        }    
      }
      //Memory ram tablets
      if(!$params["ex_memory_ram"]&&isset($product_data["features"])&&!empty($product_data["features"])){
        foreach($product_data["features"] as $gf_id=>$group_feature){  
          if($group_feature["title"]=="Μνήμη"){   
            if(isset($group_feature["featrues"])){           
              foreach($group_feature["featrues"] as $f_id=>$feature){
                if(strpos($feature["name"],"Εσωτερική μνήμη")!==false ){ 
                  $extras["memory_ram"]=$feature["value"];
                  $extras["source"]["memory_ram"]="icecat";
                }
              }
            }
          }
        }
      }
      //External memory
      if(!$params["ex_external_memory"]&&isset($product_data["features"])&&!empty($product_data["features"])){
        foreach($product_data["features"] as $gf_id=>$group_feature){  
          if($group_feature["title"]=="Μέσα Αποθήκευσης"){   
            if(isset($group_feature["featrues"])){           
              foreach($group_feature["featrues"] as $f_id=>$feature){
                if(strpos($feature["name"],"Internal storage capacity")!==false ){ 
                  $extras["external_memory"]=$feature["value"];
                  $extras["source"]["external_memory"]="icecat";
                }
              }
            }
          }
        }
      }
      $extras["Prod_id"]=$product_data["Prod_id"];
      $extras["ID"]=$product_data["ID"];
      $pr->setExtras($extras);       
      //Gia na min kanei override eans pou bazei o admin
      //if(empty($eans)){
      //  $pr->setEans($product_data["ean"]);
      //}
    }else{
      //An einai neo proion kai den iparxei sto icecat tote na mpei os krifo
      if($product_id<=0){
        $pr->setStatus("H");
      }else{
        //An einai palio proion kai den iparxei sto icecat kai den exei eikona sto site tote menei krifo
        $hasnot_main_image = db_get_field("SELECT count(p.product_id)
          FROM ?:products as p 
          left join ?:images_links as iml on (iml.object_id=p.product_id and iml.object_type='product' and iml.type='M')
          where isnull(iml.detailed_id) and p.product_id=?i
          ",$product_id);               
        if($hasnot_main_image){
          $pr->setStatus("H");
        }
      }
    }
    return $product_data;
  }

  private function getWeightFromFeatureValue($weight){
    if(!empty($weight)){      
      if(strpos($weight,"kg")!==false){        
        $weight=str_replace("kg", "", $weight);
        $weight=(float)str_replace(",", ".", $weight);           
      }elseif(strpos($weight,"g")!==false){
        $weight=(float)str_replace("g", "", $weight);
        $weight=$weight/1000;
      }
      if(!is_numeric($weight)){
        $weight=0;
      }
    }
    return($weight);
  }
  
  protected function checkRemoteFile($url) {
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL,$url);
	    // don't download content
	    curl_setopt($ch, CURLOPT_NOBODY, 1);
	    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    if(curl_exec($ch)!==FALSE)
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}
	
  public static function cleanupDE($content) {
    $strange = array();
    $report = array();
    for ($i = 0; $i < strlen($content); $i++) {
      if (strpos("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'[]|\{}+-_()*%&@!.,:;?<>/=\n" . '"` ', $content[$i]) === false)
        if (!in_array($content[$i], $strange)) {
          switch (ord($content[$i])) {
            case 129:
              $content[$i] = 'o';
              break;
            case 225:
              $content[$i] = 'a';
              break;
            case 132:
            case 228:
              $content[$i] = 'a';
              break;
            case 148:
            case 246:
              $content[$i] = 'o';
              break;
            case 161:
            case 239:
              $content[$i] = 'i';
              break;
            case 248:
            case 242:
              $content[$i] = 'o';
              break;
            case 155:
              $content[$i] = '>';
              break;
            case 153:
              $content[$i] = ' ';
              break;
            case 154:
            case 223:
              $content[$i] = 's';
              break;
            case 169:
              $content[$i] = 'C';
              break;
            case 130:
            case 174:
              $content[$i] = ' ';
              break;
            case 252:
              $content[$i] = 'u';
              break;
            case 214:
              $content[$i] = 'O';
              break;
            case 196:
              $content[$i] = 'A';
              break;
            case 220:
              $content[$i] = 'U';
              break;
            case 201:
              $content[$i] = 'E';
              break;
            case 233:
            case 232:
              $content[$i] = 'e';
              break;
          }
          $strange[] = $content[$i];
          $report[$content[$i]] = array($content[$i], ord($content[$i]), $i, 1);
        }
        else
          $report[$content[$i]][2]++;
    }
    return $content;
  }
}

