<?php

class NortonListFetcher {

  public function fetch($username, $apikey, $fn) {
    $url = sprintf("http://webservice.nortonline.gr/NortonlineService.svc/ProductsList?user=%s&apikey=%s", $username, $apikey);
    $ch = curl_init();

    # Setting CURLOPT_RETURNTRANSFER variable to 1 will force cURL
    # not to print out the results of its query.
    # Instead, it will return the results as a string return value
    # from curl_exec() instead of the usual true/false.
    $userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
    //'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
    curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
    $cookieJar = 'cookies.txt';         
    curl_setopt($ch,CURLOPT_COOKIEJAR, $cookieJar); 
    curl_setopt($ch,CURLOPT_COOKIEFILE, $cookieJar);
    curl_setopt($ch,CURLOPT_AUTOREFERER,true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_URL, $url);
    $json = curl_exec($ch);
    // CLOSE CURL
    curl_close($ch);
    
    $response = json_decode($json, true);
    
    $content = "";
    $prod_mask = <<<EOT
    
		<product invoice-code="%s" 
			price="%s" available="%s" 
			recommendedRetailPrice="%s"> 
      <family code="%s" />
      <group code="%s" />
      <category code="%s" />
			<image src="%s" />
			<title>
				<lang-el><![CDATA[%s]]></lang-el>
				<lang-en><![CDATA[%s]]></lang-en>
			</title>
			<description>
				<lang-el><![CDATA[%s]]></lang-el>
				<lang-en><![CDATA[%s]]></lang-en>
			</description>
	  </product>
    
EOT;
    foreach($response['products'] as $product) {
      $content .= 
        sprintf($prod_mask, 
          $product['invoice-code'],
          $product['price'],
          ($product['availiable'] ? 'true' : 'false'),
          $product['recommendedRetailPrice'],
          $product['familyCode'],
          $product['groupCode'],
          $product['categoryCode'],
          $product['imageUrl'],
          $product['title-el'],
          $product['title-en'],
          $product['description-el'],
          $product['description-en']
        );
    }
    $content = '<?xml version="1.0" encoding="UTF-8"?>
<nortonline>
<products>
'.$content.'
</products>
</nortonline>
';
	
    file_put_contents($fn,$content);
    
  }
}
