<?php

require_once dirname(__FILE__).'/BaseSupplierListLoader.class.php';

/**
 * Description of SkroutzSupplierListLoader
 * Created on 16-11-2012
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DotMediaSupplierListLoader extends BaseSupplierListLoader {
  
  public function loadFile() {
    $wantedProperties = array('category', 'name', 'sku', 'price', 'image', 'description');
    $db_conn = & Registry::get('runtime.dbs.main');
    printf("reading file: %s.", $this->listFile);
      printf("reading file: %s.", $this->listFile);
      $use_errors = libxml_use_internal_errors(true);
      $xml = simplexml_load_file($this->listFile);
      printf("after read");
      if ($xml) {
        echo 'finding products node<br />';
        /*
        foreach($xml->children() as $node ) {
          echo $node->getName().'<br />';
          if( $node->getName()=='NewDataSet' ) {
            $products = $node;
            break;
          }
        }
        */
        $products = $xml->children();
        if( !$products ) {
          throw new Exception("no products");
        }
        echo 'products node found<br />';
        $row = 0;
        $pingLoop = 0;
        foreach ($products as $product) {
          $pingLoop++;
          if( $product->getName()=='table1' ) {
            echo 'importing product '.$row++.'  sku='.(string)$product->ProductID;

            $categoryCode = '7-'.(string)$product->Category;
            $title = (string)$product->Description;
            $title = trim($title);

            $stock=0;
            $stock = (int)$product->CurrentStock;
            $price= (float)$product->WholesalePrice;
            $productCode = (string)$product->ProductID;
            $productCodeB = (string)$product->Barcode;
            $image = (string)$product->ImageLink;
            
          	if( !$this->checkRemoteFile($image) )
          		$stock = 0;
            $description = (string)$product->DetailedDescription;
            printf("categoryCode = %s stockk=%s <br />", $categoryCode, $stock);
            // check stock limit
            $stockLimitPassed = ($stock>1);
            printf("pcode=%s title=%s price=%s ean=%s stock=%s image=%s category=%s\n", $productCode, $title, $price, $productCodeB, $stock, $image, $categoryCode);
            if ($categoryCode 
                && $stockLimitPassed
                && $price 
              ) {
              $skip = false;
              /*
              if( $categoryCode == '215' && (strpos($title,'Apple')===false) )  {
              	printf("Not apple %s %s<br />",$title,strpos($title,'Apple'));
                $skip = true;
              }
              */
              if( !$skip ) {
              	
                $pr = new ProductRow(
                        $this->supplierId,
                        $categoryCode, 
                        $productCode, 
                        $productCodeB, 
                        $stock, 
                        $price, 
                        $title, 
                        $title, 
                        $description, 
                        $description, 
                        $image,
                        $category,
                        $suggestedRetail
                        );
                if( $pr->getCategoryId()!=-1 ) {
                  $this->addProductRow($pr);
                  echo 'loaded in category '.$pr->getCategoryId().' <br />';
                }
                else
                  echo 'No match '.$categoryCode.'<br />';
              }
            }
            else
              echo 'No categoryCode or no stock <br />';
              
						/* check if server is alive */
						if( $pingLoop>500 ) {
							$pingLoop=0;
							if (!mysqli_ping($db_conn)) {
						    printf ("Error: %s\n", mysqli_error($db_conn));
						    die(1);
							}
						}
          }
        }
      } else {
        printf("xml loader error <br />");
        foreach(libxml_get_errors() as $error) {
          printf("error: %s <br />", $this->display_xml_error($error, $xml));
        }

        libxml_clear_errors();
        libxml_use_internal_errors($use_errors);
        throw new Exception('Bad xml file<br />');
      }
    //}
  }
  
  public function matchProductCode($productCode) {
    $id= -1;
    if ($productCode)
      $id= db_get_field(sprintf("SELECT product_id FROM cscart_products WHERE product_code='%s'", $productCode));
    if( $id )
      return $id;
    else
      return -1;
  }
  
  function display_xml_error($error, $xml)
  {
      $return  = $xml[$error->line - 1] . "<br />";
      $return .= str_repeat('-', $error->column) . "^<br />";

      switch ($error->level) {
          case LIBXML_ERR_WARNING:
              $return .= "Warning $error->code: ";
              break;
           case LIBXML_ERR_ERROR:
              $return .= "Error $error->code: ";
              break;
          case LIBXML_ERR_FATAL:
              $return .= "Fatal Error $error->code: ";
              break;
      }

      $return .= trim($error->message) .
                 "<br />  Line: $error->line" .
                 "<br />  Column: $error->column";

      if ($error->file) {
          $return .= "<br />  File: $error->file";
      }

      return "$return\n\n--------------------------------------------\n\n";
  }
  
}

