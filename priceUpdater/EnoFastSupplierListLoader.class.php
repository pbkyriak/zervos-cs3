<?php

require_once dirname(__FILE__).'/BaseSupplierListLoader.class.php';

/**
 * Description of SunFastSupplierListLoader
 * Created on 6-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class EnoFastSupplierListLoader extends BaseSupplierListLoader {
  public $fileParts=1;
  
  public function loadFile() {
    $wantedProperties = array('category', 'name', 'sku', 'price', 'image', 'description');
    $db_conn = & Registry::get('runtime.dbs.main');
    printf("reading file: %s.\n", $this->listFile);
    if( $handle = fopen($this->listFile, "r") ) {
      $cnt =1;
      while( $data = fgetcsv($handle, 10000, ";") ) {
        $productCode = $data[3];
        $price = (float)$data[12];
				$stock = $data[13];
        //if( iconv("ISO-8859-7","UTF-8",$data[17])=='Διαθέσιμο' )
        //  $stock = 10;
        //else
        //  $stock = 0;
        $retailPrice = 0;
        $stockLimitPassed = ($stock>5);
        if( $stock && $stockLimitPassed ) {
          $pr = new FastProductRow(
                  $this->supplierId,
                  $productCode, 
                  $stock, 
                  $price, 
                  $retailPrice
                  );
          if( $pr->getCategoryId() ) {
            $this->addProductRow($pr);
            echo $cnt.' loaded in category '.$pr->getCategoryId()." <br />\n";
          }
        }
        $cnt++;
      }
    }
  }  
}

