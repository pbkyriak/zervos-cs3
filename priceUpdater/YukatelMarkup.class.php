<?php
require_once dirname(__FILE__).'/BasePriceMarkup.class.php';

/**
 * Description of YukatelMarkup
 * Created on 13-7-2012
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class YukatelMarkup extends BasePriceMarkup {

	public function getHat($categoryCode) {
		$hat = (float) Registry::get('settings.AutoPricing.christmass_hat');
		$addHat = db_get_field("SELECT add_hat FROM cscart_categories WHERE find_in_set(?s, supplier_category_name)", $categoryCode);
		if( $addHat=='Y') 
			return $hat;
		else
			return 0;
	}
	
  public function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode) {
  	$christmasHat = $this->getHat($categoryCode);
   	return  ($basePrice*1.07+$christmasHat)*1.23;
  }
  
  public function statusModifier(ProductRow $product) {
    $newStatus = $product->getStatus();
    return $newStatus;
  }
}

