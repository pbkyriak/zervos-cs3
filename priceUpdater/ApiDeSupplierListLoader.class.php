<?php 

class ApiDeSupplierListLoader extends BaseSupplierListLoader {

  public $idPrefix;
  public $download_url;
  private $brands;

 /* Domi csv 

0	sku
1	stock
2	currency
3	price
4	title
5	manufacturer
6	msku
7	ean
8	egId
9	lwg1
10	availability
11	weight
12	width
13  depth
14	height
15	externalStock

  */
  
  public function loadFile() {

    printf("reading file: %s.\n", $this->listFile);

    if( $handle = fopen($this->listFile, "r") ) {
      $cnt =1;
      while( $data = fgetcsv($handle, 10000, ";") ) {
		if($cnt>1) {
	  //fn_print_R($data);
	  
			if( !in_array($data[9], array('Barebones','All-in-One','MiniPC','Grafikkarten NVIDIA','Grafikkarten Matrox','Grafikkarten ATI','CPUs','USVs','Mainboards INTEL','Mainboards AMD','RC Fahrzeuge','Festplatten','Solid State Drives','Festplatten USB',
			'Aktivboxen Mobil', 'Aktivboxen', 'Digital Radio', 'Kopfhorer', 'Headsets', 'Netzteile PC','Tastaturen','RC Fahrzeuge','Netzteile PC','Festplatten USB')) ) {
				continue;
			}

	  		$productCode = 'AP'.$data[0];
			$productCodeB = $data[6];
			$ean = $data[7];
			$price = (float)$data[3]; 
			$stock = ((int)$data[1]!==0 ? (int)$data[1] : 0);
			if($data[10]!='B') {
				$stock = 0;
			}
			$retailPrice = 0;
			$stockLimitPassed = ($stock>=8);
			$brand =  $data[5];
			$titleEl = $data[4];
			$titleEn = $titleEl;
			$descriptionEl = $titleEl;
			$descriptionEn = $titleEl;
			$weight = $data[11];
			//$imageLink = $data[23];
			$eans = array($ean);
			$extras = array();
			$extras["add_images"] = array();
			
			$categoryCode = $this->makeCategoryName( $data[9] );
			printf("code=%s stock=%s cate=%s brand=%s \n", $productCode, $stock, $categoryCode, $brand);
			
			fn_my_changes_update_process($this->process_key);
			if( $stock && $stockLimitPassed && $categoryCode && !empty($brand) ) {
				  $pr = new ProductRow(
					$this->supplierId,
					$categoryCode,
					$productCode,
					$productCodeB,
					$stock,
					$price,
					$titleEl,
					$titleEn,
					$descriptionEl,
					$descriptionEn,
					$imageLink,
					$categoryCode,
					0,
					$weight,
					$brand
				//	$this->process_key
					);
				$pr->setExtras($extras);
				if ($pr->getCategoryId()!=-1) {
					if( $stock && $price && $stockLimitPassed) {
						$icecat_params=array(
							"use_upc_from_site"=>false,
							"supplier_id"=>$this->supplierId,
							"ex_title"=>true, //Sto homelike mono
							"ex_description"=>false,
							"ex_main_image"=>false,
							"ex_add_images"=>false,
							"ex_summary_description"=>false,
						);
						$product_data=$this->getDataFromIceCat($pr->getProductId(),$eans,$productCodeB,$pr,$icecat_params);
						$this->addProductRow($pr);
						echo 'loaded in category ' . $pr->getCategoryId() . (!empty($product_data)?', ICECAT':'') . "<br />\n";
					}
					else {
						echo "No stock or no price <br />\n";
					}					}
				else {
					echo 'No match ' . $categoryCode . '<br />';
				}

			}
			else {
				printf("No stock or no categorycode\n");
			}
		}
        $cnt++;
      }
    }
	else {
		throw new Exception("Bad file<br />\n");
	}
  }  

  private function makeCategoryName( $catids ) {
	$out = $this->supplierId.'-'. str_replace(array(',',' '),'-',$catids);
	return $out;
  }

}
