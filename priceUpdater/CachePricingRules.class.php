<?php

class CachePricingRules {
	private $suppliers;
	private $classTemplate;
	
	public function __construct() {
		$this->suppliers = db_get_fields("SELECT supplier_id FROM slx_supplier WHERE type=1 and default_product_status='A'");
		$this->categories = db_get_fields("SELECT category_id FROM cscart_categories WHERE status!='D' ORDER BY id_path");
		$this->classTemplate = <<<EOT
<?php

class SupplierPricingRules_Sup extends SupplierPricingRules {
		
	public function __construct() {
		@this->rules = array();
%%rules%%
		printf("rules constructor: %s pricing rules\n\n", count(@this->rules));
	}
	
}
EOT;
	}
	
	public function run() {
		foreach($this->suppliers as $supplierId ) {
			$this->makeSupplierClass($supplierId);
		}
	}
	
	private function makeSupplierClass($supplierId) {
		$rulesCode = '';
		$rulesCount = 0;
		foreach($this->categories as $categoryId) {
			$rules = $this->loadRules($supplierId, $categoryId);
			
			if( $rules ) {
				$rulesCount++;
				$rulesCode .= sprintf("\t\t@this->rules['%s'] = array();\n", $categoryId);
				foreach($rules as $rule ) {
					$a = sprintf("\t\t@this->rules['%s'][] = array('from_price'=>%s, 'markup'=>%s, 'markup_type'=>'%s', 'round_to'=>%s);\n", 
						$categoryId,
						$rule['from_price'],
						$rule['markup'],
						$rule['markup_type'],
						$rule['round_to']
						);
					
					$rulesCode .= $a;
					
				}
			} 
		}
		printf("supplier=%s found %s rules\n",$supplierId, $rulesCount);
		$classCode = str_replace('%%rules%%', $rulesCode, $this->classTemplate);
		$classCode = str_replace('%%supplier_id%%', $supplierId, $classCode);
		$classCode = str_replace('@', '$', $classCode);
		if( $supplierId=='13' ) {
			$classCode = str_replace('SupplierPricingRules_Sup', 'SupplierPricingRules_Retail' , $classCode);
		}
		$fn = sprintf(dirname(__FILE__).'/SupplierPricingRules_%s.class.php', $supplierId);
		//if( file_exists($fn) )
		//	chmod($fn, 0666);
		//printf("%s---------------------------- <Br />", $supplierId);
		//printf("%s<br />",$classCode);
		file_put_contents($fn, $classCode);
		//chmod($fn, 0666);
	}
	
  private function loadRules($sId, $cId) {
    $parents = $this->getCategoryParents($cId);
    $rules = array();
    foreach ($parents as $cId) {
      $rules = $this->loadCategoryForSupplierRules($cId, $sId);
      if (count($rules))
        break;
    }
    return $rules;
  }

  private function loadCategoryForSupplierRules($cId, $supplier) {
    $rules = db_get_array("SELECT * FROM cscart_category_pricing_rules WHERE category_id=?i AND supplier_id=?i ORDER BY from_price DESC", $cId, $supplier);
    return $rules;
  }

  private function getCategoryParents($cId) {
    $path_ids = db_get_field("SELECT id_path FROM cscart_categories WHERE category_id=?i", $cId);
    return array_reverse(explode('/', $path_ids));
  }
}