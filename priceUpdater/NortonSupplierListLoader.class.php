<?php

/**
 * Description of NortonSupplierListLoader
 * Created on 6-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class NortonSupplierListLoader extends BaseSupplierListLoader {
  
  public function loadFile() {
    printf("reading file: %s \n", $this->listFile);
    $products = null;
    $xml = simplexml_load_file($this->listFile);
    if ($xml) {
      echo "finding products node<br />\n";
      foreach ($xml->children() as $node) {
        printf(" . %s <br />\n", $node->getName());
        if ($node->getName() == 'products') {
          $products = $node;
        }
      }
      if (!$products) {
        throw new Exception("no products");
      }
      echo "products node found<br />\n";
      $this->loadProducts($products);
      
    } else {
      throw new Exception("Bad xml file<br />\n");
    }

  }

  private function loadProducts($products) {
    $row = 0;
    foreach ($products as $product) {

      if ($product->getName() == 'product') {
        echo 'importing product ' . $row++;

				
        $categoryCode = $product->category['code'];
        if( $product['available']=='true' )
          $stock = rand(2,5);
        else
          $stock = 0;

        $stockLimitPassed = ($stock>1);
        $price = (float)$product['price'];
        $suggestedRetail = (float)$product['recommendedRetailPrice'];
        $productCode = $product['invoice-code'];
        $productCodeB = $product['invoice-code'];
        $imageLink = $product->image['src'];
        $titleEl = html_entity_decode( $product->title->{'lang-el'});
        $titleEn = html_entity_decode( $product->title->{'lang-en'});
        $descriptionEl = html_entity_decode( $product->description->{'lang-el'} );
        $descriptionEl = str_replace('*Παράδοση εντός 7 ημερών απο την ημερομηνία παραγγελίας.','',$descriptionEl );
        //$descriptionEl = str_replace('Τα ασύρματα τηλέφωνα παρέχονται μόνο με πληρωμή μετρητοίς και χωρίς έκπτωση.','',$descriptionEl);
        $descriptionEl = str_replace(
	        array(
								'Τα ασύρματα τηλέφωνα παρέχονται μόνο με πληρωμή μετρητοίς και χωρίς έκπτωση.',
								'Τα κινητά τηλέφωνα παρέχονται μόνο με πληρωμή μετρητοίς και χωρίς έκπτωση.',
								'Παρέχεται μόνο με πληρωμή μετρητοίς και χωρίς έκπτωση.',
								'Οι συσκευές Φαξ παρέχονται μόνο με πληρωμή μετρητοίς και χωρίς έκπτωση.',
								)
					,array('','','','')
					,$descriptionEl
				);
        $descriptionEn = html_entity_decode( $product->description->{'lang-en'} );
        if ($categoryCode && $stock && $price && $stockLimitPassed ) {
          $pr = new ProductRow(
                          $this->supplierId,
                          $categoryCode,
                          $productCode,
                          $productCodeB,
                          $stock,
                          $price,
                          $titleEl,
                          $titleEn,
                          $descriptionEl,
                          $descriptionEn,
                          $imageLink,
                          $categoryCode,
                          $suggestedRetail
                          );
          if ($pr->getCategoryId()!=-1) {
            $this->addProductRow($pr);
            echo 'loaded in category ' . $pr->getCategoryId() . ' <br />';
          }
          else
            echo 'No match ' . $categoryCode . '<br />';
        }
        else
          echo 'No categoryCode or no stock '.$categoryCode .' '. $stock .' '. $price.' '.$product['available'].'<br />';
      }
    }
  }

  public function loadFileCategories() {
    printf("reading file: %s \n", $this->listFile);
    $families = null;
    $xml = simplexml_load_file($this->listFile);
    if ($xml) {
      if( $xml->families )
        $this->loadFamilies($xml->families);
      else
        throw new Exception("no families");
    } else {
      throw new Exception("Bad xml file<br />\n");
    }

  }
  
  /**
   *
   * @param SimpleXMLElement $families 
   */
  private function loadFamilies(SimpleXMLElement $families) {
    foreach($families->family as $family) {
      printf(" . %s %s\n", $family->getName(), $family['code']);
      $ct0 = new CategoryRow($this->supplierId, 
              $family['title-el'], $family['title-en'], 
              -1, $family['code'], 
              -1, null);
      $this->addCategoryRow($ct0);
      foreach($family->groups->group as $group) {
        printf(" .. %s %s\n", $group->getName(), $group['code']);
        $ct1 = new CategoryRow($this->supplierId, 
                $group['title-el'], $group['title-en'], 
                -1, $group['code'],
                -1, $ct0->getCategoryCode());
        $this->addCategoryRow($ct1);
        foreach($group->categories->category as $category) {
          printf(" ... %s %s\n", $category->getName(), $category['code']);
          $ct2 = new CategoryRow($this->supplierId, 
                  $category['title-el'], $category['title-en'], 
                  -1, $category['code'],
                  -1, $ct1->getCategoryCode());
          $this->addCategoryRow($ct2);
        }        
      }
    }
  }
}

