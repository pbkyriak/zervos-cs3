<?php 
//
// Ioannis Matziaris [imatz]
// imatzgr@gmail.com
// January 2015
//

use Tygh\Registry;

include_once(__DIR__."/PHPExcel/PHPExcel.php");

class QuickMobileSupplierListLoader extends BaseSupplierListLoader {
	public $idPrefix;
	private $stockLimit=5;
	
	// Domi xls
	/*
	PROSOXI!!! Kathe filo exei stin arxei 3 grammes prin arxisoun ta proionta
	Filo: 1 - Phones, Filo: 2 - Tablets
		[A] => Cod MemoX	- A
	    [B] => Brand		- B
	    [C] => Product		- F
	    [D] => Model		- C
	    [E] => Price		- L
	    [F] => Qty			- K
	    [G] => Stock		- J
	    [H] => LTE/ 3G		- D
	    [I] => Dualsim		- E
	    [J] => Colour		- G
	    [K] => Capacity		- H
	    [L] => Cod Furnizor	- -
	    [M] => Comanda		- -

    Filo: 3 - Gadgets
    	[A] => Cod MemoX		- A
	    [B] => Brand			- B
	    [C] => Category			- F
	    [D] => Product			- E
	    [E] => Model			- C
	    [F] => Price			- L
	    [G] => Qty				- K
	    [H] => Stock			- J
	    [I] => Extra Info		- D
	    [J] => Colour			- G
	    [K] => Capacity			- H
	    [L] => Cod Furnizor
	    [M] => Comanda
	*/
	
	/*	19-10-2015
	PROSOXI!!! Kathe filo exei stin arxei 3 grammes prin arxisoun ta proionta
	Filo: 1 - Phones, Filo: 2 - Tablets
		[A] => Cod MemoX
	    [B] => Brand
	    [C] => Model
	    [D] => LTE/ 3G
	    [E] => Dualsim
	    [F] => Product
	    [G] => color
	    [H] => capacity
	    [I] => provider code
	    [J] => stock
	    [K] => qty
	    [L] => price
	    [M] => order

    Filo: 3 - Gadgets
    	[A] => Cod MemoX		- A
	    [B] => Brand			- B
	    [C] => model			- 
	    [D] => extrainfo
	    [E] => product
	    [F] => category
	    [G] => color
	    [H] => capacity
	    [I] => provider code
	    [J] => stock
	    [K] => qty
	    [L] => price
	    [M] => order
		
	21-10-2015
	
	Filo: 1 - Phones, Filo: 2 - Tablets
		[A] => Cod MemoX	-> A
	    [B] => Brand		-> B
	    [C] => Model		-> D
	    [D] => LTE/ 3G		-> H
	    [E] => Dualsim		-> I
	    [F] => Product		-> C
	    [G] => color		-> J
	    [H] => capacity		-> K
	    [I] => provider code	-> L
	    [J] => stock		-> G
	    [K] => qty			-> F
	    [L] => price		-> E
	    [M] => order		-> M
	*/

	public function loadFile() {

		$out = array();
		//$inputFileType = 'Excel5';
		/**  Create a new Reader of the type defined in $inputFileType  **/
		//$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		/**  Load $inputFileName to a PHPExcel Object  **/
		//$objPHPExcel = $objReader->load($this->listFile);
		$objPHPExcel = PHPExcel_IOFactory::load($this->listFile);
		for($i=0;$i<=0;$i++){
			$sheetData = $objPHPExcel->getSheet($i)->toArray(null,true,true,true);
			foreach( $sheetData as $dataRow ) {
				if($i<=1){
					$product=trim($dataRow["C"]);
				}else{
					$product=trim($dataRow["E"]);
				}
				if(!empty($product) && $product!="Product") {
					if($i<=1){
						$stock=(int)trim($dataRow["F"]);
					}else{
						$stock=(int)trim($dataRow["J"]);
					}
					if(!is_numeric($stock)) {
						printf("Non numeric stock %s\n",$stock);
						$stock=0;
					}

					if($stock<$this->stockLimit){
	     				echo "Stock " . $stock . " less the min ". $this->stockLimit . "<br />\n";
	     				continue;
	     			}
     				if($i==0){
     					$categoryCode = $this->supplierId.'-Phone'; //.strtoupper($dataRow["B"]);
     				}elseif($i==1){
     					//$categoryCode = $this->supplierId.'-Tablets-'.strtoupper($dataRow["B"]);
     					$categoryCode = $this->supplierId.'-Tablets';
     				}elseif($i==2){
     					$categoryCode = $this->supplierId.'-Gadgets';
     				}else{
     					$categoryCode = $this->supplierId.'-'.$dataRow["C"];
     				}
					
					$brand=trim($dataRow["B"]);
					if( strpos($product, $brand)===false ) {
						$product = $brand.' '.$product;
					}
     				$titleEl = $product;
	     			$titleEn = $titleEl;	

	     			$descriptionEl = $product;
	     			$descriptionEn = $descriptionEl;	

	     			$productCode = $dataRow["A"];
	     			if(!empty($this->idPrefix))
	     				$productCode=$this->idPrefix.$productCode;	

	     			$productCodeB=$dataRow['L'];

					if($i<=1){
						$price=(float)$dataRow["E"];
					}else{
						$price=(float)$dataRow["E"];
					}
					if(empty($price)){
	     				echo "Zero price<br />\n";
	     				continue;
	     			}

					$imageLink ="";
	     			$weight = 0;
	     			$eans=array();
	     			if($i<=1){
						//Gia na mpoun ta proionta se katigories pou exoun ean prepei na ginei pseftiko ean
	     				$ean=fn_crc32($titleEl);
	     				$eans[]=$ean;
						$eanOverride=true;
						$eancategories=array();
					}else{
						$ean="";
						$eanOverride=false;
						$eancategories=array();
					}					
        			
					fn_my_changes_update_process($this->process_key);
					$pr = new ProductRow(
			            $this->supplierId,
			            $categoryCode,
			            $productCode,
			            $productCodeB,
			            $stock,
			            $price,
			            $titleEl,
			            $titleEn,
			            $descriptionEl,
			            $descriptionEn,
			            $imageLink,
			            0,0,
			            $weight,
						$brand,
						$this->process_key
					);
					
					if ($pr->getCategoryId()!=-1) {          	
						if( $stock && $price ) {
							$icecat_params=array(
								"use_upc_from_site"=>false,
								"ex_weight"=>false,
								"supplier_id"=>$this->supplierId,
								// "ex_title"=>false,
							);
							$icecat_params["ex_screen_size"]=false;
							$icecat_params["ex_external_memory"]=false;
							$icecat_params["ex_memory_ram"]=false;
							$icecat_params["ex_os"]=false;
							$icecat_params["ex_num_cpu_cores"]=false;
							$icecat_params["ex_camera_resolution"]=false;             
							$product_data=$this->getDataFromIceCat($pr->getProductId(),$eans,$productCodeB,$pr,$icecat_params);
							$this->addProductRow($pr);
							echo 'loaded in category ' . $pr->getCategoryId() . (!empty($product_data)?', ICECAT':'') . "<br />\n";
						}
						else {
							echo "No stock or no price <br />\n";
						}	            
					}
					else {
						echo 'No match ' . $categoryCode . "<br />\n";
					}
					
				}
			}
		}
	}
}