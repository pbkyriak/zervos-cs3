<?php

class PriceListFetcherB2BWatches {

  public static function create() {
    return new PriceListFetcherB2BWatches();
  }

  public function fetch($url, $fn, $username="", $password="") {
    // INIT CURL
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'http://www.b2bwatches.co.uk/accesso.php');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6");
    curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 0); 
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'login='.$username.'&password='.$password);
    $cookieJar = 'cookies.txt';         
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieJar); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // EXECUTE 1st REQUEST (FORM LOGIN)
    $store = curl_exec($ch);    
    
    //An den ginei login tote girizei: <META HTTP-EQUIV="Refresh" Content="0; URL=browse.php?rif=no_auth.php">
    if(!$store||strpos($store,"no_auth.php")!==false){    	
        $msg = <<<EOT
Η είσοδος στον B2BWatches δεν είναι δυνατή.
Η ενημέρωση των προϊόντων δεν θα γίνει μέχρι να αποκατασταθεί η πρόσβαση.
EOT;
        $this->mailReport($msg);
        die("Login to supplier not available<br />\n");
    }
    
    curl_setopt($ch, CURLOPT_POST, 0);

    $urls=unserialize($url);

    foreach($urls as $cid=>$url){
        $_fn=$fn.$cid.'.csv';
        curl_setopt($ch, CURLOPT_URL, $url);
        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($httpCode==200){
            if( file_exists($_fn) )
                unlink($_fn);
            file_put_contents($_fn,$content);
            printf("File downloaded: $_fn <br />\n");
        }else{
            $msg = <<<EOT
Δεν μπόρεσε να γίνει download του αρχείου: <file>.
Το update των προϊόντων του B2BWatches έχει διακοπεί μέχρι να γίνει έλεγχος.
EOT;
            $msg=str_replace("<file>",$_fn,$msg);
            $this->mailReport($msg);
            die("File not downloaded: $_fn <br />\n");
        }        
    }
    
    // CLOSE CURL
    curl_close($ch);   
  }

  protected function mailReport($msg) {
    global $settings, $smarty_mail, $mailer, $languages, $cart_language;    
    fn_init_mailer();
    $mailer = & Registry::get('mailer');
    $__from['email'] = Registry::get('settings.Company.company_site_administrator');
    $__from['name'] = Registry::get('settings.Company.company_name');
    $mailer->SetFrom($__from['email'], $__from['name']);
    $mailer->ClearAttachments();
    $mailer->IsHTML(false);
    $mailer->CharSet = 'UTF-8';
    $mailer->Subject = 'B2BWatches Price List Fetcher';
    $mailer->Body = $msg;
    $mailer->ClearAddresses();
    $mailer->AddAddress('ioanniszervos@gmail.com','');
    $result = $mailer->Send();
    if (!$result) {
      echo $mailer->ErrorInfo;
    } else {
      echo 'mail send';
    }
  }
}