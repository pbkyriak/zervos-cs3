<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoryRow
 * Created on 6-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class CategoryRow extends BaseDataRow {
  
  protected 
          $titleEl, 
          $titleEn, 
          $categoryId, 
          $categoryCode, 
          $parentCode,
          $parentId,
          $idPath,
          $supplierId;
  
  public function __construct($supplierId, $titleEl, $titleEn, $categoryId, $categoryCode, $parentId, $parentCode) {
    $this->titleEl = $titleEl;
    $this->titleEn = $titleEn;
    $this->categoryId = $categoryId;
    $this->categoryCode = $categoryCode;
    $this->parentId = $parentId;
    $this->parentCode = $parentCode;
    $this->supplierId = $supplierId;
    if( $this->categoryId==-1 )
      $this->categoryId = $this->matchCategoryCode($this->categoryCode, $supplierId);
    if( $this->parentId==-1 )
      $this->parentId = $this->matchCategoryCode($this->parentCode, $supplierId);
    $this->idPath = $this->makeIdPath();
  }
  
  public function getTitleEl() {
    return $this->titleEl;
  }
  public function setTitleEl($v) {
    $this->titleEl = $v;
  }
  public function getTitleEn() {
    return $this->titleEn;
  }
  public function setTitleEn($v) {
    $this->titleEn = $v;
  }
  public function getCategoryId() {
    return $this->categoryId;
  }
  public function setCategoryId($v) {
    $this->categoryId = $v;
  }
  public function getCategoryCode() {
    return $this->categoryCode;
  }
  public function setCategoryCode($v) {
    $this->categoryCode = $v;
  }
  public function getParentId() {
    return $this->parentId;
  }
  public function setParentId($v) {
    $this->parentId = $v;
  }
  public function getParentCode() {
    return $this->parentCode;
  }
  public function setParentCode($v) {
    $this->parentCode = $v;
  }
  
  /**
   *
   * @return type 
   */
  private function makeIdPath() {
    $idPath = '';
    if( $this->parentId) {
      $idPath = db_get_field("SELECT id_path FROM ?:categories WHERE category_id=?i", $this->parentId);
      if( $idPath )
        $idPath .= '/';
        $idPath .= $this->categoryId;
    }
    return $idPath;
  }
    
  /**
   * 
   */
  public function createCategory() {
    $q = "
          INSERT INTO ?:categories 
          (`parent_id` ,  `id_path`,  `owner_id`,  `usergroup_ids`,
          `status`,  `product_count`,  `position`,  `timestamp`,
          `is_op`,  `localization`,  `age_verification`,  `age_limit`,
          `parent_age_verification`,  `parent_age_limit`,  `selected_layouts`,  `default_layout`,
          `product_details_layout`,  `product_columns`,  `supplier_category_name`, supplier_import_id) VALUES
          (?i, '', 0,?s, 
          'A', 0, ?i, ?i, 
          'N', '', 'N', 0, 
          'N', '0', '', '', 
          'default', 0,?s, ?s)
    ";
    
		$position = db_get_field("SELECT max(position) FROM ?:categories WHERE parent_id = ?i", $this->parentId);
		$position = $position + 10;

    $this->categoryId = db_query(
            $q,
            $this->parentId, implode(',',$this->getUserGroups()),
            $position, strtotime(date("Y-m-d")), 
            $this->getCategoryCode(), $this->supplierId
            );
    $this->updateParent();
    db_query("UPDATE ?:categories set id_path=?s WHERE category_id=?i", $this->idPath, $this->categoryId);
    $this->updateDescriptions();
  }
  
  /**
   * 
   */
  public function updateParent() {
    if( $this->parentId==-1 )
      $this->parentId = $this->matchCategoryCode($this->parentCode, $this->supplierId);
    $this->idPath = $this->makeIdPath();
    printf("...... %s %s \n", $this->parentId, $this->idPath);
  }
  
  /**
   * 
   */
  private function updateDescriptions() {
    $q = "
      REPLACE INTO ?:category_descriptions 
      (category_id, lang_code, category, description, 
      meta_keywords, meta_description, page_title, age_warning_message)
      VALUES
      (?i, ?s, ?s, ?s,
       ?s, ?s, ?s, ?s)
      ";
    db_query(
            $q, 
            $this->getCategoryId(), 'EL', $this->getTitleEl(), '',
            $this->getTitleEl(), $this->getTitleEl(), $this->getTitleEl(), '' );
    db_query(
            $q, 
            $this->getCategoryId(), 'EN', $this->getTitleEn(), '',
            $this->getTitleEn(), $this->getTitleEn(), $this->getTitleEn(), '' );    
  }
  
  /**
   * 
   */
  public function updateCategory() {
    $q = "UPDATE ?:categories SET usergroup_ids=?s WHERE category_id=?i";
    db_query($q, implode(',',$this->getUserGroups()), $this->getCategoryId());
    $this->updateDescriptions();
  }
  
  public function __toString() {
    return sprintf("code:%s title: %s", $this->categoryCode, $this->getTitleEl());
  }
}

