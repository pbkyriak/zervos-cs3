<?php
require_once dirname(__FILE__).'/BasePriceMarkup.class.php';

class NakasMarkup extends BasePriceMarkup {
  	
  	public function getHat() {
		$hat = (float) Registry::get('settings.AutoPricing.christmass_hat');
		return $hat;
	}

	public function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode) {
  		return round( $basePrice * 1, 2);
  	}
  	
  	public function statusModifier(ProductRow $product) {
    	$newStatus = $product->getStatus();
    	return $newStatus;
  	}
}
