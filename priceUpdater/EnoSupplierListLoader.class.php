<?php 

class EnoSupplierListLoader extends BaseSupplierListLoader {

  public $idPrefix;

  public function loadFile() {
    $handle = fopen($this->listFile, "r");
    if ($handle) {
      while( $data = fgetcsv($handle, 10000, ";") ) {
        //
        // Domi tou csv
        //
        //[0] => Haupt WG - Kiria katigoria
        //[1] => WG-Nummer - Ipokatigoria
        //[2] => WG - Stis perisoteres periptoseis einai idio me to (1)
        //[3] => Artikelnummer - Product Code
        //[4] => Herstellerartikelnummer - Montelo
        //[5] => Herstellernummer - Kwdikos kataskeyasti, einai idio me to (6)
        //[6] => Hersteller - Kataskeyastis 
        //[7] => Bezeichnung - Onoma proiontos  
        //[8] => Artikel-Langtext - Link gia selida tou pronotos
        //[9] => Gewicht - Baros
        //[10] => Typ - Eida oti itan se ola keno
        //[11] => UVP 
        //[12] => HEK - Price
        //[13] => Bestand - Stock
        //[14] => Bestand Zeit - Hmer/nia apografis
        //[15] => EAN - ?
        //[16] => Lieferinfo - ?
        //[17] => Bild-URL - Image Links
        //
        $categoryCode = $this->supplierId.'-'.$data[2];
		    $categoryCode = $titleEl = iconv("ISO-8859-2","UTF-8",$categoryCode);
        $brand = $data[6];	
		    $brand = iconv("ISO-8859-7","UTF-8",$brand);        	
        $titleEl = $data[7];
		    $titleEl=str_replace(array('schwarz','Schwarz','weiss','weis','silber','rot'), array('black','black','white','white','silver','red'), $titleEl); 
        $titleEl = iconv("ISO-8859-7","UTF-8",$titleEl);
        $titleEn = iconv("ISO-8859-7","UTF-8",$titleEl);
        $stock = $data[13];
		    $stockLimitPassed = ($stock>=5);
        $price = (float)$data[12];
        $productCode = $data[3];
        $productCodeB = $data[4];
        $imageLink = $data[17];
        printf("%s<br />\n", $imageLink);
        printf("category =%s stock=%s price=%s <br />\n", $categoryCode, $stock, $price);
      	if( !$this->checkRemoteFile($imageLink) )
      		$stock = 0;
        $descriptionEl = iconv("ISO-8859-7","UTF-8",$data[7]);
        if( $descriptionEl===false )
        	$stock=0;
        $descriptionEn = $descriptionEl;
        if ($categoryCode && $stock && $price && $stockLimitPassed) {
          $pr = new ProductRow(
            $this->supplierId,
            $categoryCode,
            $productCode,
            $productCodeB,
            $stock,
            $price,
            $titleEl,
            $titleEn,
            $descriptionEl,
            $descriptionEn,
            $imageLink,
            $categoryCode,
						0,0,
						$brand);
          if ($pr->getCategoryId()!=-1) {
            $this->addProductRow($pr);
            echo 'loaded in category ' . $pr->getCategoryId() . ' <br />\n';
          }else{
            echo 'No match ' . $categoryCode . '<br />\n';
          }
        }else{
          echo 'No categoryCode or no stock <br />\n';
        }
      }
    } else {
      throw new Exception("Bad file<br />\n\n");
    }  
  }
}