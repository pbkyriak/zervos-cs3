<?php

require_once dirname(__FILE__).'/BaseSupplierListLoader.class.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SkroutzSupplierListLoader
 * Created on 6-10-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class AgorasetoFastSupplierListLoader extends BaseSupplierListLoader {
  public $fileParts=1;
  
  public function loadFile() {
    $wantedProperties = array('category', 'name', 'sku', 'price', 'image', 'description');
    $db_conn = & Registry::get('runtime.dbs.main');
    printf("reading file: %s.", $this->listFile);
    if( $handle = fopen($this->listFile, "r") ) {
      $cnt =1;
      while( $data = fgetcsv($handle, 2000) ) {
        $productCode = $data[0];
        $price = (float)$data[2];
        $stock=(int)$data[4];
        $retailPrice = (float)$data[5];
        $stockLimitPassed = ($stock>3);
        if( strpos($productCode, '14')===0 )	// block wave
        	$stockLimitPassed=false;

        if( $stock && $stockLimitPassed && $price ) {
          $pr = new FastProductRow(
                  $this->supplierId,
                  $productCode, 
                  $stock, 
                  $price, 
                  $retailPrice
                  );
          if( $pr->getCategoryId() ) {
            $this->addProductRow($pr);
            echo $cnt.' loaded in category '.$pr->getCategoryId().' <br />';
          }
        }
        $cnt++;
      }
    }
  }  
}

