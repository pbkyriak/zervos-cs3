<?php

class WaveSupplierListLoader extends BaseSupplierListLoader {

    public $idPrefix;

    //Domi csv
    // 0 - Artikel-Nr
    // 1 - Hersteller
    // 2 - Hersteller P/N {Se merika einai keno}
    // 3 - Artikel-Bezeichnung
    // 4 - EAN-Code {Se merika einai keno}
    // 5 - VK-Preis
    // 6 - Verfόgbar
    // 7 - Warengruppe
    // 8 - Gewicht


    /*
      0 ArtNo
      1 Manufacturer
      2 Name
      3 Addition
      4 Price (EUR)
      5 Scale
      6 ScalePrice (EUR)
      7 Col1
      8 Col2
      9 Col3
      0 Col4
      1 Category
      2 Availability
      3 DeliveryCondition
      4 EAN
      5 Manufacturer Id


     */
    public function loadFile() {
        $handle = fopen($this->listFile, "r");
        if ($handle) {
            $brands_correction = array(
                "Buff" => "Buffalo", "ADAT" => "Adata", "HGST" => "Hitachi", "TRC" => "Transcend", "Free" => "Freecom", "Seag" => "Seagate",
                "Tosh" => "Toshiba", "Verb" => "Verbatim", "WD" => "Western Digital", "GiBy" => "Gigabyte", "Asro" => "ASRock", "Bios" => "Biostar",
                "Sapp" => "Sapphire", "Club1GB" => "Club", "Asus1GB" => "Asus", "Pali2GB" => "Palit", "Pali3GB" => "Palit", "Sapp2GB" => "Sapphire",
                "Zota3GB" => "Zotac", "Asus2GB" => "Asus", "Gain2GB" => "Gainward", "Giby2GB" => "Gigabyte", "GiBy3GB" => "Gigabyte", "GiBy6GB" => "Gigabyte",
                "PC SYSTEA" => "Systea", "Shut" => "Shuttle", "Hanns.G" => "Hannspree", "Phil" => "Philips", "Sams" => "Samsung",
                "Sapp1GB" => "Sapphire", "Zota1GB" => "Zotac", "Giby1GB" => "Gigabyte", "Gain3GB" => "Gainward", "EVGA2GB" => "Evga", "EVGA3GB" => "Evga"
            );
            $rCnt = 0;
            while ($data = fgetcsv($handle, 10000, "\t")) {
                $rCnt++;
                if ($rCnt == 1) {
                    continue;
                }
                $categoryCode = $this->supplierId . '-' . $data[11];
				// Transportkoffer => Σετ Εργαλείων
				if( strpos($data[2], 'Transportkoffer')!==false ) {
					$categoryCode = '11-Transportkoffer';
				}
				$cTra = array('ä'=>'a', 'ß'=>'ss', 'ö'=>'o','ü'=>'u');
				$categoryCode = strtr($categoryCode, $cTra);
                $categoryCode = str_replace(array(','), array('-'), $categoryCode);
                $categoryCode = mb_convert_encoding($categoryCode, 'ISO-8859-2', 'UTF-8');

                $brand = $data[1];
                $brand = iconv("ISO-8859-7", "UTF-8", $brand);

                $titleEl = $data[2];
                $titleEl = mb_convert_encoding($titleEl, 'ISO-8859-2', 'UTF-8');
                $titleEl = iconv("ISO-8859-7", "UTF-8", $titleEl);

                //Correct te brand name inside the title
                foreach ($brands_correction as $br => $_brand) {
                    if (stripos($titleEl, $br . " ") !== false) {
                        $titleEl = str_ireplace($br . " ", $_brand . " ", $titleEl);
                    } elseif (stripos($titleEl, " " . $br . " ") !== false) {
                        $titleEl = str_ireplace($br . " ", " " . $_brand . " ", $titleEl);
                    } elseif (stripos($titleEl, " " . $br) !== false) {
                        $titleEl = str_ireplace($br . " ", " " . $_brand, $titleEl);
                    }
                }

                $titleEn = iconv("ISO-8859-7", "UTF-8", $titleEl);

                //To csv den exei pedio stock opote dinw ena tixaio
                if (trim($data[12]) == "Auf Lager") {
                    $stock = 10;
                } 
				else {
                    //$stock = 0;
					continue;
                }

                $price = (float) $data[4];

                $productCode = $this->idPrefix . $data[0];
                $productCodeB = $data[15];
                $eans = array($data[14]);
                $weight = 0; //(float) $data[8];
                printf("category =%s stock=%s price=%s <br />", $categoryCode, $stock, $price);

                $descriptionEl = iconv("ISO-8859-7", "UTF-8", $data[2]);
                if ($descriptionEl === false)
                    $stock = 0;
                $descriptionEn = $descriptionEl;

                $stockLimitPassed = ($stock >= 5);

                if ($categoryCode && $price && $stockLimitPassed) {
                    $pr = new ProductRow(
                            $this->supplierId, $categoryCode, $productCode, $productCodeB, $stock, $price, $titleEl, $titleEn, $descriptionEl, $descriptionEn, '', $categoryCode, 0, $weight, $brand //[imatz] Xrisi gia dimiourgia xaraktirisikwn kai apo ekei filtrwn
                    );
                    if ($pr->getCategoryId() != -1) {
                        if ($stock && $price && $stockLimitPassed) {
                            $icecat_params = array(
                                "use_upc_from_site" => false,
                                "ex_weight" => false,
                                "supplier_id" => $this->supplierId,
                                "ex_title" => false,
                            );
                            if ($pr->getCategoryId() == UPDATER_TABLETS_CATEGORY_ID) {
                                $icecat_params["ex_screen_size"] = false;
                                $icecat_params["ex_external_memory"] = false;
                                $icecat_params["ex_memory_ram"] = false;
                            }
                            $product_data = $this->getDataFromIceCat($pr->getProductId(), $eans, $productCodeB, $pr, $icecat_params);

							// titlos apo icecat aggliko me service apo icecat tou vnp.com 16/04/2016
							$goodTitle = false;
							$context = stream_context_create(array(
								"ssl"=>array(
									"verify_peer"=>false,
									"verify_peer_name"=>false,
								),
							));

							$data123 = file_get_contents('https://www.valuenprice.com/tools/icecat/service.php?service=icecattitle&lang=en&ean=' . urlencode(reset($eans)), false, $context);
							if( $data123 ) {
								$data124 = @json_decode($data123);
								
								if( $data124 ) {
									if( $data124->status==1 ) {
										$pr->setTitleEl((string)$data124->title);
										$pr->setTitleEn((string)$data124->title);
										$goodTitle = true;
										printf("GoodTitle %s\n",$data124->title);
									}
								}
							}
							// titlos apo icecat aggliko 16/04/2016
							
							if( $goodTitle ) {
								$this->addProductRow($pr);
								echo 'loaded in category ' . $pr->getCategoryId() . (!empty($product_data) ? ', ICECAT' : '') . "<br />\n";
							}
							else {
								printf("No english title from icecat\n");
							}
                        } 
						else {
                            echo "No stock or no price <br />\n";
                        }
                    } 
					else {
                        echo 'No match ' . $categoryCode . "<br />\n";
                    }
                } 
				else {
                    echo 'No categoryCode or no stock <br />';
                }
            }
        } 
		else {
            throw new Exception("Bad file<br />\n");
        }
    }

}
