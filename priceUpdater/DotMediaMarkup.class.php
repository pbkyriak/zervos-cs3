<?php
require_once dirname(__FILE__).'/BasePriceMarkup.class.php';

class DotMediaMarkup extends BasePriceMarkup {
  
  public function calcPriceForUserGroup($basePrice, $userGroupId, $productCode, $categoryCode) {
  	return round( $basePrice * 1.28, 2);
  }
  
}

