<?php 

class DifoxSupplierListLoader extends BaseSupplierListLoader {

  public $idPrefix;
  /*
    Pedia (Apo palio csv):
    0: Article group name
    1: Article group
    2: Item number
    3: Article description
    4: Name of supplier
    5: Supplier's item number (upc)
    6: EAN code (bar code) 1
    7: EAN code (bar code) 2
    8: EAN code (bar code) 3
    9: EAN code (bar code) 4
    10: EAN code (bar code) 5
    11: EAN code (bar code) 6
    12: Price
    13: Availability (in steps)
    14: Mark new prices
    15: Price changes (symbol)
    16: Deleted items (symbol)
    17: Kits
    18: Customs tariff number
    19: Weight (kg)
    20: Length (mm)
    21: Width (mm)
    22: Height (mm)
    23: Accessories
    24: no reporting 

    Pedia (Neo csv):
    0: Article group name
    1: Item number
    2: Article description
    3: Name of supplier
    4: Supplier's item number (upc)
    5: EAN code (bar code) 1
    6: EAN code (bar code) 2
    7: EAN code (bar code) 3
    8: EAN code (bar code) 4
    9: EAN code (bar code) 5
    10: EAN code (bar code) 6
    11: Price
    12: currency code
    13: RRP
    14: Availability (in steps)
    15: Mark new prices
    16: Price changes (symbol)
    17: Deleted items (symbol)
    18: Customs tariff number

    Pedia (Neo csv - 28/5/15):
    0: Article group
    1: Article group name
    2: Item number
    3: Article description
    4: Supplier
    5: Supplier's item number (upc)
    6: EAN number 1
    7: EAN number 2
    8: EAN number 3
    9: EAN number 4
    10: EAN number 5
    11: EAN number 6
    12: Retail Price
    13: Availability (tendency)
    15: Mark new prices
    16: Price changes
    17: Deleted items
    18: Kits
    19: Partner programme
	
	
		Pedia (Neo csv - 01/03/16):
	0: Article group
	1: Article group name
	2: Item number
	3: Article description
	4: Supplier
	5: Supplier's item number (upc)
	6: EAN number 1
	7: EAN number 2
	8: EAN number 3
	9: EAN number 4
	10: EAN number 5
	11: EAN number 6
	12: Retail Price
	13: currency code
	14: Availability (steps)
	15: Mark new prices
	16: Price changes
	17: Deleted items
	18: Kits
	19: discontinued item
  */
  public function loadFile() {
    $handle = fopen($this->listFile, "r");
    //[imatz]
      /*  
      $supplier_cats=array();
      $supplier_pros=array();
      $supplier_pros_per_cat=array();
      $supplier_pros_count=array();
      */
    //[/imatz]
    if ($handle) {
      while( $data = fgetcsv($handle, 10000, ";") ) {
        if(strtoupper($data[2])=="ITEM NUMBER")
          continue;

        //27-10-2014 [imatz]
        //Otan se category name iparxei koma tote den taytizete epeidi ta category codes kataxorountai stis katigories tou
        //katastimatos xorismena me komata. Den exei ginei se kathe periptosei komatos xoris na ginei porta elegxos
         $bad_categories=array("Network (IP) cameras, wireless","Network (IP) cameras, wired","Pans, pots, cooking accessories",
          "Spaten, Gabeln und Schaufeln");
        if(in_array($data[1],$bad_categories))
          $data[1]=str_replace(",", "", $data[1]);
        //[/imatz]        
        $categoryCode = $this->supplierId.'-'.$data[1];
        if($data[1]=="App controlled toys" && $data[4]=="Parrot"){
          $categoryCode.='-'.$data[3];
        }
		    //$categoryCode = mb_convert_encoding($categoryCode, 'ISO-8859-2', 'UTF-8');
		    //$categoryCode = str_replace("σ ","ς ");
        
        //[imatz]
          /*if(!in_array($categoryCode,$supplier_cats)){
            $supplier_cats[]=$categoryCode;         
          }
          if(!isset($supplier_pros_count[$categoryCode])){
            $supplier_pros_count[$categoryCode]=1;
          }else{
            $supplier_pros_count[$categoryCode]++;
          }
          $supplier_pros_per_cat[$categoryCode][]=$data[4].' - '.$data[3];
          if($categoryCode=="10-"){ 
            $supplier_pros[]=$data[4].' - '.$data[3];
          }*/
        //[/imatz]

        $brand = $data[4];
		    $brand = self::cleanupDE($brand);//[imatz] perasma tou brand name apo to cleanupDE
        $titleEl = $data[3];
		    $titleEl=str_replace(array('SXZG'), array(''), $titleEl);
        $titleEl = self::cleanupDE($titleEl);
        $titleEn = $titleEl;
		
		// na mhn fernei ta proionta ths Manfrotto
        $findme  = 'Manfrotto';
        $pos = strpos($titleEl, $findme);
		if ($pos!==false) {
		  echo("Mh epitrepto brand Manfrotto<br />\n");
          continue;
		}
		
        /*
        $titleEl = iconv("ISO-8859-7","UTF-8",$titleEl);
        $titleEn = iconv("ISO-8859-7","UTF-8",$titleEl);
				$titleEl=str_replace(array('KΓ€rcher'), array('Karcher'), $titleEl);
				*/
        //Se palio csv me morfi >10   
        //$stock = (int) str_replace('>','',$data[14]);
        /* 01/03/2016 */
		if(strtoupper($data[14])=="AVAILABLE"){
          $stock=5;
        }else{
          $stock=0;
        }
		
		/*
		$stock = (int) str_replace('+','',$data[14]);
		if($stock < 2){
			$stock=0;
		}
		else {
		    $stock=5; 
		}
		*/
		$stockLimitPassed = ($stock>1);
        $price = (float)$data[12];
        $productCode = '11'.$data[2];
        $productCodeB = $data[5];
		    $weight =0; //(float) $data[23];
        printf("pcode=%s category =%s stock=%s price=%s weight=%s ", $productCode, $categoryCode, $stock, $price, $weight);
        //$descriptionEl = iconv("ISO-8859-7","UTF-8",$data[7]);
        $descriptionEl = $titleEl;
        if( trim($titleEl)=='' ) {
        	printf("No title\n");
        	continue;
        }
        $descriptionEn = $descriptionEl;

        $eans=array();
        if (!empty($data[6])) $eans[]=(string)$data[6];
        if (!empty($data[7])) $eans[]=(string)$data[7];
        if (!empty($data[8])) $eans[]=(string)$data[8];
        if (!empty($data[9])) $eans[]=(string)$data[9];
        if (!empty($data[10])) $eans[]=(string)$data[10];
        if (!empty($data[11])) $eans[]=(string)$data[11];

        if ($categoryCode ) {   
          if(!empty($this->process_key))
            fn_my_changes_update_process($this->process_key);           	
          
          $pr = new ProductRow(
            $this->supplierId,
            $categoryCode,
            $productCode,
            $productCodeB,
            $stock,
            $price,
            $titleEl,
            $titleEn,
            $descriptionEl,
            $descriptionEn,
						'',
            $categoryCode,
						0,
						$weight,
						$brand,//[imatz]
            $this->process_key
				  );
          if ($pr->getCategoryId()!=-1) {          	
          	if( $stock && $price && $stockLimitPassed) {
              $icecat_params=array(
                "use_upc_from_site"=>false,
                "ex_weight"=>false,
                "supplier_id"=>$this->supplierId,
				"ex_title"=>true,
              );
              if($pr->getCategoryId()==UPDATER_TABLETS_CATEGORY_ID){
                $icecat_params["ex_screen_size"]=false;
                $icecat_params["ex_external_memory"]=false;
                $icecat_params["ex_memory_ram"]=false;
              }
              $product_data=$this->getDataFromIceCat($pr->getProductId(),$eans,$productCodeB,$pr,$icecat_params);
			  if( $pr->getCategoryId()==1103 ) {
				$pr->setTitleEl(str_replace('telephone','',$pr->getTitleEl()));
				$pr->setTitleEl($pr->getTitleEl(). ' ασύρματο τηλέφωνο');
			  }
			  if( $pr->getCategoryId()==412 ) {
				// $pr->setTitleEl(str_replace('telephone','',$pr->getTitleEl()));
				$pr->setTitleEl($pr->getTitleEl(). ' eBook reader');
			  }
              $this->addProductRow($pr);
              echo 'loaded in category ' . $pr->getCategoryId() . (!empty($product_data)?', ICECAT':'') . "<br />\n";
          	}
		        else {
		        	echo "No stock or no price <br />\n";
		        }	            
          }
          else {
            echo 'No match ' . $categoryCode . "<br />\n";
          }
        }
        else
          echo "No categoryCode \n";
      }
    } else {
      throw new Exception("Bad file<br />\n");
    }
    //[imatz]
      /*fn_print_r($supplier_pros);
      foreach($supplier_cats as $index=>$_category_code){
        echo($_category_code." - ".$supplier_pros_count[$_category_code]."<br />");
        if(isset($supplier_pros_per_cat[$_category_code])){
          foreach($supplier_pros_per_cat[$_category_code] as $_pi=>$product){
            echo($_category_code." - ".$product."<br />");
          }
        }
        echo("<br />");
      }
      //$products = $this->getProductRows();
      //fn_print_r($products);
      fn_my_changes_update_process($this->process_key,0);             
      exit;*/
    //[/imatz] 
  }
}