<?php
//
// Author Ioannis Matziaris [imatz] - imatzgr@gmail.com - Sept 2013
//

if ( !defined('AREA') ) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	return false;
}

if($mode=='execute'){
	set_time_limit(0);
	ini_set('memory_limit', '1024M');
	//define('DEBUG_QUERIES',true);

	$conn=fn_import_data_db_connection();

	fn_import_data_categories($conn);

	exit;
}
?>