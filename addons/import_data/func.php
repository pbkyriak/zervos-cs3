<?php
//
// Author Ioannis Matziaris [imatz] - imatzgr@gmail.com - Sept 2013
//

if ( !defined('AREA') ) { die('Access denied'); }

function fn_import_data_db_connection(){
	$host="localhost";
	$user="vnp2012_devusr";
	$password="9b0Z4BI8RS";
	$dbname="vnp2012_vnpdev";

	$conn=mysql_connect($host, $user, $password);
	mysql_select_db($dbname,$conn);
	mysql_query("SET NAMES utf8");

	return($conn);
}

function fn_import_data_db_close($conn){
	if(isset($conn)&&!empty($conn)){
		mysql_close($conn);
	}
}

function fn_import_data_clean_row(&$row){
	foreach($row as $index=>$value){
		if(is_numeric($index)){
			unset($row[$index]);
		}
	}
}

//Metafora:
// category, catagory description, seo names, eikones, diamorfosi timwn, 
function fn_import_data_categories($conn){
	$source_category_id=409;
	$desc_category_id=386;

	$sql="SELECT * FROM cscart_categories WHERE category_id=".$source_category_id;
	$rs_category=mysql_query($sql,$conn);
	$category = mysql_fetch_array($rs_category);
	fn_import_data_clean_row($category);
	fn_print_r($category);

	$sql="SELECT * FROM cscart_category_descriptions WHERE category_id=".$source_category_id;	
	$rs_category_desc=mysql_query($sql,$conn);
	$rs_category_desc = mysql_fetch_array($rs_category_desc);
	fn_import_data_clean_row($rs_category_desc);
	fn_print_r($rs_category_desc);

	fn_import_data_sub_categories($conn,$source_category_id);
}

function fn_import_data_sub_categories($conn,$source_parent_category_id){
	$sql="SELECT * FROM cscart_categories WHERE parent_id=".$source_parent_category_id;
	$rs_categories=mysql_query($sql,$conn);
	if(mysql_num_rows($rs_categories)){
		while ($category = mysql_fetch_array($rs_categories)){
			fn_import_data_clean_row($category);
			fn_print_r($category);

			$sql="SELECT * FROM cscart_category_descriptions WHERE category_id=".$category['category_id'];	
			$rs_category_desc=mysql_query($sql,$conn);
			$category_desc = mysql_fetch_array($rs_category_desc);
			fn_import_data_clean_row($category_desc);
			fn_print_r($category_desc);
			
			fn_import_data_sub_categories($conn,$category['category_id']);
		}
	}
}