<?php
//
// Author Ioannis Matziaris [imatz] - imatzgr@gmail.com - February 2014
//
// O user updater na mhn exei prosvasi se addons pou den exoun permissions
//

if ( !defined('AREA') ) { die('Access denied'); }

$schema['offlineorders'] = array(
	'permissions' => 'view_orders',
);


//Den exoun epiloges sto permissions

//Efarmogi prosayksisis, sroutz
$schema['pricing'] = array(
	'permissions' => 'manage_pricing',
);
//Anebasma katalogou gia import
$schema['importer'] = array(
	'permissions' => 'manage_importer',
);
//Istoselida->Karteles
$schema['tags'] = array(
	'permissions' => 'manage_tags',
);