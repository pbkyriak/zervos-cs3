<?php

//[efhristos]: 13/02/2015
//an den yparxei full description kai yparxei full description2 tote to tab xarakthristika (full description2) den emfanizetai (status =D) 
//kai to periexomeno tou paei sto tab perigrafh(full description) (auto ginetai sto description.tpl)

if ( !defined('AREA') ) { die('Access denied'); }

if ($mode == 'view') {
    
    $product = $view->get_var('product',array());
    $tabs = $view->get_var('tabs',array());
    
    if ((empty($product['full_description']) && !empty($product['full_description2'])) || empty($product['full_description2'])) {
        $tabs["10"]['status'] = 'D';
    }
    
    Registry::del('navigation.tabs');
    
    foreach ($tabs as $tab_id => $tab) {
            if ($tab['status'] == 'D') {
                    continue;
            }
            if (!empty($tab['template'])) {
                    $tabs[$tab_id]['html_id'] = fn_basename($tab['template'], ".tpl");
            } else {
                    $tabs[$tab_id]['html_id'] = 'product_tab_' . $tab_id;
            }

            if ($tab['show_in_popup'] != "Y") {
                    Registry::set('navigation.tabs.' . $tabs[$tab_id]['html_id'], array (
                            'title' => $tab['name'],
                            'js' => true
                    ));
            }
    }
    
    $view->assign('tabs', $tabs);
	
    /*my files [efhristos 15/2/2015] den to h8ele telika sto. tpl tou proiontos yparxei akoma o kwdikas
    $my_files = unserialize($product['my_files']);

    $view->assign('my_files', $my_files);
    */    
}
?>