<?php
//
// Author Ioannis Matziaris [imatz] - imatzgr@gmail.com - March 2014
//
// OktabitUpdater

use Tygh\Registry;
use Tygh\Storage;

if ( !defined('AREA') ) { die('Access denied'); }



if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	return false;
}

if($mode=='execute'){	
	set_time_limit(0);
	ini_set('memory_limit', '1024M');
	//define('DEBUG_QUERIES',true);

	$conn=fn_import_data_db_connection();
	
	//fn_vnp_align_categories($conn);	

	//fn_vnp_align_products($conn);
	
	fn_import_features($conn);

	fn_import_data_db_close($conn);
	exit;
}


function fn_import_features($conn){
		
	//product features
	$sql="SELECT * FROM cscart_product_features WHERE feature_id IN ( SELECT feature_id FROM `cscart_product_features_values` v WHERE product_id IN (SELECT product_id FROM cscart_products_categories WHERE category_id IN (SELECT category_id FROM cscart_categories WHERE id_path LIKE '%/409/%')) )";
	$rs_features=mysql_query($sql,$conn);
	while ($feature = mysql_fetch_array($rs_features)){
		
		$feature_id=fv_vnp_align_feature($conn,$feature);
		//des an einai kanoyrio (den exei variants)
		$variant_cnt=db_get_field("SELECT COUNT(variant_id) as cnt FROM ?:product_feature_variants WHERE feature_id=?i ",$feature_id);
		
	/*	$sql="SELECT v.*,vd.* FROM cscart_product_feature_variants v
		LEFT JOIN cscart_product_feature_variant_descriptions vd ON v.variant_id=vd.variant_id AND vd.lang_code='EL'
		WHERE v.feature_id=".$feature['feature_id']."
		AND v.variant_id IN (SELECT variant_id FROM `cscart_product_features_values` fv WHERE fv.feature_id=".$feature['feature_id']."
		AND fv.product_id in (SELECT product_id FROM cscart_products_categories WHERE category_id IN (SELECT category_id FROM cscart_categories WHERE id_path LIKE '%/409/%')))
		";
	*/
	
	/*	$sql="SELECT v.* FROM cscart_product_feature_variants v
		WHERE v.feature_id=".$feature['feature_id']."
		AND v.variant_id IN (SELECT variant_id FROM `cscart_product_features_values` fv WHERE fv.feature_id=".$feature['feature_id']."
		AND fv.product_id in (SELECT product_id FROM cscart_products_categories WHERE category_id IN (SELECT category_id FROM cscart_categories WHERE id_path LIKE '%/409/%')))
		";
		$rs_variants=mysql_query($sql,$conn);
		while ($variant = mysql_fetch_array($rs_variants)){
			echo '<br>****variant****:<br>';
			fn_print_r($variant);
			//define('DEBUG_QUERIES',true);
			$sql="SELECT * FROM cscart_product_feature_variant_descriptions WHERE variant_id = ".$variant['variant_id'];
			$rs_variant_descs=mysql_query($sql,$conn);
			
			$ins_variant_descs=$search_variant_desc=$found_variant_desc=array();
			//caje na to breis ap'thn perigrafh
			while ($variant_desc = mysql_fetch_array($rs_variant_descs)){
			
				$search_variant_desc=(empty($variant_cnt))?array():db_get_array("SELECT * FROM ?:product_feature_variants v
				INNER JOIN ?:product_feature_variant_descriptions vd ON v.variant_id=vd.variant_id
				WHERE v.feature_id=?i AND variant=?s AND lang_code=?s ",
				$feature_id,$variant_desc['variant'],$variant_desc['lang_code']);
				if (!empty($search_variant_desc)){
					$found_variant_desc=array_merge($search_variant_desc, $found_variant_desc);
				}else{
					$ins_variant_descs[]=$variant_desc;
				}
			}
			$variant_id=null;
			//an to brhkes caje to epikratestero id
			if (!empty($found_variant_desc)){
				echo '<br>found variant:<br>';
				fn_print_r($found_variant_desc);
			
				$vt_desc_cnt=$vt_lang_found=array();
				foreach ($found_variant_desc as $found_vt){
					++$vt_desc_cnt[$found_vt['variant_id']];
					$vt_lang_found[$found_vt['variant_id']][]=strtoupper($found_vt['lang_code']);
				}
				
				$variant_id=0;
				
				foreach($vt_desc_cnt as $id=>$no){
					if($no>$vt_desc_cnt[$variant_id]) $variant_id=$id; 
				}
				//bale to pedio vnp_vid
				db_query("UPDATE ?:product_feature_variants SET vnp_vid=?i WHERE variant_id=?i",$variant['variant_id'],$variant_id);
				
			}else{//allivs ftiaxto tvra
				echo '<br>VARIANT NOT FOUND<br>';
			
				$insert_data=$variant;
				unset($insert_data['variant_id']);
				$insert_data['feature_id']=$feature_id;
				$insert_data['vnp_vid']=$variant['variant_id'];
				echo '<br>insert_data:<br>';
				fn_import_data_clean_row($insert_data);
				fn_print_r($insert_data);
				$variant_id=db_query("INSERT INTO ?:product_feature_variants ?e",$insert_data);
				
				$el=$en=false;
				//des ti allo exoyme na perasoyme (PERIGRAFES)
				if (!empty($ins_variant_descs)){
					foreach ($ins_variant_descs as $ivd){
						$insert_desc_data=$ivd;
						$insert_desc_data['variant_id']=$variant_id;
						
						$insert_desc_data['lang_code']=strtoupper($ivd['lang_code']);
						if ($insert_desc_data['lang_code']=='EN') $en=true;
						elseif ($insert_desc_data['lang_code']=='EL') $el=true;
						
						echo '<br>variant desc for insert:<br>';
						fn_import_data_clean_row($insert_desc_data);
						fn_print_r($insert_desc_data);
						db_query("INSERT INTO ?:product_feature_variant_descriptions ?e",$insert_desc_data);
					}
				}
			}
			echo "<br>variant_id:$variant_id<br>";
		*/	
			
			/*
			if(!$el && (empty($vt_lang_found[$variant_id]) || !in_array('EL', $vt_lang_found[$variant_id]))){
				if(!empty($insert_desc_data)){
					$insert_desc_data['lang_code']='EL';
				} elseif(!empty($found_variant_desc[0])){
					$insert_desc_data=$found_variant_desc[0];
					$insert_desc_data['variant_id']=$variant_id;
					$insert_desc_data['lang_code']='EL';
				}
				echo '<br>adding variant desc for consistency:<br>';
				fn_print_r($insert_desc_data);
				//$variant_id=db_query("INSERT INTO ?:product_feature_variant_descriptions ?e",$insert_desc_data);
			}
			if(!$en && (empty($vt_lang_found[$variant_id]) || !in_array('EN', $vt_lang_found[$variant_id]))){
				if(!empty($insert_desc_data)){
					$insert_desc_data['lang_code']='EN';
				} elseif(!empty($found_variant_desc[0])){
					$insert_desc_data=$found_variant_desc[0];
					$insert_desc_data['variant_id']=$variant_id;
					$insert_desc_data['lang_code']='EN';
				}
				echo '<br>adding variant desc for consistency:<br>';
				fn_print_r($insert_desc_data);
				//$variant_id=db_query("INSERT INTO ?:product_feature_variant_descriptions ?e",$insert_desc_data);
			}
			*//* 
			$variant_id=null;
			
			if (!empty($variant_cnt)){ // an den exei brei to feature mhn kaneis kan ton kopo
				$variant_id=db_get_fields("SELECT v.variant_id FROM ?:product_feature_variants v
				INNER JOIN ?:product_feature_variant_descriptions vd ON v.variant_id=vd.variant_id
				WHERE v.feature_id=?i AND vd.variant=?s",$feature_id, $variant['variant']);
			}
			
			//An den yparxei prosuese to
			if(empty($variant_id)){
				$insert_data=array(
					'feature_id'=> $feature_id,
					'variant_name'=> $variant['variant_name'],
					'vnp_vid'=> $variant['variant_id'],
				);
				//$variant_id=db_query("INSERT INTO ?:product_feature_variants ?e",$insert_data);
			*/	/*
				$insert_data=array(
					'variant_id'=> $variant_id,
					'variant'=> $variant['variant'],
					'lang_code'=> 'EN'
				);
				//db_query("INSERT INTO ?:product_feature_variant_descriptions ?e",$insert_data);
				*/
			/*	$insert_data=array(
					'variant_id'=> $variant_id,
					'variant'=> $variant['variant'],
					'lang_code'=> 'EL'
				);
				//db_query("INSERT INTO ?:product_feature_variant_descriptions ?e",$insert_data);
				
				echo "<br>variant added:$variant_id<br>";
				fn_print_r($insert_data);
			}else{ //allivs bale to vnp_vid
				//db_query("UPDATE ?:product_feature_variants SET vnp_vid=?i WHERE variant_id=?i",$variant['variant_id'],$variant_id);
				echo "<br>variant found:$variant_id<br>";
			}
			*/
	//	}
		
	}
}
// caxnei gia feature sfs dvsmenoy feature row
// an to brei krataei to id allivs to dhmioyrgei
// epistrefei to id sth nea bash
function fv_vnp_align_feature($conn,$feature){
	echo '<br>****feature****:<br>';
	fn_print_r($feature);
	$sql="SELECT * FROM cscart_product_features_descriptions WHERE feature_id = ".$feature['feature_id'];
	$rs_feature_descs=mysql_query($sql,$conn);
	
	$ins_feature_descs=$search_feature_desc=$found_feature_desc=array();
	
	$all_align_categories=db_get_fields("SELECT category_id FROM ?:categories WHERE vnp_cid IS NOT NULL");
	
	//caje na to breis ap'thn perigrafh
	while ($feature_desc = mysql_fetch_array($rs_feature_descs)){
	//fn_print_r($feature_desc);
		$search_feature_desc=db_get_array("SELECT fd.* FROM ?:product_features f LEFT JOIN ?:product_features_descriptions fd ON f.feature_id=fd.feature_id WHERE f.feature_type=?s AND fd.description=?s AND fd.lang_code=?s ",
		$feature['feature_type'],$feature_desc['description'],$feature_desc['lang_code']);
		if (!empty($search_feature_desc)){
			$found_feature_desc=array_merge($search_feature_desc, $found_feature_desc);
		}else{
			$ins_feature_descs[]=$feature_desc;
		}
	}
	
	//feature categories
	
	$feature_categories=db_get_fields("SELECT category_id FROM ?:categories WHERE vnp_cid IN(?n)",explode(',',$feature['categories_path']));
	
	//$feature_categories=implode(',',$feature_categories);
	
	echo '<br>from categories:<br>';
	fn_print_r($feature_categories);
		
	//an to brhkes caje to epikratestero id
	if (!empty($found_feature_desc)){
		echo '<br>found feature:<br>';
		fn_print_r($found_feature_desc);
	
		$ft_desc_cnt=$ft_lang_found=array();
		foreach ($found_feature_desc as $found_ft){
			++$ft_desc_cnt[$found_ft['feature_id']];
			$ft_lang_found[$found_ft['feature_id']][]=strtoupper($found_ft['lang_code']);
		}
		
		$feature_id=0;
		
		foreach($ft_desc_cnt as $id=>$no){
			if($no>$ft_desc_cnt[$feature_id]) $feature_id=$id; 
		}
		//diaxeirish kathgorivn
		
		$existing_feature_categories=db_get_field("SELECT categories_path FROM ?:product_features WHERE feature_id=?i",$feature_id);
		if (!empty($existing_feature_categories)){
			$existing_feature_categories=explode(',',$existing_feature_categories);
			$existing_feature_categories=array_diff($existing_feature_categories,$all_align_categories);
		}
		echo '<br>existing categories:<br>';
		fn_print_r($existing_feature_categories);
		
		if (empty($existing_feature_categories)){
			$feature_categories=array();
		}elseif (empty($feature_categories)){
			$feature_categories=array_merge($existing_feature_categories,$all_align_categories);
		}else{
			$feature_categories=array_merge($existing_feature_categories,$feature_categories);
		}
		$feature_categories=implode(',',$feature_categories);
		echo '<br>final categories:<br>';
		fn_print_r($feature_categories);
		//bale to pedio vnp_fid
		//db_query("UPDATE ?:product_features SET categories_path=?s, vnp_fid=?i WHERE feature_id=?i", $feature_categories,$feature['feature_id'],$feature_id);
		db_query("UPDATE ?:product_features SET categories_path=?s WHERE feature_id=?i", $feature_categories,$feature_id);
		
	}else{//allivs ftiaxto tvra
		echo '<br>FEATURE NOT FOUND<br>';
	
		$insert_data=$feature;
		unset($insert_data['feature_id']);
		$insert_data['vnp_fid']=$feature['feature_id'];
		$insert_data['company_id']=0;
		$insert_data['categories_path']=implode(',',$feature_categories);
		if(!empty($feature['parent_id'])){
			unset($insert_data['parent_id']);
			echo '<br>****PARENT****:<br>';
			$sql="SELECT * FROM cscart_product_features WHERE feature_id=".$feature['parent_id'];
			$rs_parent=mysql_query($sql,$conn);
			if ($pfeature = mysql_fetch_array($rs_parent)){
				$insert_data['parent_id']= fv_vnp_align_feature($conn,$pfeature);
			}
		}
		echo '<br>insert_data:<br>';
		fn_import_data_clean_row($insert_data);
		fn_print_r($insert_data);
		//$feature_id=db_query("INSERT INTO ?:product_features ?e",$insert_data);
		
		//des ti allo exoyme na perasoyme (PERIGRAFES)
		if (!empty($ins_feature_descs)){
			$el=$en=false;
			foreach ($ins_feature_descs as $ifd){
				$insert_desc_data=$ifd;
				$insert_desc_data['feature_id']=$feature_id;
				
				$insert_desc_data['lang_code']=strtoupper($ifd['lang_code']);
				if ($insert_desc_data['lang_code']=='EN') $en=true;
				elseif ($insert_desc_data['lang_code']=='EL') $el=true;
				
				echo '<br>feature desc for insert:<br>';
				fn_import_data_clean_row($insert_desc_data);
				fn_print_r($insert_desc_data);
			//	db_query("INSERT INTO ?:product_features_descriptions ?e",$insert_desc_data);
			}
			
		}
	}
	echo "<br>feature_id:$feature_id<br>";
	
	
	/*
	if(!$el && (empty($ft_lang_found[$feature_id]) || !in_array('EL', $ft_lang_found[$feature_id]))){
		if(!empty($insert_desc_data)){
			$insert_desc_data['lang_code']='EL';
		} elseif(!empty($found_feature_desc[0])){
			$insert_desc_data=$found_feature_desc[0];
			$insert_desc_data['feature_id']=$feature_id;
			$insert_desc_data['lang_code']='EL';
		}
		echo '<br>adding feature desc for consistency:<br>';
		fn_print_r($insert_desc_data);
		//db_query("INSERT INTO ?:product_features_descriptions ?e",$insert_desc_data);
	}
	if(!$en && (empty($ft_lang_found[$feature_id]) || !in_array('EN', $ft_lang_found[$feature_id]))){
		if(!empty($insert_desc_data)){
			$insert_desc_data['lang_code']='EN';
		} elseif(!empty($found_feature_desc[0])){
			$insert_desc_data=$found_feature_desc[0];
			$insert_desc_data['feature_id']=$feature_id;
			$insert_desc_data['lang_code']='EN';
		}
		echo '<br>adding feature desc for consistency:<br>';
		fn_print_r($insert_desc_data);
		//db_query("INSERT INTO ?:product_features_descriptions ?e",$insert_desc_data);
	}
	*/
	return $feature_id;
}

function fn_vnp_align_categories($conn){
	$vnp_categories=db_get_array("SELECT category_id,vnp_cid,parent_id FROM ?:categories WHERE vnp_cid IS NOT NULL 
	ORDER BY ((LENGTH(id_path)-LENGTH(REPLACE(id_path, '/', ''))) / LENGTH('/'))");
	//fn_print_r($vnp_categories);
		foreach ($vnp_categories as $vnp_cat){
		echo "<br>cid:".$vnp_cat['category_id']." - old:".$vnp_cat['vnp_cid'];
		//Images
		//An den xriazete na treksei na mpei se sxolio giati den ginete elegxos an exoun mpei oi eikoines
		fn_import_data_attach_new_images($conn,$vnp_cat['vnp_cid'],$vnp_cat['category_id'],"category",'en');	
		/*
		$parent=db_get_field("SELECT category_id FROM ?:categories WHERE vnp_cid =?i",$vnp_cat['parent_id'] );
		if (empty($parent)) $parent=386; //an den brei balthn katv ap'th forhth cyxagvgia
		echo "<br>old parent:".$vnp_cat['parent_id']." - parent:$parent";
		fn_change_category_parent($vnp_cat['category_id'], $parent);
		*/
	}
	
}

//
//Products
//
function fn_vnp_align_products($conn){
	$vnp_products=db_get_fields("SELECT product_id,vnp_pid FROM ?:products WHERE vnp_pid IS NOT NULL");
		foreach ($vnp_products as $vnp_pr){
		//Images
		//An den xriazete na treksei na mpei se sxolio giati den ginete elegxos an exoun mpei oi eikoines
		fn_import_data_attach_new_images($conn,$vnp_pr['vnp_pid'],$vnp_pr['product_id'],"product",'en');		
	}
	unset($vnp_products);
}



function fn_import_data_get_old_image_data($conn,$object_id,$object_type){
	$images=array();
	$sql="SELECT * FROM cscart_images_links WHERE object_id=".$object_id." AND object_type='".$object_type."'";
	$rs_images=mysql_query($sql,$conn);
	
	if(mysql_num_rows($rs_images)){
		while ($image = mysql_fetch_array($rs_images)){
			fn_import_data_clean_row($image);
			$images[$image['pair_id']]=$image;
			$images[$image['pair_id']]['image']=array();
			$images[$image['pair_id']]['detailed']=array();

			if(!empty($image['image_id'])){
				$sql="SELECT * FROM cscart_images WHERE image_id=".$image['image_id'].";";
				$rs_image=mysql_query($sql,$conn);
				$_image = mysql_fetch_array($rs_image);
				fn_import_data_clean_row($_image);
				$_image['image_name']=$_image['image_path'];
				$_image=fn_22x_attach_absolute_image_paths($_image, $object_type);
				$images[$image['pair_id']]['image']=$_image;
				unset($rs_image);
			}
			if(!empty($image['detailed_id'])){
				$sql="SELECT * FROM cscart_images WHERE image_id=".$image['detailed_id'].";";
				$rs_image=mysql_query($sql,$conn);
				$_image = mysql_fetch_array($rs_image);
				fn_import_data_clean_row($_image);
				$_image['image_name']=$_image['image_path'];
				$_image=fn_22x_attach_absolute_image_paths($_image, 'detailed');
				$images[$image['pair_id']]['detailed']=$_image;
				unset($rs_image);
			}			
		}	
	}
	return($images);
}
function fn_import_data_attach_new_images($conn,$old_object_id,$object_id,$object_type,$lang_code){
	$images=fn_import_data_get_old_image_data($conn,$old_object_id,$object_type);
	if(!empty($images)){
		$image_types=array("image","detailed");
		foreach($images as $pair_id=>$image_data){		
			//fn_print_r($image_data);	
			foreach($image_types as $image_type){
				if(isset($image_data[$image_type])&&!empty($image_data[$image_type])){
					if (file_exists($image_data[$image_type]['absolute_path'])) {
						$name="";
						if($image_data['type']=="M"){
							$name=$object_type.'_main';
						}elseif($image_data['type']=="A"){
							$name=$object_type.'_add_additional';
						}
						if(!empty($name)){
							$image_request_ext="";
					        if($image_type=="image"){//thumbnail
					        	$image_request_ext="icon";
					        }elseif($image_type=="detailed"){
								$image_request_ext="detailed";
							}
							fn_echo("<br>Image Add. Type: ".$image_data['type'].", Name: $name, Image type: $image_type<br />");
							$_REQUEST[$name.'_image_data'] = array(
					            array(
					        		'type' => $image_data['type'],
					                'object_id' => $object_id
					            )
					        );
							//server: an einai apolito diadromi
							//url: an eina url mexri tin eikona
					        $_REQUEST['file_'.$name.'_image_'.$image_request_ext] = array($image_data[$image_type]['absolute_path']);
					        $_REQUEST['type_'.$name.'_image_'.$image_request_ext] = array('server');
		           			//fn_print_r($_REQUEST);
		           			$res=fn_attach_image_pairs($name, $object_type, $object_id, $lang_code);
		           			unset($_REQUEST[$name.'_image_data']);
		           			unset($_REQUEST['file_'.$name.'_image_'.$image_request_ext]);
		           			unset($_REQUEST['type_'.$name.'_image_'.$image_request_ext]);
		           			if(!$res){
		           				fn_echo("<b><i>Image not uploaded</i></b><br />");
		           			}else{
		           				fn_echo("<i>Image uploaded</i><br />");
		           			}
		           		}
					}
				}
			}
		}
	}
}


//
// CsCart 2.2.x
//
//
// Attach image paths
//
function fn_22x_attach_absolute_image_paths(&$image_data, $object_type)
{
//	$_OLD_HTTP_IMAGES_PATH= 'http://www.vnp.gr/images/';
//	$_OLD_DIR_IMAGE= '/home2/vnp2012/public_html/images/';
	
	$image_id = !empty($image_data['images_image_id'])? $image_data['images_image_id'] : $image_data['image_id'];
	$path = $object_type . "/" . floor($image_id / MAX_FILES_IN_DIR);

	if (!empty($image_data['image_path'])) {
		if (fn_get_file_ext($image_data['image_path']) == 'swf') { // FIXME, dirty
			$image_data['is_flash'] = true;
		}
		$image_name = $image_data['image_path'];
		$image_data['http_image_path'] = OLD_HTTP_IMAGES_PATH . $path . '/' . $image_name;
		$image_data['absolute_path'] =OLD_DIR_IMAGE . $path . '/' . $image_name;
		$image_data['image_path'] = OLD_HTTP_IMAGES_PATH . $path . '/' . $image_name;
	}

	return $image_data;
}