<?php
//
// Author: Ioannis Matziaris [imatz]
// Email: imatzgr@gmail.com
// Date: September 2014
// Details: 
//

use Tygh\Registry;
use Tygh\Storage;

if ( !defined('AREA') ) { die('Access denied'); }



if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	return false;
}

if($mode=="correct_schwarz"){
	exit;
	$titles=db_get_array("SELECT product_id, product, lang_code FROM ?:product_descriptions WHERE product LIKE '%schwarz%'");
	foreach($titles as $product){
		$title=$product["product"];
		echo("Original Title: ".$title."<br />");
	    $title = str_ireplace(" schwarz", " Black", $title);
	    $title = str_ireplace("schwarz ", "Black ", $title);
	    $title = str_ireplace("/schwarz", "/Black", $title);
	    $title = str_ireplace("schwarz/", "Black/", $title);
	    echo("Correct Title: ".$title."<br />");
	    db_query("UPDATE ?:product_descriptions SET product=?s 
	    	WHERE product_id=?i AND lang_code=?s",$title,$product["product_id"],$product["lang_code"]);
	}
	exit;
}elseif($mode=="clean_difox_images"){
	//Diagrafei eikonw apo proionta difox pou exoun erthei apo tin difox
	exit;
	$supplier_id=10;
	$all_supplier_products=db_get_fields("SELECT product_id FROM ?:products WHERE my_supplier_id=?i", $supplier_id); 
	echo("Products: ".count($all_supplier_products)."<br />");
	$count_difox_images=0;
	$count_difox_add_images=0;
	foreach($all_supplier_products as $product_id){
		echo("Product: ".$product_id."<br />");
		$main_image = fn_get_image_pairs($product_id, 'product', 'M', true, true); 
		if(!empty($main_image)){			
			$filename=basename($main_image["detailed"]["absolute_path"]);
			if(!empty($filename)){
				$difox_file="/home/vnp/public_html/difox/MediaSupply/00/".$filename;
				if(file_exists($difox_file)){
					echo("Image id: ".$main_image["detailed_id"].", Images: ".$main_image["detailed"]["image_path"]."<br />");
					echo("<br />");		
					$count_difox_images++;
					fn_delete_image_pair($main_image["pair_id"], 'product');						
				}
			}
		}
		$additional_images = fn_get_image_pairs($product_id, 'product', 'A', true, true); 	
		if(!empty($additional_images)){
			foreach($additional_images as $pair_id=>$image){
				$filename=basename($image["detailed"]["absolute_path"]);
				if(!empty($filename)){
					$difox_file="/home/vnp/public_html/difox/MediaSupply/01/".$filename;
					if(file_exists($difox_file)){
						echo("Image id: ".$image["detailed_id"].", Images: ".$image["detailed"]["image_path"]."<br />");
						echo("<br />");		
						$count_difox_add_images++;
						fn_delete_image_pair($pair_id, 'product');							
					}
				}
			}
		}
	}
	echo("Products with difox images: ".$count_difox_images."<br />");	
	echo("Additional images from difox: ".$count_difox_add_images."<br />");	
	exit;
}elseif($mode=="import_difox_images"){
	//Diabazei eikones apo fakelo. Kathe eikona exei os onoma arxeio ena tmima tou sku (aferesi df kai antikatastasi me 11)
	exit;
	$dir="/home/vnp/public_html/priceUpdater/updaterFiles/difox_images/images";
	$image_replaces=0;
	$image_new=0;
	$unknow_products=0;
	if (is_dir($dir)) {
	    if ($dh = opendir($dir)) {
	        while (($file = readdir($dh)) !== false) {
	        	if($file=="."||$file=="..")
	        		continue;
	        	$_file=pathinfo($dir."/".$file);
	        	$sku=str_replace("DF", "11", $_file["filename"]);
	            echo "SKU: ".$sku."<br />";
	            $product_id=db_get_field("SELECT product_id FROM ?:products WHERE product_code=?s",$sku);
	            if(!empty($product_id)){
	            	echo("Product found: $product_id<br />");
				    $main_image = fn_get_image_pairs($product_id, 'product', 'M', true, true); 
				    $pair_id=null;
					if(!empty($main_image)){			
						$pair_id=$main_image["pair_id"];					
						$image_replaces++;
						echo("Has image<br />");
					}else{
						$image_new++;
						echo("Hasn't image<br />");

						$im = fn_get_url_data("http://www.vnp.gr/priceUpdater/updaterFiles/difox_images/images/".$file);
						$detailed[] = $im;    
						$pair_data[] = array(
					        "pair_id" => $pair_id,
					        "type" => "M",
					        "object_id" => $product_id,
					        "image_alt" => "",
					        "detailed_alt" => "",
					    );
						fn_update_image_pairs(array(), $detailed, $pair_data, $product_id, "product");
					}
	            }else{
	            	$unknow_products++;
	            	echo("Product hasn't be found<br />");
	            }
	        }
	        closedir($dh);
	    }
	}
	echo("Unknow products: $unknow_products<br />");
	echo("New images: $image_new<br />");
	echo("Update images: $image_replaces<br />");
	exit;
}elseif($mode=="clean_features"){
	$features=db_get_array("SELECT * FROM ?:product_features ORDER BY feature_id");
	echo("Features: ".count($features)."<br />");

	$count_features_without_en_desc=0;
	$count_features_without_el_desc=0;
	$count_features_without_desc=0;
	foreach($features as $f){
		$feature_en_desc=db_get_row("SELECT * FROM ?:product_features_descriptions 
			WHERE feature_id=?i AND lang_code=?s",$f["feature_id"],"EN");
		if(empty($feature_en_desc)){
			echo("Feature without en description: ".$f["feature_id"]."<br />");
			$count_features_without_en_desc++;
		}
		$feature_el_desc=db_get_row("SELECT * FROM ?:product_features_descriptions 
			WHERE feature_id=?i AND lang_code=?s",$f["feature_id"],"EL");
		if(empty($feature_el_desc)){
			//echo("Feature without el description: ".$f["feature_id"]."<br />");
			$count_features_without_el_desc++;
		}
		if(empty($feature_en_desc)&&empty($feature_el_desc)){
			if(in_array($f["feature_type"],array('M','S','N','E'))){//Features pou prepei na exoun variants: M, S, N, E
				$count_features_without_desc++;						
				$feature_variants=db_get_array("SELECT * FROM ?:product_feature_variants 
					WHERE feature_id=?i",$f["feature_id"]);
				$products_feature_variants=db_get_array("SELECT * FROM ?:product_features_values WHERE feature_id=?i",$f["feature_id"]);
				//Elegxos an exoun variants					
				if(empty($feature_variants)){	
					//Elegxos an exoun times se proionta			
					if(empty($products_feature_variants)){
						echo("Feature me viarants<br />");
						echo("Feature: ".$f["feature_id"]." without variants and product links<br />");
					}else{
						echo("Feature me viarants<br />");
						echo("Feature: ".$f["feature_id"]." without variants<br />");
					}
				}else{
					//Elegxos an exoun times se proionta		
					if(empty($products_feature_variants)){
						echo("Feature me viarants<br />");
						echo("Feature: ".$f["feature_id"]." without product links<br />");
						if(!empty($f["feature_id"])){
							//fn_delete_feature($f["feature_id"]);
						}
					}
				}
			}else{//features xwris variants kai me times ana proion
				$products_feature_variants=db_get_array("SELECT * FROM ?:product_features_values WHERE feature_id=?i",$f["feature_id"]);
				//Elegxos an exoun times se proionta			
				if(empty($products_feature_variants)){
					echo("Feature variants ana id<br />");
					echo("Feature: ".$f["feature_id"]." without product links<br />");
					if(!empty($f["feature_id"])){
						//fn_delete_feature($f["feature_id"]);
					}
				}else{

				}
			}
		}elseif(empty($feature_en_desc)&&!empty($feature_el_desc)){
			echo("Feature with el and without en description: ".$f["feature_id"]."<br />");
		}elseif(!empty($feature_en_desc)&&empty($feature_el_desc)){
			echo("Feature with en and without el description: ".$f["feature_id"]."<br />");
		}else{
			if(in_array($f["feature_type"],array('M','S','N','E'))){//Features pou prepei na exoun variants: M, S, N, E
				$count_features_without_desc++;						
				$feature_variants=db_get_array("SELECT * FROM ?:product_feature_variants 
					WHERE feature_id=?i",$f["feature_id"]);
				$products_feature_variants=db_get_array("SELECT * FROM ?:product_features_values WHERE feature_id=?i",$f["feature_id"]);
				//Elegxos an exoun variants					
				if(empty($feature_variants)){	
					//Elegxos an exoun times se proionta			
					if(empty($products_feature_variants)){
						echo("Feature me viarants<br />");
						echo("Feature: ".$f["feature_id"]." without variants and product links<br />");
					}else{
						echo("Feature me viarants<br />");
						echo("Feature: ".$f["feature_id"]." without variants<br />");
					}
				}else{
					//Elegxos an exoun times se proionta		
					if(empty($products_feature_variants)){
						//echo("Feature me viarants<br />");
						//echo("Feature: ".$f["feature_id"]." without product links<br />");
						if(!empty($f["feature_id"])){
							//fn_delete_feature($f["feature_id"]);
						}
					}
				}
			}else{//features xwris variants kai me times ana proion
				$products_feature_variants=db_get_array("SELECT * FROM ?:product_features_values WHERE feature_id=?i",$f["feature_id"]);
				//Elegxos an exoun times se proionta			
				if(empty($products_feature_variants)){
					echo("Feature variants ana product<br />");
					echo("Feature: ".$f["feature_id"]." without product links<br />");
					if(!empty($f["feature_id"])){
						//fn_delete_feature($f["feature_id"]);
					}
				}else{

				}
			}
		}
	}
	echo("Features without en desc: $count_features_without_en_desc<br />");
	echo("Features without el desc: $count_features_without_el_desc<br />");
	echo("Features without desc: $count_features_without_desc<br />");
	exit;
}
