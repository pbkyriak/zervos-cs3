<?php

if (!defined('AREA')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	return;
}

if ($mode == 'update') {
  	Registry::set('navigation.tabs.store_bridges', array(
      	'title' => fn_get_lang_var('auto_updates'),
      	'js' => true
  	));
}