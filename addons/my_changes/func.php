<?php

if ( !defined('AREA') ) { die('Access denied'); }

function fn_my_changes_gather_additional_product_data_post(&$product, $auth, $params){
	//30-12-2014 [imatz] Prosthiki timis tilefonikis paraggelias
	if(AREA=="C"){
		$phone_price_increment=Registry::get('settings.AutoPricing.phone_price_increment');
		if(!empty($phone_price_increment)){
			$product["phone_price"]=$product["price"]+$phone_price_increment;
		}
	}
}

//Emfanisi pediou me to me poies katigories einai kathe feauter group kai feature sindedemeno
function fn_my_changes_get_product_features(&$fields, $join, $condition, $params){
    if(AREA=="A"){
        $fields[]='pf.is_brand';
    }
}

//Kaleite prin apo tin eksagogei apo tin db twn features
//Xrisi stin lipsi custom fields
function fn_my_changes_get_product_feature_data_before_select(&$fields, $join, $condition, $feature_id, $get_variants, $get_variant_images, $lang_code){
    $fields.=db_quote(', ?:product_features.is_brand');
}

//
//  Watchers
//

function fn_my_changes_allow_run_processe($process_name,$time_of_going_nothing=120){
    $last_timestamp=db_get_field("SELECT utimestamp FROM ?:processes_watchers WHERE name=?s",$process_name);
    if(!empty($last_timestamp)){
        if(($last_timestamp+$time_of_going_nothing)<time()){
            return true;
        }else{
            return false;
        }
    }else{
        $exists=db_get_field("SELECT COUNT(*) FROM ?:processes_watchers WHERE name=?s",$process_name);
        if(!$exists){
            db_query("INSERT INTO ?:processes_watchers ?e", array("name"=>$process_name));
        }
        return true;
    }
}

function fn_my_changes_update_process($process_name,$timestamp=-1){
    if($timestamp==-1){
        $timestamp=time();
    }
    $_data=array(
        "utimestamp"=>$timestamp
    );

    db_query("UPDATE ?:processes_watchers SET ?u WHERE name=?s", $_data,$process_name);
}

function fn_my_changes_add_item_to_processes($item_id,$company_id,$type,$status="W"){
    $_data=array(
        "id"=>$item_id,
        "type"=>$type,
        "company_id"=>$company_id,
        "status"=>$status
    );
    db_query("INSERT INTO ?:processes_items ?e", $_data);
}

function fn_my_changes_get_item_from_processes($item_id,$company_id,$type,$status="W"){
    $item=db_get_field("SELECT * FROM ?:processes_items
        WHERE id=?i AND type=?s AND status=?s AND company_id=?i",
        $item_id,$type,$status,$company_id);
    return($item);
}

function fn_my_changes_clear_items_from_processes($company_id,$type){
    db_query("DELETE FROM ?:processes_items WHERE type=?s AND company_id=?i", 
        $type,$company_id);
}

//
// Product Updaters
//

function fn_my_updaters_allow_product_update($product_id){
	$r=db_get_row('SELECT pricelist_updater_skip, update_only_price, update_only_stock  
		FROM ?:products WHERE product_id=?i', $product_id);
	if(isset($r['pricelist_updater_skip'])&&$r['pricelist_updater_skip']=="Y")
		return false;
	if(isset($r['update_only_price'])&&$r['update_only_price']=="Y")
		return false;
	if(isset($r['update_only_stock'])&&$r['update_only_stock']=="Y")
		return false;
	return true;
}

function fn_my_changes_get_category_name_for_adwords($product_id){
     $categname = db_get_field("SELECT pd.category 
								From cscart_products as p
								left join cscart_products_categories as pc on ( p.product_id = pc.product_id)
								left join cscart_category_descriptions as pd on (pc.category_id = pd.category_id and pd.lang_code ='el')
								where pc.link_type='M' and p.product_id=?i", $product_id);
     return $categname; 
}

function fn_my_changes_avail_opt_to_dates($availOpt) {
	$out = '';
	
	$dt1 = new DateTime();
	$dt2 = new DateTime();
	$stDays = 0;
	$edDays = 0;
	switch($availOpt) {
		case 0:
			$stDays = 0;
			$enDays = 0;
			break;
		case 1:
			$stDays = 2;
			$enDays = 3;
			break;
		case 2:
			$stDays = 3;
			$enDays = 5;
			break;
		case 3:
			$stDays = 1;
			$enDays = 2;
			break;
		case 4:
			$stDays = -1;
			$enDays = -1;
			break;
		case 5:
			$stDays = 4;
			$enDays = 7;
			break;
		case 6:
			$stDays = 7;
			$enDays = 15;
			break;
		case 7:
			$stDays = 4;
			$enDays = 10;
			break;
		case 8:
			$stDays = -1;
			$enDays = -1;
			break;
		case 9:
			$stDays = -1;
			$enDays = -1;
			break;
	}
	//printf("%s - %s -%s", $availOpt, $stDays, $enDays);
	if( $stDays>0 ) {
		if( $stDays==1 )
			$dt1->add(DateInterval::createFromDateString('1 day'));
		else
			$dt1->add(DateInterval::createFromDateString($stDays.' days'));
		if( $enDays==1 )
			$dt2->add(DateInterval::createFromDateString('1 day'));
		else
			$dt2->add(DateInterval::createFromDateString($enDays.' days'));

		$out = sprintf("%s %s - %s %s", 
			$dt1->format("d"), fn_get_lang_var('month_name_abr_'.$dt1->format("n")), 
			$dt2->format("d"), fn_get_lang_var('month_name_abr_'.$dt2->format("n"))
			);
	}
	//echo $out;
	 // return $out;	 return false; //  na mhn fernei apotelesmata jz 03/04/2017
}   