<?php
//
// Authors: Ioannis Matziaris [imatz]
// Email: imatzgr@gmail.com
// Date: Janurary 2015
// Details: my_changes
//


if (!defined('AREA')) { die('Access denied'); }

define('BESTPRICE_MERCHANT_ID', 1569);

//Updaters
define("UPDATER_BRAND_FEATURE_ID",18);

define("UPDATER_TABLETS_CATEGORY_ID",7809337);
define("UPDATER_TABLETS_SCREEN_SIZE_FEATURE_ID",2337);
define("UPDATER_TABLETS_EXTERNAL_MEMORY_FEATURE_ID",2338);
define("UPDATER_TABLETS_MEMORY_RAM_FEATURE_ID",2339);