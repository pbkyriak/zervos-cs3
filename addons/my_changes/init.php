<?php
//
// Author: Ioannis Matziaris [imatz]
// Email: imatzgr@gmail.com
// Date: December 2014
//

if ( !defined('AREA') ) { die('Access denied'); }

fn_register_hooks(
	'gather_additional_product_data_post',
	'get_product_features',
	'get_product_feature_data_before_select'
);

?>