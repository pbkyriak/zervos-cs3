<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if ( !defined('AREA') ) { die('Access denied');    }

if (!empty($_REQUEST['addon']) && $_REQUEST['addon'] == 'twigmo' && $mode != 'uninstall') {
    $twigmo_requirements_errors = fn_twg_check_requirements();
    if (!empty($twigmo_requirements_errors)) {
        foreach ($twigmo_requirements_errors as $error) {
            fn_set_notification('W', fn_get_lang_var('notice'),  $error);
        }
        return;
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($mode == 'tw_connect') {
        $tw_register = empty($_REQUEST['tw_register']) ? array() : $_REQUEST['tw_register'];
        $connector = new TwigmoConnector();
        $user_data = array(
            'email' => empty($tw_register['email']) ? '' : $tw_register['email'],
            'password' => empty($tw_register['password']) ? '' : $tw_register['password'],
            'user_id' => $auth['user_id']
        );
        $stores = empty($tw_register['stores']) ? array() : $tw_register['stores'];
        $is_connected = $connector->connect($stores, $user_data);

        $connector->displayServiceNotifications(true);
        if ($is_connected) {
            fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('twgadmin_text_store_connected'));
        }
        return array(CONTROLLER_STATUS_REDIRECT, 'addons.update&addon=twigmo');
    }

    if ($mode == 'tw_disconnect') {
        $stores = empty($_REQUEST['disconnect_stores']) ? array() : $_REQUEST['disconnect_stores'];
        TwigmoConnector::disconnect($stores, $_REQUEST['disconnect_admin'] == 'Y');
        return array(CONTROLLER_STATUS_REDIRECT, 'addons.update&addon=twigmo&disconnect=Y');
        exit;
    }

    if ($mode == 'tw_svc_auth') {
        $connector = new TwigmoConnector();
        $connector->authPage($action);
    }

    if ($mode == 'update' && $_REQUEST['addon'] == 'twigmo' && !empty($_REQUEST['tw_settings'])) {
        $company_id = fn_twg_get_current_company_id();
        TwigmoSettings::set(array('customer_connections' => array($company_id => $_REQUEST['tw_settings'])));
    }
} elseif ($mode == 'update') {
    if ($_REQUEST['addon'] == 'twigmo') {
        if (!empty($_REQUEST['selected_section']) and $_REQUEST['selected_section'] == 'twigmo_addon') {
            fn_delete_notification('twigmo_upgrade');
        }
        if (!fn_twg_is_updated()) {
            fn_set_notification('W', fn_get_lang_var('notice'),  fn_get_lang_var('twgadmin_reinstall'));
        }
        $view->assign('default_logo', fn_twg_get_default_logo_url());
        $urls = TwigmoConnector::getMobileScriptsUrls();
        $view->assign('favicon', $urls['favicon']);
        $view->assign('next_version_info', fn_twigmo_get_next_version_info());
        $stores = fn_twg_get_stores();
        $platinum_stores = fn_twg_init_push_comment(fn_twg_filter_connected_platinum_stores($stores));
        $view->assign('twg_is_connected', TwigmoConnector::anyFrontendIsConnected());
        $admin_access_id = TwigmoConnector::getAccessID('A');
        $view->assign('admin_access_id', $admin_access_id);
        $view->assign('stores', $stores);
        $view->assign('platinum_stores', $platinum_stores);
        $view->assign('max_push_length', TwigmoConnector::MAX_PUSH_LENGTH);
        $view->assign('twg_all_stores_connected', TwigmoConnector::allStoresAreConnected($stores));
        $view->assign('reset_pass_link', TwigmoConnector::getResetPassLink());
        $view->assign('is_disconnect_mode', isset($_REQUEST['disconnect']) && $admin_access_id);
        $view->assign('tw_settings', TwigmoSettings::get());
        $view->assign('connected_access_id', fn_twg_get_connected_access_id());
    }
}
