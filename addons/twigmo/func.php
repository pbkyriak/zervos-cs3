<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if ( !defined('AREA') ) { die('Access denied'); }

// To allow custom install function to work correctly
require_once(DIR_ADDONS . 'twigmo/core/fn.common.php');
require_once(DIR_ADDONS . 'twigmo/core/fn.upgrade.php');
require_once(DIR_ADDONS . 'twigmo/core/AbstractTwigmoConnector.php');
require_once(DIR_ADDONS . 'twigmo/core/TwigmoConnector.php');

function fn_twigmo_before_dispatch() // Hook
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST' || AREA != 'C' || !fn_twg_is_updated()
        || !TwigmoConnector::frontendIsConnected() || empty($_SERVER['HTTP_USER_AGENT']) || defined('AJAX_REQUEST')
        || $_REQUEST['dispatch'] == 'image.captcha') {
        return;
    }
    if (!isset($_SESSION['twg_state'])) {
        $_SESSION['twg_state'] = array();
    }
    $state = $_SESSION['twg_state'] = fn_twg_get_frontend_state($_REQUEST, $_SESSION['twg_state'], TwigmoSettings::get());
    if (!$state['twg_is_used']) {
        return;
    }
    if (fn_twg_use_https_for_customer() && !defined('HTTPS')) {
        fn_redirect(Registry::get('config.https_location') . '/' . Registry::get('config.current_url'));
    }
    $local_jsurl = Registry::get('config.twg.jsurl');
    $template = $local_jsurl ? 'mobile_index_dev.tpl' : 'mobile_index.tpl';
    Registry::set('root_template', 'addons/twigmo/' . $template);
    $view = Registry::get('view');
    $view->assign('urls', TwigmoConnector::getMobileScriptsUrls($local_jsurl));
    $view->assign('repo_revision', TwigmoSettings::get('repo_revision'));
    $view->assign('twg_state', $state);
    fn_twg_assign_google_template();
    if ($state['theme_editor_mode']) {
        header("X-Frame-Options: ");
    }
}

function fn_twigmo_dispatch_before_display() // Hook
{
    $template = Registry::get('root_template');
    $twg_path = 'addons/twigmo/';
    if ($template == $twg_path . 'mobile_index.tpl' || $template == $twg_path . 'mobile_index_dev.tpl') {
        $view = Registry::get('view');
        $local_jsurl = Registry::get('config.twg.jsurl');
        if ($local_jsurl) {
            $settings = fn_twg_get_all_settings();
            $view->assign('dev_settings', Registry::get('config.twg'));
            $view->assign('twg_settings', $settings);
            $view->assign('json_twg_settings', html_entity_decode(fn_to_json($settings), ENT_COMPAT, 'UTF-8'));
        } else {
            $view->assign('twg_settings', fn_twg_get_boot_settings());
        }
    }
}

function fn_twg_assign_google_template()
{
    if (Registry::get('addons.google_analytics.status') != 'A') {
        return;
    }
    $view = Registry::get('view');
    $google_templates = array(
        'addons/google_analytics/hooks/index/scripts.post.tpl',
        'addons/google_analytics/hooks/index/footer.post.tpl'
    );
    foreach ($google_templates as $google_template) {
        if ($view->template_exists($google_template)) {
            $view->assign('google_template', $google_template);
            return;
        }
    }
}

// Check if twigmo front end should uses https
function fn_twg_use_https_for_customer($company_id = 0)
{
    if (!$company_id) {
        $company_id = fn_twg_get_current_company_id();
    }
    if (PRODUCT_TYPE != 'ULTIMATE') {
        $company_id = null;
    }
    $settings = CSettings::instance()->get_values('General', CSettings::CORE_SECTION, true, $company_id);
    $use_https = $settings['keep_https'] == 'Y' && ($settings['secure_auth'] == 'Y' || $settings['secure_checkout'] == 'Y');
    return $use_https;
}

function fn_twg_get_lang_var($var_name, $lang_code = CART_LANGUAGE)
{
    return fn_get_lang_var($var_name, $lang_code);
}

// Check if twigmo addon was reinstalled after uploading new files
function fn_twg_is_updated()
{
    $saved_version = TwigmoSettings::get('version');
    return empty($saved_version) || $saved_version == TWIGMO_VERSION;
}

function fn_twg_get_device_type()
{
    $device = $browser = 'other';
    $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
    $device_rules = array(
        'ipad' =>               'ipad',
        'iphone' =>             'iphone',
        'ipod' =>               'iphone',
        'blackberry' =>         'blackberry',
        'android' =>            'android',
        'windows phone os 7' => 'winphone'
    );
    $browser_rules = array(
        'chrome' =>     'chrome',
        'crios' =>      'crios',
        'coast' =>      'coast',
        'firefox' =>    'firefox',
        'safari' =>     'safari',
        'opera' =>      'opera',
        'msie 9' =>     'ie9',
        'msie 10' =>    'ie10'
    );
    foreach ($device_rules as $rule => $result) {
        if (strpos($ua, $rule)) {
            $device = $result;
            break;
        }
    }
    foreach ($browser_rules as $rule => $result) {
        if (strpos($ua, $rule)) {
            $browser = $result;
            break;
        }
    }
    return array('device' => $device, 'browser' => $browser);
}

function fn_twg_get_ajax_reconnect_code()
{
    $protocol = defined('HTTPS') ? 'https' : 'http';
    $url = fn_url('helpdesk_connector.update_twigmo_connection', 'A', $protocol);
    return '<img src="' . $url . '" style="display:none" />';
}

function fn_twg_get_admin_lang_vars($lang_code = CART_LANGUAGE)
{
    return fn_get_lang_vars_by_prefix('twgadmin', $lang_code);
}

function fn_twg_get_all_lang_vars($lang_code = CART_LANGUAGE)
{
    return fn_get_lang_vars_by_prefix('twg', $lang_code);
}

function fn_twg_get_customer_lang_vars($lang_code = CART_LANGUAGE)
{
    return array_diff(fn_twg_get_all_lang_vars($lang_code), fn_twg_get_admin_lang_vars($lang_code));
}

function fn_twg_get_stores()
{
    if (PRODUCT_TYPE == 'ULTIMATE') {
        $auth = array();
        list($stores) = fn_get_companies(array(), $auth);
    } else {
        $stores = array(
            array(
                'company_id' => 0,
                'company' => Registry::get('settings.Company.company_name')
            )
        );
    }

    $stores_settings = TwigmoSettings::get('customer_connections');
    $skip_admin_checking = ACCOUNT_TYPE == 'vendor';
    $admin_is_connected = $skip_admin_checking || TwigmoConnector::adminIsConnected();
    $indexes = array();
    foreach ($stores as $key => $store) {
        $frontend_url = TwigmoConnector::getCustomerUrl($store);
        $indexes[] = $store['company_id'];
        $stores[$key]['is_connected'] = false;
        if ($admin_is_connected) {
            $store_id = $store['company_id'];
            if (!fn_is_empty($stores_settings) && isset($stores_settings[$store_id])) {
                $store = array_merge($store, $stores_settings[$store_id]);
                $store['is_connected'] = (!empty($store['access_id']) && !empty($store['url']) && $store['url'] == $frontend_url);
                $store['is_platinum'] = $store['plan_display_name'] == 'Platinum';
                $stores[$key] = $store;
            }
        }
        $stores[$key]['clear_url'] = str_replace('?dispatch=twigmo.post', '', $frontend_url);
    }
    // Set company_id as index
    return array_combine($indexes, $stores);
}


/**
 * Create new file and put serialized data here
 * @param string $file_name
 * @param array $data
 * @param boolean $serialize
 */
function fn_twg_write_to_file($file_name, $data, $serialize = true)
{
    $dir = dirname($file_name);
    if (!file_exists($dir)) {
        fn_mkdir($dir);
    }
    $file = @fopen($file_name, 'w');
    if ($file === false) {
        $message = str_replace('[file]', $file_name, fn_get_lang_var('cannot_write_file'));
        fn_set_notification('E', fn_get_lang_var('error'), $message);
        return false;
    }
    fwrite($file, $serialize ? serialize($data) : $data);
    fclose($file);
}

function fn_twg_save_version_info($version_info)
{
    fn_twg_write_to_file(TWIGMO_UPGRADE_DIR . TWIGMO_UPGRADE_VERSION_FILE, $version_info);
}

function fn_twg_get_ua_meta($ua, $ruleSections)
{
    $results = array();
    $ua = strtolower($ua);
    foreach ($ruleSections as $section => $rules) {
        $results[$section] = fn_twg_check_ua_rule($rules['rules'], $ua, $results);
    }

    return $results;

}

function fn_twg_check_ua_rule($rules, $ua, $results)
{
    $result = '';
    foreach ($rules as $rule) {
        $checked_value = isset($rule['check']) ? $results[$rule['check']] : $ua;
        if (preg_match($rule['expression'], $checked_value) xor isset($rule['is_filter'])) {
            if (isset($rule['result'])) {
                $result = $rule['result'];
            }
            if (isset($rule['rules'])) {
                $subrelsResult = fn_twg_check_ua_rule($rule['rules'], $ua, $results);
                if ($subrelsResult) {
                    $result = $subrelsResult;
                }
            }
        }
        if ($result) {
            break;
        }
    }

    return $result;
}

function fn_twg_process_ua($ua)
{
    $result = 'unknown';
    if (!file_exists(TWIGMO_UA_RULES_FILE)) {
        return $result;
    }
    $rules = unserialize(fn_get_contents(TWIGMO_UA_RULES_FILE));
    if (!is_array($rules)) {
        return $result;
    }
    $ua_meta = fn_twg_get_ua_meta($ua, $rules);
    // Save stat
    foreach ($ua_meta as $section => $value) {
        $where = array('section' => $section, 'value' => $value, 'month' => date('Y-m-1'));
        $count = db_get_field('SELECT count FROM ?:twigmo_ua_stat WHERE ?w', $where);
        if ($count) {
            db_query('UPDATE ?:twigmo_ua_stat SET count=count+1 WHERE ?w', $where);
        } else {
            $where['count'] = 1;
            db_query('INSERT INTO ?:twigmo_ua_stat ?e', $where);
        }
    }
    if ($ua_meta['device'] and in_array($ua_meta['device'], array('phone', 'tablet'))) {
        $result = $ua_meta['device'];
    }

    return $result;
}

function fn_twg_send_ua_stat()
{
    $access_id = TwigmoConnector::getAccessID('A');
    if (!$access_id) {
        return;
    }
    $query = db_quote('FROM ?:twigmo_ua_stat WHERE month<?s LIMIT ?i', date('Y-m-1'), 100);
    $needToSend = db_get_array('SELECT *, ?s as access_id ' . $query, $access_id);
    if (!count($needToSend)) {
        return;
    }
    list(, $responce) = fn_http_request('POST', TWG_UA_RULES_STAT, array('stat=' . serialize($needToSend)));
    if ($responce == 'ok') {
        db_query('DELETE ' . $query);
    }
}

function fn_twg_check_for_upgrade($display_service_notifications = true)
{
    $is_upgradable = false;
    if (isset($_SESSION['auth']) && $_SESSION['auth']['area'] == 'A' && !empty($_SESSION['auth']['user_id']) && fn_check_user_access($_SESSION['auth']['user_id'], 'upgrade_store')) {
        $is_upgradable = TwigmoConnector::checkUpdates();
        TwigmoConnector::updateUARules();
        if (TwigmoConnector::getAccessID('A')) {
            $connector = new TwigmoConnector();
            $connector->updateConnections();
            if ($display_service_notifications) {
                $connector->displayServiceNotifications();
            }
        }

        fn_twg_send_ua_stat();
    }

    return $is_upgradable;
}

function fn_twigmo_get_shipments($params, $fields_list, $joins, &$condition, $group) // Hook
{
    if (!empty($params['shipping_id'])) {
        $condition .= db_quote(' AND ?:shipments.shipping_id = ?i', $params['shipping_id']);
    }

    if (!empty($params['carrier'])) {
        $condition .= db_quote(' AND ?:shipments.carrier = ?s', $params['carrier']);
    }

    if (!empty($params['email'])) {
        $condition .= db_quote(' AND ?:orders.email LIKE ?l', '%'.trim($params['email']).'%');
    }

    return true;
}

function fn_twigmo_get_users($params, $fields, $sortings, $condition, $join) // Hook
{
    $twg_condition = fn_twg_get_users_search_condition($params);
    if ($twg_condition) {
        $condition .= $twg_condition;
    }
}

function fn_twigmo_place_order($order_id, $action = '', $__order_status = '', $cart = null)  // Hook
{
    if (AREA != 'C' || $action == 'save') {
        return;
    }
    $connector = new TwigmoConnector();
    fn_twg_send_order_push_notification($order_id, $connector);
}

function fn_twigmo_order_notification($order_info, $order_statuses, $force_notification)  // Hook
{
    $connector = new TwigmoConnector();
    $company_id = PRODUCT_TYPE == 'ULTIMATE' ? $order_info['company_id'] : 0;
    fn_twg_send_order_status_push_notification($order_info, $order_statuses, $force_notification, $connector, $company_id);
}

/*
 * Extract image from api data
 */
function fn_twg_get_image_by_api_data($api_image)
{
    if (empty($api_image['data']) || (empty($api_image['file_name']) && empty($api_image['type']))) {
        return false;
    }


    if (empty($api_image['file_name'])) {
        $api_image['file_name'] = 'image_' . strtolower(fn_generate_code('', 4)) . '.' . $api_image['type'];
    }

    $_data = base64_decode($api_image['data']);

    $image = array (
        'name' => $api_image['file_name'],
        'path' => fn_create_temp_file(),
        'size' => strlen($_data)
    );

    $fd = fopen($image['path'], 'wb');

    if (!$fd) {
        return false;
    }

    fwrite($fd, $_data, $image['size']);
    fclose($fd);
    @chmod($image['path'], DEFAULT_FILE_PERMISSIONS);

    return $image;
}

/*
 * Update additional images
 */
function fn_twg_update_images_by_api_data($images, $object_id = 0, $object_type = 'product', $lang_code = CART_LANGUAGE)
{
    $icons = array();
    $detailed = array();
    $pair_data = array();

    foreach ($images as $image) {
        $p_data = array (
            'pair_id' => 0,
            'type' => 'A',
            'image_alt' => '',
            'detailed_alt' => !empty($image['alt']) ? $image['alt'] : '',
        );


        if (!empty($image['image_id'])) {
            $image_info = db_get_row("SELECT type, pair_id FROM ?:images_links WHERE object_id = ?i AND object_type=?s AND detailed_id = ?i", $object_id, $object_type, $image['image_id']);

            if (empty($image_info) || $image_info['type'] == 'M') {
                // ignore errors in image_id
                // deny update/delete main detailed image
                continue;
            }

            if (!empty($image['deleted']) && $image['deleted'] == 'Y') {
                fn_delete_image($image['image_id'], $image_info['pair_id'], 'detailed');
                continue;
            }

            $p_data['pair_id'] = $image_info['pair_id'];
            $p_data['image_alt'] = db_get_field("SELECT a.description FROM ?:common_descriptions as a, ?:images_links as b WHERE a.object_holder = ?s AND a.lang_code = ?s AND a.object_id = b.image_id AND b.pair_id = ?i", 'images', $lang_code, $image_info['pair_id']);
        }

        $detailed_image = fn_twg_get_image_by_api_data($image);
        if (empty($image['image_id']) && empty($detailed_image)) {
            continue;
        }
        $detailed[] = $detailed_image;
        $pair_data[] = $p_data;
    }

    return fn_update_image_pairs($icons, $detailed, $pair_data, $object_id, $object_type, array(), '', 0, true, $lang_code);
}

function fn_twg_update_icons_by_api_data($image, $object_id = 0, $object_type = 'product', $lang_code = CART_LANGUAGE)
{
    if (!empty($image['deleted']) && $image['deleted'] == 'Y') {
        // delete image
        $image_info = db_get_row("SELECT image_id, pair_id FROM ?:images_links WHERE object_id = ?i AND object_type=?s AND type = 'M'", $object_id, $object_type);

        if (!empty($image_info)) {
            fn_delete_image($image_info['image_id'], $image_info['pair_id'], $object_type);
        }

        return true;
    }

    $icon_list = array();

    if ($icon = fn_twg_get_image_by_api_data($image)) {
        $icon_list[] = $icon;
    }

    $detailed_alt = db_get_field("SELECT a.description FROM ?:common_descriptions as a, ?:images_links as b WHERE a.object_holder = ?s AND a.lang_code = ?s AND a.object_id = b.detailed_id AND b.object_id = ?i AND b.object_type = ?s AND b.type = ?s", 'images', $lang_code, $object_id, $object_type, 'M');

    $icon_data = array (
        'type' => 'M',
        'image_alt' => !empty($image['alt']) ? $image['alt'] : '',
        'detailed_alt' => $detailed_alt
    );

    return fn_update_image_pairs($icon_list, array(), array($icon_data), $object_id, $object_type, array(), '', 0, true, $lang_code);
}

function fn_twg_get_carriers()
{
    return array (
        'USP'=> fn_get_lang_var('usps'),
        'UPS'=> fn_get_lang_var('ups'),
        'FDX'=> fn_get_lang_var('fedex'),
        'AUP'=> fn_get_lang_var('australia_post'),
        'DHL'=> fn_get_lang_var('dhl'),
        'CHP'=> fn_get_lang_var('chp')
    );
}

function fn_twg_get_orders_as_api_list($orders, $lang_code)
{
    $order_ids = array();
    foreach ($orders as $order) {
        $order_ids[] = $order['order_id'];
    }

    $payment_names = db_get_hash_array("SELECT order_id, payment FROM ?:orders, ?:payment_descriptions WHERE ?:payment_descriptions.payment_id = ?:orders.payment_id AND ?:payment_descriptions.lang_code = ?s AND ?:orders.order_id IN (?a)", 'order_id', $lang_code, $order_ids);

    $shippings  = db_get_hash_array("SELECT order_id, data FROM ?:order_data WHERE type = ?s AND order_id IN (?a)", 'order_id', 'L', $order_ids);

    foreach ($orders as $k => $v) {
        $orders[$k]['payment'] = !empty($payment_names[$v['order_id']]) ? $payment_names[$v['order_id']]['payment'] : '';
        $orders[$k]['shippings'] = array();
        if (!empty($shippings[$v['order_id']])) {
            $shippings = @unserialize($shippings[$v['order_id']]['data']);

            if (empty($shippings)) {
                continue;
            }

            foreach ($shippings as $shipping) {
                $orders[$k]['shippings'][] = array (
                    'carrier' => !empty($shipping['carrier']) ? $shipping['carrier'] : '',
                    'shipping' => !empty($shipping['shipping']) ? $shipping['shipping'] : '',
                );
            }
        }
    }

    $fields = array (
        'order_id',
        'user_id',
        'total',
        'timestamp',
        'status',
        'date',
        'status_info',
        'firstname',
        'lastname',
        'email',
        'payment_name',
        'shippings'
    );

    return fn_twg_get_as_api_list('orders', $orders, $fields);
}

function fn_twg_get_api_image_data($image_pair, $type='product', $image_type = 'icon', $params = array())
{
    if (empty($image_pair)) {
        return false;
    }
    if ($image_type == 'detailed' and !empty($image_pair['detailed_id']) or empty($image_pair['image_id']) or !empty($image_pair['image_id']) and empty($image_pair['icon'])) {
        $icon = isset($image_pair['detailed']) ? $image_pair['detailed'] : array();
    } elseif (!empty($image_pair['image_id'])) {
        $icon = $image_pair['icon'];
    }
    $icon['pair_id'] = $image_pair['pair_id'];

    if (isset($params['width']) and isset($params['height']) and
        (!empty($icon['image_x']) and !empty($icon['image_y'])) and
        (!isset($params['keep_proportions']) or $icon['image_x'] > $params['width'] or $icon['image_y'] > $params['height'])) {
        $icon['url'] = fn_generate_thumbnail((!empty($icon['image_path'])? $icon['image_path']: ''), $params['width'], $params['height'], !isset($params['keep_proportions']));
        $_path = str_replace(Registry::get('config.http_path') . '/', '', $icon['url']);
        $real_path = htmlspecialchars_decode(DIR_ROOT . '/' . $_path, ENT_QUOTES);
        $size = fn_get_image_size($real_path);
        $icon['image_y'] = $size ? $size[0] : $params['height'];
        $icon['image_x'] = $size ? $size[1] : $params['width'];
    } else {
        $icon['url'] = $icon['image_path'];
    }

    if (AREA == 'A') {
        $icon['url'] = (defined('HTTPS') ? 'https://' . Registry::get('config.https_host') : 'http://' . Registry::get('config.http_host')) . $icon['url'];
    }

    // Delete unnecessary fields
    if (isset($icon['absolute_path'])) {
        unset($icon['absolute_path']);
    }

    return $icon;
}

function fn_twg_get_domain_name($host)
{
    $parts = explode('.', $host);
    array_pop($parts); // remove 1st-level domain
    $domain = array_pop($parts); // get 2nd-level domain

    return $domain;
}

function fn_twg_api_get_products($params, $items_per_page = 10, $lang_code = CART_LANGUAGE)
{
    $to_unserialize = array('extend', 'variants');
    foreach ($to_unserialize as $field) {
        if (!empty($params[$field]) && is_string($params[$field])) {
            $params[$field] = unserialize($params[$field]);
        }
    }

    if (empty($params['extend'])) {
        $params['extend'] = array (
            'description'
        );
    }

    if (!empty($params['pid']) && !is_array($params['pid'])) {
        $params['pid'] = explode(',', $params['pid']);
    }

    if (!empty($params['q'])) {
        // search by product code
        $params['ppcode'] = 'Y';
        $params['subcats'] = 'Y';
        $params['status'] = 'A';
        $params['pshort'] = 'Y';
        $params['pfull'] = 'Y';
        $params['pname'] = 'Y';
        $params['pkeywords'] = 'Y';
        $params['search_performed'] = 'Y';
    }

    if (isset($params['company_id']) and $params['company_id'] == 0) {
        unset($params['company_id']);
    }

    list($products, $search, $totals) = fn_get_products($params, $items_per_page, $lang_code);

    fn_gather_additional_products_data($products, array('get_icon' => true, 'get_detailed' => true, 'get_options' => true, 'get_discounts' => true, 'get_features' => false));

    if (empty($products)) {
        return false;
    }

    $product_ids = array();
    $image_params = TwigmoSettings::get('images.catalog');
    foreach ($products  as $k => $v) {

        if (!empty($products[$k]['short_description']) || !empty($products[$k]['full_description'])) {
            $products[$k]['short_description'] = !empty($products[$k]['short_description']) ? strip_tags($products[$k]['short_description']) : fn_substr(strip_tags($products[$k]['full_description']), 0, TWG_MAX_DESCRIPTION_LEN);
            unset($products[$k]['full_description']);
        } else {
            $products[$k]['short_description'] = '';
        }

        $product_ids[] = $v['product_id'];

        // Get product image data
        if (!empty($v['main_pair'])) {
            $products[$k]['icon'] = fn_twg_get_api_image_data($v['main_pair'], 'product', 'icon', $image_params);
        }

    }

    $category_descriptions = db_get_hash_array(
            "SELECT p.product_id, p.category_id, c.category "
            . "FROM ?:products_categories AS p, ?:category_descriptions AS c "
            . "WHERE c.category_id = p.category_id AND c.lang_code = ?s AND p.product_id IN (?a) AND p.link_type = 'M'", 'product_id',
            $lang_code,
            $product_ids
   );

    foreach ($products as $k => $v) {
        if (!empty($v['product_id']) && !empty($category_descriptions[$v['product_id']])) {
            $products[$k]['category'] = $category_descriptions[$v['product_id']]['category'];
            $products[$k]['category_id'] = $category_descriptions[$v['product_id']]['category_id'];
        }
        if (!empty($v['inventory_amount']) && $v['inventory_amount'] > $v['amount']) {
            $products[$k]['amount'] = $v['inventory_amount'];
        }
    }

    $result = fn_twg_get_as_api_list('products', $products);

    return $result;
}

function fn_twg_api_get_categories($params, $lang_code = CART_LANGUAGE)
{
    $params['get_images'] = 'Y';
    $category_id = !empty($params['id']) ? $params['id'] : 0;
    $type = !empty($params['type']) ? $params['type'] : '';

    if ($type == 'one_level') {
        $type_params = array (
            'category_id' => $category_id,
            'current_category_id' => $category_id,
            'simple' => false,
            'visible' => true
        );

    } elseif ($type == 'plain_tree') {
        $type_params = array (
            'category_id' => $category_id,
            'current_category_id' => $category_id,
            'simple' => false,
            'visible' => false,
            'plain' => true
        );

    } else {
        $type_params = array (
            'simple' => false,
            'category_id' => $category_id,
            'current_category_id' => $category_id
        );
    }
    $params =  array_merge($type_params, $params);

    list($categories, ) = fn_get_categories($params, $lang_code);

    $image_params = TwigmoSettings::get('images.catalog');
    foreach ($categories as $k => $v) {
        if (!empty($v['has_children'])) {
            $categories[$k]['subcategory_count'] = db_get_field("SELECT COUNT(*) FROM ?:categories WHERE parent_id = ?i", $v['category_id']);
        }
        if (!empty($params['get_images']) && !empty($v['main_pair'])) {
            $categories[$k]['icon'] = fn_twg_get_api_image_data($v['main_pair'], 'category', 'icon', $image_params);
        }
    }

    $result = fn_twg_get_as_api_list('categories', $categories);

    return $result;
}

function fn_twigmo_get_categories(&$params, &$join, &$condition, &$fields, &$group_by, &$sortings) // Hook
{
    if (!empty($params['depth'])) {

        if (!empty($params['category_id'])) {
            $from_id_path = db_get_field("SELECT id_path FROM ?:categories WHERE category_id = ?i", $params['category_id']) . '/';

        } else {
            $from_id_path = '';
        }

        $from_id_path .= str_repeat('%/', $params['depth']) . '%';
        $condition .= db_quote(" AND NOT ?:categories.id_path LIKE ?l", "$from_id_path");
    }

    if (!empty($params['cid'])) {
        $cids = is_array($params['cid']) ? $params['cid'] : array($params['cid']);
        $condition .= db_quote(" AND ?:categories.category_id IN (?n)", $cids);
    }
}

function fn_twg_api_get_product_options($product, $lang_code = CART_LANGUAGE)
{

    $condition = $_status = $join = '';
    $extra_variant_fields = '';
    $option_ids = $variants_ids = $options = array();
    $_status .= " AND status = 'A'";
    $product_ids = $product['product_id'];

    $join = db_quote(" LEFT JOIN ?:product_options_descriptions as b ON a.option_id = b.option_id AND b.lang_code = ?s ", $lang_code);
    $fields = "a.*, b.option_name, b.option_text, b.description, b.inner_hint, b.incorrect_message, b.comment";

    if (!empty($product_ids)) {
        $_options = db_get_hash_multi_array(
            "SELECT " . $fields
            . " FROM ?:product_options as a "
            . $join
            . " WHERE a.product_id IN (?n)" . $condition . $_status
            . " ORDER BY a.position",
            array('product_id', 'option_id'), $product_ids
        );

        $global_options = db_get_hash_multi_array(
            "SELECT c.product_id AS cur_product_id, a.*, b.option_name, b.option_text, b.description, b.inner_hint, b.incorrect_message, b.comment"
            . " FROM ?:product_options as a"
            . " LEFT JOIN ?:product_options_descriptions as b ON a.option_id = b.option_id AND b.lang_code = ?s"
            . " LEFT JOIN ?:product_global_option_links as c ON c.option_id = a.option_id"
            . " WHERE c.product_id IN (?n) AND a.product_id = 0" . $condition . $_status
            . " ORDER BY a.position",
                array('cur_product_id', 'option_id'), $lang_code, $product_ids
        );

    foreach ((array) $product_ids as $product_id) {
            $_opts = (empty($_options[$product_id]) ? array() : $_options[$product_id]) + (empty($global_options[$product_id]) ? array() : $global_options[$product_id]);
            $options[$product_id] = fn_sort_array_by_key($_opts, 'position');
        }
    } else {
        //we need a separate query for global options
        $options = db_get_hash_multi_array(
            "SELECT a.*, b.option_name, b.option_text, b.description, b.inner_hint, b.incorrect_message, b.comment"
            . " FROM ?:product_options as a"
            . $join
            . " WHERE a.product_id = 0" . $condition . $_status
            . " ORDER BY a.position",
            array('product_id', 'option_id')
        );
    }

    foreach ($options as $product_id => $_options) {
        $option_ids = array_merge($option_ids, array_keys($_options));
    }

    if (empty($option_ids)) {
        if (is_array($product_ids)) {
            return $options;
        } else {
            return !empty($options[$product_ids]) ? $options[$product_ids] : array();
        }
    }

    $_status = " AND a.status='A'";

    $v_fields = "a.variant_id, a.option_id, a.position, a.modifier, a.modifier_type, a.weight_modifier, a.weight_modifier_type, $extra_variant_fields b.variant_name";
    $v_join = db_quote("LEFT JOIN ?:product_option_variants_descriptions as b ON a.variant_id = b.variant_id AND b.lang_code = ?s", $lang_code);
    $v_condition = db_quote("a.option_id IN (?n) $_status", array_unique($option_ids));
    $v_sorting = "a.position, a.variant_id";
    $variants = db_get_hash_multi_array("SELECT $v_fields FROM ?:product_option_variants as a $v_join WHERE $v_condition ORDER BY $v_sorting", array('option_id', 'variant_id'));

    foreach ($variants as $option_id => $_variants) {
        $variants_ids = array_merge($variants_ids, array_keys($_variants));
    }

    if (isset($variants_ids) && empty($variants_ids)) {
        return is_array($product_ids)? $options: $options[$product_ids];
    }

    $image_pairs = fn_get_image_pairs(array_unique($variants_ids), 'variant_image', 'V', true, true, $lang_code);

    foreach ($variants as $option_id => &$_variants) {
        foreach ($_variants as $variant_id => &$_variant) {
            $_variant['image_pair'] = !empty($image_pairs[$variant_id])? reset($image_pairs[$variant_id]) : array();
        }
    }

    foreach ($options as $product_id => &$_options) {
        foreach ($_options as $option_id => &$_option) {
            // Add variant names manually, if this option is "checkbox"
            if ($_option['option_type'] == 'C' && !empty($variants[$option_id])) {
                foreach ($variants[$option_id] as $variant_id => $variant) {
                    $variants[$option_id][$variant_id]['variant_name'] = $variant['position'] == 0 ? fn_get_lang_var('no') : fn_get_lang_var('yes');
                }
            }

            $_option['variants'] = !empty($variants[$option_id])? $variants[$option_id] : array();
        }
    }

    return is_array($product_ids)? $options: $options[$product_ids];
}

function fn_twg_get_api_product_data($product_id, $lang_code = CART_LANGUAGE)
{
    $auth = & $_SESSION['auth'];

    $get_features = $get_discounts = $get_options = AREA == 'C';
    $product = fn_get_product_data($product_id, $auth, $lang_code, '', true, true, true, $get_discounts, true, $get_features);

    if (empty($product)) {
        return array();
    }

    $product['main_product_code'] = $product['product_code'];
    fn_gather_additional_product_data($product, true, true, $get_options, $get_discounts);

    // Delete empty product feature groups
    if ($get_features && !empty($product['product_features'])) {
        foreach ($product['product_features'] as $feature_id => $feature) {
            if ($feature['feature_type'] == 'G' and empty($feature['subfeatures'])) {
                unset($product['product_features'][$feature_id]);
            }
        }
    }

    $product['product_options'] = array();
    if (!empty($product['combination'])) {
        $selected_options = fn_get_product_options_by_combination($product['combination']);
        $product['product_options'] = (!empty($selected_options)) ? fn_get_selected_product_options($product['product_id'], $selected_options, $lang_code) : $product_options[$product_id];
    }
    $product['product_options'] = fn_twg_api_get_product_options($product);
    foreach ($product['product_options'] as $k1 => $v1) {
        $option_descriptions = db_get_row("SELECT option_name, option_text, description, comment FROM ?:product_options_descriptions WHERE option_id = ?i AND lang_code = ?s", $v1['option_id'], $lang_code);
        foreach ($option_descriptions as $k2 => $v2) {
            $product['product_options'][$k1][$k2] = $v2;
        }
        $v1['variants'] = isset($v1['variants']) ? $v1['variants'] : array();
        foreach ($v1['variants'] as $vid=>$variant) {
            if ($v1['option_type'] == 'C') {
                $product['product_options'][$k1]['variants'][$vid]['variant_name'] = (empty($v1['position'])) ? fn_get_lang_var('no', $lang_code) : fn_get_lang_var('yes', $lang_code);
            } elseif ($v1['option_type'] == 'S' || $v1['option_type'] == 'R') {
                $variant_description = db_get_field("SELECT variant_name FROM ?:product_option_variants_descriptions WHERE variant_id = ?i AND lang_code = ?s", $vid, $lang_code);
                $product['product_options'][$k1]['variants'][$vid]['variant_name'] = $variant_description;
            }
        }
    }

    $product['category_id'] = $product['main_category'];
    $product['images'] = array();

    $images_config = TwigmoSettings::get('images');
    $image_params = $images_config['big'];
    if (!empty($product['main_pair'])) {
        $product['icon'] = fn_twg_get_api_image_data($product['main_pair'], 'product', 'icon', $images_config['prewiew']);
        $product['images'][] = fn_twg_get_api_image_data($product['main_pair'], 'product', 'detailed', $image_params);
    }

    foreach ($product['image_pairs'] as $v) {
        $product['images'][] = fn_twg_get_api_image_data($v, 'product', 'detailed', $image_params);
    }

    $product['category'] = db_get_field("SELECT category FROM ?:category_descriptions WHERE category_id = ?i AND lang_code = ?s", $product['main_category'], $lang_code);

    $product['product_options_exceptions'] = fn_twg_get_api_product_options_exceptions($product_id);
    $product['product_options_inventory'] = fn_twg_get_api_product_options_inventory($product_id, $lang_code);

    $_product = fn_twg_get_as_api_object('products', $product);

    $_product['avail_since_formated'] = strftime(Registry::get('settings.Appearance.date_format'), $_product['avail_since']);
    $_product['TIME'] = TIME;
    if (AREA == 'C') {
        $_product['tabs'] = fn_twg_get_product_tabs(array('product_id' => $product_id, 'descr_sl' => DESCR_SL));
    }

    $_product['default_image'] = fn_get_image_pairs($product_id, 'product', 'M', true, true);

    if (!empty($product['points_info'])) {
        $_product['points_info'] = $product['points_info'];
    }

  return $_product;
}

function fn_twg_get_product_tabs($params)
{
    $allowed_page_types = array('T', 'L', 'F');
    $allowed_templates = array('blocks/product_tabs/features.tpl', 'blocks/product_tabs/description.tpl');
    $allowed_block_types = array('html_block', 'pages', 'products', 'categories', 'banners');
    $tabs = Bm_ProductTabs::instance()->get_list('', $params['product_id'], $params['descr_sl']);
    foreach ($tabs as $k => $tab) {
        $isAllowedType = $tab['tab_type'] == 'B';
        $isAllowedType = ($isAllowedType or $tab['tab_type'] == 'T' and in_array($tab['template'], $allowed_templates));
        if ((empty($params['all_tabs']) && $tab['status'] != 'A') || !$isAllowedType) {
            unset($tabs[$k]);
            continue;
        }

        if ($tab['tab_type'] == 'B') {
            $block = fn_twg_get_block(array('block_id' => $tab['block_id'], 'area' => 'C'));
            if (empty($block['type']) || !in_array($block['type'], $allowed_block_types)) {
                unset($tabs[$k]);
                continue;
            }
            $block_scheme = Bm_SchemesManager::get_block_scheme($block['type'], array());
            if ($block['type'] == 'html_block') {
                $tabs[$k]['html'] = $block['content']['content'];
            } elseif (!empty($block_scheme['content']) && !empty($block_scheme['content']['items'])) {
                // Products and categories: get items
                $template_variable = 'items';
                $field = $block_scheme['content']['items'];
                $items = Bm_RenderManager::get_value($template_variable, $field, $block_scheme, $block);
                // Filter pages - only texts, links and forms posible
                if ($block['type'] == 'pages') {
                    foreach ($items as $item_id => $item) {
                        if (!in_array($item['page_type'], $allowed_page_types)) {
                            unset($items[$item_id]);
                        }
                    }
                }
                if (fn_is_empty($items)) {
                    unset($tabs[$k]);
                    continue;
                }
                $block_data = array('total_items' => count($items));
                // Images
                $image_params = TwigmoSettings::get('images.cart');
                if ($block['type'] == 'products' or $block['type'] == 'categories') {
                    $object_type = $block['type'] == 'products' ? 'product' : 'category';
                    foreach ($items as $items_id => $item) {
                        $main_pair = fn_get_image_pairs($item[$object_type . '_id'], $object_type, 'M', true, true);
                        if (!empty($main_pair)) {
                            $items[$items_id]['icon'] = fn_twg_get_api_image_data($main_pair, $object_type, 'icon', $image_params);
                        }
                    }
                }
                // Banners properties
                if ($block['type'] == 'banners') {
                    $rotation = $block['properties']['template'] == 'addons/banners/blocks/carousel.tpl' ? 'Y' : 'N';
                    $block_data['delay'] = $rotation == 'Y' ? $block['properties']['delay'] : 0;
                    $object_type = 'banner';
                }
                $block_data[$block['type']] = fn_twg_get_as_api_list($block['type'], $items);
                $tabs[$k] = array_merge($tab, $block_data);
            }
        } else {
            if ($tab['template'] == 'blocks/product_tabs/features.tpl') {
                $tabs[$k]['type'] = 'features';
            }
            if ($tab['template'] == 'blocks/product_tabs/description.tpl') {
                $tabs[$k]['type'] = 'description';
            }
        }
    }

    return array_values($tabs); // reindex
}

function fn_twg_get_api_category_data($category_id, $lang_code)
{
    $category = fn_get_category_data($category_id, $lang_code);

    if (!empty($category['parent_id'])) {
        $category['parent_category'] = db_get_field("SELECT category FROM ?:category_descriptions WHERE ?:category_descriptions.category_id = ?i AND ?:category_descriptions.lang_code = ?s", $category['parent_id'], $lang_code);
    }
    if (!empty($category['main_pair'])) {
        $category['icon'] = fn_twg_get_api_image_data($category['main_pair'], 'category', 'detailed', $_REQUEST);
    }

    return fn_twg_get_as_api_object('categories', $category);
}

function fn_twg_get_api_product_options_exceptions($product_id)
{
    if (PRODUCT_TYPE == 'COMMUNITY') {
        return array();
    }

    $exceptions = db_get_array("SELECT * FROM ?:product_options_exceptions WHERE product_id = ?i ORDER BY exception_id", $product_id);

    if (empty($exceptions)) {
        return array();
    }

    foreach ($exceptions as $k => $v) {
        $_comb = unserialize($v['combination']);
        $exceptions[$k]['combination'] = array();

        foreach ($_comb as $option_id => $variant_id) {
            $exceptions[$k]['combination'][] = array (
                'option_id' => $option_id,
                'variant_id' => $variant_id
            );
        }
    }

    return $exceptions;
}

function fn_twg_get_api_product_options_inventory($product_id, $lang_code = CART_LANGUAGE)
{
    $inventory = db_get_array("SELECT * FROM ?:product_options_inventory WHERE product_id = ?i ORDER BY position", $product_id);

    if (empty($inventory)) {
        return array();
    }

    $inventory_ids = array();
    foreach ($inventory as $k => $v) {
        $inventory_ids[] = $v['combination_hash'];
    }

    $image_pairs = fn_get_image_pairs($inventory_ids, 'product_option', 'M', false, true, $lang_code);

    foreach ($inventory as $k => $v) {
        $inventory[$k]['combination'] = array();
        $_comb = fn_get_product_options_by_combination($v['combination']);

        if (!empty($image_pairs[$v['combination_hash']])) {
            $inventory[$k]['image'] = fn_twg_get_api_image_data(current($image_pairs[$v['combination_hash']]), 'product_option', 'detailed', $_REQUEST);
        }

        foreach ($_comb as $option_id => $variant_id) {
            $inventory[$k]['combination'][] = array (
                'option_id' => $option_id,
                'variant_id' => $variant_id
            );
        }
    }

    return $inventory;
}

function fn_twg_api_update_order($order, $response)
{
    if (!defined('ORDER_MANAGEMENT')) {
        define('ORDER_MANAGEMENT', true);
    }

    if (!empty($order['status'])) {

        $statuses = fn_get_statuses(STATUSES_ORDER, false, true);

        if (!isset($statuses[$order['status']])) {
            $response->addError('ERROR_OBJECT_UPDATE', str_replace('[object]', 'orders', fn_get_lang_var('twgadmin_wrong_api_object_data')));
        } else {
            fn_change_order_status($order['order_id'], $order['status']);
        }
    }

    $cart = array();
    fn_clear_cart($cart, true);
    $customer_auth = fn_fill_auth(array(), array(), false, 'C');

    fn_form_cart($order['order_id'], $cart, $customer_auth);
    $cart['order_id'] = $order['order_id'];

    // update only profile data
    $profile_data = fn_check_table_fields($order, 'user_profiles');

    $cart['user_data'] = fn_array_merge($cart['user_data'], $profile_data);
    $cart['user_data'] = fn_array_merge($cart['user_data'], $order);
    fn_calculate_cart_content($cart, $customer_auth, 'A', true, 'I');

    if (!empty($order['details'])) {
        db_query('UPDATE ?:orders SET details = ?s WHERE order_id = ?i', $order['details'], $order['order_id']);
    }

    if (!empty($order['notes'])) {
        $cart['notes'] = $order['notes'];
    }

    fn_update_payment_surcharge($cart, $customer_auth);
    list($order_id, $process_payment) = fn_place_order($cart, $customer_auth, 'save');

    // place order routines with the disabled notifications
    //fn_order_placement_routines($order_id, fn_get_notification_rules(array(), true), true, 'save');
    return true;
}

function fn_twg_api_customer_login($user_login, $password)
{
    list($status, $user_data, $user_login, $password, $salt) = fn_twg_api_auth_routines($user_login, $password);

    if ($status === false) {
        return false;
    }

    if (empty($user_data) || (fn_generate_salted_password($password, $salt) != $user_data['password']) || empty($password)) {

        fn_log_event('users', 'failed_login', array (
            'user' => $user_login
        ));

        return false;
    }

    $_SESSION['auth'] = fn_fill_auth($user_data);

    // Set last login time
    db_query("UPDATE ?:users SET ?u WHERE user_id = ?i", array('last_login' => TIME), $user_data['user_id']);

    $_SESSION['auth']['this_login'] = TIME;
    $_SESSION['auth']['ip'] = $_SERVER['REMOTE_ADDR'];

    // Log user successful login
    fn_log_event('users', 'session', array(
        'user_id' => $user_data['user_id']
    ));

    if ($cu_id = fn_get_session_data('cu_id')) {
        $cart = array();
        fn_clear_cart($cart);
        fn_save_cart_content($cart, $cu_id, 'C', 'U');
        fn_delete_session_data('cu_id');
    }

    fn_init_user_session_data($_SESSION, $user_data['user_id']);

    return $user_data;
}

function fn_twg_api_customer_logout()
{
    // copied from common/auth.php - logout mode
    $auth = $_SESSION['auth'];

    fn_save_cart_content($_SESSION['cart'], $auth['user_id']);

    if (!empty($auth['user_id'])) {
        // Log user logout
        fn_log_event('users', 'session', array(
            'user_id' => $auth['user_id'],
            'time' => TIME - $auth['this_login'],
            'timeout' => false
        ));
    }

    unset($_SESSION['auth']);
    fn_clear_cart($_SESSION['cart'], false, true);

    fn_delete_session_data(AREA_NAME . '_user_id', AREA_NAME . '_password');

    return true;
}

/*
 * Copy of fn_auth_routines
 * from auth.php
 */
function fn_twg_api_auth_routines($user_login, $password)
{
    $status = true;

    $field = (Registry::get('settings.General.use_email_as_login') == 'Y') ? 'email' : 'user_login';

    $condition = '';


    if (PRODUCT_TYPE == 'ULTIMATE') {
        if (Registry::get('settings.Stores.share_users') == 'N' && AREA != 'A') {
            $condition = fn_get_company_condition('?:users.company_id');
        }
    }


    $user_data = db_get_row("SELECT * FROM ?:users WHERE $field = ?s" . $condition, $user_login);

    if (empty($user_data)) {
        $user_data = db_get_row("SELECT * FROM ?:users WHERE $field = ?s AND user_type IN ('A', 'V', 'P')", $user_login);
    }

    if (!empty($user_data)) {
        $user_data['usergroups'] = fn_get_user_usergroups($user_data['user_id']);
    }

    if (!empty($user_data) && (!fn_check_user_type_admin_area($user_data) && AREA == 'A' || !fn_check_user_type_access_rules($user_data))) {
        fn_set_notification('E', fn_get_lang_var('error'), fn_get_lang_var('error_area_access_denied'));
        $status = false;
    }

    if (!empty($user_data['status']) && $user_data['status'] == 'D') {
        fn_set_notification('E', fn_get_lang_var('error'), fn_get_lang_var('error_account_disabled'));
        $status = false;
    }

    $salt = isset($user_data['salt']) ? $user_data['salt'] : '';

    return array($status, $user_data, $user_login, $password, $salt);
}


/**
 * Copy of the fn_start_payment - to change MODE to place_order
 *
 * @param array $payment payment data
 * @param int $order_id order ID
 * @param bool $force_notification force user notification (true - notify, false - do not notify, order status properties will be skipped)
 */
function fn_twg_start_payment($order_id, $force_notification = array(), $payment_info)
{
    $order_info = fn_get_order_info($order_id);

    if (!empty($order_info['payment_info']) && !empty($payment_info)) {
        $order_info['payment_info'] = $payment_info;
    }

    list($is_processor_script, $processor_data) = fn_check_processor_script($order_info['payment_id'], '');
    if ($is_processor_script) {
        set_time_limit(300);
        $idata = array (
            'order_id' => $order_id,
            'type' => 'S',
            'data' => TIME,
        );
        db_query("REPLACE INTO ?:order_data ?e", $idata);


        $index_script = INDEX_SCRIPT;
        $mode = 'place_order'; // Change mode from 'post' to 'place_order'

        include(DIR_PAYMENT_FILES . $processor_data['processor_script']);

        return fn_finish_payment($order_id, $pp_response, $force_notification);
    }

    return false;
}

function fn_twg_cmp_products($products1, $products2)
{
    if ($products1[0]['product'] == $products2[0]['product']) {
        return 0;
    }

    return ($products1[0]['product'] < $products2[0]['product']) ? -1 : 1;
}

function fn_twg_cmp_products2($product1, $product2)
{
    if ($product1['cart_id'] == $product2['cart_id']) {
        return 0;
    }

    return ($product1['cart_id'] < $product2['cart_id']) ? -1 : 1;
}

function fn_twg_filter_payment_buttons($payment_buttons)
{
    if (!$payment_buttons) {
        return $payment_buttons;
    }
    $payment_buttons_processors = db_get_hash_single_array('SELECT p.payment_id, pp.processor FROM ?:payments as p
        INNER JOIN ?:payment_processors as pp ON pp.processor_id=p.processor_id WHERE p.payment_id IN (?n)',
        array('payment_id', 'processor'), array_keys($payment_buttons)
    );
    $tags_to_delete = array('<html>', '<body>', '</body>', '</html>', '<br/>');
    foreach ($payment_buttons as $payment_id => $button) {
        if (!isset($payment_buttons_processors[$payment_id])) {
            unset($payment_buttons[$payment_id]);
            continue;
        }
        $button = str_replace('<form ', '<form data-ajax="false" ', $button);
        // Delete bad tags for button
        $payment_buttons[$payment_id] = trim(str_replace($tags_to_delete, '', $button));
    }

    return array_values($payment_buttons);
}

function fn_twg_api_get_cart_promotion_input_field()
{
    $input_field = 'N';
    if (fn_display_promotion_input_field($_SESSION['cart'])) {
        $input_field = Registry::get('addons.gift_certificates.status') == 'A' ? 'G' : 'P';
    }

    return $input_field;
}


function fn_twg_get_payment_methods()
{
    $payment_groups = fn_prepare_checkout_payment_methods($_SESSION['cart'], $_SESSION['auth']);
    if (!$payment_groups) {
        $payment_groups = array();
    }
    $payment_methods = array();
    foreach ($payment_groups as $payment_group) {
        $payment_methods = array_merge_recursive($payment_methods, $payment_group);
    }

    // unset unsupported payments
    $unsupported_payment_methods = TwigmoSettings::get('unsupported_payment_methods');
    $unsupported_payment_methods = !empty($unsupported_payment_methods) ? $unsupported_payment_methods : array();
    foreach ($payment_methods as $key => $payment_method) {
        $is_payment_unsupported = (isset($payment_method['processor']) and in_array($payment_method['processor'], $unsupported_payment_methods)) or (isset($payment_method['processor_script']) and in_array($payment_method['processor_script'], $unsupported_payment_methods));
        if ($is_payment_unsupported) {
            unset($payment_methods[$key]);
        }
    }

    return fn_twg_get_as_api_list('payments', $payment_methods);
}


function fn_twg_api_get_session_cart($lang_code = CART_LANGUAGE)
{
    $cart_data = array('amount' => 0, 'subtotal' => 0);
    $cart = & $_SESSION['cart'];

    if (empty($cart)) {
        return $cart_data;
    }
    $payment_methods = fn_twg_get_payment_methods();
    if (isset($payment_methods['payment'])) {
        $payment_methods = $payment_methods['payment'];
    }

    if (false !=($first_method = reset($payment_methods)) && empty($cart['payment_id']) && isset($cart['total']) && floatval($cart['total']) != 0) {
        $cart['payment_id'] = $first_method['payment_id'];
    }
    if (isset($cart['total']) && floatval($cart['total']) == 0) {
        $cart['payment_id'] = 0;
    }

    // fetch cart data
    $cart_data = array_merge($cart_data, fn_twg_get_as_api_object('cart', $cart, array(), array('lang_code' => $lang_code)));
    if (!empty($cart_data['taxes'])) {
        $cart_data['taxes'] = fn_twg_get_as_api_list('taxes', $cart_data['taxes']);
    }
    if (!empty($cart_data['products'])) {
        $cart_data['products'] = array_reverse($cart_data['products']);
    }

    if (!empty($cart_data['payment_surcharge'])) {
        $cart_data['total'] += $cart_data['payment_surcharge'];
    } else {
        $cart_data['payment_surcharge'] = 0;
    }

    // ================ Payment buttons ========================================
    $payment_buttons = array();
    $checkout_processors = array('amazon_checkout.php', 'paypal_express.php', 'google_checkout.php');
    $included_files = get_included_files();
    $is_payments_included = false;
    foreach ($checkout_processors as $checkout_processor) {
        if (in_array(DIR_PAYMENT_FILES . $checkout_processor, $included_files)) {
            $is_payments_included = true;
            break;
        }
    }
    if ($is_payments_included) {
        // Get from templater
        $view = Registry::get('view');
        $smarty_vars = array('checkout_add_buttons', 'checkout_buttons');
        foreach ($smarty_vars as $smarty_var) {
            $buttons = $view->get_template_vars($smarty_var);
            if ($buttons !== null) {
                $payment_buttons = $buttons;
                break;
            }
        }
    } else {
        // Get payments fiels
        if (!empty($cart['products'])) {
            foreach ($cart['products'] as $product_key => $product) {
                if (!isset($cart['products'][$product_key]['product'])) {
                    $product_data = fn_get_product_data($product['product_id'], $_SESSION['auth']);
                    $cart['products'][$product_key]['product'] = $product_data['product'];
                    $cart['products'][$product_key]['short_description'] = $product_data['short_description'];
                }

            }
            $payment_buttons = fn_twg_get_checkout_payment_buttons($cart, $cart['products'], $_SESSION['auth']);
        }
    }
    $cart_data['payment_buttons'] = fn_twg_filter_payment_buttons($payment_buttons);
    // ================ /Payment buttons =======================================

    $cart_data['empty_payments'] = empty($payment_methods) ? 'Y' : 'N';
    $cart_data['coupons'] = empty($cart['coupons']) ? array() : array_keys($cart['coupons']);
    $cart_data['use_gift_certificates'] = array();
    $cart_data['gift_certificates_total'] = 0;
    if (isset($cart['use_gift_certificates'])) {
        foreach ($_SESSION['cart']['use_gift_certificates'] as $code => $cert) {
            $cart_data['use_gift_certificates'][] = array('code' => $code, 'cost' => $cert['cost']);
            $cart_data['gift_certificates_total'] += $cert['cost'];
        }
    }

    $auth = & $_SESSION['auth'];
    foreach ($cart_data['products'] as &$product) {
        if (!empty($product['extra']['points_info']['reward']) && !is_array($product['extra']['points_info']['reward'])) {
            $product['extra']['points_info']['reward'] = array('amount' => $product['extra']['points_info']['reward']);
        }
        if (Registry::get('addons.reward_points.status') != 'A' && !empty($product['extra']['points_info'])) {
            unset($product['extra']['points_info']);
        }
    }
    if (!empty($cart['points_info'])) {
        $cart_data['points_info'] = $cart['points_info'];
    }

    return $cart_data;
}

function fn_twg_api_get_cart_products($cart_items, $lang_code = CART_LANGUAGE)
{
    if (empty($cart_items)) {
        return array();
    }

    $api_products = array();
    $image_params = TwigmoSettings::get('images.cart');
    foreach ($cart_items as $k => $item) {
        $product = array (
            'product' => db_get_field("SELECT product FROM ?:product_descriptions WHERE product_id = ?i AND lang_code = ?s", $item['product_id'], $lang_code),
            'product_id' => $item['product_id'],
            'cart_id' => $k,
            'price' => $item['display_price'],
            'exclude_from_calculate' => !empty($item['extra']['exclude_from_calculate']) ? $item['extra']['exclude_from_calculate'] : null,
            'amount' => $item['amount'],
            'company_id' => $item['company_id'],
            'company_name' => fn_get_company_name($item['company_id']),
            'extra' => !empty($item['extra']) ? $item['extra'] : array()
        );
        $qty_data = db_get_row('SELECT min_qty, max_qty, qty_step FROM ?:products WHERE product_id = ?i', $item['product_id']);
        $product = array_merge($product, $qty_data);

        $main_pair = !empty($item['main_pair'])? $item['main_pair']: fn_get_image_pairs($item['product_id'], 'product', 'M', true, true, $lang_code);
        if (!empty($main_pair)) {
            $product['icon'] = fn_twg_get_as_api_object('images' ,fn_twg_get_api_image_data($main_pair, 'product', 'icon', $image_params));
        }

        if (!empty($item['product_options'])) {
            $advanced_options = fn_get_selected_product_options_info($item['product_options']);
            $api_options = fn_twg_get_as_api_list('cart_product_options', $advanced_options);

            if (!empty($api_options['option'])) {
                $product['product_options'] = $api_options['option'];
            }
        }

        $api_products[] = $product;
    }

    return $api_products;
}

function fn_twg_api_add_product_to_cart($products, &$cart)
{
    $products_data = array();

    foreach ($products as $product) {

        $cid = fn_generate_cart_id($product['product_id'], $product);

        if (!empty($products_data[$cid])) {
            $products_data[$cid]['amount'] += $product['amount'];
        }

        // Get product options images
        $product['combination_hash'] = $cid;
        if (!empty($product['combination_hash']) && !empty($product['product_options'])) {
            $image = fn_get_image_pairs($product['combination_hash'], 'product_option', 'M', true, true, CART_LANGUAGE);
            if (!empty($image)) {
                $product['main_pair'] = $image;
            }
        }

        $products_data[$cid] = $product;
    }

    $auth = & $_SESSION['auth'];

    // actions copied from the checkout.php 'add' action
    $ids = fn_add_product_to_cart($products_data, $cart, $auth);

    fn_save_cart_content($cart, $auth['user_id']);
    fn_calculate_cart_content($cart, $auth, 'S', true, 'F', true);
    return $ids;
}

function fn_twg_get_random_ids($qty, $field, $table, $condition = '')
{
    // max quantity of rows in tables to use the mysql rand()
    // to prevent server load for large tables
    $max_rand_items = 1000;

    if (!empty($condition)) {
        $condition = 'WHERE ' . $condition;
    }

    $total = db_get_field("SELECT COUNT(*) as total FROM $table $condition");

    if ($total <= $qty) {
        return db_get_fields("SELECT $field FROM $table $condition");
    }

    if ($total < $max_rand_items) {
        return db_get_fields("SELECT $field FROM $table $condition ORDER BY RAND() LIMIT $qty");
    }

    $ids = array();
    $rands = array();
    $min_rand = 0;
    $max_rand = (int) $total - 1;

    for ($i = 0; $i < $qty; $i++) {
        $rand_num = rand($min_rand, $max_rand);

        while (in_array($rand_num, $rands)) {
            $rand_num++;
            if ($rand_num > $max_rand) {
                $rand_num = $min_rand;
            }
            echo $rand_num . ' <br/> ';
        }

        $rands[] = $rand_num;
        $ids[] = db_get_field("SELECT $field FROM $table $condition LIMIT $rand_num, 1");
    }

    return $ids;
}

/*
 * Get all product id from category (with/not subcategories)
 */
function fn_twg_get_category_product_ids($category_id, $get_sub  = false)
{
    if (empty($category_id)) {
        return false;
    }

    $_categories[] = $category_id;

    if ($get_sub) {

        $category_params = array (
            'id' => !empty($category_id) ? $category_id : 0,
            'type' => 'plain_tree'
        );

        $categories = fn_twg_api_get_categories($category_params, $lang_code);

        if (!empty($categories)) {
            foreach ($categories['category'] as $category) {
                $_categories[] = $category['category_id'];
            }
        }
    }

    return db_get_fields("SELECT l.product_id FROM ?:products_categories AS l LEFT JOIN ?:products AS p ON p.product_id = l.product_id WHERE l.category_id IN (?a) AND l.link_type = 'M' AND p.status = 'A' ORDER BY l.position", $_categories);
}

/*
 * API functions adding data to response
 */
function fn_twg_set_response_pagination(&$response, $set_empty = false)
{
    $pagination = Registry::get('view')->get_template_vars('pagination');
    if (!empty($pagination)) {
        $response->setMeta($pagination['total_pages'], 'total_pages');
        $response->setMeta($pagination['total_items'], 'total_items');
        $response->setMeta($pagination['items_per_page'], 'items_per_page');
        $response->setMeta($pagination['current_page'], 'current_page');
    } elseif ($set_empty) {
        $response->setMeta(0, 'total_pages');
        $response->setMeta(0, 'total_items');
        $response->setMeta(0, 'items_per_page');
        $response->setMeta(1, 'current_page');
    }
}

function fn_twg_set_response_products(&$response, $params, $items_per_page = 0, $lang_code = CART_LANGUAGE)
{
    $products = fn_twg_api_get_products($params, $items_per_page, $lang_code);
    if (!empty($products)) {
        $response->setResponseList($products);
        if (!empty($params['cid'])) {
            $response->setMeta($params['cid'], 'category_id');
        }
    }
    fn_twg_set_response_pagination($response);
}

function fn_twg_set_response_categories(&$response, $params, $items_per_page = 0, $lang_code = CART_LANGUAGE)
{
    if (empty($items_per_page)) {
        $result = fn_twg_api_get_categories($params, $lang_code);
        $response->setMeta(db_get_field("SELECT COUNT(*) FROM ?:categories"), 'total_items');
        $response->setResponseList($result);

    } else {
        $default_params = array (
            'depth' => 0,
            'page' => 1
        );

        $params = array_merge($default_params, $params);
        $params['type'] = 'plain_tree';

        $categories = fn_twg_api_get_categories($params, $lang_code);

        if (!empty($categories)) {
            $total = count($categories['category']);
            $params['page'] = !empty($params['page']) ? $params['page'] : 1;
            fn_paginate($params['page'], $total, $items_per_page);

            $pagination = Registry::get('view')->get_template_vars('pagination');

            $start = $pagination['prev_page'] * $pagination['items_per_page'];
            $end = $start + $items_per_page;
            $result = array();

            for ($i = $start; $i < $end; $i++) {
                if (!isset($categories['category'][$i])) {
                    break;
                }

                $result[] = $categories['category'][$i];
            }

            $response->setResponseList(array('category' => $result));
            fn_twg_set_response_pagination($response);
        }

    }

    $category_id =  !empty($params['id']) ? $params['id'] : 0;

    if (!empty($category_id)) {
        $parent_data = db_get_row("SELECT a.parent_id, b.category FROM ?:categories AS a LEFT JOIN ?:category_descriptions AS b ON a.parent_id = b.category_id WHERE a.category_id = ?i AND b.lang_code = ?s", $category_id, $lang_code);

        if (!empty($parent_data)) {
            $response->setMeta($parent_data['parent_id'], 'grand_id');
            $response->setMeta($parent_data['category'], 'grand_category');
        }

        $response->setMeta($category_id, 'category_id');
        $category_data = array_pop(db_get_array("SELECT category, description FROM ?:category_descriptions WHERE category_id = ?i AND lang_code = ?s", $params['category_id'], $lang_code));
        $response->setMeta($category_data['category'], 'category_name');
        $response->setMeta($category_data['description'], 'description');
    }

}

function fn_twg_set_response_catalog(&$response, $params, $items_per_page = 0, $lang_code = CART_LANGUAGE)
{
    // supported params:
    // id - category id
    // sort_by - products sort
    // sort_order - products sort order
    // page - products page number
    // items_per_page
    $params['category_id'] = !empty($params['category_id']) ? $params['category_id'] : 0;

    $response->setData($params['category_id'], 'category_id');
    if (!empty($params['category_id'])) {
        $category_data = db_get_row("SELECT category, description FROM ?:category_descriptions WHERE category_id = ?i AND lang_code = ?s", $params['category_id'], $lang_code);
        $response->setData($category_data['category'], 'category_name');
        $response->setData($category_data['description'], 'description');
    }

    if (empty($params['page']) || $params['page'] == 1) {
        $category_params = array (
            'id' => !empty($params['category_id']) ? $params['category_id'] : 0,
            'type' => 'one_level'
        );

        $categories = fn_twg_api_get_categories($category_params, $lang_code);

        if (!empty($categories['category'])) {
            $response->setData($categories['category'], 'subcategories');
        }
    }

    if (!empty($params['category_id'])) {
        // set products
        $params['cid'] = $params['category_id'];
        $products = fn_twg_api_get_products($params, $items_per_page, $lang_code);

        if (!empty($products['product'])) {
            $response->setData($products['product'], 'products');
        }
    }

    fn_twg_set_response_pagination($response, true);
}

function fn_twg_api_get_base_statuses($add_hidden = true)
{
    $statuses = array (
        'A' => array(
            'status' => 'A',
            'description' => fn_get_lang_var('active'),
            'color' => '97CF4D',
        ),
        'D' =>array(
            'status' => 'D',
            'description' => fn_get_lang_var('disabled'),
            'color' => 'D2D2D2',
        ),
        'H' =>array(
            'status' => 'H',
            'description' => fn_get_lang_var('hidden'),
            'color' => '8D8D8D',
        )
    );

    if (!$add_hidden) {
        unset($statuses['H']);
    }

    return $statuses;
}

function fn_twg_api_get_object($response, $object_type, $params)
{
    $pattern = fn_get_schema('api', $object_type, 'php', false);
    $condition = array();

    if (!empty($pattern['key'])) {
        $api_key_id = current($pattern['key']);
        if ($key_id = $pattern['fields'][$api_key_id]['db_field']) {
            $condition = array($key_id => $params['id']);
        }
    }

    if (empty($condition)) {
        $response->addError('ERROR_WRONG_OBJECT_DATA', str_replace('[object]', $object_type, fn_get_lang_var('twgadmin_wrong_api_object_data')));
        $response->returnResponse();
    }

    $objects = fn_twg_get_api_schema_data($object_type, $condition);

    if (empty($objects)) {
        $response->addError('ERROR_OBJECT_WAS_NOT_FOUND', str_replace('[object]', $object_type, fn_get_lang_var('twgadmin_object_was_not_found')));
        $response->returnResponse();
    }

    $api_data = current($objects[$pattern['object_name']]);
    $response->setData($api_data);
    $response->returnResponse($pattern['object_name']);
}

function fn_twg_get_payment_options($payment_id)
{
    $template =  db_get_field("SELECT template FROM ?:payments WHERE payment_id = ?i", $payment_id);

    if ($template && preg_match('/(.+)\.tpl/', $template, $matches)) {
        $schema = fn_get_schema('api/payments', $matches[1], 'php', false);
        // Change date fields name
        if (is_array($schema)) {
            foreach ($schema as $key => $option) {
                if ($option['name'] == 'start_date') {
                    $schema[$key]['name'] = 'start';
                }
                if ($option['name'] == 'expiry_date') {
                    $schema[$key]['name'] = 'expiry';
                }
            }
        }

        return $schema;
    }

    return false;
}

function fn_twg_api_get_credit_cards()
{
    $values = fn_get_static_data_section('C', true, 'credit_card');
    $variants = array();

    foreach ($values as $k => $v) {
        $variants[] = array (
            'variant_id' => $v['param_id'],
            'variant_name' => $v['param'],
            'description' => $v['descr'],
            'position' => $v['position'],
            'display_cvv2' => $v['param_2'],
            'display_start_date' => $v['param_3'],
            'display_issue_number' => $v['param_4'],
        );
    }

    return $variants;
}

function fn_twg_check_api_user_data($user, $location = 'C', $lang_code = CART_LANGUAGE)
{
    if (empty($user['user_id']) && empty($user['is_complete_data'])) {
        return false;
    }

    if (!empty($user['profiles'])) {
        $user = array_merge($user, current($user['profiles']));
        unset($user['profiles']);
    }

    $profile_fields = fn_get_profile_fields($location);

    $is_complete_fields =  true;

    foreach ($profile_fields as $section_fields) {
        foreach ($section_fields as $k => $v) {
            if ($v['required'] == 'Y' && empty($user[$v['field_name']])) {
                $is_complete_fields =  false;
                fn_set_notification('E', fn_get_lang_var('error'), str_replace('[field]', $v['description'], fn_get_lang_var('error_twg_validator_required', $lang_code)));
            }
        }
    }

    if (!$is_complete_fields) {
        return false;
    }

    return $user;
}

function fn_twg_set_internal_errors(&$response, $error_code)
{
    $notifications = fn_get_notifications();

    if (empty($notifications)) {
        return false;
    }

    $i = 1;
    foreach ($notifications as $n) {
        if ($n['type'] != 'N') {
            $response->addError($error_code . $i, $n['message']);
            $i++;
        }
    }

    if ($i > 1) {
        return true;
    }

    return false;
}

function fn_twg_api_set_cart_user_data($user_data, $response, $lang_code = CART_LANGUAGE)
{
    $cart = & $_SESSION['cart'];
    $auth = & $_SESSION['auth'];

    // User update or registration
    $user = fn_twg_parse_api_object($user_data, 'users');
    $user = fn_twg_check_api_user_data($user, 'C', $lang_code);
    if (empty($user)) {
        if (!fn_twg_set_internal_errors($response, 'ERROR_FAIL_UPDATE_USER')) {
            $response->addError('ERROR_WRONG_OBJECT_DATA', str_replace('[object]', 'user', fn_get_lang_var('wrong_api_object_data', $lang_code)));
        }
        $response->returnResponse();
    }
    $cart['user_data'] = $user;

    return true;
}

function fn_twg_api_place_order($data, $response, $lang_code = CART_LANGUAGE)
{
    $cart = & $_SESSION['cart'];
    $auth = & $_SESSION['auth'];

    if (empty($cart)) {
        $response->addError('ERROR_ACCESS_DENIED', fn_get_lang_var('access_denied', $lang_code));
        $response->returnResponse();
    }

    if (!empty($data['user'])) {
        fn_twg_api_set_cart_user_data($data['user'], $response, $lang_code);
    }

    if (empty($auth['user_id']) && empty($cart['user_data'])) {
        $response->addError('ERROR_ACCESS_DENIED', fn_get_lang_var('access_denied', $lang_code));
        $response->returnResponse();
    }

    if (empty($data['payment_info']) && !empty($cart['extra_payment_info'])) {
        $data['payment_info'] = $cart['extra_payment_info'];
    }

    if (!empty($data['payment_info'])) {
        $cart['payment_id'] = (int) $data['payment_info']['payment_id'];
        unset($data['payment_info']['payment_id']);

        if (!empty($data['payment_info'])) {
            $cart['payment_info'] = $data['payment_info'];
        }

        unset($cart['payment_updated']);
        fn_update_payment_surcharge($cart, $auth);

        fn_save_cart_content($cart, $auth['user_id']);
    }

    unset($cart['payment_info']['secure_card_number']);

    // Remove previous failed order
    if (!empty($cart['failed_order_id']) || !empty($cart['processed_order_id'])) {
        $_order_ids = !empty($cart['failed_order_id']) ? $cart['failed_order_id'] : $cart['processed_order_id'];

        foreach ($_order_ids as $_order_id) {
            fn_delete_order($_order_id);
        }
        $cart['rewrite_order_id'] = $_order_ids;
        unset($cart['failed_order_id'], $cart['processed_order_id']);
    }

    if (!empty($data['shippings'])) {
            if (!fn_checkout_update_shipping($cart, $data['shippings'])) {
                unset($cart['shipping']);
            }
    }

    list (, $_SESSION['shipping_rates']) = fn_calculate_cart_content($cart, $auth, 'E');

    if (empty($cart['shipping']) && $cart['shipping_failed']) {
        $response->addError('ERROR_WRONG_CHECKOUT_DATA', fn_get_lang_var('wrong_shipping_info', $lang_code));
        $response->returnResponse();
    }
    if (empty($cart['payment_info']) && !isset($cart['payment_id'])) {
        $response->addError('ERROR_WRONG_CHECKOUT_DATA', fn_get_lang_var('wrong_payment_info', $lang_code));
        $response->returnResponse();
    }

    if (!empty($data['notes'])) {
        $cart['notes'] = $data['notes'];
    }

    $cart['details'] = fn_twg_get_twigmo_order_note();

    list($order_id, $process_payment) = fn_place_order($cart, $auth);

    if (empty($order_id)) {
        return false;
    }

    if ($process_payment == true) {
        $payment_info = !empty($cart['payment_info']) ? $cart['payment_info'] : array();
        fn_twg_start_payment($order_id, array(), $payment_info);
    }

    fn_twg_order_placement_routines($order_id);

    return $order_id;
}

/**
 * Func copies
 */

function fn_twg_get_checkout_payment_buttons(&$cart, &$cart_products, &$auth) // Hook
{
    $checkout_buttons = array();

    $ug_condition = 'AND (' . fn_find_array_in_set($auth['usergroup_ids'], 'b.usergroup_ids', true) . ')';
    $checkout_payments = db_get_fields("SELECT b.payment_id FROM ?:payment_processors as a LEFT JOIN ?:payments as b ON a.processor_id = b.processor_id WHERE a.type != 'P' AND b.status = 'A' ?p", $ug_condition);

    if (!empty($checkout_payments)) {
        foreach ($checkout_payments as $_payment_id) {
            $processor_data = fn_get_processor_data($_payment_id);
            if (!empty($processor_data['processor_script']) && file_exists(DIR_PAYMENT_FILES . $processor_data['processor_script'])) {
                include(DIR_PAYMENT_FILES . $processor_data['processor_script']);
            }
        }
    }

    return $checkout_buttons;
}


function fn_twg_order_placement_routines($order_id, $force_notification = array(), $clear_cart = true, $action = '')
{
    // don't show notifications
    // only clear cart
    $order_info = fn_get_order_info($order_id, true);
    $display_notification = true;

    fn_set_hook('placement_routines', $order_id, $order_info, $force_notification, $clear_cart, $action, $display_notification);

    if (!empty($_SESSION['cart']['placement_action'])) {
        if (empty($action)) {
            $action = $_SESSION['cart']['placement_action'];
        }
        unset($_SESSION['cart']['placement_action']);
    }

    if (AREA == 'C' && !empty($order_info['user_id'])) {
        $__fake = '';
        fn_save_cart_content($__fake, $order_info['user_id']);
    }

    $edp_data = fn_generate_ekeys_for_edp(array(), $order_info);
    fn_order_notification($order_info, $edp_data, $force_notification);

    // Empty cart
    if ($clear_cart == true && (substr_count('OPT', $order_info['status']) > 0)) {
        $_SESSION['cart'] = array(
            'user_data' => !empty($_SESSION['cart']['user_data']) ? $_SESSION['cart']['user_data'] : array(),
            'profile_id' => !empty($_SESSION['cart']['profile_id']) ? $_SESSION['cart']['profile_id'] : 0,
            'user_id' => !empty($_SESSION['cart']['user_id']) ? $_SESSION['cart']['user_id'] : 0,
        );

        db_query('DELETE FROM ?:user_session_products WHERE session_id = ?s AND type = ?s', Session::get_id(), 'C');
    }

    $is_twg_hook = true;
    $_error = false;
    fn_set_hook('order_placement_routines', $order_id, $force_notification, $order_info, $_error, $is_twg_hook);

}

function fn_twg_api_get_shippings()
{
    $_SESSION['cart']['calculate_shipping'] = true;
    list ($cart_products, $_SESSION['shipping_rates']) = fn_calculate_cart_content($_SESSION['cart'], $_SESSION['auth'], 'A', true);
    $shippings = array();

    foreach ($_SESSION['shipping_rates'] as $shipping_id => $shipping_info) {
        $shipping_info['shipping_id'] = $shipping_id;
        $shippings[] = $shipping_info;
    }

    return array($shippings, isset($_SESSION['companies_rates']) ? $_SESSION['companies_rates'] : false);
}

/**
 * Add vendors list to products search result
 * @param array $response
 * @param array $params
 * @param string $lang_code
 */
function fn_twg_add_response_vendors(&$response, $params)
{
    if (empty($params['q'])) {
        return;
    }

    $params['q'] = trim(preg_replace('/\s+/', ' ', $params['q']));

    $all_comapnies = fn_get_companies(array('status' => 'A'), $_SESSION['auth']);

    $all_comapnies = $all_comapnies[0];
    if (empty($all_comapnies)) {
        return;
    }

    $companies = array();
    foreach ($all_comapnies as $company) {
        if (preg_match('/\b' . preg_quote($company['company'], '/') . '\b/iU', $params['q'])) {
            $logo = unserialize($company['logos']);
            if (empty($logo['Customer_logo'])) {
                $url = '';
            } else {
                $url = (defined('HTTPS') ? 'https://' . Registry::get('config.https_host') : 'http://' . Registry::get('config.http_host')) . Registry::get('config.images_path') . $logo['Customer_logo']['filename'];
            }
            $companies[] = array('company_id' => $company['company_id'], 'title' => $company['company'], 'q' => trim(preg_replace('/\b' . $company['company'] . '\b/iU', '', $params['q'])), 'icon' => array('url' => $url));
        }
    }
    $response->setMeta($companies, 'companies');
    if (empty($response->data) and $companies) {
        $response->setMeta(1, 'current_page');
    }
}

/**
 * Save vendor's rates to session
 */
function fn_twigmo_calculate_cart($cart, $cart_products, $auth, $calculate_shipping, $calculate_taxes, $apply_cart_promotions) // Hook
{
    if ((PRODUCT_TYPE == 'MULTIVENDOR' || (Registry::get('settings.Suppliers.enable_suppliers') == 'Y' && Registry::get('settings.Suppliers.display_shipping_methods_separately') === 'Y')) and ($cart['shipping_required'] == true)) {
        $companies_rates = Registry::get('view')->get_template_vars('suppliers');
        if ($companies_rates !== null) {
            $companies_rates = empty($companies_rates) ? array() : $companies_rates;
            foreach ($companies_rates as $company_id => $rate) {
                $companies_rates[$company_id]['company_id'] = $company_id;
                foreach ($rate['rates'] as $shipping_id => $shipping) {
                    $companies_rates[$company_id]['rates'][$shipping_id]['shipping_id'] = $shipping_id;
                    $companies_rates[$company_id]['rates'][$shipping_id]['rates'] = array();
                }
            }
            $_SESSION['companies_rates'] = $companies_rates;
        }
    }
}


/**
 * Delete vendor's rates when placing order
 */
function fn_twigmo_order_placement_routines($order_id, $force_notification, $order_info, $_error, $is_twg_hook = false) // Hook
{
    if (!empty($_SESSION['companies_rates']) and empty($_SESSION['shipping_rates']) and !isset($_SESSION['shipping_hash'])) {
        $_SESSION['companies_rates'] = array();
    }
}

/**
 * Delete vendor's rates when init user
 */
function fn_twigmo_user_init($auth, $user_info, $first_init) { // Hook
    if ($first_init and !empty($_SESSION['companies_rates'])) {
        $_SESSION['companies_rates'] = array();
    }
}


/**
 * Get order data
 */
function fn_twg_get_order_info($order_id)
{
    $object = fn_get_order_info($order_id, false, true, true);
    $object['date'] = fn_twg_format_time($object['timestamp']);
    $status_data = fn_get_status_data($object['status']);
    if (AREA == 'C') {
        $object['status'] = empty($status_data['description']) ? '' : $status_data['description'];
    }

    $object['items'] = fn_twg_set_products_points_info(
        array(
            'products' => array_values($object['items'])
        )
    );

    $object['shipping'] = array_values(isset($object['shipping']) ? $object['shipping'] : array());
    $object['taxes'] = array_values($object['taxes']);

    if (Registry::get('settings.General.use_shipments') == 'Y') {
        $shipments = db_get_array(
            'SELECT ?:shipments.shipment_id, ?:shipments.comments, ?:shipments.tracking_number, '
            . '?:shipping_descriptions.shipping AS shipping, ?:shipments.carrier '
            . 'FROM ?:shipments '
            . 'LEFT JOIN ?:shipment_items ON (?:shipments.shipment_id = ?:shipment_items.shipment_id) '
            . 'LEFT JOIN ?:shipping_descriptions ON (?:shipments.shipping_id = ?:shipping_descriptions.shipping_id) '
            . 'WHERE ?:shipment_items.order_id = ?i AND ?:shipping_descriptions.lang_code = ?s '
            . 'GROUP BY ?:shipments.shipment_id',
            $order_id,
            DESCR_SL
        );
        if (!empty($shipments)) {
            foreach ($shipments as $id => $shipment) {
                $shipments[$id]['items'] = db_get_array(
                    'SELECT item_id, amount FROM ?:shipment_items WHERE shipment_id = ?i',
                    $shipment['shipment_id']
                );
            }
        }
        $object['shipments'] = $shipments;
    }
    return $object;
}

function fn_twg_set_products_points_info($params)
{
    if (empty($params['products'])) {
        return array();
    }
    foreach ($params['products'] as &$product) {
        if (!empty($product['extra']['points_info'])) {
            if (Registry::get('addons.reward_points.status') == 'A') {
                $product['points_info'] = $product['extra']['points_info'];
            }
            unset($product['extra']['points_info']);
        }
    }
    return $params['products'];
}

/**
 * Return order/orders info after the order placing
 * @param int $order_id
 * @param array $response
 */
function fn_twg_return_placed_orders($order_id, &$response, $items_per_page, $lang_code)
{
    $order = fn_twg_get_order_info($order_id);

    $_error = false;

    $status = db_get_field('SELECT status FROM ?:orders WHERE order_id=?i', $order_id);

    if ($status == STATUS_PARENT_ORDER) {
        $child_orders = db_get_hash_single_array("SELECT order_id, status FROM ?:orders WHERE parent_order_id = ?i", array('order_id', 'status'), $order_id);
        $status = reset($child_orders);
        $child_orders = array_keys($child_orders);
        $order['child_orders'] = $child_orders;
    }

    if (!in_array($status, fn_get_order_paid_statuses())) {
        $_error = true;
        if ($status != 'B') {
            if (!empty($child_orders)) {
                array_unshift($child_orders, $order_id);
            } else {
                $child_orders = array();
                $child_orders[] = $order_id;
            }
            $_SESSION['cart'][($status == 'N' ? 'processed_order_id' : 'failed_order_id')] = $child_orders;

            $cart = &$_SESSION['cart'];
            if (!empty($cart['failed_order_id'])) {
                $_ids = !empty($cart['failed_order_id']) ? $cart['failed_order_id'] : $cart['processed_order_id'];
                $_order_id = reset($_ids);
                $_payment_info = db_get_field("SELECT data FROM ?:order_data WHERE order_id = ?i AND type = 'P'", $_order_id);
                if (!empty($_payment_info)) {
                    $_payment_info = unserialize(fn_decrypt_text($_payment_info));
                }
                $_msg = !empty($_payment_info['reason_text']) ? $_payment_info['reason_text'] : '';
                $_msg .= empty($_msg) ? fn_get_lang_var('text_order_placed_error') : '';
                $response->addError('ERROR_FAIL_POST_ORDER', $_msg);
                $cart['processed_order_id'] = $cart['failed_order_id'];
                unset($cart['failed_order_id']);
            } elseif (!fn_twg_set_internal_errors($response, 'ERROR_FAIL_POST_ORDER')) {
                $response->addError('ERROR_FAIL_POST_ORDER', fn_get_lang_var('fail_post_order', $lang_code));
            }
        } else {
            if (!fn_twg_set_internal_errors($response, 'ERROR_ORDER_BACKORDERED')) {
                $response->addError('ERROR_ORDER_BACKORDERED', fn_get_lang_var('text_order_backordered', $lang_code));
            }
        }
        $response->returnResponse();
    }

    $auth = & $_SESSION['auth'];
    $user = fn_get_user_info($auth['user_id']);
    $profile_points = !empty($user['points']) ? $user['points'] : 0;
    if (empty($order['child_orders'])) {

        $response->setData(array(
            'order' => $order,
            'profile_points' => $profile_points
        ));

    } else {

        $params = array();
        if (empty($auth['user_id'])) {
            $params['order_id'] = $auth['order_ids'];
        } else {
            $params['user_id'] = $auth['user_id'];
        }
        list($orders, $search, $totals) = fn_get_orders($params, $items_per_page, true);
        $response->setMeta(!empty($totals['gross_total']) ? $totals['gross_total'] : 0, 'gross_total');
        $response->setMeta(!empty($totals['totally_paid']) ? $totals['totally_paid'] : 0, 'totally_paid');
        $response->setMeta($order, 'order');
        $response->setResponseList(fn_twg_get_orders_as_api_list($orders, $lang_code));
        $response->setData($profile_points, 'profile_points');
        fn_twg_set_response_pagination($response);

    }
}


/**
 * Check if a user have an access to an order
 * @param array $response
 * @param array $auth
 */
function fn_twg_check_if_order_allowed($order_id, &$_auth, &$response)
{
    // If user is not logged in and trying to see the order, redirect him to login form
    if (empty($_auth['user_id']) && empty($_auth['order_ids'])) {
        $response->addError('ERROR_ACCESS_DENIED', fn_get_lang_var('access_denied'));
        $response->returnResponse();
    }

    $allowed_id = 0;

    if (!empty($_auth['user_id'])) {
        $allowed_id = db_get_field("SELECT user_id FROM ?:orders WHERE user_id = ?i AND order_id = ?i", $_auth['user_id'], $order_id);
    } elseif (!empty($_auth['order_ids'])) {
        $allowed_id = in_array($order_id, $_auth['order_ids']);
    }

    // Check order status (incompleted order)
    if (!empty($allowed_id)) {
        $status = db_get_field('SELECT status FROM ?:orders WHERE order_id = ?i', $order_id);
        if ($status == STATUS_INCOMPLETED_ORDER) {
            $allowed_id = 0;
        }
    }
    fn_set_hook('is_order_allowed', $order_id, $allowed_id);

    if (empty($allowed_id)) { // Access denied
        $response->addError('ERROR_ACCESS_DENIED', fn_get_lang_var('access_denied'));
        $response->returnResponse();
    }
}


/**
 * Get blocks for the twigmo homepage
 * @param string $dispatch Dispatch of needed location
 * @param array $allowed_objects - array of blocks types
 * @return array blocks
 */
function fn_twg_get_blocks_for_location($dispatch, $allowed_objects)
{
    $allowed_page_types = array('T', 'L', 'F');
    $blocks = array();
    $location = Bm_Location::instance()->get($dispatch);

    if (!$location) {
        return $blocks;
    }

    $container = Bm_Container::instance()->get_list($location['location_id']);
    if (!$container or !$container['CENTRAL']) {
        return $blocks;
    }

    $grids = Bm_Grid::get_list(array($container['CENTRAL']['container_id']), array('g.*'));

    if (!$grids) {
        return $blocks;
    }

    $block_grids = Bm_Block::instance()->get_list(
        array('?:bm_snapping.*','?:bm_blocks.*', '?:bm_blocks_descriptions.*'),
        Bm_Grid::get_ids($grids)
    );

    $image_params = TwigmoSettings::get('images.catalog');
    foreach ($block_grids as $block_grid) {
        foreach ($block_grid as $block) {
            if ($block['status'] != 'A' or !in_array($block['type'], $allowed_objects)) {
                continue;
            }
            $block_data = array('block_id' => $block['block_id'], 'title' => $block['name'], 'hide_header' => isset($block['properties']['hide_header']) ? $block['properties']['hide_header'] : 'N', 'user_class' => $block['user_class']);
            $block_scheme = Bm_SchemesManager::get_block_scheme($block['type'], array());

            if ($block['type'] == 'html_block') {
                // Html block
                if (isset($block['content']['content']) and fn_string_not_empty($block['content']['content'])) {
                    $block_data['html'] = $block['content']['content'];
                }
            } elseif (!empty($block_scheme['content']) and !empty($block_scheme['content']['items'])) {
                // Products and categories: get items
                $template_variable = 'items';
                $field = $block_scheme['content']['items'];
                fn_set_hook('render_block_content_pre', $template_variable, $field, $block_scheme, $block);
                $items = Bm_RenderManager::get_value($template_variable, $field, $block_scheme, $block);
                // Filter pages - only texts, links and forms posible
                if ($block['type'] == 'pages') {
                    foreach ($items as $item_id => $item) {
                        if (!in_array($item['page_type'], $allowed_page_types)) {
                            unset($items[$item_id]);
                        }
                    }
                }
                if (empty($items)) {
                    continue;
                }
                $block_data['total_items'] = count($items);
                // Images
                if ($block['type'] == 'products' or $block['type'] == 'categories') {
                    $object_type = $block['type'] == 'products' ? 'product' : 'category';
                    foreach ($items as $items_id => $item) {
                        if (!empty($item['main_pair'])) {
                            $main_pair = $item['main_pair'];
                        } else {
                            $main_pair = fn_get_image_pairs($item[$object_type . '_id'], $object_type, 'M', true, true);
                        }
                        if (!empty($main_pair)) {
                            $items[$items_id]['icon'] = fn_twg_get_api_image_data($main_pair, $object_type, 'icon', $image_params);
                        }
                    }
                }
                // Banners properties
                if ($block['type'] == 'banners') {
                    $rotation = $block['properties']['template'] == 'addons/banners/blocks/carousel.tpl' ? 'Y' : 'N';
                    $block_data['delay'] = $rotation == 'Y' ? $block['properties']['delay'] : 0;
                }
                $block_data[$block['type']] = fn_twg_get_as_api_list($block['type'], $items);
            }
            $blocks[$block['block_id']] = $block_data;
        }
    }

    return $blocks;
}


/**
 * Returns info for homepage
 * @param object $response
 */
function fn_twg_set_response_homepage($response)
{
    $home_page_content = TwigmoSettings::get('home_page_content');

    if (empty($home_page_content)) {
        $home_page_content = 'random_products';
    }

    if ($home_page_content == 'home_page_blocks' or $home_page_content == 'tw_home_page_blocks') {
        // Use block manager: get blocks


        if ($home_page_content == 'home_page_blocks') {
            $location = 'index.index';
        } else {
            $location = 'twigmo.post';
        }
        $blocks = fn_twg_get_blocks_for_location($location, TwigmoSettings::get('block_types'));
        // Return blocks
        $response->setData($blocks);
    } else {
        $block = array();
        // Random products or category products
        if ($home_page_content == 'random_products') {
            $product_ids = fn_twg_get_random_ids(TWG_RESPONSE_ITEMS_LIMIT, 'product_id', '?:products', db_quote("status = ?s", 'A'));
            $block['title'] = 'random_products';
        } else {
            $product_ids = fn_twg_get_category_product_ids($home_page_content, false) or array();
            $block['title'] = fn_get_category_name($home_page_content);
        }
        $block['products'] = fn_twg_api_get_products(array('pid' => $product_ids), count($product_ids));
        $block['total_items'] = count($block['products']);
        $response->setData(array($block));
    }

}


/**
 * Hook - multi type filter - for twigmo should be text page or link
 */
function fn_twigmo_get_pages($params, $join, $condition, $fields, $group_by, $sortings, $lang_code) // Hook
{
    if (!empty($params['page_types'])) {
        $condition .= db_quote(" AND ?:pages.page_type IN (?a)", $params['page_types']);
    }
}


/**
 * Hook - as far as we use ajax requests we cant send 302 responce - will use meta redirect
 */
function fn_twigmo_redirect_complete() // Hook
{
    if (isset($_REQUEST['dispatch']) and $_REQUEST['dispatch'] == 'twigmo.post' and $_SERVER['REQUEST_METHOD'] == 'POST' and isset($_SESSION ['twg_state']) and $_SESSION['twg_state']['twg_is_used']) {
        fn_define('META_REDIRECT', true);
    }
}


/**
 * Prepare profile fields - delete unnecessary fields and also make arrays instead of objects to have an ability use foreach in closure templates
 */
function fn_twg_prepare_profile_fields($fields, $only_reguired_profile_fields)
{
    $allowed_keys = array('C', 'B', 'S');

    foreach ($fields as $key => $value) {
        if (!in_array($key, $allowed_keys)) {
            unset($fields[$key]);
            continue;
        }

        foreach ($fields[$key] as $field_key => $field) {
            if ($only_reguired_profile_fields == 'Y' and $field['required'] == 'N') {
                unset($fields[$key][$field_key]);
                continue;
            }
            if (!empty($field['values'])) {
                $values = array();
                foreach ($field['values'] as $value_id => $option_value) {
                    $values[] = array('id' => $value_id, 'value' => $option_value);
                }
                $fields[$key][$field_key]['values'] = $values;
            }
            if ($field['field_type'] == 'N') { // Address type
                $fields[$key][$field_key]['values'] = array(
                    array('id' => 'residental', 'value' => 'residental'),
                    array('id' => 'commercial', 'value' => 'commercial')
                );
            }
        }
        $fields[$key] = array_values($fields[$key]);
        if (empty($fields[$key])) {
            unset($fields[$key]);
            continue;
        }
    }

    return $fields;
}


/**
 * If stat addon is activated - we should keep current url and description
 */
function fn_twg_get_banner_url($banner_id, $url)
{
    if (Registry::get('addons.statistics.status') == 'A') {
        $url = db_get_field('SELECT url FROM ?:banners WHERE banner_id=?i', $banner_id);
    }

    return $url;
}

function fn_twg_get_twigmo_onclick($url)
{
    $onclick = array();
    // Process SEO links
    if (Registry::get('addons.seo.status') == 'A') {
        $_SERVER['REQUEST_URI'] = $url;
        $request = array('sef_rewrite' => 1);
        fn_seo_get_route($request);
        if ($_SERVER['REQUEST_URI'] != $url) {
            $url = $_SERVER['REQUEST_URI'];
        }
    }

    $twigmo_links = array(
        array(
            'pattern' => '/product_id=([0-9]+)/',
            'actionType' => 'product'
        ),
        array(
            'pattern' => '/page_id=([0-9]+)/',
            'actionType' => 'cmsPage'
        ),
        array(
            'pattern' => '/category_id=([0-9]+)/',
            'actionType' => 'category'
        )
    );

    foreach ($twigmo_links as $link) {
        if (preg_match($link['pattern'], $url, $matches)) {
            $onclick = $link;
            $onclick['actionId'] = $matches[1];
            unset($onclick['pattern']);
            break;
        }
    }
    return $onclick;
}

function fn_twg_get_page_onclick($url, $page_type, $page_id)
{
    $onclick = array(
        'actionType' => 'cmsPage',
        'actionId'   => $page_id
    );
    if ($page_type == 'L' && $url) {
        $url_onclick = fn_twg_get_twigmo_onclick($url);
        if (!empty($url_onclick)) {
            $onclick = $url_onclick;
        }
    }
    return $onclick;
}

/**
 * @param string $url
 * @param string $target
 * @param string $type
 * @param int $banner_id
 */
function fn_twg_get_banner_onclick($url, $target, $type, $banner_id)
{
    $onclick = array();
    if ($target == 'T' and !empty($url) and $type == 'G') {
        if (Registry::get('addons.statistics.status') == 'A') {
            $url = db_get_field('SELECT url FROM ?:banners WHERE banner_id=?i', $banner_id);
        }
        $onclick = fn_twg_get_twigmo_onclick($url);
    }

    return $onclick;
}


/**
 * Get user info
 */
function fn_twg_get_user_info($user_id)
{
    $profile = array();
    if (!$user_id) {
        $profile['user_id'] = 0;
    } else {
        $profile = fn_get_user_info($user_id);
        if (empty($profile)) {
            return false;
        }
    }
    if (AREA == 'C' && !empty($_SESSION['cart']['user_data'])) {
        $profile = array_merge($profile, $_SESSION['cart']['user_data']);
    }
    // Clear empty profile fields
    if (!empty($profile['fields'])) {
        $profile['fields'] = array_filter($profile['fields']);
    }
    if (!empty($profile['fields'])) {
        $date_format = Registry::get('settings.Appearance.calendar_date_format') == 'month_first' ? 'm/d/Y' : 'd/m/Y';
        $fields = fn_get_profile_fields();
        foreach ($fields as $section => $section_fields) {
            foreach ($section_fields as $key => $field) {
                if ($field['field_type'] == 'D' && isset($profile['fields'][$field['field_id']])) {
                    $profile['fields'][$field['field_id']] = date($date_format, $profile['fields'][$field['field_id']]);
                }
            }
        }
    }
    $profile['ship_to_another']['profile'] = fn_check_shipping_billing($profile, fn_get_profile_fields());
    $checkout_profile_fields = fn_get_profile_fields('O');
    $profile['ship_to_another']['cart'] = (fn_check_shipping_billing($profile, $checkout_profile_fields) || !fn_compare_shipping_billing($checkout_profile_fields));
    if ($user_id) {
        $profile['b_email'] = !empty($profile['b_email']) ? $profile['b_email'] : $profile['email'];
        $profile['s_email'] = !empty($profile['s_email']) ? $profile['s_email'] : $profile['email'];
    }

    return $profile;
}


/**
 * Get user info
 * @param int $page_id
 */
function fn_twg_api_get_page($page_id)
{
    if (!$page_id) {
        return false;
    }
    $page = fn_get_page_data($page_id);
    if (!$page) {
        return false;
    }

    return fn_twg_get_as_api_object('page', $page);
}


/**
 * Get form elements
 * @param array $elements
 */
function fn_twg_api_get_form_elements($elements)
{
    fn_twg_process_form_elements($elements);
    $result = array();
    if ($elements) {
        foreach ($elements as $element) {
            $element = fn_twg_get_as_api_object('form_element', $element);
            if (empty($element['variants'])) {
                unset($element['variants']);
            }
            $result[] = $element;
        }
    }

    return $result;
}

/**
 * Additional elements processing for special field types
 * @param array $elements
 */
function fn_twg_process_form_elements(&$elements)
{
    if (!is_array($elements)) {
        return false;

    }
    foreach ($elements as &$element) {
        if (defined('FORM_COUNTRIES') && !empty($element['element_type']) && $element['element_type'] == FORM_COUNTRIES) {
            $element['element_type'] = 'S';
            fn_twg_fill_countries_field($element);
        }
    }
}

function fn_twg_fill_countries_field(&$element)
{
    $countries = array_merge(
        array('' => '- ' . fn_get_lang_var('select_country') . ' -'),
        fn_get_simple_countries(true)
    );
    foreach ($countries as $country_name) {
        $element['variants'][] = array(
            'element_type' => 'G',
            'description' => $country_name
        );
    }
}

/**
 * Get form info
 * @param array $page_id
 */
function fn_twg_api_get_form_info($form)
{
    if (!$form) {
        return false;
    }

    return array('sent_message' => $form['general']['L'], 'elements' => fn_twg_api_get_form_elements($form['elements']));
}


/**
 * Get customer images path
 */
function fn_twg_get_images_path()
{
    $skin_path = fn_get_skin_path('[relative]', 'customer');

    return $skin_path . '/' . Registry::get('settings.skin_name_customer') . '/customer/images/';
}


/**
 * Get default logo's url for twigmo
 */
function fn_twg_get_default_logo_url()
{
    $manifest = fn_get_manifest('customer');

    return fn_twg_add_path_to_url(fn_twg_get_images_path() . $manifest['Customer_logo']['filename']);
}

/**
 * Add store path to url
 * @param string $url
 */
function fn_twg_add_path_to_url($url)
{
    return Registry::get('config.current_path') . '/' . $url;
}


/**
 * Check if grid belongs to twigmo location
 * @param int $grid_id
 */
function fn_twg_is_twigmo_grid($grid_id)
{
    $grid = Bm_Grid::get_by_id($grid_id);
    if (!$grid) {
        return false;
    }
    $container = Bm_Container::get_by_id($grid['container_id']);
    // Compare with twigmo location
    $twigmo_location = Bm_Location::instance()->get('twigmo.post');

    return fn_twg_is_twigmo_location($container['location_id']);
}


/**
 * Check if it's a twigmo location
 * @param int $grid_id
 */
function fn_twg_is_twigmo_location($location_id)
{
    // Compare with twigmo location
    $twigmo_location = Bm_Location::instance()->get('twigmo.post');
    if ($twigmo_location['is_default'] or ($twigmo_location['location_id'] and $twigmo_location['location_id'] != $location_id)) {
        return false;
    }

    return true;
}

/**
 * Get external info url
 * @param string $url
 * @return string
 */
function fn_twg_get_external_info_url($url)
{
    $url = trim($url);
    if (!$url) {
        return '';
    }

    return (strpos($url, 'http') === 0 ? '' : 'http://') . $url;
}

/**
 * Get available product sortings
 * @return array - [sort_label, sort_order, sort_by]
 */
function fn_twg_get_sortings()
{
    $sortings = fn_get_products_sorting(false);
    $sorting_orders = fn_get_products_sorting_orders();
    $avail_sorting = Registry::get('settings.Appearance.available_product_list_sortings');
    $default_products_sorting = fn_get_default_products_sorting();

    $result = array($default_products_sorting);
    $result[0]['sort_label'] = fn_get_lang_var('sort_by_' . $default_products_sorting['sort_by'] . '_' . $default_products_sorting['sort_order']);

    // Reverse sorting (for usage in view)
    $default_products_sorting['sort_order'] = ($default_products_sorting['sort_order'] == 'asc') ? 'desc' : 'asc';
    foreach ($sortings as $option => $value) {
        if ($default_products_sorting['sort_by'] == $option) {
            $sort_order = $default_products_sorting['sort_order'];
        } else {
            if ($value['default_order']) {
                $sort_order = $value['default_order'];
            } else {
                $sort_order = 'asc';
            }
        }
        foreach ($sorting_orders as $sort_order) {
            if ($default_products_sorting['sort_by'] != $option or $default_products_sorting['sort_order'] == $sort_order) {
                if (!$avail_sorting or !empty($avail_sorting[$option . '-' . $sort_order]) and $avail_sorting[$option . '-' . $sort_order] == 'Y') {
                    $result[] = array('sort_by' => $option, 'sort_order' => $sort_order, 'sort_label' => fn_get_lang_var('sort_by_' . $option . '_' . $sort_order));
                }
            }
        }
    }

    return $result;
}

function fn_twg_get_feature_value($value, $feature_type, $value_int, $variant_id, $variants)
{
    if ($feature_type == "D") {
        $value = fn_date_format($value_int, REGISTRY::get('settings.Appearance.date_format'));
    } elseif ($feature_type == "M") {
        $value = array();
        foreach ($variants as $variant) {
            if ($variant['selected']) {
                $value[] = $variant['variant'];
            }
        }
    } elseif ($variant_id) {
        $value = $variants[$variant_id]['variant'];
    } elseif ($value_int) {
        $value = $value_int;
    }

    return $value;
}

// Check if it is product with the recurring plans
function fn_twg_check_for_recurring($recurring_plans)
{
    return (is_array($recurring_plans) and count($recurring_plans)) ? 'Y' : 'N';
}

function fn_twg_format_time($timestamp)
{
    $settings = Registry::get('settings.Appearance');
    return strftime($settings['date_format'] . ', ' . $settings['time_format'], $timestamp);
}

function fn_twg_get_order_status($status_type, $order_id)
{
    $status = '';
    $status_info = fn_get_status_data($status_type, STATUSES_ORDER, $order_id, CART_LANGUAGE);
    if (!empty($status_info['description'])) {
        $status = $status_info['description'];
    }
    return $status;
}

function fn_twg_get_cache_request($request)
{
    $schema = fn_get_schema('twg_pre_cache', 'requests');
    if (!isset($schema[$request['dispatch']])) {
        return '';
    }
    $schema = $schema[$request['dispatch']];
    $params = array();
    foreach ($schema['params'] as $param_name => $param) {
        $params[$param_name] = $param;
    }
    if (isset($schema['param_values'])) {
        foreach ($schema['param_values'] as $param_name => $param) {
            $params[$param_name] = $request[$param];
        }
    }
    return $params;
}

/**
 * Prepare first part of settings which is used to render page
 */
function fn_twg_get_boot_settings()
{
    $settings = array();
    $addon_settings = TwigmoSettings::get();
    if (defined('HTTPS')) {
        $request_url = 'https://' . Registry::get('config.https_host') . Registry::get('config.https_path');
    } else {
        $request_url = 'http://' . Registry::get('config.http_host') . Registry::get('config.http_path');
    }
    $settings['url'] = array(
        'base'      => (defined('HTTPS')) ? Registry::get('config.https_path') : Registry::get('config.http_path'),
        'host'      => $request_url . '/',
        'index'     => INDEX_SCRIPT,
        'dispatch'  => '?dispatch=twigmo.post'
    );
    $settings['logoURL'] = empty($addon_settings['logo_url']) ? fn_twg_get_default_logo_url() : fn_twg_add_path_to_url($addon_settings['logo_url']);
    $settings['logoURL'] = str_replace(array('http://', 'https://'), '//', $settings['logoURL']);
    if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'get_settings.js' && isset($_SESSION['twg_state']['boot_request'])) {
        $settings['request'] = $_SESSION['twg_state']['boot_request'];
    } else {
        $settings['request'] = $_SESSION['twg_state']['boot_request'] = $_REQUEST;
    }
    $settings['cacheRequest'] = fn_twg_get_cache_request($settings['request']);

    $controller = $addon_settings['home_page_content'] == 'home_page_blocks' ? 'index.index' : 'twigmo.post';
    $settings['home_page_title'] = fn_twg_get_location_page_title($controller);
    $settings['companyName'] = Registry::get('settings.Company.company_name');
    $settings['geolocation'] = isset($addon_settings['geolocation']) ? $addon_settings['geolocation'] : 'Y';
    fn_set_hook('twg_get_boot_settings', $settings);
    return $settings;
}

function fn_twg_get_states()
{
    $states = fn_get_all_states();
    // Unset country_code field
    foreach ($states as $country_id => $country) {
        foreach ($country as $state_id => $state) {
            unset($states[$country_id][$state_id]['country_code']);
        }
    }
    return $states;
}

function fn_twg_get_api_data($response, $format, $required = true)
{
    $data = array();

    if (!empty($_REQUEST['data'])) {
        $data = ApiData::parseDocument(base64_decode(rawurldecode($_REQUEST['data'])), $format);
    } elseif ($required) {
        $response->addError('ERROR_WRONG_DATA', fn_get_lang_var('twgadmin_wrong_api_data'));
        $response->returnResponse();
    }

    return $data;
}

function fn_twg_filter_profile_fields($value)
{
    return $value !== NULL;
}

/**
 * Prepare all settings, wich should be passed to js
 */
function fn_twg_get_all_settings()
{
    $settings = fn_twg_get_boot_settings();
    $addon_settings = TwigmoSettings::get();
    $settings['access_id'] = $addon_settings['access_id'];

    $settings['currency'] = Registry::get('currencies.' . CART_SECONDARY_CURRENCY);
    $settings['primaryCurrency'] = Registry::get('currencies.' . CART_PRIMARY_CURRENCY);
    $settings['url_for_facebook'] = isset($addon_settings['url_for_facebook']) ? fn_twg_get_external_info_url($addon_settings['url_for_facebook']) : '';
    $settings['url_for_twitter'] = isset($addon_settings['url_for_twitter']) ? fn_twg_get_external_info_url($addon_settings['url_for_twitter']) : '';

    $settings['lang'] = array_merge(fn_twg_get_default_customer_langvars(), fn_twg_get_customer_lang_vars());

    // Countries/states
    $settings = array_merge($settings, fn_twg_get_as_api_list('countries', fn_get_countries(CART_LANGUAGE, true)));
    $settings['states'] = fn_twg_get_states();

    // Info pages
    $pages_location = $addon_settings['home_page_content'] == 'tw_home_page_blocks' ? 'twigmo.post' : 'index.index';
    $pages = fn_twg_get_blocks_for_location($pages_location, array('pages'));
    $settings['info_pages'] = array();
    foreach ($pages as $page) {
        $settings['info_pages'] = array_merge($settings['info_pages'], $page['pages']['page']);
    }
    // If page link begin with # then interpret this link as twigmo page
    foreach ($settings['info_pages'] as $k => $page) {
        if (preg_match('/^\#.*$/', $page['link'])) {
            $settings['info_pages'][$k]['twigmo_page'] = substr($page['link'], 1);
        }
    }

    $only_req_profile_fields = isset($addon_settings['only_req_profile_fields']) ? $addon_settings['only_req_profile_fields']  : 'N';
    $settings['profileFields'] = fn_twg_prepare_profile_fields(fn_get_profile_fields(), $only_req_profile_fields);
    $settings['profileFieldsCheckout'] = fn_twg_prepare_profile_fields(fn_get_profile_fields('O'), $only_req_profile_fields);
    $settings['show_product_code'] = isset($addon_settings['show_product_code']) ? $addon_settings['show_product_code']  : 'N';

    $settings['titles'] = array_values(fn_get_static_data_section('T'));

    $settings['profile'] = fn_twg_get_user_info($_SESSION['auth']['user_id']);

    $settings['use_email_as_login'] = Registry::get('settings.General.use_email_as_login');

    $settings['cart'] = fn_twg_api_get_session_cart();

    $settings['sortings'] = fn_twg_get_sortings();

    $settings['allow_negative_amount'] = Registry::get('settings.General.allow_negative_amount');
    $settings['inventory_tracking'] = Registry::get('settings.General.inventory_tracking');
    $settings['default_location'] = array('country' => Registry::get('settings.General.default_country'), 'state' => Registry::get('settings.General.default_state'));
    $settings['allow_anonymous_shopping'] = Registry::get('settings.General.allow_anonymous_shopping');
    $settings['disable_anonymous_checkout'] = Registry::get('settings.General.disable_anonymous_checkout');
    $settings['cart_prices_w_taxes'] = Registry::get('settings.Appearance.cart_prices_w_taxes');
    $settings['show_prices_taxed_clean'] = Registry::get('settings.Appearance.show_prices_taxed_clean');
    $settings['tax_calculation'] = Registry::get('settings.General.tax_calculation');
    $settings['display_supplier'] = Registry::get('settings.Suppliers.display_supplier');
    $settings['security_hash'] = fn_generate_security_hash();
    $settings['no_image_path'] = Registry::get('config.no_image_path');
    $settings['productType'] = PRODUCT_TYPE;
    $settings['suppliers_vendor'] = Registry::get('settings.Suppliers.apply_for_vendor');
    $settings['languages'] = fn_twg_get_languages();
    $settings['cart_language'] = CART_LANGUAGE;
    $settings['min_order_amount'] = Registry::get('settings.General.min_order_amount');
    $settings['min_order_amount_type'] = Registry::get('settings.General.min_order_amount_type');
    $settings['address_position'] = Registry::get('settings.General.address_position');
    $settings['agree_terms_conditions'] = Registry::get('settings.General.agree_terms_conditions');
    $settings['month_first'] = Registry::get('settings.Appearance.calendar_date_format') == 'month_first';
    $settings['show_modifiers'] = Registry::get('settings.General.display_options_modifiers');
    if (PRODUCT_TYPE == 'MULTIVENDOR') {
        $settings['company_data'] = fn_get_company_data(!empty($_SESSION['auth']['company_id'])? $_SESSION['auth']['company_id']: 0);
    } else {
        $settings['company_data'] = array();
    }

    $settings['lang'] = fn_twg_process_langvars($settings['lang']);

    fn_set_hook('twg_get_all_settings', $settings);

    return $settings;
}

/**
 * Get blocks from central container
 * @param string $controller - controller.method
 * @return array ([block_id] => block_name)
 */
function fn_twg_get_location_page_title($controller = 'twigmo.post')
{
    $location = Bm_Location::instance()->get($controller);

    return $location['title'];
}

function fn_twg_get_block($params)
{
    if (!empty($params['block_id'])) {
        $key_map = array('products' => 'product_id',
                                         'categories' => 'category_id');
        $key = $key_map[$_REQUEST['object']];
        $block_id =  $params['block_id'];
        $snapping_id = !empty($params['snapping_id']) ? $params['snapping_id'] : 0;

        $dispatch = isset($_REQUEST['object']) ? $_REQUEST['object'] . '.view' : 'index.index';

        $area = !empty($params['area']) ?  $params['area'] : AREA;

        if (!empty($params['dynamic_object'])) {
            $dynamic_object = $params['dynamic_object'];
        } elseif (!empty($_REQUEST['dynamic_object']) && $area != 'C') {
            $dynamic_object = $_REQUEST['dynamic_object'];
        } else {
                $dynamic_object_scheme = Bm_SchemesManager::get_dynamic_object($dispatch, $area);
                $twg_request = array('dispatch' => $dispatch,
                                                      $dynamic_object_scheme['key'] => $_REQUEST['id']);
            if (!empty($dynamic_object_scheme) && !empty($twg_request[$dynamic_object_scheme['key']])) {

                $dynamic_object['object_type'] = $dynamic_object_scheme['object_type'];
                $dynamic_object['object_id'] = $twg_request[$dynamic_object_scheme['key']];
            } else {
                $dynamic_object = array();
            }
        }
        $block = Bm_Block::instance()->get_by_id($block_id, $snapping_id, $dynamic_object, DESCR_SL);

        return $block;
    }
}

function fn_twg_get_current_company_id()
{
    return defined('COMPANY_ID') ? COMPANY_ID : 0;
}

/**
 * Hook for override reward points calculation, as CONTROLLER constant is used.
 * @param type $cart
 * @param type $cart_products
 * @param type $shipping_rates
 * @param type $calculate_taxes
 * @param type $auth
 */
function fn_twigmo_calculate_cart_taxes_pre(&$cart, &$cart_products, &$shipping_rates, &$calculate_taxes, &$auth) // Hook
{
    if (Registry::get('addons.reward_points.status') != 'A' || CONTROLLER != 'twigmo') {
        return true;
    }

    fn_set_hook('reward_points_cart_calculation', $cart_products, $cart, $auth);
    // calculating price in points
	$in_use_total_points = false;
	if (isset($cart['points_info']['total_price'])) {
		if (!empty($cart['points_info']['in_use']['points'])
            && $cart['points_info']['in_use']['points'] <= $cart['points_info']['total_price']
        ) {
			$in_use_total_points = true;
		}
		unset($cart['points_info']['total_price']);
	}

	if (Registry::get('addons.reward_points.price_in_points_order_discount') == 'Y'
        && !empty($cart['subtotal_discount']) && !empty($cart['subtotal'])
    ) {
		$price_coef = 1 - $cart['subtotal_discount'] / $cart['subtotal'];
	} else {
		$price_coef = 1;
	}

	foreach ((array) $cart_products as $k => $v) {

        fn_set_hook('reward_points_calculate_item', $cart_products, $cart, $k, $v);

		if (!isset($v['exclude_from_calculate'])) {
			if (isset($cart['products'][$k]['extra']['points_info'])) {
				unset($cart['products'][$k]['extra']['points_info']);
			}
			fn_twg_gather_reward_points_data($cart_products[$k], $auth);

			if (isset($cart_products[$k]['points_info']['raw_price'])) {
				$product_price = $price_coef * $cart_products[$k]['points_info']['raw_price'];
				$cart['products'][$k]['extra']['points_info']['raw_price'] = $product_price;
				$cart['products'][$k]['extra']['points_info']['display_price']
                    = $cart['products'][$k]['extra']['points_info']['price'] = round($product_price);
				$cart['points_info']['total_price']
                    = (isset($cart['points_info']['total_price']) ?
                    $cart['points_info']['total_price'] : 0)
                    + $product_price;
			}
		}
	}

	$cart['points_info']['raw_total_price'] = isset($cart['points_info']['total_price']) ?
        $cart['points_info']['total_price'] : 0;
	$cart['points_info']['total_price'] = ceil($cart['points_info']['raw_total_price']);

	if (!empty($cart['points_info']['in_use']['points'])
        && $cart['points_info']['in_use']['points'] > $cart['points_info']['total_price'] && $in_use_total_points) {
		$cart['points_info']['in_use']['points'] = $cart['points_info']['total_price'];
	}

	if (!empty($cart['points_info']['in_use'])) {
		fn_set_point_payment($cart, $cart_products, $auth);
	}


	// calculating reward points
	if (isset($cart['points_info']['reward'])) {
		unset($cart['points_info']['reward']);
	}

	if (isset($cart['points_info']['additional'])) {
		$cart['points_info']['reward'] = $cart['points_info']['additional'];
		unset($cart['points_info']['additional']);
	}

	$discount = 0;
	if (Registry::get('addons.reward_points.reward_points_order_discount') == 'Y'
        && !empty($cart['subtotal_discount']) && !empty($cart['subtotal'])
    ) {
		$discount += $cart['subtotal_discount'];
	} elseif (!empty($cart['points_info']) && !empty($cart['points_info']['in_use'])
        && !empty($cart['points_info']['in_use']['cost'])
    ) {
		$discount += $cart['points_info']['in_use']['cost'];
	}

	if ($discount && !empty($cart['subtotal'])) {
		$reward_coef = 1 - $discount / $cart['subtotal'];
	} else {
		$reward_coef = 1;
	}

	foreach ((array) $cart_products as $k => $v) {

        fn_set_hook('reward_points_calculate_item', $cart_products, $cart, $k, $v);

		if (!isset($v['exclude_from_calculate'])) {
			if (isset($cart_products[$k]['points_info']['reward'])) {
				$product_reward = $v['amount'] * (!empty($v['product_options']) ?
                    fn_apply_options_modifiers(
                        $cart['products'][$k]['product_options'],
                        $cart_products[$k]['points_info']['reward']['raw_amount'],
                        POINTS_MODIFIER_TYPE
                    ) : $cart_products[$k]['points_info']['reward']['raw_amount']
                );
				$cart['products'][$k]['extra']['points_info']['reward'] = round($product_reward);
				$cart_reward = round($reward_coef * $product_reward);
				$cart['points_info']['reward'] = (isset($cart['points_info']['reward']) ?
                    $cart['points_info']['reward'] : 0) + $cart_reward;
			}
		}
	}
}
