<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/


if ( !defined('AREA') ) { die('Access denied'); }

function fn_twigmo_get_upgrade_dirs($install_src_dir)
{
    $dirs = array();
    $addon_path = 'twigmo/';
    $full_addon_path = 'addons/twigmo/';
    $repo_path = 'var/skins_repository/basic/';
    $backup_files_path = 'backup_files/';
    $available_skins = array_keys(fn_get_available_skins());
    $installed_skins = array_keys(fn_twg_get_installed_skins());

    $dirs['installed'] = array(
        'addon' => DIR_ADDONS . $addon_path,
    );
    foreach ($installed_skins as $skin) {
        $dirs['installed']['skin_admin'][$skin] = fn_get_skin_path('[skins]/' . $skin . '/admin/', 'admin')
                . $full_addon_path;
        $dirs['installed']['skins_customer'][$skin] = fn_get_skin_path('[skins]/' . $skin . '/customer/', 'customer')
                . $full_addon_path;
    }

    $dirs['repo'] = array(
        'addon' => DIR_ADDONS . $addon_path,
    );
    foreach ($available_skins as $skin) {
        $dirs['repo']['skin_admin'][$skin] = fn_get_skin_path('[repo]/' . $skin . '/admin/') . $full_addon_path;
        $dirs['repo']['skin_customer'][$skin] = fn_get_skin_path('[repo]/' . $skin . '/customer/') . $full_addon_path;
    }

    $dirs['distr'] = array(
        'addon' => $install_src_dir . $full_addon_path
    );
    foreach ($available_skins as $skin) {
        $dirs['distr']['skin_admin'][$skin] = $install_src_dir . $repo_path . 'admin/' . $full_addon_path;
        $dirs['distr']['skin_customer'][$skin] = $install_src_dir . $repo_path . 'customer/' . $full_addon_path;
    }

    $dirs['backup_root'] = fn_twigmo_get_backup_dir();

    $dirs['backup_files'] = array(
        'addon' => $dirs['backup_root'] . $backup_files_path. $full_addon_path
    );
    foreach ($installed_skins as $skin) {
        $dirs['backup_files']['skin_admin'][$skin] = $dirs['backup_root'] . $backup_files_path
            . fn_get_skin_path(
                '[relative]/' . $skin . '/admin/',
                'admin'
            ) . $full_addon_path;
        $dirs['backup_files']['skins_customer'][$skin] = $dirs['backup_root'] . $backup_files_path
            . fn_get_skin_path(
                '[relative]/' . $skin . '/customer/',
                'customer'
            ) . $full_addon_path;
    }

    $dirs['backup_settings'] = $dirs['backup_root'] . 'backup_settings/';
    $dirs['backup_company_settings'] = array($dirs['backup_settings'] . 'companies/0/');

    if (PRODUCT_TYPE == 'ULTIMATE') {
        $dirs['backup_files']['skins_customer'] = array();
        $dirs['installed']['skins_customer'] = array();
        $dirs['backup_company_settings'] = array();
        $company_ids = fn_get_all_companies_ids();
        foreach ($company_ids as $company_id) {
            foreach ($installed_skins as $skin) {
                $relative_skin_path = 'stores/' . $company_id . '/skins/' . $skin . '/customer/' . $full_addon_path;
                if (is_dir($relative_skin_path)) {
                    $path_to_backup = $dirs['backup_root'] . $backup_files_path;
                    $dirs['backup_files']['skins_customer'][$company_id][$skin] = $path_to_backup . $relative_skin_path;
                    $dirs['installed']['skins_customer'][$company_id][$skin] = $relative_skin_path;
                }
            }
            $dirs['backup_company_settings'][$company_id] = $dirs['backup_settings'] . 'companies/' . $company_id . '/';
        }
    }

    return $dirs;
}


function fn_twigmo_check_upgrade_permissions($upgrade_dirs, $is_writable = true)
{
    foreach ($upgrade_dirs as $upgrade_dir) {
        if (is_array($upgrade_dir)) {
            $is_writable = fn_twigmo_check_upgrade_permissions($upgrade_dir, $is_writable);

        } elseif (!is_dir($upgrade_dir)) {
            fn_uc_mkdir($upgrade_dir);
            fn_twigmo_check_upgrade_permissions(array($upgrade_dir));

        } elseif (!fn_uc_is_writable_dest($upgrade_dir)) {
            return false;

        }

        if (!is_array($upgrade_dir)) {
            $check_result = array();
            fn_uc_check_files($upgrade_dir, array(), $check_result, '', '');
            $is_writable = empty($check_result);

        }
        if (!$is_writable) {
            break;

        }
    }

    return $is_writable;
}


function fn_twigmo_get_next_version_info()
{
    $version_info = fn_get_contents(TWIGMO_UPGRADE_DIR . TWIGMO_UPGRADE_VERSION_FILE);
    if ($version_info) {
        $version_info = unserialize($version_info);
    } else {
        $version_info = array('next_version' => '', 'description' => '', 'update_url' => '');
    }
    return $version_info;
}


function fn_twigmo_download_distr()
{
    // Get needed version
    $version_info = fn_twigmo_get_next_version_info();
    if (!$version_info['next_version'] || !$version_info['update_url']) {
        return false;
    }
    $download_file_dir = TWIGMO_UPGRADE_DIR . $version_info['next_version'] . '/';
    $download_file_path = $download_file_dir . 'twigmo.tgz';
    $unpack_path = $download_file_path . '_unpacked';
    fn_rm($download_file_dir);
    fn_mkdir($download_file_dir);
    fn_mkdir($unpack_path);

    $data = fn_get_contents($version_info['update_url']);
    if (!fn_is_empty($data)) {
        fn_put_contents($download_file_path, $data);
        $res = fn_decompress_files($download_file_path, $unpack_path);

        if (!$res) {
            fn_set_notification('E', fn_get_lang_var('error'), fn_get_lang_var('text_uc_failed_to decompress_files'));
            return false;
        }
        return $unpack_path . '/';
    } else {
        fn_set_notification('E', fn_get_lang_var('error'), fn_get_lang_var('text_uc_cant_download_package'));
        return false;
    }
}


function fn_twigmo_get_backup_dir()
{
    $_version = fn_twigmo_get_next_version_info();
    $version = $_version['next_version'];
    if (!$version) {
        return false;
    }
    return TWIGMO_UPGRADE_DIR . $version . '/';
}


function fn_twigmo_copy_files($source, $dest)
{
    if (is_array($source)) {
        foreach ($source as $key => $src) {
            fn_twigmo_copy_files($src, $dest[$key]);
        }
    } else {
        fn_uc_copy_files($source, $dest);
    }
    return;
}


function fn_twigmo_remove_directory_content($path)
{
    if (is_array($path)) {
        foreach ($path as $directory) {
            fn_twigmo_remove_directory_content($directory);
        }

    } else {
        fn_twigmo_remove_files(fn_get_dir_contents($path, true, true, '', $path));

    }

    return true;
}


function fn_twigmo_remove_files($source)
{
    if (is_array($source)) {
        foreach ($source as $src) {
            fn_twigmo_remove_files($src);
        }
    } else {
        fn_uc_rm($source);
    }
    return;
}


function fn_twigmo_exec_upgrade_func($install_src_dir, $file_name)
{
    $file = $install_src_dir . '/addons/twigmo/' . $file_name . '.php';
    if (file_exists($file)) {
        require_once($file);
    }
    return;
}


function fn_twigmo_backup_settings($upgrade_dirs)
{
    // Backup addon's settings to the session
    $_SESSION['twigmo_backup_settings'] = TwigmoSettings::get();

    // Backup twigmo blocks
    foreach ($upgrade_dirs['backup_company_settings'] as $company_id => $dir) {
        $location = Bm_Location::instance($company_id)->get('twigmo.post');
        if ($location) {
            $content = Bm_Exim::instance($company_id)->export(array($location['location_id']), array());
            if ($content) {
                fn_twg_write_to_file($dir . '/blocks.xml', $content, false);
            }
        }
    }

    // Backup twigmo langvars
    $languages = fn_get_languages();
    foreach ($languages as $language) {
        // Prepare langvars for backup
        $langvars = fn_twg_get_all_lang_vars($language['lang_code']);
        $langvars_formated = array();
        foreach ($langvars as $name => $value) {
            $langvars_formated[] = array('name' => $name, 'value' => $value);
        }
        fn_twg_write_to_file($upgrade_dirs['backup_settings'] . '/lang_' . $language['lang_code'] . '.bak', $langvars_formated);
    }
    if (PRODUCT_TYPE == 'ULTIMATE') {
        db_export_to_file(
                $upgrade_dirs['backup_settings'] . 'lang_ult.sql', array(db_quote('?:ult_language_values')), 'Y', 'Y', false, false, false);
    }
    return true;
}


function fn_twigmo_restore_settings($upgrade_dirs)
{
    // Restore langvars - for all languages except EN and RU
    $languages = fn_get_languages();
    $except_langs = array('EN', 'RU');
    foreach ($languages as $language) {
        $backup_file = $upgrade_dirs['backup_settings'] . 'lang_' . $language['lang_code'] . '.bak';
        if (!in_array($language['lang_code'], $except_langs) and file_exists($backup_file)) {
            fn_update_lang_var(unserialize(fn_get_contents($backup_file)), $language['lang_code']);
        }
    }

    // Restore blocks
    foreach ($upgrade_dirs['backup_company_settings'] as $company_id => $dir) {
        $backup_file = $dir . 'blocks.xml';
        if (file_exists($backup_file)) {
            Bm_Exim::instance($company_id)->import_from_file($backup_file);
        }
    }

    // Restore settings if addon was connected
    $restored_settings = array(
        'my_private_key',
        'my_public_key',
        'his_public_key',
        'email',
        'customer_connections',
        'admin_connection'
    );
    $settings = array();
    foreach ($_SESSION['twigmo_backup_settings'] as $setting => $value) {
        if (in_array($setting, $restored_settings)) {
            $settings[$setting] = $value;
        }
    }
    $settings['version'] = TWIGMO_VERSION;
    unset($_SESSION['twigmo_backup_settings']);
    TwigmoSettings::set($settings);
    $connector = new TwigmoConnector();
    if (!$connector->updateConnections(true)) {
        $connector->disconnect(array(), true);
    }
}

function fn_twigmo_update_files($upgrade_dirs)
{
    // Remove all addon's files
    foreach ($upgrade_dirs['repo'] as $dir) {
        fn_twigmo_remove_directory_content($dir);
    }
    // Copy files from distr to repo
    fn_twigmo_copy_files($upgrade_dirs['distr'], $upgrade_dirs['repo']);

    return true;
}

function fn_twg_get_installed_skins()
{
    $skins_dir = 'skins';
    $skins = fn_get_dir_contents(DIR_ROOT . '/' . $skins_dir, true);
    sort($skins);
    $result = array();
    if (PRODUCT_TYPE == 'ULTIMATE') {
        $companies_ids = fn_get_all_companies_ids();
        foreach ($companies_ids as $company_id) {
            $skins_dir = 'stores/' . $company_id . '/skins';
            $skins = fn_get_dir_contents(DIR_ROOT . '/' . $skins_dir, true);
            sort($skins);
            foreach ($skins as $skin) {
                if (is_dir(DIR_ROOT . '/' . $skins_dir . '/' . $skin)) {
                    $manifest_path = DIR_ROOT . '/' . $skins_dir . '/' . $skin . '/' . SKIN_MANIFEST;
                    $skin_data = fn_twg_get_skin_data($manifest_path);
                    if (!empty($skin_data)) {
                        $result[$skin] = $skin_data;

                    }
                }
            }
        }
    } else {
        foreach ($skins as $skin) {
            if (is_dir(DIR_ROOT . '/' . $skins_dir . '/' . $skin)) {
                $manifest_path = DIR_ROOT . '/' . $skins_dir . '/' . $skin . '/' . SKIN_MANIFEST;
                $skin_data = fn_twg_get_skin_data($manifest_path);
                if (!empty($skin_data)) {
                    $result[$skin] = $skin_data;

                }
            }
        }
    }

    return $result;
}

function fn_twg_get_skin_data($manifest_path)
{
    $skin_data = array();
    if (is_file($manifest_path)) {
        $skin_data = parse_ini_file($manifest_path);

    }
    return $skin_data;
}
