<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/


if ( !defined('AREA') ) { die('Access denied'); }

require_once(DIR_ADDONS . 'twigmo/core/class.apidata.php');
require_once(DIR_ADDONS . 'twigmo/core/api/class.twgapibase.php');
require_once(DIR_ADDONS . 'twigmo/core/api/class.twgapi.php');
require_once(DIR_ADDONS . 'twigmo/core/api/class.twgapiv2.php');
require_once(DIR_ADDONS . 'twigmo/core/fn.api.php');

fn_register_hooks(
    'additional_fields_in_search',
    'before_dispatch',
    'calculate_cart',
    array('calculate_cart_taxes_pre', 250),
    'check_static_location',
    'dispatch_before_display',
    'get_categories',
    'get_pages',
    'get_products',
    'get_shipments',
    'get_users',
    'order_placement_routines',
    'place_order',
    'init_secure_controllers',
    'redirect_complete',
    'order_notification',
    'user_init'
);
