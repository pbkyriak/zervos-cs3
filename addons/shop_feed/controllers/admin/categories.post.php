<?php

if (!defined('AREA')) {
  die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if ($mode == 'update') {
    if (isset($_REQUEST['category_data']['exclude_from_shopfeed_subcategories']) && $_REQUEST['category_data']['exclude_from_shopfeed_subcategories'] == 'Y') {
      $id_path = db_get_field("SELECT id_path FROM cscart_categories WHERE category_id=?i", $_REQUEST['category_id']);
      $q = sprintf("UPDATE cscart_categories SET exclude_from_shopfeed='%s' WHERE id_path LIKE '%s/%%' ", $_REQUEST['category_data']['exclude_from_shopfeed'], $id_path);
      db_query($q);
    }
  }
  return;
}

if ($mode == 'update') {
  Registry::set('navigation.tabs.shopfeed', array(
      'title' => fn_get_lang_var('shop_feed'),
      'js' => true
  ));
}

