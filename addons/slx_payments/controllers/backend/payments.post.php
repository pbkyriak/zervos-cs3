<?php

/**
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Αυγ 2014
 */

use Tygh\Http;
use Tygh\Registry;
use Tygh\Database;

if ( !defined('AREA') ) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //
    // Update payment method
    //
    if ($mode == 'update') {
        if( isset($_REQUEST['insts_data']) || isset($_REQUEST['add_insts']) || isset($_REQUEST['delete_insts']) ) {
            fn_update_payment_installments( 
                            $_REQUEST['payment_id'],
							$_REQUEST['insts_data'], 
                            $_REQUEST['add_insts'],
							$_REQUEST['delete_insts']
                );
            
        }
    }

    return array(CONTROLLER_STATUS_OK, "payments.manage");
}

if ($mode == 'processor') {
    $paymentId = $_REQUEST['payment_id'];
    $processor_data = fn_get_processor_data($paymentId);
    $view->assign('installments', @$processor_data['installments']);
}

function fn_update_payment_installments($payment_id, $insts_data, $add_insts, $delete_insts) {
    if (!empty($add_insts)) {
        foreach ($add_insts as $_data) {
            if (!empty($_data['amount_from']) && !empty($_data['installments'])) {
                $q = 'INSERT INTO ?:installments ?e';
                db_query($q, array_merge( array('payment_id'=>$payment_id), $_data));
            }
        }
    }

    if (!empty($insts_data)) {
        foreach ($insts_data as $k => $_data) {
            $q = 'UPDATE ?:installments SET ?u where installment_id=?i';
            db_query($q,array_merge( array('payment_id'=>$payment_id), $_data),$k);
        }
    }
    if (!empty($delete_insts)) {
        foreach ($delete_insts as $_id => $val) {
            if ($val == 'Y') {
                db_query("DELETE FROM ?:installments WHERE installment_id = '$_id'");
            }
        }
    }
}