<?php

/**
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Αυγ 2014
 */

if ( !defined('AREA') ) { die('Access denied'); }

fn_register_hooks(
	'update_payment_surcharge_post',
    'get_processor_data_post',
    'delete_payment_post',
	'form_cart',
	'update_cart_by_data_post',
	'calculate_cart_post'
);
