<?php

/**
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Αυγ 2014
 */

// [panos]
define('PAYMENT_EUROBANK_B', 168 );
define('PAYMENT_PIRAEUS_B', 169 );
define('PAYMENT_ALPHA_B', 180 );
// [/panos]

/**
 * Hook for get_default_credit_card
 * 
 * @param array $cart
 * @param array $user_data
 */
function fn_slx_payments_update_payment_surcharge_post(&$cart, $user_data)
{
	if( !isset($cart['payment_id']) ) {
		return true;
	}
	if(AREA=='A') {
		$payment_params = fn_get_payment_method_data($cart['payment_id']);
		if ( in_array($payment_params['processor_id'], fn_get_slx_payment_ids()) ) {
			$period = 1;
			if(isset($cart['payment_period']) ) {
				$period = (int)$cart['payment_period'];
			}
			$cart['total'] = $cart['total']-$cart['interest'];
			$cart['interest'] = fn_slx_payments_check_installment(
				(float) $cart['total'],
				$period, 
				$payment_params
				);
				fn_save_cart_content($cart, $user_data['user_id']);
		}
		else {
			$cart['interest'] = 0;
		}
		$cart['total'] = $cart['total'] + $cart['interest'];
	}
	else {
		$cart['interest'] = 0;
		$payment_params = fn_get_payment_method_data($cart['payment_id']);
		if ( in_array($payment_params['processor_id'], fn_get_slx_payment_ids()) ) {
			$period = 1;
			if(isset($cart['payment_period']) ) {
				$period = (int)$cart['payment_period'];
			}
			
			$cart['interest'] = fn_slx_payments_check_installment(
				(float) $cart['total'],
				$period, 
				$payment_params
				);
				fn_save_cart_content($cart, $user_data['user_id']);
		}
		$cart['total'] = $cart['total'] + $cart['interest'];
	}
    //printf("period=%s interest=%s total=%s<br />",$cart['payment_period'],$cart['interest'],$cart['total']);die(1);
}

/**
 * Hook for get_processor_data
 * 
 * @param int $payment_id
 * @param array $processor_data
 */
function fn_slx_payments_get_processor_data_post($payment_id, $pdata, &$processor_data) {
    $processor_data['installments'] = db_get_array("SELECT * FROM ?:installments WHERE payment_id=?i ORDER BY `amount_from`", $payment_id);
}

/**
 * Hook for delete_payment_post
 * @param int $payment_id
 * @param int $result
 */
function fn_slx_payments_delete_payment_post($payment_id, $result) {
    db_query("DELETE FROM ?:installments WHERE payment_id=?i",$payment_id);
}


function fn_slx_payments_form_cart($order_info, &$cart) {
	$cart['payment_period'] = $order_info['payment_period'];
	$cart['interest'] = $order_info['interest'];
}

function fn_slx_payments_update_cart_by_data_post(&$cart, $new_cart_data, $auth) {

	$cart['payment_period'] = $new_cart_data['payment_period'];
	$cart['interest'] = $new_cart_data['interest'];
	$cart['total'] = $cart['total'] + $cart['interest'];
}
function fn_slx_payments_calculate_cart_post(&$cart, $auth, $calculate_shipping, $calculate_taxes, $options_style, $apply_cart_promotions, $cart_products, $product_groups) {

	if (AREA == 'A') {
		$cart['total'] = $cart['total'] + $cart['interest'];
	}
}

function fn_get_min_installments($payment_id) {
    $out = db_get_field("SELECT min(installments) FROM ?:installments WHERE payment_id=?i ", $payment_id);
    if(!$out ) {
        $out = 1;
    }
    return $out;
}

function fn_slx_payments_check_installments($total = 0, $payment_id)
{
	$total = (float)$total;
	return db_get_fields("SELECT installments FROM ?:installments WHERE payment_id=?i AND (`amount_from` <= $total ) ORDER BY installments ASC ", $payment_id);
}

function fn_slx_payments_check_interest($total = 0, $payment_id)
{
	return db_get_field("SELECT interest_from FROM ?:installments WHERE payment_id=?i AND (`amount_from` <= $total ) ORDER BY installments ASC LIMIT 0,1", $payment_id);
}

function fn_slx_payments_check_installment($total = 0, $installment, $payment_id)
{
	if( is_array($payment_id) )
		$payment_id = $payment_id['payment_id'];
	$q = sprintf("SELECT * FROM ?:installments WHERE payment_id=?i AND `amount_from` <= %s AND installments=%s ORDER BY installments ASC LIMIT 0,1", $total, $installment);
	$_insts = db_get_row($q, $payment_id);
	$_interest = 0;

	if (!empty($_insts['amount_from']) ) {
		if (floatval($_insts['a_interest'])) {
			$_interest += $_insts['a_interest'];
		}
		if (floatval($_insts['p_interest'])) {
			$_interest += fn_format_price($total * $_insts['p_interest'] / 100);
		}
	} 
	return $_interest;
}

function fn_get_slx_payment_ids() {
	return array(PAYMENT_ALPHA_B);
}

function fn_slx_payments_get_first_payment_with_installments() {
    $out=0;
    $query = "SELECT p.payment_id
            FROM cscart_payments p
            left join cscart_installments as i on (p.payment_id=i.payment_id)
            WHERE p.processor_id in (?n)
            group by p.payment_id
            having count(i.installment_id)>1
            ORDER BY p.position
            limit 0,1";
    
    $fld = db_get_field($query, fn_get_slx_payment_ids());
    
    if( $fld ) {
        $out = $fld;
    }
    return $out;
}