
Ελεγχος του addon.xml

Installation:

1. install addon

2. (Admin templates) copy files to design/backend/templates/views/payments/components/cc_processors

3. (processors) copy files to app/payments

4. Hacks:

    1. app/functions/fn.cart.php
      at the end of the function fn_update_payment_surcharge (around line 6160) add the line:
      fn_set_hook('update_payment_surcharge', $cart, $auth);  // [panos]

    2. app/functions/fn.cart.php
      at the end of the function fn_get_processor_data (before return) add the line:
      fn_set_hook('get_processor_data', $payment_id, $processor_data);  // [panos]

    3. design/themes/basic/templates/views/checkout/components/checkout_totals_info.tpl
      at the bottom after payment surecharge (around line 63, before closing ul) add the line:
      {hook name="checkout:checkout_interest"}{/hook} {* [panos] *}

    4. design/themes/basic/templates/views/orders/details.tpl
      around line 226 after surcharge's /if add the line:
      {hook name="orders:subtotals"}{/hook} {* [panos] *}


---------------------------------------------
Αλλαγές για VNP

1. To hook:
	fn_set_hook('update_payment_surcharge', $cart, $auth);  // [panos]
Sto hook:
	fn_set_hook('update_payment_surcharge_post',$cart,$auth, $lang_code);

2. To hook:
	fn_set_hook('get_processor_data', $payment_id, $processor_data);  // [panos]
Sto hook
	fn_set_hook('get_processor_data_post', $payment_id,$pdata,$processor_data);//Created by My Custom Hooks
	
	