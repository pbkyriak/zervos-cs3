<?php
//
// Author Ioannis Matziaris - imatzgr@gmail.com - April 2013
//

if ( !defined('AREA') )	{ die('Access denied');	}

if ($mode == 'checkout') {
	$edit_step = !empty($_REQUEST['edit_step']) ? $_REQUEST['edit_step'] : (!empty($_SESSION['edit_step']) ? $_SESSION['edit_step'] : '');
	if($edit_step == 'step_two'){
		if (!empty($auth['user_id'])) {
			$cart = & $_SESSION['cart'];
			if($cart["invoice_or_receipt"]=='I'){
				fn_profile_fields_empty_vals("remove",array("B"),$cart['user_data']);
				$view->assign('user_data',$cart['user_data']);
				$view->assign('cart',$cart);
			}
		}	
	}
}

?>