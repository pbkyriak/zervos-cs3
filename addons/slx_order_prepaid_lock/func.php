<?php


function fn_settings_variants_addons_slx_order_prepaid_lock_status_from() {
	return db_get_hash_single_array("SELECT s.status, sd.description FROM cscart_statuses s LEFT JOIN cscart_status_descriptions sd ON (s.status=sd.status AND sd.lang_code=?s) WHERE s.type='O'", array('status', 'description'), 'el');
}

function fn_slx_order_prepaid_lock_change_order_status(&$status_to, $status_from, $order_info, $force_notification, $order_statuses, $place_order) {
	$statuses = explode(',',Registry::get('addons.slx_order_prepaid_lock.status_from'));
	if( !in_array($status_from, $statuses) ) {
		return;
	}

	if( $order_info['payment_mode']=='U' ) {
		fn_set_notification('W', fn_get_lang_var('error'), fn_get_lang_var('prepaid_not_set'));
		$status_to = $status_from;
	}
	elseif( $order_info['payment_mode']=='Y' && (float)$order_info['prepayed_amount']==0 ) {
		fn_set_notification('W', fn_get_lang_var('error'),fn_get_lang_var('prepaid_not_set_amount'));
		$status_to = $status_from;		
	}
}


function fn_slx_order_prepaid_lock_get_orders($params, $fields, $sortings, &$condition, $join, $group) {

	if(!empty($params['is_prepaid']) ) {
		$condition .= db_quote(" AND ?:orders.payment_mode='Y'");
	}
}

function fn_slx_order_prepaid_lock_get_orders_totals($paid_statuses, $join, $condition, $group) {
	$prepaid_total = db_get_field("SELECT sum(t.prepayed_amount) FROM ( SELECT prepayed_amount FROM ?:orders $join WHERE 1 $condition $group) as t");
	Registry::get('view')->assign('prepaid_total', $prepaid_total);
}