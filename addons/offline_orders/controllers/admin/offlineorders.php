<?php

if ( !defined('AREA') ) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if( $mode=='create' ) {
  	//define("DEBUG_QUERIES",1);
  	$data = $_REQUEST['order'];
    $id = $data['id'];
    $order_date = salixLocalDateToTimestamp($data['order_date']);
    $item = $data['item'];
    $salesman = $data['salesman'];
    $client = $data['client'];
    $price = $data['price'];
    $shipment = $data['shipment'];
    $phone = $data['phone'];
    $memo = $data['memo'];
    $color = $data['color'];
    $closed = $data['closed'];
    if( $closed=='Y' )
    	$color = 2;
    if( $id ) {
    	$q = "UPDATE slx_offline_orders SET item=?s,salesman=?s,client=?s,price=?s,shipment=?s,phone=?s,memo=?s,color=?i,closed=?s WHERE id=?i";
    	db_query($q, $item,$salesman,$client,$price,$shipment,$phone,$memo,$color,$closed, $id);
    }
    else {
    	$q = "INSERT INTO slx_offline_orders (order_date,item,salesman,client,price,shipment,phone,memo, closed) VALUES (UNIX_TIMESTAMP(), ?s, ?s, ?s, ?s, ?s, ?s, ?s, ?s)";
    	$id = db_query($q,$item,$salesman,$client,$price,$shipment,$phone,$memo, $closed);
    }
    /*
    if( $id ) {
      return array(CONTROLLER_STATUS_OK, "offlineorders.update?id=$id");
    }
    */
    return array(CONTROLLER_STATUS_OK, "offlineorders.manage");

  }
  
  if($mode=='m_delete') {
    $data = $_REQUEST['order_ids'];
    if( $data ) {
      foreach($data as $id ) {
      	db_query("UPDATE slx_offline_orders SET deleted=1 where id=?i",$id);
      }
    }
    return array(CONTROLLER_STATUS_OK, "offlineorders.manage");
  }
  
}
if( $mode == 'manage'  ) {
  $fDate = salixLocalDateToTimestamp($_REQUEST['f_date']);
  $tDate = salixLocalDateToTimestamp($_REQUEST['t_date']);
  $showDeleted = $_REQUEST['showDeleted'] ? $_REQUEST['showDeleted'] : 0;
  $whereClause = " WHERE ";
  if( $showDeleted==0 )
  	$whereClause .= " deleted=0 ";
  if( $showDeleted==1 )
  	$whereClause .= " deleted=1 ";
  	
  if( !empty($fDate) )
    $dateClause = sprintf("order_date>='%s'",$fDate);
  if( !empty($tDate) ) 
    $dateClause .= (empty($dateClause) ? '' : ' AND '). sprintf("order_date<='%s'",$tDate);
  if( !empty($dateClause) )
    $whereClause = sprintf("%s and ( %s ) ", $whereClause, $dateClause);

  $page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page']; // default page is 1
  $items_per_page = 30;
	$limit = '';
	if (!empty($items_per_page)) {
		$total = db_get_field("SELECT COUNT(DISTINCT id) FROM slx_offline_orders ".$dateClause);
		$limit = fn_paginate($page, $total, $items_per_page);
	}

  $query = "
    SELECT id,order_date,item,salesman,client,price,shipment,phone,memo,closed,deleted,color
    FROM slx_offline_orders
    $whereClause
    ORDER BY order_date DESC, id desc
    ";
  $query .= $limit;
	$orders = db_get_array($query);	
	$view->assign('orders', $orders);
	$view->assign('showDeleted', $_REQUEST['showDeleted']);
	$view->assign('f_date', $_REQUEST['f_date']);
	$view->assign('t_date', $_REQUEST['t_date']);
}

if( $mode=="delete" ) {
  $id = $_REQUEST['id'];
  db_query("UPDATE slx_offline_orders set deleted=1 where id=?i",$id);
  return array(CONTROLLER_STATUS_OK, "offlineorders.manage");
}

if( $mode=="undelete" ) {
  $id = $_REQUEST['id'];
  db_query("UPDATE slx_offline_orders set deleted=0 where id=?i",$id);
  return array(CONTROLLER_STATUS_OK, "offlineorders.manage");
}

if( $mode=='update' ) {
	$id = $_REQUEST['id'];
	if( $id ) {
		$order = db_get_row("SELECT * FROM slx_offline_orders where id=?i", $id);
		if( $order ) {
			$view->assign('order', $order);
		}
		else {
			return array(CONTROLLER_STATUS_OK, "offlineorders.manage");
		}
	}
	else 
		$view->assign('order', array('id'=>0, 'order_date'=>date('d/m/Y'), 'item'=>'', 'salesman'=>'', 'client'=>'', 'price'=>'', 'shipment'=>'', 'phone'=>'', 'status'=>''));
}

function salixLocalDateToMysqlDate($date) {
  $parts = explode('/', $date);
  if( count($parts)==3 ) {
    return sprintf("%s-%s-%s", $parts[2], $parts[1], $parts[0]);
  }
  return null;
}

function salixLocalDateToTimestamp($date) {
  $parts = explode('/', $date);
  if( count($parts)==3 ) {
    return mktime(0,0,0,$parts[1], $parts[0], $parts[2]);
  }
  return 0;
}