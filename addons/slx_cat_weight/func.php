<?php

function slx_cat_weight_display_weight_banner($productId) {
	$resp = db_get_field(
		"select c.display_weight_banner
		from cscart_products_categories pc
		left join cscart_categories c on (c.category_id=pc.category_id)
		where pc.product_id=?i and pc.link_type='M'", 
		$productId);
	return ($resp=='Y');
}