<?php

$schema['products']['content']['items']['fillings']['instock'] = array (
		'params' => array (
			'sort_by' => 'price',
			'sort_order' => 'desc',
			'shop_availability' => 0,
			'request' => array (
				'cid' => '%CATEGORY_ID%'
			)
		),
	);

return $schema;