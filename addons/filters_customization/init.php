<?php
//
// Author Ioannis Matziaris - imatzgr@gmail.com - April 2013
//

if ( !defined('AREA') ) { die('Access denied'); }

fn_register_hooks(
	'get_filters_products_count_before_select_filters',
	'get_filters_products_count_query_params',
	'get_products_pre'
);

?>