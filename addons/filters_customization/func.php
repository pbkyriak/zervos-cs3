<?php
//
// Author Ioannis Matziaris - imatzgr@gmail.com - May 2013
//

if ( !defined('AREA') ) { die('Access denied'); }

//
//Hooks
//

//Klisi stin function fn_get_filters_products_count 
//meta tin proetoimasia tou erwtimatos kai prin tin ektelesi tou gia tin evresi twn filtrwn 
//Xrisi gia apothikeysi sto session twn params wste na ginoun xrisi stin epomeni function
function fn_filters_customization_get_filters_products_count_before_select_filters($sf_fields, $sf_join, &$condition, $sf_sorting, $params){
	if(AREA=='C'){
		$settings = Registry::get('addons.filters_customization');
		
		$category_ids=array();	
		if(!empty($settings['category_ids'])){
			$category_ids=explode(",",$settings['category_ids']);
		}
		if(!empty($category_ids)&&Registry::get('settings.General.show_products_from_subcategories') == 'Y') {
			if (!empty($params['category_id'])) {
				if(in_array($params['category_id'],$category_ids)){
					$_SESSION['filters_customization']['params']=$params;
				}
			}
		}
	}
}

//Klisi stin function fn_get_filters_products_count 
//Prin apo ton evresi twn variants twn filtrwn gia ta proionta sigkekrimenwn katigoriwn
//Xrisi gia tin mi emfanisi filtrwn ipokatigoriwn se katigories pou orizontai apo to addon
function fn_filters_customization_get_filters_products_count_query_params($values_fields, $join, $sliders_join, $feature_ids, &$where, $sliders_where, $filter_vq, $filter_rq){
	if(AREA=='C'){
		$settings = Registry::get('addons.filters_customization');
		
		$category_ids=array();	
		if(!empty($settings['category_ids'])){
			$category_ids=explode(",",$settings['category_ids']);
		}
		$params=array();
		if(isset($_SESSION['filters_customization']['params'])){
			$params=$_SESSION['filters_customization']['params'];
			unset($_SESSION['filters_customization']['params']);
		}
		if(!empty($category_ids)&&Registry::get('settings.General.show_products_from_subcategories') == 'Y'&&!empty($params)) {
			if (!empty($params['category_id'])) {
				if(in_array($params['category_id'],$category_ids)){
					$begin_pos=-1;
					$end_pos=-1;
					if(strpos($where,"AND ?:products_categories.category_id IN")!==false){
						$begin_pos=strpos($where,"AND ?:products_categories.category_id IN");
						if(strpos($where,")")!==false){
							$end_pos=strpos($where,")");
						}
					}
					if($begin_pos!=-1&&$end_pos!=-1){
						$ids[] = $params['category_id'];

						$where=db_quote(" AND ?:products_categories.category_id IN (?n)", $ids).substr($where,$end_pos+1);
					}
				}
			}
		}
	}
}

//Klisi stin arxi tis fn_get_products
//Xrisi wste na min emfanizontai ta proionta ipokatigoriwn katiogiras pou exei
//oristei sto addon oti den tha emfanizei filtra twn ipokatigoriwn tis
function fn_filters_customization_get_products_pre(&$params, $items_per_page, $lang_code){
	if(AREA=='C'){
		$settings = Registry::get('addons.filters_customization');
		
		$category_ids=array();	
		if(!empty($settings['category_ids'])){
			$category_ids=explode(",",$settings['category_ids']);
		}
		
		if(isset($params['dispatch'])&&$params['dispatch']=='categories.view'&&isset($params['features_hash'])){
			if(!empty($category_ids)&&Registry::get('settings.General.show_products_from_subcategories') == 'Y') {
				if(in_array($params['category_id'],$category_ids)){
					if(isset($params['subcats'])&&$params['subcats']=='Y'){
						$params['subcats']='N';
					}
				}	
			}
		}
	}
}
?>