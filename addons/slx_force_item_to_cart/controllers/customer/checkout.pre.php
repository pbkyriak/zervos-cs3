<?php
//use Tygh\Registry;
$pitem = fn_get_product_data(Registry::get("addons.slx_force_item_to_cart.product_id"), $auth);
$pitemTitle = !empty($pitem) ? $pitem['product'] : '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //
    // Add product to cart
    //
    if ($mode == 'add') {
		$cart = & $_SESSION['cart'];
		$newProducts = $_REQUEST['product_data'];
		$pitems = [
			Registry::get("addons.slx_force_item_to_cart.product_id") => [
				'product_id' => Registry::get("addons.slx_force_item_to_cart.product_id"),
				'amount' => 1,
			]
		];
		//fn_set_notification("N", fn_get_lang_var('notice'), print_r($newProducts,true));
		foreach($newProducts as $newProduct) {
			$newProduct = fn_get_product_data($newProduct['product_id'], $auth);
			if(Registry::get("addons.slx_force_item_to_cart.price_limit")<$newProduct['price']) {
				fn_add_product_to_cart($pitems, $cart, $auth);
				fn_set_notification("N", fn_get_lang_var('notice'), sprintf("Προστέθηκε στο καλάθι σας και: %s",$pitemTitle));
			}
		}
	}
}