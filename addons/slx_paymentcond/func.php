<?php

/**
 * for 3 series
 */
function fn_slx_paymentcond_prepare_checkout_payment_methods($cart, $auth, &$payment_groups) {
    if (!empty($payment_groups)) {
        $total = $cart['total'];
		//printf("Cart total=%s<br />", $total);
        foreach ($payment_groups as $tab_id => $payments) {
            foreach ($payments as $paymentId => $payment_data) {
                $good = false;
				//printf("payid=%s, from=%s to=%s<br />", $paymentId , $payment_data['active_from'],$payment_data['active_to']);
                if($payment_data['active_from']==0 && $payment_data['active_to']==0) {                    
                    $good= true;
                }
                elseif($payment_data['active_from']!=0 && $payment_data['active_to']==0) {
                    if( $payment_data['active_from']<=$total) {
                        $good = true;
                    }
                }
                elseif($payment_data['active_from']==0 && $payment_data['active_to']!=0) {
                    if( $total<=$payment_data['active_to']) {
                        $good = true;
                    }
                }
                elseif($payment_data['active_from']!=0 && $payment_data['active_to']!=0) {
                    if( $payment_data['active_from']<=$total && $total<=$payment_data['active_to']) {
                        $good = true;
                    }
                }
                //printf("%s--%s<%s<%s good=%s<br />",$paymentId,$payment_data['active_from'],$total,$payment_data['active_to'],$good);
                if (!$good) {
                    unset($payment_groups[$tab_id][$paymentId]);
                    if (empty($payment_groups[$tab_id])) {
                        unset($payment_groups[$tab_id]);
                    }
                }
            }
        }
        ksort($payment_groups);
    }
    return true;
}

/**
 * for 3 series
 */
function fn_slx_paymentcond_checkout_select_default_payment_method(&$cart, &$payment_methods, &$completed_steps) {
    $pids = array();
    foreach ($payment_methods as $tab_id => $payments) {
        foreach ($payments as $paymentId => $payment_data) {
            $pids[] = $paymentId;
        }
    }
    if ($cart['payment_id'] == 0 || !in_array($cart['payment_id'], $pids)) {
        if ($pids) {
            $cart['payment_id'] = $pids[0];
        }
    }
    return true;
}


function fn_slx_paymentcond_get_payment_methods(&$payment_methods, $auth){
    $pids = array_keys($payment_methods);
    $totRanges = db_get_hash_array("SELECT payment_id, active_from, active_to FROM ?:payments WHERE payment_id in (?a)", 'payment_id', $pids);
    foreach($totRanges as $pid=>$vals) {
        $payment_methods[$pid] = array_merge($payment_methods[$pid], $vals);
    }
}