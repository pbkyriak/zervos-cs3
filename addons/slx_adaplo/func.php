<?php

function fn_slx_adaplo_count_orders_for_user($user_id) {
    $query = "SELECT count(*) 
              FROM ?:orders 
              WHERE user_id=?i";
    $row = db_get_field($query, $user_id);
    return $row;
}

function fn_slx_adaplo_get_product_code($product_id) {
    return  db_get_field("SELECT product_code from cscart_products where product_id=?i", $product_id);
}


function fn_slx_adaplo_find_brand_name($product_id) {
    $query = "SELECT pfvd.variant FROM ?:product_features_values as pfv
              left join ?:product_feature_variant_descriptions as pfvd on (pfv.variant_id=pfvd.variant_id and pfvd.lang_code='el')
              where pfv.product_id=?i and pfv.feature_id=18 and pfv.lang_code='el';";
    $row = db_get_field($query, $product_id);
    return $row;
}

function fn_slx_adaplo_find_category_name($product_id) {
    /*$query = "select cd.category from ?:products_categories as pc
		      left join ?:category_descriptions as cd on (pc.category_id=cd.category_id and link_type='M')
			  where product_id=?i;";*/	
	$query = "select id_path from cscart_categories where category_id in (select category_id from cscart_products_categories where product_id=?i and link_type='M') limit 0,1;";
    $idPath = db_get_field($query, $product_id);
	$query = "select group_concat(category SEPARATOR  '/') from cscart_category_descriptions where category_id in (?a) and lang_code ='el'";
	$catPath = db_get_field($query, explode('/',$idPath));
    return $catPath;
}
