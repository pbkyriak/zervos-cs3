<?php

if (!defined('AREA')) {
  die('Access denied');
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if ($mode == 'market_miner') {
    return array(CONTROLLER_STATUS_OK, "pricing.market_miner&q=".$_REQUEST['q']);
  }
}
if( $mode=='apply_hat_to_nonauto' ) {
	$updated_cnt =0;
	$hat = (float) Registry::get('settings.AutoPricing.nonauto_hat');
	$products = db_get_array("
			SELECT p.product_id as product_id, pp.price as price
			FROM cscart_products as p
			left join `cscart_product_prices` as pp on (p.product_id=pp.product_id and pp.usergroup_id=0)
			where p.status='A' and p.my_supplier_id='' ");
	if( $products ) {
		foreach( $products as $product ) {
			$cprice = $product['price'];
			$cprice = ( ($cprice/1.23)+$hat) * 1.23;
			$cprice = fn_round_to_123($cprice,0.5);
			db_query( "UPDATE cscart_product_prices SET price=?i WHERE product_id=?i", $cprice, $product['product_id']);
			$updated_cnt++;
			//printf("%s. old price=%s new price=%s <br />", $updated_cnt, $product['price'], $cprice);
		}
	}
	fn_set_notification('N', fn_get_lang_var('notice'), sprintf('Ενημερώθηκαν %s εγγραφές.',$updated_cnt));
  return array(CONTROLLER_STATUS_OK, "pricing.dashboard");
}

if( $mode=='dashboard') {
}
if( $mode=='market_miner' ) {
  $page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page']; // default page is 1
  $q = $_REQUEST['q'];
  $q_where = '';
  if( $q )
  	$q_where = sprintf(" AND pd.product LIKE '%%%s%%' ", $q);
  $items_per_page = 30;
	$limit = '';
	if (!empty($items_per_page)) {
		$total = db_get_field("
		SELECT COUNT(DISTINCT (p.product_id)) 
		FROM cscart_products as p
		LEFT JOIN cscart_product_descriptions as pd ON (p.product_id=pd.product_id and pd.lang_code='EL')
		WHERE p.market_link!=''".$q_where);
		$limit = fn_paginate($page, $total, $items_per_page);
	}

  $query = "
    SELECT p.product_id, p.market_link, p.market_min_price, p.market_max_price, p.market_our_position,p.market_last_run, pd.product, p.market_suspend, pp.price as price
    FROM cscart_products as p LEFT JOIN cscart_product_descriptions as pd ON (p.product_id=pd.product_id and lang_code='EL')
    left join `cscart_product_prices` as pp on (p.product_id=pp.product_id and pp.usergroup_id=0)
    WHERE market_link!='' $q_where ORDER BY pd.product
    ";
  $query .= $limit;
	$products = db_get_array($query);	
	$view->assign('products', $products);

}

if( $mode=='m_update' ) {
	$ids = $_REQUEST['product_ids'];
  $page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page']; // default page is 1
  $items_per_page = 30;
	$limit = '';
	if (!empty($items_per_page)) {
		$total = db_get_field("SELECT COUNT(DISTINCT (cscart_products.product_id)) FROM cscart_products WHERE market_link!=''");
		$limit = fn_paginate($page, $total, $items_per_page);
	}

  $query = "SELECT p.product_id FROM cscart_products as p WHERE market_link!=''";
  $query .= $limit;
	$products = db_get_fields($query);	
	$q = sprintf("UPDATE cscart_products SET market_suspend='N' WHERE product_id IN (%s)", implode(',', $products));
	db_query( $q );
	$q = sprintf("UPDATE cscart_products SET market_suspend='Y' WHERE product_id IN (%s)", implode(',', $ids));
	db_query( $q );
	return array(CONTROLLER_STATUS_OK, "pricing.market_miner");
}

  function fn_round_to_123($number, $increments) {
    $increments = 1 / $increments;
    return (round($number * $increments) / $increments);
  }