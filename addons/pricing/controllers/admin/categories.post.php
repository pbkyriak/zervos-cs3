<?php

if (!defined('AREA')) {
  die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if ($mode == 'update') {
    $update = isset($_REQUEST['rules_data'])?$_REQUEST['rules_data']:array();
    $insert = isset($_REQUEST['add_rules'])?$_REQUEST['add_rules']:array();
    $delete = isset($_REQUEST['delete_rules'])?$_REQUEST['delete_rules']:array();
    $categoryId = $_REQUEST['category_id'];

    foreach ($insert as $row) {
      if (!empty($row['from_price']) || !empty($row['markup'])) {
        db_query("INSERT INTO cscart_category_pricing_rules 
          (category_id, supplier_id, from_price, markup, markup_type, round_to) 
          VALUES (?i, ?i, ?i, ?i, ?s, ?i)", $categoryId, $row['supplier_id'], $row['from_price'], $row['markup'], $row['markup_type'], $row['round_to']);
      }
    }
    if (count($update)) {
      foreach ($update as $id => $row) {
        db_query(
                "UPDATE cscart_category_pricing_rules 
                SET supplier_id=?i, from_price=?i, markup=?i, 
                markup_type=?s, round_to=?i WHERE id=?i", $row['supplier_id'], $row['from_price'], $row['markup'], $row['markup_type'], $row['round_to'], $id);
      }
    }
    if (count($delete)) {
      foreach ($delete as $id) {
        db_query("DELETE FROM cscart_category_pricing_rules  WHERE id=?i", $id);
      }
    }
  }
  return;
}

if ($mode == 'update') {

  Registry::set('navigation.tabs.pricemarkup', array(
      'title' => fn_get_lang_var('price_markup'),
      'js' => true
  ));
  $category_id = $_REQUEST['category_id'];
  $rules_data = db_get_array(
          "SELECT id, category_id, supplier_id, from_price, markup, markup_type,
            round_to FROM cscart_category_pricing_rules WHERE category_id=?i 
            order by supplier_Id, from_price", $category_id);
  $view->assign('pricing_rules', $rules_data);
}


