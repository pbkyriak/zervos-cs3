<?php

if (!defined('AREA')) {
  die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if ($mode == 'update' || $mode == 'add') {
    $query = "
          SELECT p.product_id, 
                p.market_link, p.market_min_price, p.market_max_price, p.market_our_position, p.market_last_run, p.market_suspend, 
                pd.product, p.supplierA_price as costPrice, pc.price as salePrice, p.my_supplier_id
              FROM cscart_products as p 
          LEFT JOIN cscart_product_descriptions as pd ON (p.product_id=pd.product_id and lang_code='EL')
          LEFT JOIN cscart_product_prices as pc ON (p.product_id=pc.product_id and pc.usergroup_id=0 and pc.lower_limit=1)
              WHERE market_link!=''
        ";
    $products = db_get_array($query);
    $handle = fopen(DIR_ROOT . '/var/marketminer/link_list.txt', 'w+');
    foreach ($products as $product) {
      $skip = !($product['market_suspend'] == 'Y');
      if (!$skip) {
        fputcsv($handle, array($product['product_id'], $product['market_link']));
      }
    }
    fclose($handle);
    return;
  }
  
  //
  // Processing additon of new product element
  //
  if ($mode == 'fastadd') {
    if (!empty($_REQUEST['product_data']['product'])) {  // Checking for required fields for new product
      // fillin default values
      $product_data = $_REQUEST['product_data'];
      $product_data['product_code'] = 'H'.sprintf("%05d", (int)db_get_field("select max(product_id)+1 from cscart_products"));
      $product_data['amount'] = 5; //rand(2,5);
      $product_data['options_type'] = 'P';
      $product_data['company_id'] = 0;
      $product_data['exceptions_type'] = 'F';
      $product_data['list_price'] = 0;
      $product_data['zero_price_action'] = 'R';
      $product_data['tracking'] = 'B';
      $product_data['min_qty'] = 0;
      $product_data['max_qty'] = 0;
      $product_data['qty_step'] = 0;
      $product_data['list_qty_count'] = 0;
      $product_data['tax_ids'] = array(6 => 6);
      $product_data['page_title'] = $product_data['product'];
      $product_data['meta_description'] = $product_data['product'];
      $product_data['short_description'] = $product_data['product'];
      $product_data['meta_keywords'] = '';
      $product_data['usergroup_ids'] = array(0);
      $product_data['timestamp'] = date('d/m/Y');
      $product_data['avail_since'] = '';
      $product_data['out_of_stock_actions'] = 'N';
      $product_data['shop_availability'] = 1;
      $product_data['skroutz_availability'] = 1;
      $product_data['pricelist_updater_skip_skroutz'] = 'N';
      $product_data['details_layout'] = 'default';
      $product_data['feature_comparison'] = 'N';
      $product_data['is_edp'] = 'N';
      $product_data['product_short_descr'] = '';
      $product_data['popularity'] = 0;
      $product_data['search_words'] = '';
      
      // Adding product record
      $product_id = fn_update_product($product_data);

      if (!empty($product_id)) {
        // Attach main product images pair
        fn_attach_image_pairs('product_main', 'product', $product_id, DESCR_SL);

        // Attach additional product images
        fn_attach_image_pairs('product_add_additional', 'product', $product_id, DESCR_SL);

				if (!empty($_REQUEST['product_data']['categories'])) {
					$_add_categories = explode(',', $_REQUEST['product_data']['categories']);

					$main_category = (!empty($_REQUEST['product_data']['main_category'])) ? $_REQUEST['product_data']['main_category'] : $_add_categories[0];

					$_data = array (
						'product_id' => $product_id,
					);

					foreach ($_add_categories as $c_id) {
						// check if main category already exists
						if (is_numeric($c_id)) {
							$is_ex = db_get_field("SELECT COUNT(*) FROM ?:products_categories WHERE product_id = ?i AND category_id = ?i", $product_id, $c_id);
							if (!empty($is_ex)) {
								continue;
							}
							$_data['link_type'] = ($c_id == $main_category) ? "M" : "A";
							$_data['category_id'] = $c_id;
							db_query('INSERT INTO ?:products_categories ?e', $_data);
						}
					}

					fn_update_product_count($_add_categories);
				}

      }

      // -----------------------
      $suffix = ".update?product_id=$product_id";
    } else {
      $suffix = ".fastadd";
    }
    return array(CONTROLLER_STATUS_OK, "products$suffix");
  }
}

if ($mode == 'fastadd') {
  // [Breadcrumbs]
  fn_add_breadcrumb(fn_get_lang_var('products'), "products.manage.reset_view");
  fn_add_breadcrumb(fn_get_lang_var('search_results'), "products.manage.last_view");
  // [/Breadcrumbs]
  // [Page sections]
  Registry::set('navigation.tabs', array(
      'detailed' => array(
          'title' => fn_get_lang_var('general'),
          'js' => true
      )
  ));
  // [/Page sections]
}