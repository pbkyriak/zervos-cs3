<?php

if (!defined('AREA')) {
  die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //7-6-2015
  if( $mode == 'upload_import_file_quickmobile' ) {
    $uploaddir = realpath(dirname(__FILE__).'/../../../../priceUpdater/updaterFiles');
    $uploadfile = $uploaddir .'/quickmobile.xls';
    if(!file_exists($_FILES['catalog']['tmp_name'])){
      fn_set_notification('W', fn_get_lang_var('warning'), fn_get_lang_var('select_file_to_upload'));
    }elseif (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
      fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('file_uploaded_successfully'));
    }
    return array(CONTROLLER_STATUS_OK,"importer.upload_import_file");
  }
  if( $mode == 'upload_import_file_nexion' ) {
	$uploaddir = realpath(dirname(__FILE__).'/../../../../priceUpdater/updaterFiles');
	$uploadfile = $uploaddir .'/nexion.xls';
	if(!file_exists($_FILES['catalog']['tmp_name'])){
	  fn_set_notification('W', fn_get_lang_var('warning'), fn_get_lang_var('select_file_to_upload'));
	}elseif (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
	  fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('file_uploaded_successfully'));
	}
	return array(CONTROLLER_STATUS_OK,"importer.upload_import_file");
  }
  //[imatz]
  //30-9-2013
  if( $mode == 'upload_import_file_eurostar' ) {
    $uploaddir = realpath(dirname(__FILE__).'/../../../../priceUpdater/updaterFiles');
    $uploadfile = $uploaddir .'/eurostar.csv';
    if (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
      fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('file_uploaded_successfully'));
    }
    return array(CONTROLLER_STATUS_OK,"importer.upload_import_file");
  }
  //7-6-2015
  if( $mode == 'upload_import_file_lego' ) {
    $uploaddir = realpath(dirname(__FILE__).'/../../../../priceUpdater/updaterFiles');
    $uploadfile = $uploaddir .'/lego.xls';
    if(!file_exists($_FILES['catalog']['tmp_name'])){
      fn_set_notification('W', fn_get_lang_var('warning'), fn_get_lang_var('select_file_to_upload'));
    }elseif (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
      fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('file_uploaded_successfully'));
    }
    return array(CONTROLLER_STATUS_OK,"importer.upload_import_file");
  }
  //[/imatz]
  if( $mode == 'upload_import_file' ) {
    $uploaddir = realpath(dirname(__FILE__).'/../../../../priceUpdater/updaterFiles');
    $uploadfile = $uploaddir .'/vector.csv';
    if (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
      fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('file_uploaded_successfully'));
    }
    return array(CONTROLLER_STATUS_OK,"importer.upload_import_file");
  }
  if( $mode == 'upload_import_file_norton' ) {
    $uploaddir = realpath(dirname(__FILE__).'/../../../../priceUpdater/updaterFiles');
    $uploadfile = $uploaddir .'/Nortonline.xml';
    if (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
      fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('file_uploaded_successfully'));
    }
    return array(CONTROLLER_STATUS_OK,"importer.upload_import_file");
  }
  if( $mode == 'upload_import_file_amy' ) {
    $uploaddir = realpath(dirname(__FILE__).'/../../../../priceUpdater/updaterFiles');
    $uploadfile = $uploaddir .'/amy.csv';
    if (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
      fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('file_uploaded_successfully'));
    }
    return array(CONTROLLER_STATUS_OK,"importer.upload_import_file");
  }
  if( $mode == 'upload_import_file_yukaprices' ) {
    $uploaddir = realpath(dirname(__FILE__).'/../../../../priceUpdater/updaterFiles');
    $uploadfile = $uploaddir .'/yukatel-pricelist.xlsx';
    if (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
      fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('file_uploaded_successfully'));
    }
    return array(CONTROLLER_STATUS_OK,"importer.upload_import_file");
  }
  if( $mode == 'upload_import_file_yukastock' ) {
    $uploaddir = realpath(dirname(__FILE__).'/../../../../priceUpdater/updaterFiles');
    $uploadfile = $uploaddir .'/yukatel-stocklist.xlsx';
    if (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
      fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('file_uploaded_successfully'));
    }
    return array(CONTROLLER_STATUS_OK,"importer.upload_import_file");
  }
}

if ($mode == 'upload_import_file') {
  fn_add_breadcrumb(fn_get_lang_var('upload_import_file'));
}

if ( $mode == 'turn_off_product' ) {
	$product_id = $_REQUEST['pid'];
	db_query("update cscart_products set status='D', pricelist_updater_skip='Y', market_suspend='N' where product_id=?i", $product_id);
	fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('product_turned_off'));
	return array(CONTROLLER_STATUS_OK,"products.update&product_id=".$product_id);
}
